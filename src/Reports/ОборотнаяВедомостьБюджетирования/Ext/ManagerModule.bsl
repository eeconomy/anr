﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Для подсистемы "Варианты отчетов" при работе в модели сервиса.
// Возвращаемое значение:
//  Массив - Содержит массив структур со свойствами:
//     * ВариантОтчета - Структура - Вариант отчета.
//        ** Имя             - Строка - Имя варианта отчета.
//        ** Представление   - Строка - Представление варианта отчета.
//
Функция ВариантыНастроек() Экспорт
	
	Результат = Новый Массив;
	
	Вариант = Новый Структура;
	Вариант.Вставить("Имя",           "ОборотнаяВедомость");
	Вариант.Вставить("Представление", НСтр("ru = 'Оборотная ведомость по статьям бюджетов';
											|en = 'Turnover balance report of budget items'"));
	Результат.Добавить(Вариант);
	
	Вариант = Новый Структура;
	Вариант.Вставить("Имя",           "ОборотнаяВедомостьПоСтатьеБюджетов");
	Вариант.Вставить("Представление", НСтр("ru = 'Оборотная ведомость по статье бюджетов';
											|en = 'Turnover balance report of a budget item'"));
	Результат.Добавить(Вариант);
	
	Вариант = Новый Структура;
	Вариант.Вставить("Имя",           "Расшифровка");
	Вариант.Вставить("Представление", НСтр("ru = 'Оборотная ведомость по статьям бюджетов';
											|en = 'Turnover balance report of budget items'"));
	Результат.Добавить(Вариант);
	
	Возврат Результат;
	
КонецФункции

#Область КомандыПодменюОтчеты

// Добавляет команду отчета в список команд.
// 
// Параметры:
//   КомандыОтчетов - ТаблицаЗначений - состав полей см. в функции МенюОтчеты.СоздатьКоллекциюКомандОтчетов.
//
Функция ДобавитьКомандуОтчета(КомандыОтчетов) Экспорт

	Если ПравоДоступа("Просмотр", Метаданные.Отчеты.ОборотнаяВедомостьБюджетирования) 
			И ПолучитьФункциональнуюОпцию("ИспользоватьБюджетирование") Тогда
		
		КомандаОтчет = КомандыОтчетов.Добавить();
		
		КомандаОтчет.Менеджер = Метаданные.Отчеты.ОборотнаяВедомостьБюджетирования.ПолноеИмя();
		КомандаОтчет.Представление = НСтр("ru = 'Оборотная ведомость';
											|en = 'Turnover balance report'");
		
		КомандаОтчет.МножественныйВыбор = Ложь;
		КомандаОтчет.Важность = "Важное";
		КомандаОтчет.КлючВарианта = "ОборотнаяВедомостьПоСтатьеБюджетов";
		
		Возврат КомандаОтчет;
		
	КонецЕсли;

	Возврат Неопределено;

КонецФункции

#КонецОбласти

#КонецОбласти

#КонецЕсли
﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий



Процедура ПриКомпоновкеРезультата(ДокументРезультат, ДанныеРасшифровки, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	НомерОтчета = НомерОтчета + 1;
	
	ДокументРезультат.Вывести(СформироватьОтчетЛокализация( 
		ДатаНачала,
		КонецДня(ДатаОкончания),
		Склад,
		Организация,
		НомерОтчета));  
	
КонецПроцедуры // ПриКомпоновкеРезультата()

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ФорматСумм(Сумма, Валюта = Неопределено, ЧН = "", ЧРГ = "")

	ФорматнаяСтрока = "ЧЦ=15;ЧДЦ=2" +
					?(НЕ ЗначениеЗаполнено(ЧН), "", ";" + "ЧН=" + ЧН) +
					?(НЕ ЗначениеЗаполнено(ЧРГ),"", ";" + "ЧРГ=" + ЧРГ);
	РезультирующаяСтрока = СокрЛ(Формат(Сумма, ФорматнаяСтрока));
	
	Если ЗначениеЗаполнено(Валюта) Тогда
		РезультирующаяСтрока = РезультирующаяСтрока + " " + СокрП(Валюта);
	КонецЕсли;

	Возврат РезультирующаяСтрока;

КонецФункции // ФорматСумм()

Функция ПолучитьКоэффициентПересчетаИзВалютыВВалюту(Валюта1, Валюта2)
	
	Если Валюта1.Валюта <> Валюта2.Валюта Тогда
		Если Валюта1.Кратность > 0
			И Валюта1.Курс > 0
			И Валюта2.Кратность > 0
			И Валюта2.Курс > 0 Тогда
			Возврат Валюта1.Курс * Валюта2.Кратность / (Валюта2.Курс * Валюта1.Кратность);
		Иначе
			Возврат 0
		КонецЕсли;
	Иначе
		Возврат 1;
	КонецЕсли;
	
КонецФункции // ПолучитьКоэффициентПересчетаИзВалютыВВалюту()

Функция СформироватьОтчетЛокализация(ДатаНачала, ДатаОкончания, Склад, Организация, НомерОтчета)
		
	ТабДокумент = Новый ТабличныйДокумент;
	ТабДокумент.РазмерКолонтитулаСверху = 0;
	ТабДокумент.РазмерКолонтитулаСнизу = 0;
	ТабДокумент.АвтоМасштаб = Истина;
	ТабДокумент.ОриентацияСтраницы = ОриентацияСтраницы.Портрет;
	ТабДокумент.ПолеСверху = 10;
	ТабДокумент.ПолеСлева = 10;
	ТабДокумент.ПолеСнизу = 10;
	ТабДокумент.ПолеСправа = 10;
	
	ВидЦены = ?(ЗначениеЗаполнено(Склад.УчетныйВидЦены), Склад.УчетныйВидЦены, Склад.РозничныйВидЦены);
	Если Не ЗначениеЗаполнено(ВидЦены) Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Для склада не установлен вид цены.'"));
		Возврат ТабДокумент;
	КонецЕсли;
	Если Не ЗначениеЗаполнено(Константы.ВалютаУправленческогоУчета.Получить()) Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Не установлена валюта управленческого учета.'"));
		Возврат ТабДокумент;
	КонецЕсли;
	
	ВалютаУправленческогоУчета = Константы.ВалютаУправленческогоУчета.Получить();
	
	Макет = ПолучитьМакет("Чд_Макет_Локализация");
	
	// Выведем заголовок.
	СведенияОПокупателе = ФормированиеПечатныхФорм.СведенияОЮрФизЛице(Организация, ДатаОкончания);
	
	ОбластьМакета = Макет.ПолучитьОбласть("Заголовок");
	ОбластьМакета.Параметры.ОрганизацияПредставление = ФормированиеПечатныхФорм.ОписаниеОрганизации(СведенияОПокупателе);
	ОбластьМакета.Параметры.ОтчетныйПериод = "с " + Формат(ДатаНачала,"ДЛФ=DD") + " по " + Формат(ДатаОкончания,"ДЛФ=DD");
	ОбластьМакета.Параметры.Магазин = Склад;
	
	ОбластьМакета.Параметры.Номер = НомерОтчета;
	
	ТабДокумент.Вывести(ОбластьМакета);
	
	ОбластьМакета = Макет.ПолучитьОбласть("Шапка");
	ТабДокумент.Вывести(ОбластьМакета);
	
	// Повторение шапки
	ПовторятьПриПечатиСтроки = ТабДокумент.Область(1 + ОбластьМакета.ВысотаТаблицы, ,2 + ОбластьМакета.ВысотаТаблицы);
	
	СхемаКомпоновкиДанных = Отчеты.Чд_ТоварныйОтчет.ПолучитьМакет("ОсновнаяСхемаКомпоновкиДанных");
			
	// Подготовка компоновщика макета компоновки данных.
	Компоновщик = Новый КомпоновщикНастроекКомпоновкиДанных;
	Компоновщик.Инициализировать(Новый ИсточникДоступныхНастроекКомпоновкиДанных(СхемаКомпоновкиДанных));
	Компоновщик.ЗагрузитьНастройки(СхемаКомпоновкиДанных.НастройкиПоУмолчанию);
	
	// Выбранные поля.
	ОбязательныеПоля = Новый Массив;
	ОбязательныеПоля.Добавить("Регистратор");
	ОбязательныеПоля.Добавить("Номенклатура");
	
	ОбязательныеПоля.Добавить("КоличествоНачальныйОстаток");
	ОбязательныеПоля.Добавить("КоличествоКонечныйОстаток");
	ОбязательныеПоля.Добавить("КоличествоПриход");
	ОбязательныеПоля.Добавить("КоличествоРасход");
	
	ОбязательныеПоля.Добавить("СтоимостьПоступленияБезНДСНачальныйОстаток");
	ОбязательныеПоля.Добавить("СтоимостьПоступленияБезНДСКонечныйОстаток");
	ОбязательныеПоля.Добавить("СтоимостьПоступленияБезНДСПриход");
	ОбязательныеПоля.Добавить("СтоимостьПоступленияБезНДСРасход");
	
	ОбязательныеПоля.Добавить("СтоимостьПоступленияНачальныйОстаток");
	ОбязательныеПоля.Добавить("СтоимостьПоступленияКонечныйОстаток");
	ОбязательныеПоля.Добавить("СтоимостьПоступленияПриход");
	ОбязательныеПоля.Добавить("СтоимостьПоступленияРасход");
	
	ОбязательныеПоля.Добавить("НДСПоступленияНачальныйОстаток");
	ОбязательныеПоля.Добавить("НДСПоступленияКонечныйОстаток");
	ОбязательныеПоля.Добавить("НДСПоступленияПриход");
	ОбязательныеПоля.Добавить("НДСПоступленияРасход");
	
	ОбязательныеПоля.Добавить("СтоимостьПродажиПриход");
	ОбязательныеПоля.Добавить("СтоимостьПродажиРасход");
	
	ОбязательныеПоля.Добавить("СтоимостьПродажиБезНДСПриход");
	ОбязательныеПоля.Добавить("СтоимостьПродажиБезНДСРасход");
	
	ОбязательныеПоля.Добавить("НаценкаПриход");
	ОбязательныеПоля.Добавить("НаценкаРасход");
	
	//Компоновщик.Настройки.Выбор.Элементы.Очистить();
	//Для Каждого ОбязательноеПоле Из ОбязательныеПоля Цикл
	//	ПолеСКД = КомпоновкаДанныхСервер.НайтиПолеСКДПоПолномуИмени(Компоновщик.Настройки.Выбор.ДоступныеПоляВыбора.Элементы, ОбязательноеПоле);
	//	Если ПолеСКД <> Неопределено Тогда
	//		ВыбранноеПоле = Компоновщик.Настройки.Выбор.Элементы.Добавить(Тип("ВыбранноеПолеКомпоновкиДанных"));
	//		ВыбранноеПоле.Поле = ПолеСКД.Поле;
	//	КонецЕсли;
	//КонецЦикла;
	//
	//// Добавление группировок.
	//Компоновщик.Настройки.Структура.Очистить();
	//КомпоновкаДанныхКлиентСервер.ДобавитьГруппировку(Компоновщик, "Регистратор");
	//КомпоновкаДанныхКлиентСервер.ДобавитьГруппировку(Компоновщик, "Номенклатура");
	//
	//// Отключение итогов.
	//КомпоновкаДанныхКлиентСервер.УстановитьПараметрВывода(Компоновщик,"ВертикальноеРасположениеОбщихИтогов", РасположениеИтоговКомпоновкиДанных.Нет);
	//КомпоновкаДанныхКлиентСервер.УстановитьПараметрВывода(Компоновщик,"ГоризонтальноеРасположениеОбщихИтогов", РасположениеИтоговКомпоновкиДанных.Нет);
	//КомпоновкаДанныхКлиентСервер.УстановитьПараметрВывода(Компоновщик,"РасположениеГруппировок", РасположениеИтоговКомпоновкиДанных.Нет);
	
	// Установка отборов.
	КомпоновкаДанныхКлиентСервер.ДобавитьОтбор(Компоновщик, "Склад",       Склад);
		
	Период = Новый СтандартныйПериод;
	Период.ДатаНачала    = НачалоДня(ДатаНачала);
	Период.ДатаОкончания = КонецДня(ДатаОкончания);
	КомпоновкаДанныхКлиентСервер.УстановитьПараметр(Компоновщик.Настройки, "Период", Период);
	
	// Компоновка макета компоновки данных.
	КомпоновщикМакета = Новый КомпоновщикМакетаКомпоновкиДанных;
	МакетКомпоновкиДанных = КомпоновщикМакета.Выполнить(СхемаКомпоновкиДанных, Компоновщик.Настройки,,,Тип("ГенераторМакетаКомпоновкиДанныхДляКоллекцииЗначений"));
	
	// Построение таблицы значений.
	Процессор = Новый ПроцессорКомпоновкиДанных;
	Процессор.Инициализировать(МакетКомпоновкиДанных, , , Истина);
	ПроцессорВывода = Новый ПроцессорВыводаРезультатаКомпоновкиДанныхВКоллекциюЗначений();
	ИсходныеДанные = Новый ТаблицаЗначений;
	ПроцессорВывода.УстановитьОбъект(ИсходныеДанные);
	ПроцессорВывода.Вывести(Процессор);
	
	ИсходныеДанные.Колонки.Добавить("Номер", Новый ОписаниеТипов("Строка"));
	ИсходныеДанные.Колонки.Добавить("Дата", Новый ОписаниеТипов("Дата"));
	
	Для каждого Строка Из ИсходныеДанные Цикл
		Если ЗначениеЗаполнено(Строка.Регистратор) Тогда
			Строка.Номер = Строка.Регистратор.Номер;
			Строка.Дата = Строка.Регистратор.Дата;
		КонецЕсли; 	
	КонецЦикла; 
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	Таблица.Регистратор КАК Регистратор,
	|	Таблица.Номенклатура КАК Номенклатура,
	|	ЕСТЬNULL(Таблица.КоличествоНачальныйОстаток, 0) КАК КоличествоНачальныйОстаток,
	|	ЕСТЬNULL(Таблица.КоличествоПриход, 0) КАК КоличествоПриход,
	|	ЕСТЬNULL(Таблица.КоличествоРасход, 0) КАК КоличествоРасход,
	|	ЕСТЬNULL(Таблица.КоличествоКонечныйОстаток, 0) КАК КоличествоКонечныйОстаток,
	|	ЕСТЬNULL(Таблица.СтоимостьПоступленияБезНДСНачальныйОстаток, 0) КАК СтоимостьПоступленияБезНДСНачальныйОстаток,
	|	ЕСТЬNULL(Таблица.СтоимостьПоступленияБезНДСПриход, 0) КАК СтоимостьПоступленияБезНДСПриход,
	|	ЕСТЬNULL(Таблица.СтоимостьПоступленияБезНДСРасход, 0) КАК СтоимостьПоступленияБезНДСРасход,
	|	ЕСТЬNULL(Таблица.СтоимостьПоступленияБезНДСКонечныйОстаток, 0) КАК СтоимостьПоступленияБезНДСКонечныйОстаток,
	|	ЕСТЬNULL(Таблица.СтоимостьПоступленияНачальныйОстаток, 0) КАК СтоимостьПоступленияНачальныйОстаток,
	|	ЕСТЬNULL(Таблица.СтоимостьПоступленияПриход, 0) КАК СтоимостьПоступленияПриход,
	|	ЕСТЬNULL(Таблица.СтоимостьПоступленияРасход, 0) КАК СтоимостьПоступленияРасход,
	|	ЕСТЬNULL(Таблица.СтоимостьПоступленияКонечныйОстаток, 0) КАК СтоимостьПоступленияКонечныйОстаток,
	|	ЕСТЬNULL(Таблица.НДСПоступленияНачальныйОстаток, 0) КАК НДСПоступленияНачальныйОстаток,
	|	ЕСТЬNULL(Таблица.НДСПоступленияПриход, 0) КАК НДСПоступленияПриход,
	|	ЕСТЬNULL(Таблица.НДСПоступленияРасход, 0) КАК НДСПоступленияРасход,
	|	ЕСТЬNULL(Таблица.НДСПоступленияКонечныйОстаток, 0) КАК НДСПоступленияКонечныйОстаток,
	|	ЕСТЬNULL(Таблица.СтоимостьПродажиПриход, 0) КАК СтоимостьПродажиПриход,
	|	ЕСТЬNULL(Таблица.СтоимостьПродажиРасход, 0) КАК СтоимостьПродажиРасход,
	|	ЕСТЬNULL(Таблица.СтоимостьПродажиБезНДСПриход, 0) КАК СтоимостьПродажиБезНДСПриход,
	|	ЕСТЬNULL(Таблица.СтоимостьПродажиБезНДСРасход, 0) КАК СтоимостьПродажиБезНДСРасход,
	|	ЕСТЬNULL(Таблица.НаценкаПриход, 0) КАК НаценкаПриход,
	|	ЕСТЬNULL(Таблица.НаценкаРасход, 0) КАК НаценкаРасход
	|ПОМЕСТИТЬ ТаблицаДокументов
	|ИЗ
	|	&ИсходныеДанные КАК Таблица
	|ГДЕ
	|	(Таблица.Регистратор <> НЕОПРЕДЕЛЕНО
	|			ИЛИ Таблица.Номенклатура <> НЕОПРЕДЕЛЕНО)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаДокументов.Регистратор КАК Регистратор,
	|	ТаблицаДокументов.Номенклатура КАК Номенклатура,
	|	ТаблицаДокументов.КоличествоНачальныйОстаток КАК КоличествоНачальныйОстаток,
	|	ТаблицаДокументов.КоличествоПриход КАК КоличествоПриход,
	|	ТаблицаДокументов.КоличествоРасход КАК КоличествоРасход,
	|	ТаблицаДокументов.КоличествоКонечныйОстаток КАК КоличествоКонечныйОстаток,
	|	ТаблицаДокументов.СтоимостьПоступленияБезНДСНачальныйОстаток КАК СтоимостьПоступленияБезНДСНачальныйОстаток,
	|	ТаблицаДокументов.СтоимостьПоступленияБезНДСПриход КАК СтоимостьПоступленияБезНДСПриход,
	|	ТаблицаДокументов.СтоимостьПоступленияБезНДСРасход КАК СтоимостьПоступленияБезНДСРасход,
	|	ТаблицаДокументов.СтоимостьПоступленияБезНДСКонечныйОстаток КАК СтоимостьПоступленияБезНДСКонечныйОстаток,
	|	ТаблицаДокументов.СтоимостьПоступленияНачальныйОстаток КАК СтоимостьПоступленияНачальныйОстаток,
	|	ТаблицаДокументов.СтоимостьПоступленияПриход КАК СтоимостьПоступленияПриход,
	|	ТаблицаДокументов.СтоимостьПоступленияРасход КАК СтоимостьПоступленияРасход,
	|	ТаблицаДокументов.СтоимостьПоступленияКонечныйОстаток КАК СтоимостьПоступленияКонечныйОстаток,
	|	ТаблицаДокументов.НДСПоступленияНачальныйОстаток КАК НДСПоступленияНачальныйОстаток,
	|	ТаблицаДокументов.НДСПоступленияПриход КАК НДСПоступленияПриход,
	|	ТаблицаДокументов.НДСПоступленияРасход КАК НДСПоступленияРасход,
	|	ТаблицаДокументов.НДСПоступленияКонечныйОстаток КАК НДСПоступленияКонечныйОстаток,
	|	ТаблицаДокументов.КоличествоПриход * ЕСТЬNULL(ЦеныНоменклатурыСрезПоследних.Цена, 0) + 0.2 * (ТаблицаДокументов.КоличествоПриход * ЕСТЬNULL(ЦеныНоменклатурыСрезПоследних.Цена, 0)) КАК СтоимостьПродажиПриход,
	|	ТаблицаДокументов.СтоимостьПродажиРасход КАК СтоимостьПродажиРасход,
	|	ТаблицаДокументов.КоличествоПриход * ЕСТЬNULL(ЦеныНоменклатурыСрезПоследних.Цена, 0) КАК СтоимостьПродажиБезНДСПриход,
	|	ТаблицаДокументов.СтоимостьПродажиБезНДСРасход КАК СтоимостьПродажиБезНДСРасход,
	|	ТаблицаДокументов.НаценкаПриход КАК НаценкаПриход,
	|	ТаблицаДокументов.НаценкаРасход КАК НаценкаРасход
	|ПОМЕСТИТЬ ВТ_ДокументыСЦенамиНоменклатуры
	|ИЗ
	|	ТаблицаДокументов КАК ТаблицаДокументов
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатуры.СрезПоследних(&ДатаОтчета, ВидЦены = &ВидЦены) КАК ЦеныНоменклатурыСрезПоследних
	|		ПО ТаблицаДокументов.Номенклатура = ЦеныНоменклатурыСрезПоследних.Номенклатура
	|			И (ЦеныНоменклатурыСрезПоследних.Валюта = &Валюта)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВТ_ДокументыСЦенамиНоменклатуры.Регистратор КАК Документ,
	|	ВТ_ДокументыСЦенамиНоменклатуры.Регистратор.Дата КАК Дата,
	|	ВТ_ДокументыСЦенамиНоменклатуры.Регистратор.Номер КАК Номер,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.КоличествоНачальныйОстаток) КАК КоличествоНачальныйОстаток,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.КоличествоПриход) КАК КоличествоПриход,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.КоличествоРасход) КАК КоличествоРасход,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.КоличествоКонечныйОстаток) КАК КоличествоКонечныйОстаток,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.СтоимостьПоступленияБезНДСНачальныйОстаток) КАК СтоимостьПоступленияБезНДСНачальныйОстаток,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.СтоимостьПоступленияБезНДСПриход) КАК СтоимостьПоступленияБезНДСПриход,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.СтоимостьПоступленияБезНДСРасход) КАК СтоимостьПоступленияБезНДСРасход,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.СтоимостьПоступленияБезНДСКонечныйОстаток) КАК СтоимостьПоступленияБезНДСКонечныйОстаток,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.СтоимостьПоступленияНачальныйОстаток) КАК СтоимостьПоступленияНачальныйОстаток,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.СтоимостьПоступленияПриход) КАК СтоимостьПоступленияПриход,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.СтоимостьПоступленияРасход) КАК СтоимостьПоступленияРасход,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.СтоимостьПоступленияКонечныйОстаток) КАК СтоимостьПоступленияКонечныйОстаток,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.НДСПоступленияНачальныйОстаток) КАК НДСПоступленияНачальныйОстаток,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.НДСПоступленияПриход) КАК НДСПоступленияПриход,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.НДСПоступленияРасход) КАК НДСПоступленияРасход,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.НДСПоступленияКонечныйОстаток) КАК НДСПоступленияКонечныйОстаток,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.СтоимостьПродажиПриход) КАК СтоимостьПродажиПриход,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.СтоимостьПродажиРасход) КАК СтоимостьПродажиРасход,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.СтоимостьПродажиБезНДСПриход) КАК СтоимостьПродажиБезНДСПриход,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.СтоимостьПродажиБезНДСРасход) КАК СтоимостьПродажиБезНДСРасход,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.НаценкаПриход) КАК НаценкаПриход,
	|	СУММА(ВТ_ДокументыСЦенамиНоменклатуры.НаценкаРасход) КАК НаценкаРасход
	|ИЗ
	|	ВТ_ДокументыСЦенамиНоменклатуры КАК ВТ_ДокументыСЦенамиНоменклатуры
	|
	|СГРУППИРОВАТЬ ПО
	|	ВТ_ДокументыСЦенамиНоменклатуры.Регистратор
	|
	|УПОРЯДОЧИТЬ ПО
	|	Дата,
	|	Документ");
	
	Запрос.УстановитьПараметр("ИсходныеДанные", ИсходныеДанные);
	Запрос.УстановитьПараметр("ВидЦены", ВидЦены);
	Запрос.УстановитьПараметр("Валюта", ВалютаУправленческогоУчета);
	Запрос.УстановитьПараметр("ДатаОтчета", КонецДня(ДатаОкончания));
	
	УстановитьПривилегированныйРежим(Истина);
	РезультатЗапроса = Запрос.Выполнить().Выгрузить();
	УстановитьПривилегированныйРежим(Ложь);

	Если ИсходныеДанные.Количество() = 0 ИЛИ НЕ ЗначениеЗаполнено(ИсходныеДанные[0].Регистратор) Тогда
		
		КоличествоНачальныйОстаток = 0;
		КоличествоКонечныйОстаток = 0;
		СтоимостьПоступленияБезНДСНачальныйОстаток = 0;
		СтоимостьПоступленияБезНДСКонечныйОстаток = 0;
		СтоимостьПоступленияНачальныйОстаток = 0;
		СтоимостьПоступленияКонечныйОстаток = 0;
		НДСПоступленияНачальныйОстаток = 0;
		НДСПоступленияКонечныйОстаток = 0;
		СтоимостьПоступленияБезНДСПриход = 0;
		СтоимостьПоступленияБезНДСРасход = 0;
		НДСПоступленияПриход = 0;
		НДСПоступленияРасход = 0;
		НачОстТара = 0;
		КонОстТара = 0;
		//
		СтоимостьПродажиРасход = 0;
		
	ИначеЕсли ИсходныеДанные.Количество() > 1 Тогда
		
		НомерСтрокиИтогов = ИсходныеДанные.Количество() - 1;
		
		Если ИсходныеДанные[НомерСтрокиИтогов].Регистратор = Неопределено И ИсходныеДанные[НомерСтрокиИтогов].Регистратор = Неопределено Тогда
			КоличествоНачальныйОстаток = ИсходныеДанные[НомерСтрокиИтогов].КоличествоНачальныйОстаток;
			КоличествоКонечныйОстаток = ИсходныеДанные[НомерСтрокиИтогов].КоличествоКонечныйОстаток;
			СтоимостьПоступленияБезНДСНачальныйОстаток = ИсходныеДанные[НомерСтрокиИтогов].СтоимостьПоступленияБезНДСНачальныйОстаток;
			СтоимостьПоступленияБезНДСКонечныйОстаток = ИсходныеДанные[НомерСтрокиИтогов].СтоимостьПоступленияБезНДСКонечныйОстаток;
			СтоимостьПоступленияНачальныйОстаток = ИсходныеДанные[НомерСтрокиИтогов].СтоимостьПоступленияНачальныйОстаток;
			СтоимостьПоступленияКонечныйОстаток = ИсходныеДанные[НомерСтрокиИтогов].СтоимостьПоступленияКонечныйОстаток;
			НДСПоступленияНачальныйОстаток = ИсходныеДанные[НомерСтрокиИтогов].НДСПоступленияНачальныйОстаток;
			НДСПоступленияКонечныйОстаток = ИсходныеДанные[НомерСтрокиИтогов].НДСПоступленияКонечныйОстаток;
			СтоимостьПоступленияБезНДСПриход = ИсходныеДанные[НомерСтрокиИтогов].СтоимостьПоступленияБезНДСПриход;
			СтоимостьПоступленияБезНДСРасход = ИсходныеДанные[НомерСтрокиИтогов].СтоимостьПоступленияБезНДСРасход;
			НДСПоступленияПриход = ИсходныеДанные[НомерСтрокиИтогов].НДСПоступленияПриход;
			НДСПоступленияРасход = ИсходныеДанные[НомерСтрокиИтогов].НДСПоступленияРасход;
			СтоимостьПродажиРасход = ИсходныеДанные[НомерСтрокиИтогов].СтоимостьПродажиРасход;
			СтоимостьПродажиБезНДСРасход = ИсходныеДанные[НомерСтрокиИтогов].СтоимостьПродажиБезНДСРасход;
			НачОстТара = 0;
		Иначе
			КоличествоНачальныйОстаток = 0;
			КоличествоКонечныйОстаток = 0;
			СтоимостьПоступленияБезНДСНачальныйОстаток = 0;
			СтоимостьПоступленияБезНДСКонечныйОстаток = 0;
			СтоимостьПоступленияНачальныйОстаток = 0;
			СтоимостьПоступленияКонечныйОстаток = 0;
			НДСПоступленияНачальныйОстаток = 0;
			НДСПоступленияКонечныйОстаток = 0;
			СтоимостьПоступленияБезНДСПриход = 0;
			СтоимостьПоступленияБезНДСРасход = 0;
			НДСПоступленияПриход = 0;
			НДСПоступленияРасход = 0;
			СтоимостьПродажиРасход = 0;
			СтоимостьПродажиБезНДСРасход = 0;
			НачОстТара = 0;
		КонецЕсли;
		
		КонОстТара = 0;
		
	КонецЕсли;
	
	ОбластьМакета = Макет.ПолучитьОбласть("ОстатокНачала");
	ОбластьМакета.Параметры.ДатаНачала = НСтр("ru = 'Остаток на'") + " " + Формат(ДатаНачала, "ДЛФ=Д");
	ОбластьМакета.Параметры.НачСтоимостьСНДСВсего = ФорматСумм(СтоимостьПоступленияБезНДСНачальныйОстаток + НДСПоступленияНачальныйОстаток);
	ОбластьМакета.Параметры.НачСтоимостьВсего = ФорматСумм(СтоимостьПоступленияБезНДСНачальныйОстаток);
	ОбластьМакета.Параметры.НачСтоимостьТара  = ФорматСумм(НачОстТара);
	ТабДокумент.Вывести(ОбластьМакета);
	
	ОбластьМакета = Макет.ПолучитьОбласть("Приход");
	ТабДокумент.Вывести(ОбластьМакета);
	
	ТипыКорректировок = Новый Массив;
	ТипыКорректировок.Добавить(Тип("ДокументСсылка.ВводОстатков"));
	ТипыКорректировок.Добавить(Тип("ДокументСсылка.КорректировкаОбособленногоУчетаЗапасов"));
	ТипыКорректировок.Добавить(Тип("ДокументСсылка.КорректировкаРегистров"));
	
	ОбластьМакета = Макет.ПолучитьОбласть("Строка");
	Для Каждого СтрокаТЗ Из РезультатЗапроса Цикл
		
		Если СтрокаТЗ.КоличествоПриход = 0 Тогда
			Продолжить;
		КонецЕсли;
				
		ОбластьМакета.Параметры.Контрагент       = ПолучитьПредставлениеДокумента(СтрокаТЗ, ТипыКорректировок, "Приход");
		ОбластьМакета.Параметры.Расшифровка    = СтрокаТЗ.Документ;
		
		Если ТипЗнч(СтрокаТЗ.Документ) = Тип("ДокументСсылка.ПриобретениеТоваровУслуг")
			ИЛИ ТипЗнч(СтрокаТЗ.Документ) = Тип("ДокументСсылка.ВозвратТоваровПоставщику") 
			ИЛИ ТипЗнч(СтрокаТЗ.Документ) = Тип("ДокументСсылка.ВозвратТоваровОтКлиента") Тогда
			
			ДатаБСО = СтрокаТЗ.Документ.ДатаВходящегоДокумента;
			НомерБСО = СтрокаТЗ.Документ.НомерВходящегоДокумента;
			Если НЕ ЗначениеЗаполнено(НомерБСО) Тогда
				НомерБСО = ПрефиксацияОбъектовКлиентСервер.ПолучитьНомерНаПечать(СтрокаТЗ.Документ.Номер, Ложь, Истина);
			КонецЕсли;
			
		ИначеЕсли ТипЗнч(СтрокаТЗ.Документ) = Тип("ДокументСсылка.ПеремещениеТоваров") Тогда
			// 4D:АНРЭКС, МаксимК , 24.08.2021
			//  Перенести с 2.2 в 2.4 товарный отчет, №29008
			// {
			НомерБСО = СтрокаТЗ.Документ.НомерБланкаСтрогойОтчетности;
			//НомерБСО = СтрокаТЗ.Документ.НомерИсходящегоДокумента;
			// }
			// 4D

			//раскомментить в случае когда опять захотят серию исходящего документа
			//НомерБСО = СтрокаТЗ.Документ.СерияИсходящегоДокумента + СтрокаТЗ.Документ.НомерИсходящегоДокумента;
			ДатаБСО = СтрокаТЗ.Документ.Дата;

			
		ИначеЕсли ТипЗнч(СтрокаТЗ.Документ) = Тип("ДокументСсылка.РеализацияТоваровУслуг") Тогда
			
			НомерБСО = СтрокаТЗ.Документ.НомерБланкаСтрогойОтчетности;
	
		Иначе
			НомерБСО = ПрефиксацияОбъектовКлиентСервер.ПолучитьНомерНаПечать(СтрокаТЗ.Документ.Номер, Ложь, Истина);
		КонецЕсли; 
		
		ОбластьМакета.Параметры.ДатаБСО  = ?(ДатаБСО = '00010101000000', СтрокаТЗ.Дата, ДатаБСО);
		ОбластьМакета.Параметры.НомерБСО = ?(НомерБСО = "", ПрефиксацияОбъектовКлиентСервер.ПолучитьНомерНаПечать(СтрокаТЗ.Документ.Номер, Ложь, Истина), НомерБСО);
		
		ОбластьМакета.Параметры.СуммаСНДСТовара	= ФорматСумм(СтрокаТЗ.СтоимостьПоступленияБезНДСПриход + СтрокаТЗ.НДСПоступленияПриход);
		ОбластьМакета.Параметры.СуммаТовара	= ФорматСумм(СтрокаТЗ.СтоимостьПоступленияБезНДСПриход);
		ОбластьМакета.Параметры.СуммаТары   = ФорматСумм(0);
				
		ТабДокумент.Вывести(ОбластьМакета);
		
	КонецЦикла;
	
	ПриходТары	= 0;
	
	ОбластьМакета = Макет.ПолучитьОбласть("ИтогоПриход");
	ОбластьМакета.Параметры.ПрихСтоимостьСНДСВсего = ФорматСумм(СтоимостьПоступленияБезНДСПриход + НДСПоступленияПриход);
	ОбластьМакета.Параметры.ПрихСтоимостьВсего = ФорматСумм(СтоимостьПоступленияБезНДСПриход);
	ОбластьМакета.Параметры.ПрихСтоимостьТара  = ФорматСумм(0);
	ТабДокумент.Вывести(ОбластьМакета);
	
	ОбластьМакета = Макет.ПолучитьОбласть("ВсегоПриход");
	ОбластьМакета.Параметры.ПриходСНДССОстатком	= ФорматСумм(СтоимостьПоступленияБезНДСПриход + НДСПоступленияПриход + СтоимостьПоступленияБезНДСНачальныйОстаток + НДСПоступленияНачальныйОстаток);
	ОбластьМакета.Параметры.ПриходСОстатком	= ФорматСумм(СтоимостьПоступленияБезНДСПриход + СтоимостьПоступленияБезНДСНачальныйОстаток);
	ОбластьМакета.Параметры.ПриходСОстаткомТара = ФорматСумм(0);
	ТабДокумент.Вывести(ОбластьМакета);
	
	ТабДокумент.ВывестиГоризонтальныйРазделительСтраниц();
	
	ОбластьМакета = Макет.ПолучитьОбласть("Расход");
	ТабДокумент.Вывести(ОбластьМакета);
	
	СуммаПереоценки = 0;
	
	ОбластьМакета = Макет.ПолучитьОбласть("Строка");
	
	СуммаСНДСТовараРасходДоп = 0;
	СуммаТовараРасходДоп = 0;
	
	Для Каждого СтрокаТЗ Из РезультатЗапроса Цикл
		
		Если СтрокаТЗ.КоличествоРасход = 0 Тогда
			Продолжить;
		КонецЕсли;
				
		ОбластьМакета.Параметры.Контрагент    = ПолучитьПредставлениеДокумента(СтрокаТЗ, ТипыКорректировок, "Расход");
		ОбластьМакета.Параметры.Расшифровка = СтрокаТЗ.Документ;
		
		Если ТипЗнч(СтрокаТЗ.Документ) = Тип("ДокументСсылка.ПриобретениеТоваровУслуг")
			ИЛИ ТипЗнч(СтрокаТЗ.Документ) = Тип("ДокументСсылка.ВозвратТоваровОтКлиента") Тогда
			ДатаБСО = СтрокаТЗ.Документ.ДатаВходящегоДокумента;
			НомерБСО = СтрокаТЗ.Документ.НомерВходящегоДокумента;
		ИначеЕсли ТипЗнч(СтрокаТЗ.Документ) = Тип("ДокументСсылка.РеализацияТоваровУслуг")
			ИЛИ ТипЗнч(СтрокаТЗ.Документ) = Тип("ДокументСсылка.ВозвратТоваровПоставщику") Тогда
			НомерБСО = СтрокаТЗ.Документ.СерияИсходящегоДокумента + СтрокаТЗ.Документ.НомерИсходящегоДокумента;
			Если НЕ ЗначениеЗаполнено(НомерБСО) Тогда
				НомерБСО = ПрефиксацияОбъектовКлиентСервер.ПолучитьНомерНаПечать(СтрокаТЗ.Документ.Номер, Ложь, Истина);
			КонецЕсли;
			ДатаБСО = СтрокаТЗ.Документ.Дата;
			
		ИначеЕсли ТипЗнч(СтрокаТЗ.Документ) = Тип("ДокументСсылка.ПеремещениеТоваров") Тогда
			НомерБСО = СтрокаТЗ.Документ.НомерБланкаСтрогойОтчетности;
			//раскомментить в случае когда опять захотят серию исходящего документа
			//НомерБСО = СтрокаТЗ.Документ.СерияИсходящегоДокумента + СтрокаТЗ.Документ.НомерИсходящегоДокумента;
			ДатаБСО = СтрокаТЗ.Документ.Дата;
			
		ИначеЕсли ТипЗнч(СтрокаТЗ.Документ) = Тип("ДокументСсылка.РеализацияТоваровУслуг") Тогда
			
			НомерБСО = СтрокаТЗ.Документ.НомерБСО;
			
		Иначе
			НомерБСО = ПрефиксацияОбъектовКлиентСервер.ПолучитьНомерНаПечать(СтрокаТЗ.Документ.Номер, Ложь, Истина);
		КонецЕсли; 
		
		ОбластьМакета.Параметры.ДатаБСО  = ?(ДатаБСО = '00010101000000', СтрокаТЗ.Дата, ДатаБСО);
        ОбластьМакета.Параметры.НомерБСО = ?(НомерБСО = "", ПрефиксацияОбъектовКлиентСервер.ПолучитьНомерНаПечать(СтрокаТЗ.Документ.Номер, Ложь, Истина), НомерБСО);
		
		Если ТипЗнч(СтрокаТЗ.Документ) = Тип("ДокументСсылка.ВозвратТоваровПоставщику")
			ИЛИ ТипЗнч(СтрокаТЗ.Документ) = Тип("ДокументСсылка.ПеремещениеТоваров") Тогда
			
			СуммаСНДСТовара = СтрокаТЗ.СтоимостьПоступленияБезНДСРасход + СтрокаТЗ.НДСПоступленияРасход;
			СуммаТовара = СтрокаТЗ.СтоимостьПоступленияБезНДСРасход;
			
			СуммаСНДСТовараРасходДоп = СуммаСНДСТовараРасходДоп + СуммаСНДСТовара;
			//раскомментировать если надо вернуть добавление доп расходов по цене поступления
			//СуммаТовараРасходДоп = СуммаТовараРасходДоп + СуммаТовара;
			
		Иначе
			СуммаСНДСТовара = СтрокаТЗ.СтоимостьПродажиРасход;
			СуммаТовара = СтрокаТЗ.СтоимостьПродажиБезНДСРасход;
		КонецЕсли; 
		//удалить строчку если надо сумму товара по цене расхода 
		СуммаТовара = СтрокаТЗ.СтоимостьПоступленияБезНДСРасход;
		
		ОбластьМакета.Параметры.СуммаСНДСТовара	= ФорматСумм(СуммаСНДСТовара);
		ОбластьМакета.Параметры.СуммаТовара	= ФорматСумм(СуммаТовара);
		ОбластьМакета.Параметры.СуммаТары   = ФорматСумм(0);
			
		ТабДокумент.Вывести(ОбластьМакета);
		
	КонецЦикла;
	
	//вывести сумму переценки
	//Если СуммаПереоценки <> 0 Тогда
	//	ОбластьМакета.Параметры.Документ = "Сумма скидок/наценок";
	//	ОбластьМакета.Параметры.СуммаТовара = ФорматСумм(СуммаПереоценки);
	//	ОбластьМакета.Параметры.СуммаТары = ФорматСумм(0);
	//    ТабДокумент.Вывести(ОбластьМакета);
	//КонецЕсли; 	 
	
	РасходТары	= 0;
	
	ОбластьМакета = Макет.ПолучитьОбласть("ИтогоРасход");
	ОбластьМакета.Параметры.РасхСтоимостьСНДСВсего = ФорматСумм(СтоимостьПродажиРасход + СуммаСНДСТовараРасходДоп);
	ОбластьМакета.Параметры.РасхСтоимостьВсего = ФорматСумм(СтоимостьПоступленияБезНДСРасход + СуммаТовараРасходДоп);
	ОбластьМакета.Параметры.РасхСтоимостьТара  = ФорматСумм(0);
	ТабДокумент.Вывести(ОбластьМакета);
	
	ОбластьМакета = Макет.ПолучитьОбласть("ОстатокКонец");
	ОбластьМакета.Параметры.ДатаКонца = НСтр("ru = 'Остаток на'") + " "  + Формат(ДатаОкончания, "ДЛФ=Д");
	ОбластьМакета.Параметры.КонСтоимостьСНДСВсего = "Х";
	ОбластьМакета.Параметры.КонСтоимостьВсего = ФорматСумм(СтоимостьПоступленияБезНДСКонечныйОстаток);
	ОбластьМакета.Параметры.КонСтоимостьТара  = ФорматСумм(0);
	ТабДокумент.Вывести(ОбластьМакета);
	
	ОбластьМакета = Макет.ПолучитьОбласть("Подвал");
	//ОбластьМакета.Параметры.КонСтоимостьВсего = ФорматСумм(СтоимостьПоступленияБезНДСКонечныйОстаток + НДСПоступленияКонечныйОстаток);
	//ОбластьМакета.Параметры.КонСтоимостьТара  = ФорматСумм(0);
	ОбластьМакета.Параметры.МОЛ = Склад.ТекущийОтветственный;
	ОбластьМакета.Параметры.ДолжностьМОЛ = Склад.ТекущаяДолжностьОтветственного;
	ТабДокумент.Вывести(ОбластьМакета);
	
	ТабДокумент.ПовторятьПриПечатиСтроки = ПовторятьПриПечатиСтроки;
		
	Возврат ТабДокумент;
	
КонецФункции // СформироватьОтчетЛокализация()
   
Функция ПолучитьПредставлениеДокумента(СтрокаТЗ, ТипыКорректировок, ТипДвижения = "Расход")
	
	ТипДокумента = ТипЗнч(СтрокаТЗ.Документ);
	Если ТипДокумента = Тип("ДокументСсылка.УстановкаЦенНоменклатуры") Тогда
		Представление =  ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(СтрокаТЗ, НСтр("ru = 'Установка цен'"));
	ИначеЕсли ТипДокумента = Тип("ДокументСсылка.ВозвратТоваровОтКлиента") Тогда
		Представление =  СтрокаТЗ.Документ.Контрагент.НаименованиеПолное;
	ИначеЕсли ТипДокумента = Тип("ДокументСсылка.ВозвратТоваровПоставщику") Тогда
		Представление =  СтрокаТЗ.Документ.Контрагент.НаименованиеПолное;
	ИначеЕсли ТипДокумента = Тип("ДокументСсылка.ОприходованиеИзлишковТоваров") Тогда
		Представление =  ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(СтрокаТЗ, НСтр("ru = 'Акт об оприходовании товаров'"));
	ИначеЕсли ТипДокумента = Тип("ДокументСсылка.ПрочееОприходованиеТоваров") Тогда
		Представление =  ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(СтрокаТЗ, НСтр("ru = 'Акт об оприходовании товаров'"));
	ИначеЕсли ТипДокумента = Тип("ДокументСсылка.ОтчетОРозничныхПродажах") Тогда
		Представление =  ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(СтрокаТЗ, НСтр("ru = 'Отчет о розничных продажах'"));
	ИначеЕсли ТипДокумента = Тип("ДокументСсылка.ПеремещениеТоваров") Тогда
		Представление =  СтрокаТЗ.Документ.Организация.НаименованиеСокращенное + " " + СтрокаТЗ.Документ.СкладОтправитель.Наименование;
	ИначеЕсли ТипДокумента = Тип("ДокументСсылка.ПересортицаТоваров") Тогда
		Представление =  ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(СтрокаТЗ, НСтр("ru = 'Акт о пересортице товаров'"));
	ИначеЕсли ТипДокумента = Тип("ДокументСсылка.ПорчаТоваров") Тогда
		Представление =  ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(СтрокаТЗ, НСтр("ru = 'Акт о порче товаров'"));
	ИначеЕсли ТипДокумента = Тип("ДокументСсылка.ПриобретениеТоваровУслуг") Тогда
		Представление =  СтрокаТЗ.Документ.Контрагент.НаименованиеПолное;
	ИначеЕсли ТипДокумента = Тип("ДокументСсылка.КорректировкаПриобретения") Тогда
		Представление =  ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(СтрокаТЗ, НСтр("ru = 'Корректировка приходной накладной'"));
	ИначеЕсли ТипДокумента = Тип("ДокументСсылка.РеализацияТоваровУслуг") Тогда
		Представление =  СтрокаТЗ.Документ.Контрагент.НаименованиеПолное;
	ИначеЕсли ТипДокумента = Тип("ДокументСсылка.КорректировкаРеализации") Тогда
		Представление =  ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(СтрокаТЗ, НСтр("ru = 'Корректировка расходной накладной'"));
	ИначеЕсли ТипДокумента = Тип("ДокументСсылка.СборкаТоваров") Тогда
		Представление =  ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(СтрокаТЗ, НСтр("ru = 'Сборка товаров'"));
	ИначеЕсли ТипДокумента = Тип("ДокументСсылка.СписаниеНедостачТоваров") Тогда
		Представление =  ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(СтрокаТЗ, НСтр("ru = 'Акт о списании товаров'"));
	ИначеЕсли ТипДокумента = Тип("ДокументСсылка.ВнутреннееПотреблениеТоваров") Тогда
		Представление =  ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(СтрокаТЗ, НСтр("ru = 'Требование-накладная'"));
	ИначеЕсли ТипДокумента = Тип("ДокументСсылка.ПередачаТоваровМеждуОрганизациями") Тогда
		Если ТипДвижения = "Расход" Тогда
			Представление =  СтрокаТЗ.Документ.Контрагент.НаименованиеПолное;
		Иначе // Приход
			Представление =  СтрокаТЗ.Документ.Контрагент.НаименованиеПолное;
		КонецЕсли;
	ИначеЕсли ТипыКорректировок.Найти(ТипДокумента) <> Неопределено Тогда
		Представление = ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(СтрокаТЗ, НСтр("ru = 'Корректировка остатков'"));
	Иначе
		Представление = ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(СтрокаТЗ, Строка(ТипДокумента));
	КонецЕсли;
	
	Возврат Представление;
	
КонецФункции

#КонецОбласти

#КонецЕсли

﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
#Область ПрограммныйИнтерфейс
	
Функция ВерсияФорматаВыгрузки(Знач НаДату = Неопределено, ВыбраннаяФорма = Неопределено) Экспорт
	
	Если НаДату = Неопределено Тогда
		НаДату = ТекущаяДатаСеанса();
	КонецЕсли;
	
	Если НаДату > '20080601' Тогда
		Возврат Перечисления.ВерсииФорматовВыгрузки.Версия500;
	ИначеЕсли НаДату > '20050101' Тогда
		Возврат Перечисления.ВерсииФорматовВыгрузки.Версия300;
	КонецЕсли;
	
КонецФункции

Функция ТаблицаФормОтчета() Экспорт
	
	ОписаниеТиповСтрока = Новый ОписаниеТипов("Строка", , Новый КвалификаторыСтроки(254));
	
	МассивТипов = Новый Массив;
	МассивТипов.Добавить(Тип("Дата"));
	ОписаниеТиповДата = Новый ОписаниеТипов(МассивТипов, , Новый КвалификаторыДаты(ЧастиДаты.Дата));
	
	ТаблицаФормОтчета = Новый ТаблицаЗначений;
	ТаблицаФормОтчета.Колонки.Добавить("ФормаОтчета",        ОписаниеТиповСтрока);
	ТаблицаФормОтчета.Колонки.Добавить("ОписаниеОтчета",     ОписаниеТиповСтрока, "Утверждена",  20);
	ТаблицаФормОтчета.Колонки.Добавить("ДатаНачалоДействия", ОписаниеТиповДата,   "Действует с", 5);
	ТаблицаФормОтчета.Колонки.Добавить("ДатаКонецДействия",  ОписаниеТиповДата,   "         по", 5);
	ТаблицаФормОтчета.Колонки.Добавить("РедакцияФормы",      ОписаниеТиповСтрока, "Редакция формы", 20);
	
		
	НоваяФорма = ТаблицаФормОтчета.Добавить();
	НоваяФорма.ФормаОтчета        = "ФормаОтчета2018";
	НоваяФорма.ОписаниеОтчета     = "Утверждена Постановлением Министерства по налогам и сборам Республики Беларусь от 24.12.2014 № 42 (в ред. постановления Министерства по налогам и сборам Республики Беларусь 31.01.2018 № 2)";
	НоваяФорма.РедакцияФормы      = "от 21 января 2018 г. № 2@."; 
	НоваяФорма.ДатаНачалоДействия = '2018-01-01';
	НоваяФорма.ДатаКонецДействия  = '2019-02-16';
	
	НоваяФорма = ТаблицаФормОтчета.Добавить();
	НоваяФорма.ФормаОтчета        = "ФормаОтчета2019";
	НоваяФорма.ОписаниеОтчета     = "Утверждена Постановлением Министерства по налогам и сборам Республики Беларусь от 03.01.2019 № 2";
	НоваяФорма.РедакцияФормы      = "от 03 января 2019 г. № 2@."; 
	НоваяФорма.ДатаНачалоДействия = '2019-02-16';
	НоваяФорма.ДатаКонецДействия  = '2021-02-09'; //РегламентированнаяОтчетностьКлиентСервер.ПустоеЗначениеТипа(Тип("Дата"));
	
	НоваяФорма = ТаблицаФормОтчета.Добавить();
	НоваяФорма.ФормаОтчета        = "ФормаОтчета2021";
	НоваяФорма.ОписаниеОтчета     = "Утверждена Постановлением Министерства по налогам и сборам Республики Беларусь от 03.01.2019 № 2 (в ред. постановления Министерства по налогам и сборам Республики Беларусь 15.01.2021 № 2)";
	НоваяФорма.РедакцияФормы      = "от 15 января 2021 г. № 2@."; 
	НоваяФорма.ДатаНачалоДействия = '2021-02-10';
	НоваяФорма.ДатаКонецДействия  = РегламентированнаяОтчетностьКлиентСервер.ПустоеЗначениеТипа(Тип("Дата"));
	
	
	
	Возврат ТаблицаФормОтчета;
	
КонецФункции
//проверить
Функция ДанныеРеглОтчета(ЭкземплярРеглОтчета) Экспорт
	
	ТаблицаДанныхРеглОтчета = ИнтерфейсыВзаимодействияБРО.НовыйТаблицаДанныхРеглОтчета();
	
	Если ЭкземплярРеглОтчета.ВыбраннаяФорма = "ФормаОтчета2012Кв1"
	 ИЛИ ЭкземплярРеглОтчета.ВыбраннаяФорма = "ФормаОтчета2013Кв4"
	 ИЛИ ЭкземплярРеглОтчета.ВыбраннаяФорма = "ФормаОтчета2015Кв1"
	 ИЛИ ЭкземплярРеглОтчета.ВыбраннаяФорма = "ФормаОтчета2016Кв4" Тогда
		
		ДанныеРеглОтчета = ЭкземплярРеглОтчета.ДанныеОтчета.Получить();
		Если ТипЗнч(ДанныеРеглОтчета) <> Тип("Структура") Тогда
			Возврат ТаблицаДанныхРеглОтчета;
		КонецЕсли;
		
		Если ДанныеРеглОтчета.ДанныеМногостраничныхРазделов.Свойство("Раздел1_1") Тогда
			
			НалогКУплате = ДанныеРеглОтчета.ДанныеМногостраничныхРазделов.Раздел1_1;
			
			Период = ЭкземплярРеглОтчета.ДатаОкончания;
			
			Аванс = (КонецМесяца(Период) <> КонецГода(Период)); // все платежи, кроме годового - авансы
			
			КодСтрокиОКАТО    = "П000110001003";
			КодСтрокиКБК_ФБ   = "П000110003003";
			КодСтрокиСумма_ФБ = "П000110004003";
			КодСтрокиКБК_РБ   = "П000110006003";
			КодСтрокиСумма_РБ = "П000110007003";
			
			Для Каждого МестоУплаты Из НалогКУплате Цикл
				
				// Федеральный.
				Сумма = ТаблицаДанныхРеглОтчета.Добавить();
				Сумма.Период = Период;
				Сумма.ОКАТО  = МестоУплаты.Данные[КодСтрокиОКАТО];
				Сумма.КБК    = МестоУплаты.Данные[КодСтрокиКБК_ФБ];
				Сумма.Сумма  = МестоУплаты.Данные[КодСтрокиСумма_ФБ];
				Сумма.Аванс  = Аванс;
				
				// Региональный.
				Сумма = ТаблицаДанныхРеглОтчета.Добавить();
				Сумма.Период = Период;
				Сумма.ОКАТО  = МестоУплаты.Данные[КодСтрокиОКАТО];
				Сумма.КБК    = МестоУплаты.Данные[КодСтрокиКБК_РБ];
				Сумма.Сумма  = МестоУплаты.Данные[КодСтрокиСумма_РБ];
				Сумма.Аванс  = Аванс;
				
			КонецЦикла;
			
		КонецЕсли;
		
		Если ДанныеРеглОтчета.ДанныеМногостраничныхРазделов.Свойство("Раздел1_2") Тогда
			
			АвансовыеПлатежи = ДанныеРеглОтчета.ДанныеМногостраничныхРазделов.Раздел1_2;
			
			Период          = КонецКвартала(ЭкземплярРеглОтчета.ДатаОкончания);
			КодСтрокиОКАТО  = "П000120001003";
			КодСтрокиКБК_ФБ = "П000120011003";
			КодСтрокиСумма_ФБ = Новый Соответствие; 
			КодСтрокиСумма_ФБ.Вставить(1, "П000120012003");
			КодСтрокиСумма_ФБ.Вставить(2, "П000120013003");
			КодСтрокиСумма_ФБ.Вставить(3, "П000120014003");
			
			Если Месяц(Период) = 9 Тогда
				КодСтрокиСумма_ФБ.Вставить(4, "П000120012003");
				КодСтрокиСумма_ФБ.Вставить(5, "П000120013003");
				КодСтрокиСумма_ФБ.Вставить(6, "П000120014003");
			КонецЕсли;
			
			КодСтрокиКБК_РБ = "П000120021003";
			КодСтрокиСумма_РБ = Новый Соответствие; 
			КодСтрокиСумма_РБ.Вставить(1, "П000120022003");
			КодСтрокиСумма_РБ.Вставить(2, "П000120023003");
			КодСтрокиСумма_РБ.Вставить(3, "П000120024003");
			
			Если Месяц(Период) = 9 Тогда
				КодСтрокиСумма_РБ.Вставить(4, "П000120022003");
				КодСтрокиСумма_РБ.Вставить(5, "П000120023003");
				КодСтрокиСумма_РБ.Вставить(6, "П000120024003");
			КонецЕсли;
			
			Для Каждого МестоУплаты Из АвансовыеПлатежи Цикл
				
				// Федеральный.
				Для Каждого Месяц Из КодСтрокиСумма_ФБ Цикл
					
					Сумма = ТаблицаДанныхРеглОтчета.Добавить();
					Сумма.Период = КонецМесяца(ДобавитьМесяц(Период, Месяц.Ключ));
					Сумма.ОКАТО  = МестоУплаты.Данные[КодСтрокиОКАТО];
					Сумма.КБК    = МестоУплаты.Данные[КодСтрокиКБК_ФБ];
					Сумма.Сумма  = МестоУплаты.Данные[Месяц.Значение];
					Сумма.Аванс  = Истина;
					
				КонецЦикла;
				
				// Региональный.
				Для Каждого Месяц Из КодСтрокиСумма_РБ Цикл
					
					Сумма = ТаблицаДанныхРеглОтчета.Добавить();
					Сумма.Период = КонецМесяца(ДобавитьМесяц(Период, Месяц.Ключ));
					Сумма.ОКАТО  = МестоУплаты.Данные[КодСтрокиОКАТО];
					Сумма.КБК    = МестоУплаты.Данные[КодСтрокиКБК_РБ];
					Сумма.Сумма  = МестоУплаты.Данные[Месяц.Значение];
					Сумма.Аванс  = Истина;
					
				КонецЦикла;
				
			КонецЦикла;
			
		КонецЕсли;
		
		Если ДанныеРеглОтчета.ДанныеМногостраничныхРазделов.Свойство("Раздел1_3") Тогда
			НалогСПроцентов = ДанныеРеглОтчета.ДанныеМногостраничныхРазделов.Раздел1_3;
			// Не обрабатываем.
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат ТаблицаДанныхРеглОтчета;
	
КонецФункции

Функция ДеревоФормИФорматов() Экспорт
	
	ФормыИФорматы = Новый ДеревоЗначений;
	ФормыИФорматы.Колонки.Добавить("Код");
	ФормыИФорматы.Колонки.Добавить("ДатаПриказа");
	ФормыИФорматы.Колонки.Добавить("НомерПриказа");
	ФормыИФорматы.Колонки.Добавить("ДатаНачалаДействия");
	ФормыИФорматы.Колонки.Добавить("ДатаОкончанияДействия");
	ФормыИФорматы.Колонки.Добавить("ИмяОбъекта");
	ФормыИФорматы.Колонки.Добавить("Описание");
	
	Форма20101201 = ОпределитьФормуВДеревеФормИФорматов(ФормыИФорматы, "1151006", '2010-12-15', "ММВ-7-3/730@", "ФормаОтчета2010Кв4");
	ОпределитьФорматВДеревеФормИФорматов(Форма20101201, "5.03");
	
	Форма20120101 = ОпределитьФормуВДеревеФормИФорматов(ФормыИФорматы, "1151006", '2012-03-22', "ММВ-7-3/174@", "ФормаОтчета2012Кв1");
	ОпределитьФорматВДеревеФормИФорматов(Форма20120101, "5.04");
	
	Форма20131231 = ОпределитьФормуВДеревеФормИФорматов(ФормыИФорматы, "1151006", '2013-11-14', "ММВ-7-3/501@", "ФормаОтчета2013Кв4");
	ОпределитьФорматВДеревеФормИФорматов(Форма20131231, "5.05");
	
	Форма20141231 = ОпределитьФормуВДеревеФормИФорматов(ФормыИФорматы, "1151006", '2014-11-26', "ММВ-7-3/600@", "ФормаОтчета2015Кв1");
	ОпределитьФорматВДеревеФормИФорматов(Форма20141231, "5.06");
	
	Форма2016Кв4 = ОпределитьФормуВДеревеФормИФорматов(ФормыИФорматы, "1151006", '2016-10-19', "ММВ-7-3/572@", "ФормаОтчета2016Кв4");
	ОпределитьФорматВДеревеФормИФорматов(Форма2016Кв4, "5.07");
	
	Возврат ФормыИФорматы;
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ОпределитьФормуВДеревеФормИФорматов(ДеревоФормИФорматов, Код, ДатаПриказа = '00010101', НомерПриказа = "", ИмяОбъекта = "",
			ДатаНачалаДействия = '00010101', ДатаОкончанияДействия = '00010101', Описание = "")
	
	НовСтр = ДеревоФормИФорматов.Строки.Добавить();
	НовСтр.Код = СокрЛП(Код);
	НовСтр.ДатаПриказа = ДатаПриказа;
	НовСтр.НомерПриказа = СокрЛП(НомерПриказа);
	НовСтр.ДатаНачалаДействия = ДатаНачалаДействия;
	НовСтр.ДатаОкончанияДействия = ДатаОкончанияДействия;
	НовСтр.ИмяОбъекта = СокрЛП(ИмяОбъекта);
	НовСтр.Описание = СокрЛП(Описание);
	Возврат НовСтр;
	
КонецФункции

Функция ОпределитьФорматВДеревеФормИФорматов(Форма, Версия, ДатаПриказа = '00010101', НомерПриказа = "",
			ДатаНачалаДействия = Неопределено, ДатаОкончанияДействия = Неопределено, ИмяОбъекта = "", Описание = "")
	
	НовСтр = Форма.Строки.Добавить();
	НовСтр.Код = СокрЛП(Версия);
	НовСтр.ДатаПриказа = ДатаПриказа;
	НовСтр.НомерПриказа = СокрЛП(НомерПриказа);
	НовСтр.ДатаНачалаДействия = ?(ДатаНачалаДействия = Неопределено, Форма.ДатаНачалаДействия, ДатаНачалаДействия);
	НовСтр.ДатаОкончанияДействия = ?(ДатаОкончанияДействия = Неопределено, Форма.ДатаОкончанияДействия, ДатаОкончанияДействия);
	НовСтр.ИмяОбъекта = СокрЛП(ИмяОбъекта);
	НовСтр.Описание = СокрЛП(Описание);
	Возврат НовСтр;
	
КонецФункции

#КонецОбласти

#КонецЕсли
﻿#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ВосстановитьНачальныеЗначения(Команда)
		ЗарплатаКадрыКлиент.ВосстановитьНачальныеЗначенияСправочника(ЭтаФорма, "Справочник.ЕдинаяТарифнаяСетка");

КонецПроцедуры

#КонецОбласти



#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура Подключаемый_ОжиданиеВыполненияДлительнойОперации()
	
	Если ЗарплатаКадрыКлиент.ВосстановлениеНачальныхЗначенийВыполнено(ЭтаФорма) Тогда
		Элементы.Список.Обновить();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

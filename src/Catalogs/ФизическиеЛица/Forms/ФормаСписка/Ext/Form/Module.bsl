﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПараметрыРазмещения = ПодключаемыеКоманды.ПараметрыРазмещения();
	ПараметрыРазмещения.КоманднаяПанель = Элементы.КоманднаяПанельФормы;
	ПодключаемыеКоманды.ПриСозданииНаСервере(ЭтотОбъект, ПараметрыРазмещения);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	Если Параметры.РежимВыбора = Истина Тогда
		
		Элементы.Список.РежимВыбора = Истина;
		Если НЕ ЭтаФорма.ЗакрыватьПриВыборе Тогда
		
			Если НЕ ПустаяСтрока(Параметры.АдресСпискаПодобранныхСотрудников) Тогда
				МассивПодобранных = ПолучитьИзВременногоХранилища(Параметры.АдресСпискаПодобранныхСотрудников);
				СписокПодобранных.ЗагрузитьЗначения(МассивПодобранных);
			КонецЕсли; 
		
		КонецЕсли; 
		
	КонецЕсли;
	
	УстановитьСписокПодобранныхСотрудников();
	
	ЗарплатаКадры.ПриСозданииНаСервереФормыСДинамическимСписком(ЭтотОбъект, "Список");
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если Элементы.Список.РежимВыбора И ИмяСобытия = "СозданоФизическоеЛицо" И Источник = Элементы.Список Тогда
		
		Если Элементы.Список.МножественныйВыбор И ТипЗнч(Параметр) <> Тип("Массив") Тогда
			ПараметрОповещения = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(Параметр);
		Иначе
			ПараметрОповещения = Параметр;
		КонецЕсли; 
		
		ОповеститьОВыборе(ПараметрОповещения);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти


#Область ОбработчикиСобытийЭлементовТаблицыФормыСписок

&НаКлиенте
Процедура СписокВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	ОценкаПроизводительностиКлиент.НачатьЗамерВремени(Истина, "ОткрытиеФормыЭлементаСправочникаФизическиеЛица");
КонецПроцедуры

&НаКлиенте
Процедура СписокВыборЗначения(Элемент, Значение, СтандартнаяОбработка)
	
	Если Элементы.Список.РежимВыбора И НЕ ЗакрыватьПриВыборе Тогда
		ОбновитьСписокПодобранных(Значение);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СписокПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	
	ОценкаПроизводительностиКлиент.НачатьЗамерВремени(Истина, "СозданиеНовогоЭлементаСправочникаФизическиеЛица");
	
	Если Не Группа И Элементы.Список.РежимВыбора Тогда
		
		Отказ = Истина;
		
		ПараметрыОткрытия = Новый Структура;
		ПараметрыОткрытия.Вставить("РежимВыбора", Истина);
		
		ОткрытьФорму("Справочник.ФизическиеЛица.ФормаОбъекта", ПараметрыОткрытия, Элемент);

	КонецЕсли; 
	
КонецПроцедуры

&НаСервере
Процедура СписокПередЗагрузкойПользовательскихНастроекНаСервере(Элемент, Настройки)
	ЗарплатаКадры.ПроверитьПользовательскиеНастройкиДинамическогоСписка(ЭтотОбъект, Настройки);
КонецПроцедуры

&НаСервере
Процедура СписокПриОбновленииСоставаПользовательскихНастроекНаСервере(СтандартнаяОбработка)
	ЗарплатаКадры.ПроверитьПользовательскиеНастройкиДинамическогоСписка(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура СписокПриАктивизацииСтроки(Элемент)
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
КонецПроцедуры

#КонецОбласти


#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.ПодключаемыеКоманды
&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	ПодключаемыеКомандыКлиент.ВыполнитьКоманду(ЭтотОбъект, Команда, Элементы.Список);
КонецПроцедуры

&НаСервере
Процедура Подключаемый_ВыполнитьКомандуНаСервере(Контекст, Результат) Экспорт
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, Контекст, Элементы.Список, Результат);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Элементы.Список);
КонецПроцедуры
// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

#КонецОбласти


#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ОбновитьСписокПодобранных(Значение)
	
	Если ТипЗнч(Значение) = Тип("Массив") Тогда
		
		Для каждого ВыбранноеЗначение Из Значение Цикл
			СписокПодобранных.Добавить(ВыбранноеЗначение);
		КонецЦикла;
		
	Иначе
		СписокПодобранных.Добавить(Значение);
	КонецЕсли;
	
	УстановитьСписокПодобранныхСотрудников();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьСписокПодобранныхСотрудников()
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(
		Список,
		"СписокПодобранных",
		СписокПодобранных.ВыгрузитьЗначения());
		
КонецПроцедуры

#КонецОбласти

﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
#Область ПрограммныйИнтерфейс

#Область ДляВызоваИзДругихПодсистем

// СтандартныеПодсистемы.УправлениеДоступом

// См. УправлениеДоступомПереопределяемый.ПриЗаполненииСписковСОграничениемДоступа.
Процедура ПриЗаполненииОграниченияДоступа(Ограничение) Экспорт
	Ограничение.Текст =
	"РазрешитьЧтениеИзменение
	|ГДЕ
	|	ЗначениеРазрешено(Владелец)";
КонецПроцедуры

// Конец СтандартныеПодсистемы.УправлениеДоступом

#КонецОбласти

#КонецОбласти
	
#Область СлужебныйПрограммныйИнтерфейс

// Формирует представление об образовании физического лица, по переделанной коллекции записей.
//
// Параметры:
//		ЗаписиОбОбразовании - Коллекция записей с полями:
//				* ОсновноеОбразование	- Булево.
//				* ВидОбразования.
//				* ВидДополнительногоОбучения.
//				* УчебноеЗаведение.
//				* Окончание				- Дата
//				* Специальность
//				* Квалификация
//
// Возвращаемое значение:
//		Строка
//
Функция ПредставлениеСведенийОбОбразовании(ЗаписиОбОбразовании) Экспорт
	
	ПредставлениеСведенийОбОбразовании = НСтр("ru='Нет сведений'");
	
	ДанныеОбразования = Неопределено;
	Для Каждого СтрокаОбразование Из ЗаписиОбОбразовании Цикл
		
		Если НЕ СтрокаОбразование.ОсновноеОбразование Тогда
			Продолжить;
		КонецЕсли; 
		
		Если ДанныеОбразования = Неопределено ИЛИ ДанныеОбразования.Окончание < СтрокаОбразование.Окончание Тогда
			ДанныеОбразования = СтрокаОбразование;
		КонецЕсли; 
		
	КонецЦикла;
	
	Если ДанныеОбразования <> Неопределено Тогда
		
		ПредставлениеСведенийОбОбразовании = ?(ЗначениеЗаполнено(ДанныеОбразования.ВидОбразования), Строка(ДанныеОбразования.ВидОбразования), "");
		
		ПредставлениеСведенийОбОбразовании = ?(ПустаяСтрока(ПредставлениеСведенийОбОбразовании), "", ПредставлениеСведенийОбОбразовании + Символы.ПС)
			+ ?(ЗначениеЗаполнено(ДанныеОбразования.УчебноеЗаведение), Строка(ДанныеОбразования.УчебноеЗаведение), "")
			+ ?(ЗначениеЗаполнено(ДанныеОбразования.Окончание), " (" + Формат(ДанныеОбразования.Окончание, "ДФ=гггг") + ")", ""); 
			
		ПредставлениеСведенийОбОбразовании = ?(ПустаяСтрока(ПредставлениеСведенийОбОбразовании), "", ПредставлениеСведенийОбОбразовании + Символы.ПС)
			+ ?(ЗначениеЗаполнено(ДанныеОбразования.Специальность), Строка(ДанныеОбразования.Специальность), "")
			+ ?(ЗначениеЗаполнено(ДанныеОбразования.Квалификация), ", " + Строка(ДанныеОбразования.Квалификация), "");
			
	КонецЕсли; 
		
	Возврат ПредставлениеСведенийОбОбразовании;
	
КонецФункции

#КонецОбласти

#Область ОбработчикиСобытий

// Формирует представление об образовании физического лица.
Процедура ОбработкаПолученияПредставления(Данные, Представление, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Если Данные.ОсновноеОбразование 
		И Данные.ВидОбразования = Справочники.ВидыОбразованияФизическихЛиц.ПослевузовскоеОбразование Тогда
		Представление = Строка(Данные.ВидПослевузовскогоОбразования);
	ИначеЕсли Данные.ОсновноеОбразование Тогда	
		Представление = Строка(Данные.ВидОбразования);
	ИначеЕсли ЗначениеЗаполнено(Данные.ВидДополнительногоОбучения ) Тогда
		Представление = Строка(Данные.ВидДополнительногоОбучения);
	КонецЕсли;
	
	Представление = Представление + ?(ЗначениеЗаполнено(Данные.УчебноеЗаведение)," "+ Строка(Данные.УчебноеЗаведение), "")
								+ ?(ЗначениеЗаполнено(Данные.Окончание), " (" + Формат(Данные.Окончание, "ДФ=гггг") + ")", ""); 
 
	 Если ПустаяСтрока(Представление) Тогда 
		 Представление = НСтр("ru='Нет Данных'");
	 КонецЕсли;

КонецПроцедуры

Процедура ОбработкаПолученияПолейПредставления(Поля, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	Поля.Добавить("ОсновноеОбразование");
	Поля.Добавить("ВидОбразования");
	Поля.Добавить("ВидПослевузовскогоОбразования");
	Поля.Добавить("ВидДополнительногоОбучения");
	Поля.Добавить("УчебноеЗаведение");
	Поля.Добавить("Окончание");
	
КонецПроцедуры

#КонецОбласти


#Область СлужебныеПроцедурыИФункции

Процедура ЗаполнитьОбразованияФизическихЛиц() Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	ОбразованиеФЛ.Ссылка
		|ИЗ
		|	Справочник.ОбразованиеФизическихЛиц КАК ОбразованиеФЛ";
		
	РезультатЗапроса = Запрос.Выполнить();
	Если РезультатЗапроса.Пустой() Тогда
		
		Запрос.Текст =
			"ВЫБРАТЬ
			|	ОбразованиеФЛ.ФизическоеЛицо КАК Владелец,
			|	ОбразованиеФЛ.ВидОбразования,
			|	ОбразованиеФЛ.ВидПослевузовскогоОбразования,
			|	ОбразованиеФЛ.УчебноеЗаведение,
			|	ОбразованиеФЛ.Специальность,
			|	ЗНАЧЕНИЕ(Справочник.ВидыДокументовОбОбразовании.Диплом) КАК ВидДокумента,
			|	ОбразованиеФЛ.Диплом КАК Номер,
			|	ОбразованиеФЛ.ГодОкончания,
			|	ОбразованиеФЛ.Квалификация,
			|	ИСТИНА КАК ОсновноеОбразование
			|ИЗ
			|	РегистрСведений.УдалитьОбразованиеФизическихЛиц КАК ОбразованиеФЛ";
			
		РезультатЗапроса = Запрос.Выполнить();
		Если НЕ РезультатЗапроса.Пустой() Тогда
			
			Выборка = РезультатЗапроса.Выбрать();
			Пока Выборка.Следующий() Цикл
				
				ОбразованиеОбъект = Справочники.ОбразованиеФизическихЛиц.СоздатьЭлемент();
				ЗаполнитьЗначенияСвойств(ОбразованиеОбъект, Выборка);
				
				Если Выборка.ГодОкончания > 0 Тогда
					ОбразованиеОбъект.ДатаВыдачи = Дата(Выборка.ГодОкончания, 1, 1);
					ОбразованиеОбъект.Окончание = ОбразованиеОбъект.ДатаВыдачи;
				КонецЕсли; 
				
				ОбразованиеОбъект.ОбменДанными.Загрузка = Истина;
				ОбразованиеОбъект.Записать();
				
			КонецЦикла; 
			
		КонецЕсли; 
		
	КонецЕсли; 
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
﻿
////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ И ФУНКЦИИ ОБЩЕГО НАЗНАЧЕНИЯ

Процедура УстановитьКоличествоТомовВДеле()
	
	Если Не ЭтотОбъект.Ссылка.Родитель.ТипГруппы = "Дело" Тогда
		Возврат;
	КонецЕсли;
	
	Дело = ЭтотОбъект.Ссылка.Родитель;
	
	Запрос = Новый Запрос;
	Запрос.Параметры.Вставить("Дело", Дело);
	
	Запрос.Текст = "ВЫБРАТЬ РАЗРЕШЕННЫЕ
	               |	КОЛИЧЕСТВО(РАЗЛИЧНЫЕ EDiN_РКК_ЭД.Ссылка) КАК КоличествоТомов
	               |ИЗ
	               |	Справочник.EDiN_РКК_ЭД КАК EDiN_РКК_ЭД
	               |ГДЕ
	               |	EDiN_РКК_ЭД.Родитель = &Дело
	               |	И EDiN_РКК_ЭД.ЭтоГруппа = ИСТИНА
	               |	И EDiN_РКК_ЭД.ТипГруппы = ""Том""";
	
	Результат = Запрос.Выполнить();
	Если Результат.Пустой() Тогда
		Возврат;
	КонецЕсли;
	
	Выборка = Результат.Выбрать(); 
	Выборка.Следующий();
	
	Если Дело.КоличествоТомов = Выборка.КоличествоТомов Тогда
		Возврат;
	КонецЕсли;
	
	ДелоОбъект = Дело.ПолучитьОбъект();
	ДелоОбъект.КоличествоТомов = Выборка.КоличествоТомов;
	ДелоОбъект.Записать();	
	
КонецПроцедуры	

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ

Процедура ПередЗаписью(Отказ)
	
	Если ЭтоГруппа Тогда
		
		Возврат;
		
	КонецЕсли;	
	
	Если Не ЗначениеЗаполнено(Номер) Тогда
		Если Не ЗначениеЗаполнено(Код) Тогда 
			УстановитьНовыйКод();
		КонецЕсли;	
		Номер = Код;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Дата) Тогда
		Дата = ТекущаяДата();
	КонецЕсли;	
	
КонецПроцедуры

Процедура ПриЗаписи(Отказ)
	
	Если ЭтоГруппа Тогда
		
		Если ТипГруппы = "Том" Тогда
			
			УстановитьКоличествоТомовВДеле();	
			
		КонецЕсли;	
		
	КонецЕсли;	

КонецПроцедуры

﻿
#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

Функция ПолучитьСписокРеквизитов() Экспорт
	СписокРеквизитов = Новый Массив;
	
	ВсеМетаданные = Метаданные();
	Для каждого ОбъектРеквизит Из ВсеМетаданные.Реквизиты Цикл
		СписокРеквизитов.Добавить(ОбъектРеквизит.Имя);
	КонецЦикла;
	
	Возврат СписокРеквизитов;
КонецФункции

#КонецОбласти

#Область ОбработчикиСобытий

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	Если УстановкаЦен Тогда
		ОшибочныеИдентификаторы = Новый Массив;
		ОшибочныеИдентификаторы.Добавить("ВидНоменклатуры");
		ОшибочныеИдентификаторы.Добавить("Марка");
		ОшибочныеИдентификаторы.Добавить("СтавкаНДС");
		
		Для каждого СтрКлюч Из ОшибочныеИдентификаторы Цикл
			ПоисковыйИндекс = ПроверяемыеРеквизиты.Найти(СтрКлюч);
			Если Не ПоисковыйИндекс = Неопределено Тогда
				ПроверяемыеРеквизиты.Удалить(ПоисковыйИндекс);
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#КонецЕсли
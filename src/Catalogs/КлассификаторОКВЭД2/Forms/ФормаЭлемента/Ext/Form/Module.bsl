﻿	
#Область ЛокализацияБел
 
&НаСервере
Процедура Подключаемый_ПриСозданииНаСервереЛокализацияБел(Отказ, СтандартнаяОбработка)
	
	ЭтаФорма.АвтоЗаголовок = Ложь;
	ЭтаФорма.Заголовок = ЭтаФорма.Объект.Код + " (Классификатор ОКЭД)";	
	
КонецПроцедуры

#Область Инициализация

#Если НаСервере Тогда

ПодключаемыеОбработчикиСобытийФормы = Новый Массив;
ПодключаемыеОбработчикиСобытийФормы.Добавить("ПриСозданииНаСервере");

Для Каждого Обработчик Из ПодключаемыеОбработчикиСобытийФормы Цикл
	УстановитьДействие(Обработчик, "Подключаемый_" + Обработчик + "ЛокализацияБел");
КонецЦикла;

#КонецЕсли

#КонецОбласти

#КонецОбласти
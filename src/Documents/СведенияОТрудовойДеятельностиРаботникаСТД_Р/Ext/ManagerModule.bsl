﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

#Область ДляВызоваИзДругихПодсистем

// СтандартныеПодсистемы.УправлениеДоступом

// См. УправлениеДоступомПереопределяемый.ПриЗаполненииСписковСОграничениемДоступа.
Процедура ПриЗаполненииОграниченияДоступа(Ограничение) Экспорт
	Ограничение.Текст =
	"РазрешитьЧтениеИзменение
	|ГДЕ
	|	ЗначениеРазрешено(ФизическоеЛицо)
	|	И ЗначениеРазрешено(Организация)";
КонецПроцедуры

// Конец СтандартныеПодсистемы.УправлениеДоступом

#КонецОбласти

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает описание состава документа
//
// Возвращаемое значение:
//  Структура - см. ЗарплатаКадрыСоставДокументов.НовоеОписаниеСоставаОбъекта.
Функция ОписаниеСоставаОбъекта() Экспорт
	
	Возврат ЗарплатаКадрыСоставДокументов.ОписаниеСоставаОбъектаФизическоеЛицоВШапке("ФизическоеЛицо", "");
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати.
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.Обработчик = "УправлениеПечатьюБЗККлиент.ВыполнитьКомандуПечати";
	КомандаПечати.МенеджерПечати = "Документ.СведенияОТрудовойДеятельностиРаботникаСТД_Р";
	КомандаПечати.Идентификатор = "ПФ_MXL_СТД_Р";
	КомандаПечати.Представление = НСтр("ru = 'СТД-Р';
										|en = 'STD-R'");
	КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
	
КонецПроцедуры

Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "ПФ_MXL_СТД_Р") Тогда
		
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(
			КоллекцияПечатныхФорм,
			"ПФ_MXL_СТД_Р", НСтр("ru = 'Сведения о трудовой деятельности работника, СТД-Р';
								|en = 'Employee labor activity information, STD-R'"),
			ТабличныйДокументСТД_Р(МассивОбъектов, ОбъектыПечати), ,
			"Документ.СведенияОТрудовойДеятельностиРаботникаСТД_Р.ПФ_MXL_СТД_Р");
			
	КонецЕсли;
	
КонецПроцедуры

Функция ТабличныйДокументСТД_Р(МассивОбъектов, ОбъектыПечати)
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("МассивОбъектов", МассивОбъектов);
	
	ОписаниеИсточникаДанных = ПерсонифицированныйУчет.ОписаниеИсточникаДанныхДляСоздатьВТСведенияОбОрганизациях();
	ОписаниеИсточникаДанных.ИмяТаблицы = "Документ.СведенияОТрудовойДеятельностиРаботникаСТД_Р";
	ОписаниеИсточникаДанных.ИмяПоляОрганизация = "Организация";
	ОписаниеИсточникаДанных.ИмяПоляПериод = "Дата";
	ОписаниеИсточникаДанных.СписокСсылок = МассивОбъектов;

	ПерсонифицированныйУчет.СоздатьВТСведенияОбОрганизацияхПоОписаниюДокументаИсточникаДанных(Запрос.МенеджерВременныхТаблиц, ОписаниеИсточникаДанных);
	
	Запрос.Текст =
		"ВЫБРАТЬ
		|	СведенияОТрудовойДеятельностиРаботникаСТД_Р.Ссылка КАК Ссылка,
		|	СведенияОТрудовойДеятельностиРаботникаСТД_Р.Дата КАК Дата,
		|	СведенияОТрудовойДеятельностиРаботникаСТД_Р.Организация КАК Организация,
		|	СведенияОТрудовойДеятельностиРаботникаСТД_Р.ФизическоеЛицо КАК ФизическоеЛицо,
		|	СведенияОТрудовойДеятельностиРаботникаСТД_Р.Фамилия КАК Фамилия,
		|	СведенияОТрудовойДеятельностиРаботникаСТД_Р.Имя КАК Имя,
		|	СведенияОТрудовойДеятельностиРаботникаСТД_Р.Отчество КАК Отчество,
		|	СведенияОТрудовойДеятельностиРаботникаСТД_Р.ДатаРождения КАК ДатаРождения,
		|	СведенияОТрудовойДеятельностиРаботникаСТД_Р.СтраховойНомерПФР КАК СтраховойНомерПФР,
		|	СведенияОТрудовойДеятельностиРаботникаСТД_Р.ЗаявлениеОПродолженииДата КАК ЗаявлениеОПродолженииДата,
		|	СведенияОТрудовойДеятельностиРаботникаСТД_Р.ЗаявлениеОПредоставленииДата КАК ЗаявлениеОПредоставленииДата,
		|	СведенияОТрудовойДеятельностиРаботникаСТД_Р.РуководительКадровойСлужбы КАК РуководительКадровойСлужбы,
		|	СведенияОТрудовойДеятельностиРаботникаСТД_Р.ДолжностьРуководителяКадровойСлужбы КАК ДолжностьРуководителяКадровойСлужбы,
		|	СведенияОТрудовойДеятельностиРаботникаСТД_Р.ОснованиеПодписиРуководителяКадровойСлужбы КАК ОснованиеПодписиРуководителяКадровойСлужбы
		|ПОМЕСТИТЬ ВТДанныеДокумента
		|ИЗ
		|	Документ.СведенияОТрудовойДеятельностиРаботникаСТД_Р КАК СведенияОТрудовойДеятельностиРаботникаСТД_Р
		|ГДЕ
		|	СведенияОТрудовойДеятельностиРаботникаСТД_Р.Ссылка В(&МассивОбъектов)";
	
	Запрос.Выполнить();
	
	ЗарплатаКадры.СоздатьВТФИООтветственныхЛиц(Запрос.МенеджерВременныхТаблиц, Истина, "РуководительКадровойСлужбы", "ВТДанныеДокумента");
	
	Запрос.Текст =
		"ВЫБРАТЬ
		|	МероприятияСТД_Р.ИдМероприятия КАК ИдМероприятия,
		|	МероприятияСТД_Р.Отменено КАК Отменено
		|ПОМЕСТИТЬ ВТМероприятияДокумента
		|ИЗ
		|	Документ.СведенияОТрудовойДеятельностиРаботникаСТД_Р.Мероприятия КАК МероприятияСТД_Р
		|ГДЕ
		|	МероприятияСТД_Р.Ссылка В(&МассивОбъектов)";
	
	Запрос.Выполнить();
	
	ЗапросДанныхМероприятий = ЗапросВТДанныеМероприятий();
	ЗапросДанныхМероприятий.МенеджерВременныхТаблиц = Запрос.МенеджерВременныхТаблиц;
	ЗапросДанныхМероприятий.Выполнить();
	
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ДанныеДокумента.Ссылка КАК Ссылка,
		|	МероприятияСТД_Р.НомерСтроки КАК НомерСтроки,
		|	ДанныеМероприятий.ДатаМероприятия КАК ДатаМероприятия,
		|	ДанныеМероприятий.ВидМероприятия КАК ВидМероприятия,
		|	ДанныеМероприятий.ЯвляетсяСовместителем КАК ЯвляетсяСовместителем,
		|	ДанныеМероприятий.Подразделение КАК Подразделение,
		|	ДанныеМероприятий.Должность КАК Должность,
		|	ДанныеМероприятий.РазрядКатегория КАК РазрядКатегория,
		|	ДанныеМероприятий.ОснованиеУвольнения КАК ОснованиеУвольнения,
		|	ДанныеМероприятий.НаименованиеДокументаОснования КАК НаименованиеДокументаОснования,
		|	ДанныеМероприятий.ДатаДокументаОснования КАК ДатаДокументаОснования,
		|	ДанныеМероприятий.НомерДокументаОснования КАК НомерДокументаОснования,
		|	ДанныеМероприятий.КодПоРееструДолжностей КАК КодПоРееструДолжностей,
		|	ДанныеМероприятий.СерияДокументаОснования КАК СерияДокументаОснования,
		|	ДанныеМероприятий.ДатаС КАК ДатаС,
		|	ДанныеМероприятий.ДатаПо КАК ДатаПо,
		|	ДанныеМероприятий.ДатаОтмены КАК ДатаОтмены,
		|	ДанныеДокумента.Дата КАК Дата,
		|	ДанныеДокумента.Организация КАК Организация,
		|	ДанныеДокумента.ФизическоеЛицо КАК ФизическоеЛицо,
		|	ДанныеДокумента.Фамилия КАК Фамилия,
		|	ДанныеДокумента.Имя КАК Имя,
		|	ДанныеДокумента.Отчество КАК Отчество,
		|	ДанныеДокумента.ДатаРождения КАК ДатаРождения,
		|	ДанныеДокумента.СтраховойНомерПФР КАК СтраховойНомерПФР,
		|	ДанныеДокумента.ЗаявлениеОПродолженииДата КАК ЗаявлениеОПродолженииДата,
		|	ДанныеДокумента.ЗаявлениеОПредоставленииДата КАК ЗаявлениеОПредоставленииДата,
		|	ДанныеДокумента.РуководительКадровойСлужбы КАК РуководительКадровойСлужбы,
		|	ДанныеДокумента.ДолжностьРуководителяКадровойСлужбы КАК ДолжностьРуководителяКадровойСлужбы,
		|	ДанныеДокумента.ОснованиеПодписиРуководителяКадровойСлужбы КАК ОснованиеПодписиРуководителяКадровойСлужбы,
		|	ФИООтветственныхЛиц.РасшифровкаПодписи КАК РуководительКадровойСлужбыРасшифровкаПодписи,
		|	СведенияОбОрганизациях.НаименованиеСокращенное КАК ОрганизацияНаименованиеСокращенное,
		|	СведенияОбОрганизациях.НаименованиеПолное КАК ОрганизацияНаименованиеПолное,
		|	СведенияОбОрганизациях.РегистрационныйНомерПФР КАК РегистрационныйНомерПФР,
		|	СведенияОбОрганизациях.ИНН КАК ИНН,
		|	СведенияОбОрганизациях.КПП КАК КПП,
		|	ВЫБОР
		|		КОГДА МероприятияСТД_Р.Отменено
		|			ТОГДА ""Х""
		|		ИНАЧЕ """"
		|	КОНЕЦ КАК ПризнакОтмены,
		|	ДанныеМероприятий.ТрудоваяФункцияОписание КАК ТрудоваяФункцияОписание,
		|	ДанныеМероприятий.ТрудоваяФункцияКод КАК ТрудоваяФункцияКод,
		|	ДанныеМероприятий.Сведения КАК Сведения,
		|	ДанныеМероприятий.ПредставлениеДолжности КАК ПредставлениеДолжности,
		|	ДанныеМероприятий.ПредставлениеПодразделения КАК ПредставлениеПодразделения,
		|	ДанныеМероприятий.ОснованиеУвольненияТекстОснования КАК ОснованиеУвольненияТекстОснования,
		|	ДанныеМероприятий.ОснованиеУвольненияСтатья КАК ОснованиеУвольненияСтатья,
		|	ДанныеМероприятий.ОснованиеУвольненияЧасть КАК ОснованиеУвольненияЧасть,
		|	ДанныеМероприятий.ОснованиеУвольненияПункт КАК ОснованиеУвольненияПункт,
		|	ДанныеМероприятий.ОснованиеУвольненияПодПункт КАК ОснованиеУвольненияПодПункт,
		|	ДанныеМероприятий.ОписаниеДолжности КАК ОписаниеДолжности,
		|	ДанныеМероприятий.Передано КАК Передано
		|ИЗ
		|	ВТДанныеДокумента КАК ДанныеДокумента
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.СведенияОТрудовойДеятельностиРаботникаСТД_Р.Мероприятия КАК МероприятияСТД_Р
		|			ЛЕВОЕ СОЕДИНЕНИЕ ВТДанныеМероприятий КАК ДанныеМероприятий
		|			ПО МероприятияСТД_Р.ИдМероприятия = ДанныеМероприятий.ИдМероприятия
		|				И МероприятияСТД_Р.Отменено = ДанныеМероприятий.Отменено
		|		ПО ДанныеДокумента.Ссылка = МероприятияСТД_Р.Ссылка
		|		ЛЕВОЕ СОЕДИНЕНИЕ ВТСведенияОбОрганизациях КАК СведенияОбОрганизациях
		|		ПО ДанныеДокумента.Организация = СведенияОбОрганизациях.Организация
		|			И ДанныеДокумента.Дата = СведенияОбОрганизациях.Период
		|		ЛЕВОЕ СОЕДИНЕНИЕ ВТФИООтветственныхЛиц КАК ФИООтветственныхЛиц
		|		ПО ДанныеДокумента.РуководительКадровойСлужбы = ФИООтветственныхЛиц.ФизическоеЛицо
		|			И ДанныеДокумента.Дата = ФИООтветственныхЛиц.Дата
		|
		|УПОРЯДОЧИТЬ ПО
		|	Ссылка,
		|	НомерСтроки";
	
	Выборка = Запрос.Выполнить().Выбрать();
	Возврат ЭлектронныеТрудовыеКнижки.ВывестиМакетыДокументов(Выборка, "Документ.СведенияОТрудовойДеятельностиРаботникаСТД_Р.ПФ_MXL_СТД_Р", ОбъектыПечати);
	
КонецФункции

Функция ЗапросВТДанныеМероприятий(ИмяВТДанныеМероприятий = "ВТДанныеМероприятий") Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	МероприятияДокумента.ИдМероприятия КАК ИдМероприятия,
		|	МероприятияДокумента.Отменено КАК Отменено,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.ФизическоеЛицо, МероприятияТрудовойДеятельностиПрочие.ФизическоеЛицо) КАК ФизическоеЛицо,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.Организация, МероприятияТрудовойДеятельностиПрочие.Организация) КАК Организация,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.Сотрудник, МероприятияТрудовойДеятельностиПрочие.Сотрудник) КАК СотрудникЗаписи,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.ДатаМероприятия, МероприятияТрудовойДеятельностиПрочие.ДатаМероприятия) КАК ДатаМероприятия,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.ВидМероприятия, МероприятияТрудовойДеятельностиПрочие.ВидМероприятия) КАК ВидМероприятия,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельностиПереданные.Сведения, ЕСТЬNULL(МероприятияТрудовойДеятельности.Сведения, МероприятияТрудовойДеятельностиПрочие.Сведения)) КАК Сведения,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.Подразделение, МероприятияТрудовойДеятельностиПрочие.Подразделение) КАК Подразделение,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.Должность, МероприятияТрудовойДеятельностиПрочие.Должность) КАК Должность,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.РазрядКатегория, МероприятияТрудовойДеятельностиПрочие.РазрядКатегория) КАК РазрядКатегория,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.КодПоРееструДолжностей, МероприятияТрудовойДеятельностиПрочие.КодПоРееструДолжностей) КАК КодПоРееструДолжностей,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.ТрудоваяФункция, МероприятияТрудовойДеятельностиПрочие.ТрудоваяФункция) КАК ТрудоваяФункция,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.НаименованиеДокументаОснования, МероприятияТрудовойДеятельностиПрочие.НаименованиеДокументаОснования) КАК НаименованиеДокументаОснования,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.ДатаДокументаОснования, МероприятияТрудовойДеятельностиПрочие.ДатаДокументаОснования) КАК ДатаДокументаОснования,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.СерияДокументаОснования, МероприятияТрудовойДеятельностиПрочие.СерияДокументаОснования) КАК СерияДокументаОснования,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.НомерДокументаОснования, МероприятияТрудовойДеятельностиПрочие.НомерДокументаОснования) КАК НомерДокументаОснования,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.ОснованиеУвольнения, МероприятияТрудовойДеятельностиПрочие.ОснованиеУвольнения) КАК ОснованиеУвольнения,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.ДатаС, МероприятияТрудовойДеятельностиПрочие.ДатаС) КАК ДатаС,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.ДатаПо, МероприятияТрудовойДеятельностиПрочие.ДатаПо) КАК ДатаПо,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.ДатаОтмены, МероприятияТрудовойДеятельностиПрочие.ДатаОтмены) КАК ДатаОтмены,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.ЯвляетсяСовместителем, МероприятияТрудовойДеятельностиПрочие.ЯвляетсяСовместителем) КАК ЯвляетсяСовместителем,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельностиПереданные.ПредставлениеДолжности, ЕСТЬNULL(МероприятияТрудовойДеятельности.ПредставлениеДолжности, МероприятияТрудовойДеятельностиПрочие.ПредставлениеДолжности)) КАК ПредставлениеДолжности,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельностиПереданные.ПредставлениеПодразделения, ЕСТЬNULL(МероприятияТрудовойДеятельности.ПредставлениеПодразделения, МероприятияТрудовойДеятельностиПрочие.ПредставлениеПодразделения)) КАК ПредставлениеПодразделения,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельностиПереданные.ОснованиеУвольненияТекстОснования, ЕСТЬNULL(МероприятияТрудовойДеятельности.ОснованиеУвольненияТекстОснования, МероприятияТрудовойДеятельностиПрочие.ОснованиеУвольненияТекстОснования)) КАК ОснованиеУвольненияТекстОснования,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельностиПереданные.ОснованиеУвольненияСтатья, ЕСТЬNULL(МероприятияТрудовойДеятельности.ОснованиеУвольненияСтатья, МероприятияТрудовойДеятельностиПрочие.ОснованиеУвольненияСтатья)) КАК ОснованиеУвольненияСтатья,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельностиПереданные.ОснованиеУвольненияЧасть, ЕСТЬNULL(МероприятияТрудовойДеятельности.ОснованиеУвольненияЧасть, МероприятияТрудовойДеятельностиПрочие.ОснованиеУвольненияЧасть)) КАК ОснованиеУвольненияЧасть,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельностиПереданные.ОснованиеУвольненияПункт, ЕСТЬNULL(МероприятияТрудовойДеятельности.ОснованиеУвольненияПункт, МероприятияТрудовойДеятельностиПрочие.ОснованиеУвольненияПункт)) КАК ОснованиеУвольненияПункт,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельностиПереданные.ОснованиеУвольненияПодПункт, ЕСТЬNULL(МероприятияТрудовойДеятельности.ОснованиеУвольненияПодПункт, МероприятияТрудовойДеятельностиПрочие.ОснованиеУвольненияПодПункт)) КАК ОснованиеУвольненияПодПункт,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельностиПереданные.ОписаниеДолжности, ЕСТЬNULL(МероприятияТрудовойДеятельности.ОписаниеДолжности, МероприятияТрудовойДеятельностиПрочие.ОписаниеДолжности)) КАК ОписаниеДолжности,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельностиПереданные.ТрудоваяФункцияОписание, ВЫБОР
		|			КОГДА ЕСТЬNULL(МероприятияТрудовойДеятельности.ТрудоваяФункция.Описание, МероприятияТрудовойДеятельностиПрочие.ТрудоваяФункция.Описание) = """"
		|				ТОГДА ЕСТЬNULL(МероприятияТрудовойДеятельности.ТрудоваяФункция.Наименование, МероприятияТрудовойДеятельностиПрочие.ТрудоваяФункция.Наименование)
		|			ИНАЧЕ ЕСТЬNULL(МероприятияТрудовойДеятельности.ТрудоваяФункция.Описание, МероприятияТрудовойДеятельностиПрочие.ТрудоваяФункция.Описание)
		|		КОНЕЦ) КАК ТрудоваяФункцияОписание,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельностиПереданные.ТрудоваяФункцияКод, ЕСТЬNULL(МероприятияТрудовойДеятельности.ТрудоваяФункция.КодПрофессиональнойДеятельности, МероприятияТрудовойДеятельностиПрочие.ТрудоваяФункция.КодПрофессиональнойДеятельности)) КАК ТрудоваяФункцияКод,
		|	ВЫБОР
		|		КОГДА МероприятияТрудовойДеятельностиПринятые.ИдМероприятия ЕСТЬ NULL
		|			ТОГДА ЛОЖЬ
		|		ИНАЧЕ МероприятияТрудовойДеятельностиПринятые.ПринятоВПФР
		|	КОНЕЦ КАК Передано
		|ПОМЕСТИТЬ ВТДанныеМероприятий
		|ИЗ
		|	ВТМероприятияДокумента КАК МероприятияДокумента
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.МероприятияТрудовойДеятельности КАК МероприятияТрудовойДеятельности
		|		ПО МероприятияДокумента.ИдМероприятия = МероприятияТрудовойДеятельности.ИдМероприятия
		|			И МероприятияДокумента.Отменено = МероприятияТрудовойДеятельности.Отменено
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.МероприятияТрудовойДеятельностиПрочие КАК МероприятияТрудовойДеятельностиПрочие
		|		ПО МероприятияДокумента.ИдМероприятия = МероприятияТрудовойДеятельностиПрочие.ИдМероприятия
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.МероприятияТрудовойДеятельностиПереданные КАК МероприятияТрудовойДеятельностиПринятые
		|		ПО МероприятияДокумента.ИдМероприятия = МероприятияТрудовойДеятельностиПринятые.ИдМероприятия
		|			И МероприятияДокумента.Отменено = МероприятияТрудовойДеятельностиПринятые.Отменено
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.МероприятияТрудовойДеятельностиПереданные КАК МероприятияТрудовойДеятельностиПереданные
		|		ПО МероприятияДокумента.ИдМероприятия = МероприятияТрудовойДеятельностиПереданные.ИдМероприятия
		|			И (НЕ МероприятияТрудовойДеятельностиПереданные.Отменено)";
	
	ЗарплатаКадрыОбщиеНаборыДанных.ЗаменитьИмяСоздаваемойВременнойТаблицы(Запрос.Текст, "ВТДанныеМероприятий", ИмяВТДанныеМероприятий);
	
	Возврат Запрос;
	
КонецФункции

Процедура ЗаполнитьДокумент(ОбъектДокумента) Экспорт
	
	Если ЗначениеЗаполнено(ОбъектДокумента.ФизическоеЛицо) Тогда
		
		Запрос = Новый Запрос;
		Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
		
		Если ЗначениеЗаполнено(ОбъектДокумента.Дата) Тогда
			ДатаЗаполнения = ОбъектДокумента.Дата;
		Иначе
			ДатаЗаполнения = ТекущаяДатаСеанса();
		КонецЕсли;
		
		ПараметрыПолучения = КадровыйУчет.ПараметрыПолученияСотрудниковОрганизацийПоСпискуФизическихЛиц();
		ПараметрыПолучения.Организация = ОбъектДокумента.Организация;
		ПараметрыПолучения.ОкончаниеПериода = ДатаЗаполнения;
		ПараметрыПолучения.СписокФизическихЛиц = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(ОбъектДокумента.ФизическоеЛицо);
		ПараметрыПолучения.КадровыеДанные = "ДатаПриема,ДатаУвольнения,Фамилия,Имя,Отчество,СтраховойНомерПФР,ДатаРождения,ВидЗанятости,"
			+ "ЗаявленияОПредоставленииСведенийОТрудовойДеятельностиДата,ЗаявленияОПредоставленииСведенийОТрудовойДеятельностиВид";
		
		КадровыйУчет.СоздатьВТСотрудникиОрганизации(
			Запрос.МенеджерВременныхТаблиц,
			Истина,
			ПараметрыПолучения,
			"ВТКадровыеДанныеСотрудников");
		
		Запрос.Текст =
			"ВЫБРАТЬ ПЕРВЫЕ 1
			|	КадровыеДанные.Фамилия КАК Фамилия,
			|	КадровыеДанные.Имя КАК Имя,
			|	КадровыеДанные.Отчество КАК Отчество,
			|	КадровыеДанные.СтраховойНомерПФР КАК СтраховойНомерПФР,
			|	КадровыеДанные.ДатаРождения КАК ДатаРождения,
			|	КадровыеДанные.ДатаУвольнения КАК ДатаУвольнения,
			|	ВЫБОР
			|		КОГДА КадровыеДанные.ЗаявленияОПредоставленииСведенийОТрудовойДеятельностиВид = ЗНАЧЕНИЕ(Перечисление.ВидыЗаявленийОПредоставленииСведенийОТрудовойДеятельности.ВедениеБумажнойТрудовойКнижки)
			|			ТОГДА КадровыеДанные.ЗаявленияОПредоставленииСведенийОТрудовойДеятельностиДата
			|		ИНАЧЕ ДАТАВРЕМЯ(1, 1, 1)
			|	КОНЕЦ КАК ЗаявлениеОПродолженииДата,
			|	ВЫБОР
			|		КОГДА КадровыеДанные.ЗаявленияОПредоставленииСведенийОТрудовойДеятельностиВид = ЗНАЧЕНИЕ(Перечисление.ВидыЗаявленийОПредоставленииСведенийОТрудовойДеятельности.СведенияОТрудовойДеятельностиВЭлектроннойФорме)
			|			ТОГДА КадровыеДанные.ЗаявленияОПредоставленииСведенийОТрудовойДеятельностиДата
			|		ИНАЧЕ ДАТАВРЕМЯ(1, 1, 1)
			|	КОНЕЦ КАК ЗаявлениеОПредоставленииДата
			|ИЗ
			|	ВТКадровыеДанныеСотрудников КАК КадровыеДанные
			|ГДЕ
			|	КадровыеДанные.ВидЗанятости В (ЗНАЧЕНИЕ(Перечисление.ВидыЗанятости.ОсновноеМестоРаботы), ЗНАЧЕНИЕ(Перечисление.ВидыЗанятости.Совместительство))";
		
		СотрудникУволен = Ложь;
		Выборка = Запрос.Выполнить().Выбрать();
		Если Выборка.Следующий() Тогда
			ЗаполнитьЗначенияСвойств(ОбъектДокумента, Выборка);
			Если ЗначениеЗаполнено(Выборка.ДатаУвольнения) Тогда
				СотрудникУволен = Истина;
			КонецЕсли;
		КонецЕсли;
		
		Запрос.УстановитьПараметр("Организация", ОбъектДокумента.Организация);
		Запрос.Текст =
			"ВЫБРАТЬ
			|	&Организация КАК Организация,
			|	КадровыеДанныеСотрудников.Сотрудник КАК Сотрудник,
			|	КадровыеДанныеСотрудников.ФизическоеЛицо КАК ФизическоеЛицо,
			|	КадровыеДанныеСотрудников.ДатаПриема КАК НачалоПериода,
			|	ВЫБОР
			|		КОГДА КадровыеДанныеСотрудников.ДатаУвольнения = ДАТАВРЕМЯ(1, 1, 1)
			|			ТОГДА КадровыеДанныеСотрудников.Период
			|		КОГДА КадровыеДанныеСотрудников.ДатаУвольнения > КадровыеДанныеСотрудников.Период
			|			ТОГДА КадровыеДанныеСотрудников.Период
			|		ИНАЧЕ КадровыеДанныеСотрудников.ДатаУвольнения
			|	КОНЕЦ КАК ОкончаниеПериода
			|ПОМЕСТИТЬ ВТСотрудникиПериодыДанных
			|ИЗ
			|	ВТКадровыеДанныеСотрудников КАК КадровыеДанныеСотрудников";
		
		Запрос.Выполнить();
		
		ПереданныеМероприятия = ЭлектронныеТрудовыеКнижки.МероприятияТрудовойДеятельностиСТД_Р(
			Запрос.МенеджерВременныхТаблиц, Истина, Не СотрудникУволен);
		
		ОбъектДокумента.Мероприятия.Загрузить(ПереданныеМероприятия);
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ЗаполнитьИдМероприятий(ПараметрыОбновления = Неопределено) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("ПустойИдентификатор", Новый УникальныйИдентификатор("00000000-0000-0000-0000-000000000000"));
	
	Если ПараметрыОбновления = Неопределено Тогда
		МассивОбновленных = Новый Массив;
	Иначе
		
		Если ПараметрыОбновления.Свойство("МассивОбновленных") Тогда
			МассивОбновленных = ПараметрыОбновления.МассивОбновленных;
		Иначе
			МассивОбновленных = Новый Массив;
			ПараметрыОбновления.Вставить("МассивОбновленных", МассивОбновленных);
		КонецЕсли;
		
	КонецЕсли;
	
	Запрос.УстановитьПараметр("МассивОбновленных", МассивОбновленных);
	
	Запрос.Текст =
		"ВЫБРАТЬ РАЗЛИЧНЫЕ ПЕРВЫЕ 1000
		|	СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.Ссылка КАК Ссылка
		|ПОМЕСТИТЬ ВТРегистраторы
		|ИЗ
		|	Документ.СведенияОТрудовойДеятельностиРаботникаСТД_Р.Мероприятия КАК СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия
		|ГДЕ
		|	НЕ СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.Ссылка В (&МассивОбновленных)
		|	И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.ИдМероприятия = &ПустойИдентификатор
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ ПЕРВЫЕ 1
		|	Регистраторы.Ссылка КАК Ссылка
		|ИЗ
		|	ВТРегистраторы КАК Регистраторы";
	
	Если ПараметрыОбновления = Неопределено Тогда
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "ПЕРВЫЕ 1000","");
	КонецЕсли;
	
	РезультатЗапроса = Запрос.Выполнить();
	Если РезультатЗапроса.Пустой() Тогда
		ОбновлениеИнформационнойБазыЗарплатаКадрыБазовый.УстановитьПараметрОбновления(ПараметрыОбновления, "ОбработкаЗавершена", Истина);
		Возврат;
	КонецЕсли;
	
	ОбновлениеИнформационнойБазыЗарплатаКадрыБазовый.УстановитьПараметрОбновления(ПараметрыОбновления, "ОбработкаЗавершена", Ложь);
	Запрос.Текст =
		"ВЫБРАТЬ
		|	Регистраторы.Ссылка КАК Ссылка,
		|	СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.НомерСтроки КАК НомерСтроки,
		|	ЕСТЬNULL(МероприятияТрудовойДеятельности.ИдМероприятия, МероприятияТрудовойДеятельностиПрочие.ИдМероприятия) КАК ИдМероприятия
		|ИЗ
		|	ВТРегистраторы КАК Регистраторы
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.СведенияОТрудовойДеятельностиРаботникаСТД_Р.Мероприятия КАК СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия
		|			ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.МероприятияТрудовойДеятельности КАК МероприятияТрудовойДеятельности
		|			ПО СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.Ссылка.Организация = МероприятияТрудовойДеятельности.Организация
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.Ссылка.ФизическоеЛицо = МероприятияТрудовойДеятельности.ФизическоеЛицо
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.УдалитьДатаМероприятия = МероприятияТрудовойДеятельности.ДатаМероприятия
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.УдалитьВидМероприятия = МероприятияТрудовойДеятельности.ВидМероприятия
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.УдалитьПодразделение = МероприятияТрудовойДеятельности.Подразделение
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.УдалитьДолжность = МероприятияТрудовойДеятельности.Должность
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.УдалитьТрудоваяФункция = МероприятияТрудовойДеятельности.ТрудоваяФункция
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.УдалитьЯвляетсяСовместителем = МероприятияТрудовойДеятельности.ЯвляетсяСовместителем
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.УдалитьОснованиеУвольнения = МероприятияТрудовойДеятельности.ОснованиеУвольнения
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.УдалитьДатаОтмены = МероприятияТрудовойДеятельности.ДатаОтмены
		|			ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.МероприятияТрудовойДеятельностиПрочие КАК МероприятияТрудовойДеятельностиПрочие
		|			ПО СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.Ссылка.Организация = МероприятияТрудовойДеятельностиПрочие.Организация
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.Ссылка.ФизическоеЛицо = МероприятияТрудовойДеятельностиПрочие.ФизическоеЛицо
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.УдалитьДатаМероприятия = МероприятияТрудовойДеятельностиПрочие.ДатаМероприятия
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.УдалитьВидМероприятия = МероприятияТрудовойДеятельностиПрочие.ВидМероприятия
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.УдалитьПодразделение = МероприятияТрудовойДеятельностиПрочие.Подразделение
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.УдалитьДолжность = МероприятияТрудовойДеятельностиПрочие.Должность
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.УдалитьТрудоваяФункция = МероприятияТрудовойДеятельностиПрочие.ТрудоваяФункция
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.УдалитьЯвляетсяСовместителем = МероприятияТрудовойДеятельностиПрочие.ЯвляетсяСовместителем
		|				И СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.УдалитьОснованиеУвольнения = МероприятияТрудовойДеятельностиПрочие.ОснованиеУвольнения
		|		ПО Регистраторы.Ссылка = СведенияОТрудовойДеятельностиРаботникаСТД_РМероприятия.Ссылка
		|
		|УПОРЯДОЧИТЬ ПО
		|	Ссылка,
		|	НомерСтроки";
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.СледующийПоЗначениюПоля("Ссылка") Цикл
		
		МассивОбновленных.Добавить(Выборка.Ссылка);
		
		Если Не ОбновлениеИнформационнойБазыЗарплатаКадрыБазовый.ПодготовитьОбновлениеДанных(ПараметрыОбновления, "Документ.СведенияОТрудовойДеятельностиРаботникаСТД_Р", "Ссылка", Выборка.Ссылка) Тогда
			Продолжить;
		КонецЕсли;
		
		ЗаписатьИзменения = Ложь;
		
		ДокументОбъект = Выборка.Ссылка.ПолучитьОбъект();
		Пока Выборка.Следующий() Цикл
			
			Если ЗначениеЗаполнено(Выборка.ИдМероприятия) Тогда
				
				ЗаписатьИзменения = Истина;
				СтрокаМероприятия = ДокументОбъект.Мероприятия.Найти(Выборка.НомерСтроки, "НомерСтроки");
				Если СтрокаМероприятия <> Неопределено Тогда
					СтрокаМероприятия.ИдМероприятия = Выборка.ИдМероприятия;
				КонецЕсли;
				
			КонецЕсли;
			
		КонецЦикла;
		
		Если ЗаписатьИзменения Тогда
			
			ДокументОбъект.ДополнительныеСвойства.Вставить("ОтключитьПроверкуДатыЗапретаИзменения", Истина);
			ОбновлениеИнформационнойБазы.ЗаписатьОбъект(ДокументОбъект);
			
		КонецЕсли;
		
		ОбновлениеИнформационнойБазыЗарплатаКадрыБазовый.ЗавершитьОбновлениеДанных(ПараметрыОбновления);
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли

﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Если Документы.ВедомостьПрочихДоходовВБанк.ЭтоДанныеЗаполненияНезачисленнымиСтроками(ДанныеЗаполнения) Тогда
		ЗаполнитьНезачисленнымиСтроками(ДанныеЗаполнения)
	Иначе	
		ВедомостьПрочихДоходов.ОбработкаЗаполнения(ЭтотОбъект, ДанныеЗаполнения, СтандартнаяОбработка);
	КонецЕсли
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	Если НЕ ЗначениеЗаполнено(ЗарплатныйПроект) Тогда
		ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "СоставВыплаты.НомерЛицевогоСчета");
		ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "Состав.НомерЛицевогоСчета");
	КонецЕсли;
	
	ОбменСБанкамиПоЗарплатнымПроектамПереопределяемый.ОбработкаПроверкиЗаполнения(ЭтотОбъект, Отказ, ПроверяемыеРеквизиты);
	ВедомостьПрочихДоходов.ОбработкаПроверкиЗаполнения(ЭтотОбъект, Отказ, ПроверяемыеРеквизиты)	
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ЗарплатаКадры.ОтключитьБизнесЛогикуПриЗаписи(ЭтотОбъект) Тогда
		Возврат;
	КонецЕсли;
	
	ВедомостьПрочихДоходов.ПередЗаписью(ЭтотОбъект, Отказ, РежимЗаписи);
	ОбменСБанкамиПоЗарплатнымПроектамРасширенный.ВедомостьПрочихДоходовВБанкПередЗаписью(ЭтотОбъект);
	
КонецПроцедуры

Процедура ПриЗаписи(Отказ)
	
	Если ЗарплатаКадры.ОтключитьБизнесЛогикуПриЗаписи(ЭтотОбъект) Тогда
		Возврат;
	КонецЕсли;
	
	ОбменСБанкамиПоЗарплатнымПроектамРасширенный.ВедомостьПрочихДоходовВБанкПриЗаписи(ЭтотОбъект, Отказ);
	
КонецПроцедуры


Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	ВедомостьПрочихДоходов.ОбработкаПроведения(ЭтотОбъект, Отказ);
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)
	НомерРеестра = 0;
КонецПроцедуры

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает признак изменения данных, влияющих на формирование электронного документа.
// 
Функция ИзменилисьКлючевыеРеквизитыЭлектронногоДокумента() Экспорт
	
	ИзменилисьКлючевыеРеквизиты = 
		ЭлектронноеВзаимодействиеБЗК.ИзменилисьРеквизитыОбъекта(ЭтотОбъект, "Дата, Номер, Организация, ЗарплатныйПроект, НомерРеестра, ПометкаУдаления")	
		Или ЭлектронноеВзаимодействиеБЗК.ИзмениласьТабличнаяЧастьОбъекта(ЭтотОбъект, "Выплаты", "ФизическоеЛицо, КВыплате, НомерЛицевогоСчета");
		
	Возврат ИзменилисьКлючевыеРеквизиты;
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПроцедурыИФункцииЗаполненияДокумента

Функция МожноЗаполнитьВыплаты() Экспорт
	Возврат ВедомостьПрочихДоходов.МожноЗаполнитьВыплаты(ЭтотОбъект)
КонецФункции

Процедура ЗаполнитьВыплаты() Экспорт
	ВедомостьПрочихДоходов.ЗаполнитьВыплаты(ЭтотОбъект);
КонецПроцедуры

Процедура ДополнитьВыплаты(ФизическиеЛица) Экспорт
	ВедомостьПрочихДоходов.ДополнитьВыплаты(ЭтотОбъект, ФизическиеЛица);
КонецПроцедуры

#КонецОбласти

Процедура ЗаполнитьПоТаблицеВыплат(ТаблицаВыплат) Экспорт
	
	ДополнитьТаблицуВыплатНомерамиЛицевыхСчетов(ТаблицаВыплат);
	ВедомостьПрочихДоходов.ЗаполнитьПоТаблицеВыплат(ЭтотОбъект, ТаблицаВыплат);
	
КонецПроцедуры

Процедура ДополнитьПоТаблицеВыплат(ТаблицаВыплат) Экспорт
	
	ДополнитьТаблицуВыплатНомерамиЛицевыхСчетов(ТаблицаВыплат);
	ВедомостьПрочихДоходов.ДополнитьПоТаблицеВыплат(ЭтотОбъект, ТаблицаВыплат)
	
КонецПроцедуры

Процедура ДополнитьТаблицуВыплатНомерамиЛицевыхСчетов(ТаблицаВыплат)
	
	МетаданныеРеквизита = Метаданные().ТабличныеЧасти.Состав.Реквизиты.НомерЛицевогоСчета;
	ТаблицаВыплат.Колонки.Добавить(МетаданныеРеквизита.Имя, МетаданныеРеквизита.Тип);
	
	ЛицевыеСчетаФизическихЛиц = ОбменСБанкамиПоЗарплатнымПроектам.ЛицевыеСчетаФизическихЛиц(ТаблицаВыплат.ВыгрузитьКолонку("ФизическоеЛицо"), Истина, Организация, Дата, ЗарплатныйПроект);
	
	Для Каждого СтрокаВыплаты Из ТаблицаВыплат Цикл
		СтрокаЛС = ЛицевыеСчетаФизическихЛиц.Найти(СтрокаВыплаты.ФизическоеЛицо, "ФизическоеЛицо");
		Если СтрокаЛС = Неопределено Тогда
			СтрокаВыплаты.НомерЛицевогоСчета = "";
		Иначе
			СтрокаВыплаты.НомерЛицевогоСчета = СтрокаЛС.НомерЛицевогоСчета
		КонецЕсли
	КонецЦикла
	
КонецПроцедуры

// Заполняет документ на основании существующего,
// перенося в новый документ только указанных физических лиц.
// 
// Параметры:
//	Документ - исходный документ (объект или ссылка).
//  Физлица - массив физических лиц.
//
Процедура ЗаполнитьНезачисленнымиСтроками(ДанныеЗаполнения) Экспорт
	
	Реквизиты = Новый Массив;
	Для Каждого Реквизит Из ДанныеЗаполнения.Ведомость.Метаданные().Реквизиты Цикл
		Реквизиты.Добавить(Реквизит.Имя);
	КонецЦикла;
	
	Шапка = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ДанныеЗаполнения.Ведомость, Реквизиты);
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, Шапка, , "НомерРеестра");
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Ссылка", ДанныеЗаполнения.Ведомость.Ссылка);
	Запрос.УстановитьПараметр("Физлица", ДанныеЗаполнения.Физлица);
	
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ВедомостьВыплаты.ФизическоеЛицо КАК ФизическоеЛицо,
	|	ВедомостьВыплаты.СтатьяФинансирования КАК СтатьяФинансирования,
	|	ВедомостьВыплаты.СтатьяРасходов КАК СтатьяРасходов,
	|	ВедомостьВыплаты.ДокументОснование КАК ДокументОснование,
	|	ВедомостьВыплаты.КВыплате КАК КВыплате,
	|	ВедомостьВыплаты.НомерЛицевогоСчета КАК НомерЛицевогоСчета
	|ИЗ
	|	Документ.ВедомостьПрочихДоходовВБанк.Выплаты КАК ВедомостьВыплаты
	|ГДЕ
	|	ВедомостьВыплаты.Ссылка = &Ссылка
	|	И ВедомостьВыплаты.ФизическоеЛицо В(&Физлица)";
	
	ТаблицаВыплат = Запрос.Выполнить().Выгрузить();
	
	ВедомостьПрочихДоходов.ЗаполнитьПоТаблицеВыплат(ЭтотОбъект, ТаблицаВыплат);
	
КонецПроцедуры

Процедура ОчиститьСостав() Экспорт
	
	Состав.Очистить();
	Выплаты.Очистить();
	НДФЛ.Очистить();
	Основания.Очистить();
	
КонецПроцедуры	

#КонецОбласти

#Иначе
ВызватьИсключение НСтр("ru = 'Недопустимый вызов объекта на клиенте.'");
#КонецЕсли
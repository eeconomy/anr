﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Объект, ЭтотОбъект);
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		
		ПриЧтенииСозданииНаСервере();
		
	КонецЕсли;
	
	#Область СтандартныеПодсистемы
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);
	
	// ИнтеграцияС1СДокументооборотом
	ИнтеграцияС1СДокументооборот.ПриСозданииНаСервере(ЭтаФорма);
	// Конец ИнтеграцияС1СДокументооборотом
	
	// СтандартныеПодсистемы.ВерсионированиеОбъектов
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);
	// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКоманды.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
	ОбщегоНазначенияУТ.НастроитьПодключаемоеОборудование(ЭтаФорма);
	
	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
	
	#КонецОбласти

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	МенеджерОборудованияКлиент.НачатьПодключениеОборудованиеПриОткрытииФормы(Неопределено, ЭтаФорма, "СканерШтрихкода");
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиент.НачатьОбновлениеКоманд(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	МенеджерОборудованияКлиент.НачатьОтключениеОборудованиеПриЗакрытииФормы(Неопределено, ЭтаФорма);
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)

	// СтандартныеПодсистемы.УправлениеДоступом
	Если ОбщегоНазначения.ПодсистемаСуществует("СтандартныеПодсистемы.УправлениеДоступом") Тогда
		МодульУправлениеДоступом = ОбщегоНазначения.ОбщийМодуль("УправлениеДоступом");
		МодульУправлениеДоступом.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	КонецЕсли;
	// Конец СтандартныеПодсистемы.УправлениеДоступом
	
	// Обработчик механизма "ДатыЗапретаИзменения"
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	
	ПриЧтенииСозданииНаСервере();

	СобытияФорм.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	
	// СтандартныеПодсистемы.ПодключаемыеКоманды
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
	// Конец СтандартныеПодсистемы.ПодключаемыеКоманды
КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	
	СобытияФормКлиент.ПередЗаписью(ЭтотОбъект, Отказ, ПараметрыЗаписи);
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	МодификацияКонфигурацииПереопределяемый.ПередЗаписьюНаСервере(ЭтаФорма, Отказ, ТекущийОбъект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	Оповестить("Запись_ВозвратОСОтАрендатора2_4", ПараметрыЗаписи, Объект.Ссылка);
	
	МодификацияКонфигурацииКлиентПереопределяемый.ПослеЗаписи(ЭтаФорма, ПараметрыЗаписи);
	
	ОбщегоНазначенияУТКлиент.ВыполнитьДействияПослеЗаписи(ЭтаФорма, Объект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)

	// СтандартныеПодсистемы.УправлениеДоступом
	УправлениеДоступом.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	// Конец СтандартныеПодсистемы.УправлениеДоступом
	
	МодификацияКонфигурацииПереопределяемый.ПослеЗаписиНаСервере(ЭтаФорма, ТекущийОбъект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// ПодключаемоеОборудование
	Если Источник = "ПодключаемоеОборудование" И ВводДоступен() Тогда
		Если ИмяСобытия = "ScanData" И МенеджерОборудованияУТКлиент.ЕстьНеобработанноеСобытие() Тогда
			ОбработатьШтрихкоды(МенеджерОборудованияУТКлиент.ПреобразоватьДанныеСоСканераВСтруктуру(Параметр));
		КонецЕсли;
	КонецЕсли;
	// Конец ПодключаемоеОборудование
	
	СобытияФормКлиент.ОбработкаОповещения(ЭтотОбъект, ИмяСобытия, Параметр, Источник);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

#Область СтраницаОсновное

&НаКлиенте
Процедура ДокументНаОснованииПриИзменении(Элемент)
	
	Если Объект.ДокументНаОсновании Тогда
		ПараметрыОтбора = Новый Структура;
		ПараметрыОтбора.Вставить("Организация", Объект.Организация);
		ПараметрыОтбора.Вставить("Арендатор", Объект.Арендатор);
		ПараметрыОтбора.Вставить("Подразделение", Объект.Подразделение);
		ПараметрыОтбора.Вставить("Проведен", Истина);
		ПараметрыФормы = Новый Структура;
		ПараметрыФормы.Вставить("Отбор", ПараметрыОтбора);
		
		ОписаниеОповещения = Новый ОписаниеОповещения("ВыборДокументаОснования", ЭтаФорма);
		ОткрытьФорму("Документ.ПередачаОСАрендатору2_4.ФормаВыбора", ПараметрыФормы, ЭтаФорма, ЭтаФорма,,, ОписаниеОповещения);
	Иначе
		Объект.ДокументОснование = Неопределено;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ВыборДокументаОснования(Результат, ДополнительныеПараметры) Экспорт
	
	Если ЗначениеЗаполнено(Результат) Тогда
		Объект.ДокументОснование = Результат;
	Иначе
		Объект.ДокументНаОсновании = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура РасчетыМеждуОрганизациямиАрендаторПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы("РасчетыМеждуОрганизациямиАрендатор");
	
КонецПроцедуры

&НаКлиенте
Процедура ПодразделениеПриИзменении(Элемент)
	
	Если ПредыдущееЗначениеПодразделение = Объект.ПодразделениеПолучатель Тогда
		Объект.ПодразделениеПолучатель = Объект.Подразделение;
	КонецЕсли;
	
	ПредыдущееЗначениеПодразделение = Объект.Подразделение;
	
КонецПроцедуры

#КонецОбласти

#Область СтраницаУчетУАрендодателя

&НаКлиенте
Процедура АдресМестонахожденияПриИзменении(Элемент)
	
	ПриИзмененииПредставленияАдреса(Элемент);
	
КонецПроцедуры

&НаКлиенте
Процедура АдресМестонахожденияНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ВнеоборотныеАктивыКлиент.ОткрытьФормуВыбораАдресаИОбработатьРезультат(
		ЭтотОбъект,
		Элемент,
		Объект,, 
		СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура АдресМестонахожденияОчистка(Элемент, СтандартнаяОбработка)
	
	ПриИзмененииПредставленияАдреса(Элемент);
	
КонецПроцедуры

&НаКлиенте
Процедура НачислениеАмортизацииБУПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы("НачислятьАмортизацию", "БУ");
	
КонецПроцедуры

&НаКлиенте
Процедура НачислятьАмортизациюУУПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы("НачислятьАмортизацию", "УУ");
	
КонецПроцедуры

&НаКлиенте
Процедура СтатьяРасходовБУПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы("СтатьяРасходов", "БУ");
	
КонецПроцедуры

&НаКлиенте
Процедура СтатьяРасходовУУПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы("СтатьяРасходов", "УУ");
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыОС

&НаКлиенте
Процедура ОСОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	ВнеоборотныеАктивыКлиентСервер.ОбработкаВыбораЭлемента(Объект.ОС, "ОсновноеСредство", ВыбранноеЗначение);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗаписатьДокумент(Команда)
	
	ОбщегоНазначенияУТКлиент.Записать(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура Подобрать(Команда)
	
	ПараметрыПодбораОС = ВнеоборотныеАктивыКлиентСервер.ПараметрыПодбора(Элементы.ОСОсновноеСредство, ЭтаФорма);
	
	ОткрытьФорму("Справочник.ОбъектыЭксплуатации.ФормаВыбора", 
					ПараметрыПодбораОС, Элементы.ОС,,,,, 
					РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

#Область ШтрихкодыИТорговоеОборудование

&НаКлиенте
Процедура ЗагрузитьДанныеИзТСД(Команда)
	
	ВыполнитьЗагрузкуДанныеИзТСД();
	
КонецПроцедуры

#КонецОбласти

#Область УниверсальныеМеханизмы_Команды

// СтандартныеПодсистемы.ПодключаемыеКоманды
&НаКлиенте
Процедура Подключаемый_ВыполнитьКоманду(Команда)
	ПодключаемыеКомандыКлиент.ВыполнитьКоманду(ЭтотОбъект, Команда, Объект);
КонецПроцедуры

&НаСервере
Процедура Подключаемый_ВыполнитьКомандуНаСервере(Контекст, Результат) Экспорт
	ПодключаемыеКоманды.ВыполнитьКоманду(ЭтотОбъект, Контекст, Объект, Результат);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ОбновитьКоманды()
	ПодключаемыеКомандыКлиентСервер.ОбновитьКоманды(ЭтотОбъект, Объект);
КонецПроцедуры
// Конец СтандартныеПодсистемы.ПодключаемыеКоманды

#КонецОбласти

#Область СтандартныеПодсистемы_Команды

&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуИнтеграции(Команда)
	
	ИнтеграцияС1СДокументооборотКлиент.ВыполнитьПодключаемуюКомандуИнтеграции(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры

#КонецОбласти

&НаКлиенте
Процедура ПровестиИЗакрыть(Команда)
	
	ОбщегоНазначенияУТКлиент.ПровестиИЗакрыть(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ПровестиДокумент(Команда)
	
	ОбщегоНазначенияУТКлиент.Провести(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПриИзмененииРеквизитов

&НаКлиенте
Процедура ПриИзмененииПредставленияАдреса(Элемент)

	ВнеоборотныеАктивыКлиент.ПриИзмененииПредставленияАдреса(
		Элемент,
		Объект.АдресМестонахождения,
		Объект.АдресМестонахожденияЗначение);

КонецПроцедуры

#КонецОбласти

#Область ШтрихкодыИТорговоеОборудование

&НаСервере
Процедура ОбработатьШтрихкоды(Знач ДанныеШтрихкодов)
	
	ПараметрыПодбора = ВнеоборотныеАктивыКлиентСервер.ПараметрыПодбора(Элементы.ОСОсновноеСредство, ЭтаФорма);
	МассивОбъектов = ВнеоборотныеАктивы.НайтиОсновныеСредстваПоШтрихкодам(ДанныеШтрихкодов, ПараметрыПодбора);
	ВнеоборотныеАктивыКлиентСервер.ОбработкаВыбораЭлемента(Объект.ОС, "ОсновноеСредство", МассивОбъектов);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыполнитьЗагрузкуДанныеИзТСД()
	
	ОчиститьСообщения();
	
	МенеджерОборудованияКлиент.НачатьЗагрузкуДанныеИзТСД(
		Новый ОписаниеОповещения("ЗагрузитьИзТСДЗавершение", ЭтотОбъект),
		УникальныйИдентификатор);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗагрузитьИзТСДЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат.Результат Тогда
		ОбработатьШтрихкоды(Результат.ТаблицаТоваров);
	Иначе
		МенеджерОборудованияУТКлиент.СообщитьОбОшибке(Результат);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаКлиенте
Процедура Подключаемый_ЗакрытьФорму()
	
	Закрыть();
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	
	ПредыдущееЗначениеПодразделение = Объект.Подразделение;
	Элементы.РасчетыМеждуОрганизациямиАрендатор.ТолькоПросмотр = НЕ ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоОрганизаций");
	НастроитьЗависимыеЭлементыФормы();
	
КонецПроцедуры

&НаСервере
Процедура НастроитьЗависимыеЭлементыФормы(ИзмененныеРеквизиты = "", ВидУчета = Неопределено)

	// Процедура выполняется на сервере, т.к. форма начинает "мигать" при изменении видимости большого количества элементов.
	СтруктураИзмененныхРеквизитов = Новый Структура(ИзмененныеРеквизиты);
	ОбновитьВсе = СтруктураИзмененныхРеквизитов.Количество() = 0;
	
	// Управление видимостью, доступностью и т.п.
	ПараметрыРеквизитовОбъекта = Документы.ВозвратОСОтАрендатора2_4.ЗначенияСвойствЗависимыхРеквизитов(Объект, ИзмененныеРеквизиты, ВидУчета);
	ВнеоборотныеАктивыКлиентСервер.НастроитьЗависимыеЭлементыФормы(ЭтаФорма, ПараметрыРеквизитовОбъекта);

	Если СтруктураИзмененныхРеквизитов.Свойство("СтатьяРасходов") Тогда
		
		ДоходыИРасходыСервер.СтатьяРасходовПриИзменении(Объект, Объект["СтатьяРасходов" + ВидУчета], Объект["АналитикаРасходов" + ВидУчета]);
		ЭтаФорма["АналитикаРасходов" + ВидУчета + "Обязательна"] =
			Объект["НачислятьАмортизацию" + ВидУчета]
			И ЗначениеЗаполнено(Объект["СтатьяРасходов" + ВидУчета])
			И ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Объект["СтатьяРасходов" + ВидУчета], "КонтролироватьЗаполнениеАналитики");
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	УсловноеОформление.Элементы.Очистить();
	
	РеквизитыРасходов = "СтатьяРасходовБУ,АналитикаРасходовБУ,СтатьяРасходовУУ,АналитикаРасходовУУ";
	ПланыВидовХарактеристик.СтатьиРасходов.УстановитьУсловноеОформлениеАналитик(УсловноеОформление,РеквизитыРасходов);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
#Область ПрограммныйИнтерфейс

// СтандартныеПодсистемы.ВерсионированиеОбъектов

// Определяет настройки объекта для подсистемы ВерсионированиеОбъектов.
//
// Параметры:
//  Настройки - Структура - настройки подсистемы.
Процедура ПриОпределенииНастроекВерсионированияОбъектов(Настройки) Экспорт

КонецПроцедуры

// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов

#Область ДляВызоваИзДругихПодсистем

// СтандартныеПодсистемы.УправлениеДоступом

// См. УправлениеДоступомПереопределяемый.ПриЗаполненииСписковСОграничениемДоступа.
Процедура ПриЗаполненииОграниченияДоступа(Ограничение) Экспорт
	Ограничение.Текст =
	"РазрешитьЧтениеИзменение
	|ГДЕ
	|	ЗначениеРазрешено(Организация)
	|	И ЗначениеРазрешено(Сотрудник)";
КонецПроцедуры

// Конец СтандартныеПодсистемы.УправлениеДоступом

#КонецОбласти

#КонецОбласти

	
#Область СлужебныйПрограммныйИнтерфейс

// Возвращает описание состава документа
//
// Возвращаемое значение:
//  Структура - см. ЗарплатаКадрыСоставДокументов.НовоеОписаниеСоставаДокумента
Функция ОписаниеСоставаОбъекта() Экспорт
	
	ОписаниеСостава = ЗарплатаКадрыСоставДокументов.НовоеОписаниеСоставаОбъекта();
	ОписаниеСостава.ЗаполнятьТабличнуюЧастьФизическиеЛицаДокумента = Ложь;
	ОписаниеСостава.ЗаполнятьФизическиеЛицаПоСотрудникам = Ложь;
	ОписаниеСостава.ИспользоватьКраткийСостав = Ложь;
		
	ЗарплатаКадрыСоставДокументов.ДобавитьОписаниеХраненияСотрудниковФизическихЛиц(
		ОписаниеСостава.ОписаниеХраненияСотрудниковФизическихЛиц,
		,
		"Сотрудник");
	
	Возврат ОписаниеСостава;
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ПроверитьДанныеФизическогоЛица(Ссылка, ПроверяемыеДанные, ДатаДокумента, ПутьКДаннымФизическогоЛица, НомерСтроки = Неопределено, Отказ = Ложь) Экспорт
	
	Ошибки = Новый Соответствие;
	
	ПравилаПроверки = ПравилаПроверкиДанныхФизическогоЛица(ПроверяемыеДанные.Гражданство, ДатаДокумента);
	
	ДанныеФизическогоЛицаДляПроверки = ДанныеФизическогоЛицаДляПроверки(ПроверяемыеДанные);
	
	ФизическиеЛицаЗарплатаКадры.ПроверитьДанныеФизическогоЛица(
		ДанныеФизическогоЛицаДляПроверки,
		ПравилаПроверки,
		Ошибки);
				
	Если НомерСтроки <> Неопределено Тогда
		ПутьКДанным = ПутьКДаннымФизическогоЛица + "[" + Формат(НомерСтроки - 1, "ЧН=0; ЧГ=") + "].";
	ИначеЕсли Не ПустаяСтрока(ПутьКДаннымФизическогоЛица) Тогда
		ПутьКДанным = ПутьКДаннымФизическогоЛица + ".";	
	Иначе
		ПутьКДанным = ПутьКДаннымФизическогоЛица;	
	КонецЕсли;		
	
	Для Каждого ОшибкиПоФизЛицу Из Ошибки Цикл
		Для Каждого ОшибкаДанныхФизическогоЛица Из ОшибкиПоФизЛицу.Значение Цикл			
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			ОшибкаДанныхФизическогоЛица.ТекстОшибки,
			Ссылка,
			ПутьКДанным + ОшибкаДанныхФизическогоЛица.ПолеФормы,,
			Отказ);
		КонецЦикла;		
	КонецЦикла;	
	
	Если Не ЗначениеЗаполнено(ПроверяемыеДанные.СтатусНалогоплательщика) Тогда
		ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
						НСтр("ru = 'Сотрудник %1: не заполнено поле ""Статус налогоплательщика""'"),
						ПроверяемыеДанные.СотрудникНаименование);
						
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			ТекстОшибки,
			Ссылка,
			ПутьКДанным + "СтатусНалогоплательщика",,
			Отказ);
					
	КонецЕсли;	
	
	//Если ПроверяемыеДанные.Гражданство <> Справочники.СтраныМира.Беларусь
	//	И Не ЗначениеЗаполнено(ПроверяемыеДанные.Адрес)
	//	И Не ЗначениеЗаполнено(ПроверяемыеДанные.АдресЗаРубежом) Тогда
	//	
	//	ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
	//					НСтр("ru = 'Сотрудник %1: для физического лица нерезидента или иностранца должен быть заполнен ""Адрес зарубежом""'"),
	//					ПроверяемыеДанные.СотрудникНаименование);

	//	ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
	//		ТекстОшибки,
	//		Ссылка,
	//		ПутьКДанным + "АдресЗаРубежом",,
	//		Отказ);
	//КонецЕсли;
		
КонецПроцедуры

Функция ПравилаПроверкиДанныхФизическогоЛица(Гражданство, ДатаДокумента)
	Правила = Новый Массив;
	
	//ФизическиеЛицаЗарплатаКадры.ДобавитьПравилоПроверки(
	//	Правила,
	//	"ИНН",
	//	"ИНН",
	//	"ИНН",
	//	Ложь);
		
	ФизическиеЛицаЗарплатаКадры.ДобавитьПравилоПроверкиФИО(
		Правила,
		"Фамилия",
		"Имя",
		"Отчество",
		"Гражданство",
		"ФИО");
		
	ФизическиеЛицаЗарплатаКадры.ДобавитьПравилоПроверкиДатыРождения(
		Правила,
		"ДатаРождения",
		"Дата рождения",
		ДатаДокумента,
		Истина);

	АдресРФОбязателен =  Гражданство = Справочники.СтраныМира.Беларусь;
	
	//ФизическиеЛицаЗарплатаКадры.ДобавитьПравилоПроверки(
	//	Правила,
	//	"Адрес",
	//	"Адрес",
	//	"Адрес в РБ",
	//	АдресРФОбязателен);
		
// {{ Локализация_КомплекснаяАвтоматизацияДляБеларуси	
	ФизическиеЛицаЗарплатаКадры.ДобавитьПравилоПроверкиУдостоверенияЛичности(
		Правила,
		"ВидДокумента",
		"СерияДокумента",
		"НомерДокумента",
		"ДатаВыдачи",
		"КемВыдан",
		"ЛичныйНомер",		
		Истина,
		Ложь);
// Локализация_КомплекснаяАвтоматизацияДляБеларуси }}		
		
	Возврат Правила;	
				
КонецФункции	

Функция ДанныеФизическогоЛицаДляПроверки(ПроверяемыеДанные)
	ДанныеФизическогоЛицаДляПроверки = Новый Структура;
	ДанныеФизическогоЛицаДляПроверки.Вставить("ФизическоеЛицо", ПроверяемыеДанные.Сотрудник);
	ДанныеФизическогоЛицаДляПроверки.Вставить("Наименование", ПроверяемыеДанные.СотрудникНаименование);
	ДанныеФизическогоЛицаДляПроверки.Вставить("Фамилия");
	ДанныеФизическогоЛицаДляПроверки.Вставить("Имя");
	ДанныеФизическогоЛицаДляПроверки.Вставить("Отчество");
	//ДанныеФизическогоЛицаДляПроверки.Вставить("ИНН");
	ДанныеФизическогоЛицаДляПроверки.Вставить("Адрес");
	ДанныеФизическогоЛицаДляПроверки.Вставить("ВидДокумента");
	ДанныеФизическогоЛицаДляПроверки.Вставить("СерияДокумента");
	ДанныеФизическогоЛицаДляПроверки.Вставить("НомерДокумента");
	ДанныеФизическогоЛицаДляПроверки.Вставить("Гражданство");
	ДанныеФизическогоЛицаДляПроверки.Вставить("ДатаРождения");
	ДанныеФизическогоЛицаДляПроверки.Вставить("АдресЗарубежом");
	ДанныеФизическогоЛицаДляПроверки.Вставить("ДатаВыдачи");
	ДанныеФизическогоЛицаДляПроверки.Вставить("КемВыдан");
	ДанныеФизическогоЛицаДляПроверки.Вставить("ЛичныйНомер");

	ЗаполнитьЗначенияСвойств(ДанныеФизическогоЛицаДляПроверки, ПроверяемыеДанные);

	Возврат  ДанныеФизическогоЛицаДляПроверки;
КонецФункции	

#Область ПроцедурыИФункцииПечати

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати.
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
	// Справка о доходах (2-НДФЛ)
	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.Идентификатор = "Форма2НДФЛ";
	КомандаПечати.Представление = НСтр("ru = 'Справка о доходах'");
	КомандаПечати.ПроверкаПроведенияПередПечатью = Ложь;
	
КонецПроцедуры

Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт	
	
	Отказ = Ложь;
		
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "Форма2НДФЛ") Тогда
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, "Форма2НДФЛ", "Справка о доходах", СформироватьПечатнуюФорму2НДФЛ(МассивОбъектов, ОбъектыПечати));	
	КонецЕсли;
	
КонецПроцедуры	

Функция ТаблицыДанныхДокументаДляПечати(Ссылка)
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	СправкаНДФЛ.Ссылка КАК Ссылка,
	|	СправкаНДФЛ.ВерсияДанных КАК ВерсияДанных,
	|	СправкаНДФЛ.ПометкаУдаления КАК ПометкаУдаления,
	|	СправкаНДФЛ.Номер КАК Номер,
	|	СправкаНДФЛ.Дата КАК Дата,
	|	СправкаНДФЛ.Проведен КАК Проведен,
	|	СправкаНДФЛ.НалоговыйПериод КАК НалоговыйПериод,
	|	СправкаНДФЛ.Организация КАК Организация,
	|	СправкаНДФЛ.СправкуПодписал КАК СправкуПодписал,
	|	СправкаНДФЛ.ДолжностьПодписавшегоЛица КАК ДолжностьПодписавшегоЛица,
	|	СправкаНДФЛ.СпособФормирования КАК СпособФормирования,
	|	СправкаНДФЛ.Сотрудник КАК Сотрудник,
	|	СправкаНДФЛ.Сотрудник.Наименование КАК СотрудникНаименование,
	|	СправкаНДФЛ.ИНН КАК ИНН,
	|	СправкаНДФЛ.Номер КАК НомерСправки,
	|	СправкаНДФЛ.РегистрацияВНалоговомОргане КАК РегистрацияВНалоговомОргане,
	|	СправкаНДФЛ.Фамилия КАК Фамилия,
	|	СправкаНДФЛ.Имя КАК Имя,
	|	СправкаНДФЛ.Отчество КАК Отчество,
	|	СправкаНДФЛ.Адрес КАК Адрес,
	|	СправкаНДФЛ.ВидДокумента КАК ВидДокумента,
	|	СправкаНДФЛ.СерияДокумента КАК СерияДокумента,
	|	СправкаНДФЛ.НомерДокумента КАК НомерДокумента,
	|	СправкаНДФЛ.Гражданство КАК Гражданство,
	|	СправкаНДФЛ.ДатаРождения КАК ДатаРождения,
	|	СправкаНДФЛ.СтатусНалогоплательщика КАК СтатусНалогоплательщика,
	|	СправкаНДФЛ.ДатаУведомления КАК ДатаУведомления,
	|	СправкаНДФЛ.НомерУведомления КАК НомерУведомления,
	|	СправкаНДФЛ.Ответственный КАК Ответственный,
	|	СправкаНДФЛ.Комментарий КАК Комментарий,
	|	СправкаНДФЛ.АдресЗаРубежом КАК АдресЗаРубежом,
	|	СправкаНДФЛ.ОбщаяСуммаДоходаПоСтавке13 КАК ОбщаяСуммаДоходаПоСтавке13,
	|	СправкаНДФЛ.ОблагаемаяСуммаДоходаПоСтавке13 КАК ОблагаемаяСуммаДоходаПоСтавке13,
	|	СправкаНДФЛ.ИсчисленоПоСтавке13 КАК ИсчисленоПоСтавке13,
	|	СправкаНДФЛ.УдержаноПоСтавке13 КАК УдержаноПоСтавке13,
	|	СправкаНДФЛ.ЗадолженностьПоСтавке13 КАК ЗадолженностьПоСтавке13,
	|	СправкаНДФЛ.ИзлишнеУдержаноПоСтавке13 КАК ИзлишнеУдержаноПоСтавке13,
	|	СправкаНДФЛ.ПеречисленоПоСтавке13 КАК ПеречисленоПоСтавке13,
	|	СправкаНДФЛ.ОбщаяСуммаДоходаПоСтавке30 КАК ОбщаяСуммаДоходаПоСтавке30,
	|	СправкаНДФЛ.ОблагаемаяСуммаДоходаПоСтавке30 КАК ОблагаемаяСуммаДоходаПоСтавке30,
	|	СправкаНДФЛ.ИсчисленоПоСтавке30 КАК ИсчисленоПоСтавке30,
	|	СправкаНДФЛ.УдержаноПоСтавке30 КАК УдержаноПоСтавке30,
	|	СправкаНДФЛ.ЗадолженностьПоСтавке30 КАК ЗадолженностьПоСтавке30,
	|	СправкаНДФЛ.ИзлишнеУдержаноПоСтавке30 КАК ИзлишнеУдержаноПоСтавке30,
	|	СправкаНДФЛ.ПеречисленоПоСтавке30 КАК ПеречисленоПоСтавке30,
	|	СправкаНДФЛ.ОбщаяСуммаДоходаПоСтавке9 КАК ОбщаяСуммаДоходаПоСтавке9,
	|	СправкаНДФЛ.ОблагаемаяСуммаДоходаПоСтавке9 КАК ОблагаемаяСуммаДоходаПоСтавке9,
	|	СправкаНДФЛ.ИсчисленоПоСтавке9 КАК ИсчисленоПоСтавке9,
	|	СправкаНДФЛ.УдержаноПоСтавке9 КАК УдержаноПоСтавке9,
	|	СправкаНДФЛ.ЗадолженностьПоСтавке9 КАК ЗадолженностьПоСтавке9,
	|	СправкаНДФЛ.ИзлишнеУдержаноПоСтавке9 КАК ИзлишнеУдержаноПоСтавке9,
	|	СправкаНДФЛ.ПеречисленоПоСтавке9 КАК ПеречисленоПоСтавке9,
	|	СправкаНДФЛ.ОбщаяСуммаДоходаПоСтавке15 КАК ОбщаяСуммаДоходаПоСтавке15,
	|	СправкаНДФЛ.ОблагаемаяСуммаДоходаПоСтавке15 КАК ОблагаемаяСуммаДоходаПоСтавке15,
	|	СправкаНДФЛ.ИсчисленоПоСтавке15 КАК ИсчисленоПоСтавке15,
	|	СправкаНДФЛ.УдержаноПоСтавке15 КАК УдержаноПоСтавке15,
	|	СправкаНДФЛ.ЗадолженностьПоСтавке15 КАК ЗадолженностьПоСтавке15,
	|	СправкаНДФЛ.ИзлишнеУдержаноПоСтавке15 КАК ИзлишнеУдержаноПоСтавке15,
	|	СправкаНДФЛ.ПеречисленоПоСтавке15 КАК ПеречисленоПоСтавке15,
	|	СправкаНДФЛ.ОбщаяСуммаДоходаПоСтавке35 КАК ОбщаяСуммаДоходаПоСтавке35,
	|	СправкаНДФЛ.ОблагаемаяСуммаДоходаПоСтавке35 КАК ОблагаемаяСуммаДоходаПоСтавке35,
	|	СправкаНДФЛ.ИсчисленоПоСтавке35 КАК ИсчисленоПоСтавке35,
	|	СправкаНДФЛ.УдержаноПоСтавке35 КАК УдержаноПоСтавке35,
	|	СправкаНДФЛ.ЗадолженностьПоСтавке35 КАК ЗадолженностьПоСтавке35,
	|	СправкаНДФЛ.ИзлишнеУдержаноПоСтавке35 КАК ИзлишнеУдержаноПоСтавке35,
	|	СправкаНДФЛ.ПеречисленоПоСтавке35 КАК ПеречисленоПоСтавке35,
	|	СправкаНДФЛ.КодНалоговогоОрганаУведомления КАК КодНалоговогоОрганаУведомления,
	|	СправкаНДФЛ.ДатаВыдачи КАК ДатаВыдачи,
	|	СправкаНДФЛ.ЛичныйНомер КАК ЛичныйНомер
	|ИЗ
	|	Документ.СправкаНДФЛ КАК СправкаНДФЛ
	|ГДЕ
	|	СправкаНДФЛ.Ссылка В(&Ссылка)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СправкаНДФЛСведенияОДоходах.Ссылка КАК Ссылка,
	|	СправкаНДФЛСведенияОДоходах.НомерСтроки КАК НомерСтроки,
	|	СправкаНДФЛСведенияОДоходах.МесяцНалоговогоПериода КАК МесяцНалоговогоПериода,
	|	СУММА(СправкаНДФЛСведенияОДоходах.СуммаДохода) КАК СуммаДохода,
	|	СправкаНДФЛСведенияОДоходах.КодВычета КАК КодВычета,
	|	СУММА(СправкаНДФЛСведенияОДоходах.СуммаВычета) КАК СуммаВычета,
	|	СправкаНДФЛСведенияОДоходах.Ссылка.Номер КАК НомерСправки,
	|	СправкаНДФЛСведенияОДоходах.Ссылка.Сотрудник КАК Сотрудник,
	|	СУММА(СправкаНДФЛСведенияОДоходах.Исчислено) КАК Исчислено,
	|	СправкаНДФЛСведенияОДоходах.СтатусНалогоплательщика КАК СтатусНалогоплательщика
	|ИЗ
	|	Документ.СправкаНДФЛ.СведенияОДоходах КАК СправкаНДФЛСведенияОДоходах
	|ГДЕ
	|	СправкаНДФЛСведенияОДоходах.Ссылка В(&Ссылка)
	|
	|СГРУППИРОВАТЬ ПО
	|	СправкаНДФЛСведенияОДоходах.Ссылка,
	|	СправкаНДФЛСведенияОДоходах.КодВычета,
	|	СправкаНДФЛСведенияОДоходах.МесяцНалоговогоПериода,
	|	СправкаНДФЛСведенияОДоходах.НомерСтроки,
	|	СправкаНДФЛСведенияОДоходах.Ссылка.Номер,
	|	СправкаНДФЛСведенияОДоходах.Ссылка.Сотрудник,
	|	СправкаНДФЛСведенияОДоходах.СтатусНалогоплательщика
	|
	|УПОРЯДОЧИТЬ ПО
	|	МесяцНалоговогоПериода
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СправкаНДФЛСведенияОВычетах.Ссылка КАК Ссылка,
	|	СправкаНДФЛСведенияОВычетах.НомерСтроки КАК НомерСтроки,
	|	СправкаНДФЛСведенияОВычетах.КодВычета КАК КодВычета,
	|	СправкаНДФЛСведенияОВычетах.СуммаВычета КАК СуммаВычета,
	|	СправкаНДФЛСведенияОВычетах.Ссылка.Номер КАК НомерСправки,
	|	СправкаНДФЛСведенияОВычетах.Ссылка.Сотрудник КАК Сотрудник,
	|	СправкаНДФЛСведенияОВычетах.МесяцНалоговогоПериода КАК МесяцНалоговогоПериода
	|ИЗ
	|	Документ.СправкаНДФЛ.СведенияОВычетах КАК СправкаНДФЛСведенияОВычетах
	|ГДЕ
	|	СправкаНДФЛСведенияОВычетах.Ссылка В(&Ссылка)
	|
	|УПОРЯДОЧИТЬ ПО
	|	МесяцНалоговогоПериода
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СправкаНДФЛСведенияОНалогах.Ссылка КАК Ссылка,
	|	СправкаНДФЛСведенияОНалогах.НомерСтроки КАК НомерСтроки,
	|	СправкаНДФЛСведенияОНалогах.МесяцНалоговогоПериода КАК МесяцНалоговогоПериода,
	|	СправкаНДФЛСведенияОНалогах.СуммаНалога КАК СуммаНалога,
	|	СправкаНДФЛСведенияОНалогах.Ссылка.Номер КАК НомерСправки,
	|	СправкаНДФЛСведенияОНалогах.Ссылка.Сотрудник КАК Сотрудник
	|ИЗ
	|	Документ.СправкаНДФЛ.СведенияОНалогах КАК СправкаНДФЛСведенияОНалогах
	|ГДЕ
	|	СправкаНДФЛСведенияОНалогах.Ссылка В(&Ссылка)
	|
	|УПОРЯДОЧИТЬ ПО
	|	МесяцНалоговогоПериода";
	
	Результаты = Запрос.ВыполнитьПакет();
	
	Сотрудники = Результаты[0].Выгрузить();
	
	Для Каждого СтрокаТаблицы Из Сотрудники Цикл
		СтрокаТаблицы.НомерСправки = НомерСправкиБезПрефиксов(СтрокаТаблицы.НомерСправки);			
	КонецЦикла;	
	
	СведенияОДоходах = Результаты[1].Выгрузить();
	
	Для Каждого СтрокаТаблицы Из СведенияОДоходах Цикл
		СтрокаТаблицы.НомерСправки =  НомерСправкиБезПрефиксов(СтрокаТаблицы.НомерСправки);		
	КонецЦикла;	
	
	СведенияОВычетах = Результаты[2].Выгрузить();
	
	Для Каждого СтрокаТаблицы Из СведенияОВычетах Цикл
		СтрокаТаблицы.НомерСправки =  НомерСправкиБезПрефиксов(СтрокаТаблицы.НомерСправки);		
	КонецЦикла;	
	
	СведенияОНалогах = Результаты[3].Выгрузить();
	
	Для Каждого СтрокаТаблицы Из СведенияОНалогах Цикл
		СтрокаТаблицы.НомерСправки =  НомерСправкиБезПрефиксов(СтрокаТаблицы.НомерСправки);		
	КонецЦикла;	
	
	Возврат Новый Структура("Сотрудники, СведенияОДоходах, СведенияОВычетах, СведенияОНалогах", Сотрудники, СведенияОДоходах, СведенияОВычетах, СведенияОНалогах);
	
КонецФункции	

Функция НомерСправкиБезПрефиксов(НомерДокумента)
	Возврат ПрефиксацияОбъектовКлиентСервер.ПолучитьНомерНаПечать(НомерДокумента, Истина, Истина);	
КонецФункции	

Функция СформироватьПечатнуюФорму2НДФЛ(МассивОбъектов, ОбъектыПечати)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;	
	ДанныеНА 					  = УчетНДФЛ.СправкиНДФЛДанныеДляПечати(МассивОбъектов);
	ТаблицыДокумента 			  = ТаблицыДанныхДокументаДляПечати(МассивОбъектов);
	
	Возврат УчетНДФЛ.СформироватьПечатнуюФорму2НДФЛ_Локализация(
												ОбъектыПечати, 
												МассивОбъектов, 
												ДанныеНА, 
												ТаблицыДокумента.Сотрудники, 
												ТаблицыДокумента.СведенияОДоходах, 
												ТаблицыДокумента.СведенияОВычетах,
												ТаблицыДокумента.СведенияОНалогах); 
	
КонецФункции	

#КонецОбласти

#КонецОбласти

#КонецЕсли
﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

Процедура ДобавитьКомандыОтчетов(КомандыОтчетов, Параметры) Экспорт
	ВариантыОтчетовУТПереопределяемый.ДобавитьКомандуСтруктураПодчиненности(КомандыОтчетов);
КонецПроцедуры

Процедура ДобавитьКомандыСозданияНаОсновании(КомандыСоздатьНаОсновании, Параметры) Экспорт
	Документы.ОприходованиеИзлишковТоваров.ДобавитьКомандуСоздатьНаОсновании(КомандыСоздатьНаОсновании);
	Документы.СписаниеИзЭксплуатации.ДобавитьКомандуСоздатьНаОсновании(КомандыСоздатьНаОсновании);
КонецПроцедуры

Функция ДобавитьКомандуСоздатьНаОсновании(КомандыСоздатьНаОсновании) Экспорт
	Если ПравоДоступа("Добавление", Метаданные.Документы.ИнвентаризационнаяОписьБСО) Тогда
		КомандаСоздатьНаОсновании = КомандыСоздатьНаОсновании.Добавить();
		КомандаСоздатьНаОсновании.Менеджер = Метаданные.Документы.ИнвентаризационнаяОписьБСО.ПолноеИмя();
		КомандаСоздатьНаОсновании.Представление = ОбщегоНазначенияУТ.ПредставлениеОбъекта(Метаданные.Документы.ИнвентаризационнаяОписьБСО);
		КомандаСоздатьНаОсновании.РежимЗаписи = "Проводить";
		Возврат КомандаСоздатьНаОсновании;
	КонецЕсли;
	
	Возврат Неопределено;
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Проведение

#КонецОбласти

#Область Печать

Функция СформироватьПечатнуюФормуИНВПриложение18К(МассивОбъектов, ОбъектыПечати)
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("МассивОбъектов", МассивОбъектов);
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ИнвентаризационнаяОписьБСО.Дата КАК Дата,
	|	ИнвентаризационнаяОписьБСО.Организация.Наименование КАК НаименованиеОрганизации,
	|	ИнвентаризационнаяОписьБСО.Организация.НаименованиеПолное КАК Организация,
	|	ИнвентаризационнаяОписьБСО.Подразделение.Наименование КАК Подразделение,
	|	ИнвентаризационнаяОписьБСО.Ответственный КАК Ответственный,
	|	ИнвентаризационнаяОписьБСО.Организация.ИНН КАК ИНН,
	|	ИнвентаризационнаяОписьБСО.ДатаНачала КАК ДатаНачала,
	|	ИнвентаризационнаяОписьБСО.ДатаОкончания КАК ДатаОкончания,
	|	ИнвентаризационнаяОписьБСО.Ссылка КАК Ссылка,
	|	ИнвентаризационнаяОписьБСО.Склад.ТекущийОтветственный КАК ТекущийОтветственныйПоСкладу,
	|	ИнвентаризационнаяОписьБСО.Склад.ТекущаяДолжностьОтветственного КАК ТекущаяДолжностьОтветственногоПоСкладу,
	|	ИнвентаризационнаяОписьБСО.Номер КАК Номер,
	|	ИнвентаризационнаяОписьБСО.ФизическоеЛицо КАК ФизическоеЛицо,
	|	ИнвентаризационнаяОписьБСО.Комиссия КАК Комиссия
	|ИЗ
	|	Документ.ИнвентаризационнаяОписьБСО КАК ИнвентаризационнаяОписьБСО
	|ГДЕ
	|	ИнвентаризационнаяОписьБСО.Ссылка В(&МассивОбъектов)
	|
	|УПОРЯДОЧИТЬ ПО
	|	ИнвентаризационнаяОписьБСО.Ссылка
	|АВТОУПОРЯДОЧИВАНИЕ
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Товары.НомерСтроки КАК НомерСтроки,
	|	Товары.Номенклатура КАК Номенклатура,
	|	Товары.Номенклатура.ЕдиницаИзмерения.Наименование КАК ЕдИзм,
	|	Товары.Характеристика КАК ХарактеристикаНоменклатуры,
	|	Товары.СерияБланкаСтрогойОтчетности КАК СерияБланка,
	|	Товары.НачальныйНомерБланкаСтрогойОтчетности КАК НомерС,
	|	Товары.КонечныйНомерБланкаСтрогойОтчетности КАК НомерПо,
	|	Товары.Количество КАК Количество,
	|	Товары.НачальныйНомерБланкаСтрогойОтчетности + "" - "" + Товары.КонечныйНомерБланкаСтрогойОтчетности КАК Страницы,
	|	Товары.НачальныйНомерБланкаСтрогойОтчетностиФакт КАК НомерСФакт,
	|	Товары.КонечныйНомерБланкаСтрогойОтчетностиФакт КАК НомерПоФакт,
	|	Товары.КоличествоФакт КАК КоличествоФакт,
	|	Товары.НачальныйНомерБланкаСтрогойОтчетностиФакт + "" - "" + Товары.КонечныйНомерБланкаСтрогойОтчетностиФакт КАК СтраницыФакт,
	|	1 КАК ЛеваяГраница,
	|	1 КАК ПраваяГраница,
	|	1 КАК ЛеваяГраницаФакт,
	|	1 КАК ПраваяГраницаФакт,
	|	1 КАК Номер,
	|	0 КАК КолНедостача,
	|	0 КАК КолИзлишек,
	|	Товары.НачальныйНомерБланкаСтрогойОтчетностиФакт + "" - "" + Товары.КонечныйНомерБланкаСтрогойОтчетностиФакт КАК ИнтервалНедостача,
	|	Товары.НачальныйНомерБланкаСтрогойОтчетностиФакт + "" - "" + Товары.КонечныйНомерБланкаСтрогойОтчетностиФакт КАК ИнтервалИзлишек,
	|	Товары.Цена КАК Цена,
	|	Товары.Цена * Товары.Количество КАК Сумма,
	|	Товары.Ссылка КАК Ссылка
	|ИЗ
	|	Документ.ИнвентаризационнаяОписьБСО.Товары КАК Товары
	|ГДЕ
	|	Товары.Ссылка В(&МассивОбъектов)
	|
	|УПОРЯДОЧИТЬ ПО
	|	Товары.Ссылка,
	|	НомерСтроки
	|АВТОУПОРЯДОЧИВАНИЕ";
	
	Результат = Запрос.ВыполнитьПакет();
	Если Результат[1].Пустой() Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	ТаблДокумент = Новый ТабличныйДокумент;
	ТаблДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_ИнвентаризацияБСО_ИНВПриложение18К";
	ТаблДокумент.АвтоМасштаб = Истина;
	ТаблДокумент.ОриентацияСтраницы = ОриентацияСтраницы.Портрет;
	ТаблДокумент.ПолеСверху = 5;
	ТаблДокумент.ПолеСлева = 10;
	ТаблДокумент.ПолеСнизу = 5;
	ТаблДокумент.ПолеСправа = 5;
	
	ТабличнаяЧастьПоДокументам = Результат[1].Выгрузить();
	ПервыйДокумент = Истина;
	
	Шапка = Результат[0].Выбрать();
	Пока Шапка.Следующий() Цикл
		ПараметрыОтбора = Новый Структура("Ссылка", Шапка.Ссылка);
		ТабличнаяЧастьБСО = ТабличнаяЧастьПоДокументам.Скопировать(ПараметрыОтбора);
		Если ТабличнаяЧастьБСО.Количество() = 0 Тогда
			Продолжить;
		КонецЕсли;
		
		Если Не ПервыйДокумент Тогда
			ТаблДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли;
		
		ПервыйДокумент = Ложь;
		НомерСтрокиНачало = ТаблДокумент.ВысотаТаблицы + 1;
		
		Для каждого Стр Из ТабличнаяЧастьБСО Цикл
			Стр.СерияБланка = СокрЛП(Стр.СерияБланка);
		КонецЦикла;
		
		Макет = УправлениеПечатью.МакетПечатнойФормы("Документ.ИнвентаризационнаяОписьБСО.ПФ_MXL_ИНВПриложение18К");
		
		ОблШапка = Макет.ПолучитьОбласть("Шапка");
		ОблПодписиМОЛ = Макет.ПолучитьОбласть("ПодписиМОЛ");
		ОблПодписиМОЛ2 = Макет.ПолучитьОбласть("ПодписиМОЛ2");
		ОблПодписиМОЛБух = Макет.ПолучитьОбласть("ПодписиМОЛБух");
		ОблШапкаФакт = Макет.ПолучитьОбласть("ШапкаФакт");
		ОблСтрокаФакт = Макет.ПолучитьОбласть("СтрокаФакт");
		ОблИтогоФакт = Макет.ПолучитьОбласть("ИтогоФакт");
		ОблИтогоФактОбщий = Макет.ПолучитьОбласть("ИтогоФактОбщий");
		ОблШапкаБУ = Макет.ПолучитьОбласть("ШапкаБУ");
		ОблСтрокаБУ = Макет.ПолучитьОбласть("СтрокаБУ");
		ОблИтогоБУ = Макет.ПолучитьОбласть("ИтогоБУ");
		ОблПодвал1 = Макет.ПолучитьОбласть("Подвал1");
		ОблПодвал2 = Макет.ПолучитьОбласть("Подвал2");
		ОблПодвал3 = Макет.ПолучитьОбласть("Подвал3");
		ОблПодписи = Макет.ПолучитьОбласть("Подписи");
		ОблПодвал4 = Макет.ПолучитьОбласть("Подвал4");
		ОблПустаяСтрока = Макет.ПолучитьОбласть("ПустаяСтрока");
		ОбластьПодпись = Макет.ПолучитьОбласть("Подпись");
		ОбластьПодписиСтрока = Макет.ПолучитьОбласть("ПодписиСтрока");
		
		ОблШапка.Параметры.Заполнить(Шапка);
		ОблШапка.Параметры.НомерДок = ПрефиксацияОбъектовКлиентСервер.НомерНаПечать(Шапка.Номер, Истина, Истина);
		Если Не ЗначениеЗаполнено(ОблШапка.Параметры.Организация) Тогда
			ОблШапка.Параметры.Организация = Шапка.НаименованиеОрганизации;
		КонецЕсли;
		ТаблДокумент.Вывести(ОблШапка);
		ТаблДокумент.Вывести(ОблПустаяСтрока);
		
		Если ЗначениеЗаполнено(Шапка.ТекущийОтветственныйПоСкладу) Тогда
			ОблПодписиМОЛ.Параметры.Должность = Шапка.ТекущаяДолжностьОтветственногоПоСкладу;
			ОблПодписиМОЛ.Параметры.Представление = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(Шапка.ТекущийОтветственныйПоСкладу);
		Иначе
			ДанныеФизЛица = "";
			Если ТипЗнч(ДанныеФизЛица) = Тип("Структура") Тогда
				ОблПодписиМОЛ.Параметры.Должность = ДанныеФизЛица.Должность;
				ОблПодписиМОЛ.Параметры.Представление = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(Шапка.ФизическоеЛицо);
			КонецЕсли;
		КонецЕсли;
		
		ОблПодписиМОЛ2.Параметры.Заполнить(ОблПодписиМОЛ.Параметры);
		
		ТаблДокумент.Вывести(ОблПодписиМОЛ);
		ТаблДокумент.Вывести(ОблПустаяСтрока);
		ТаблДокумент.Вывести(ОблШапкаФакт);
		
		ВремСумма = 0;
		ИтогоКоличествоФакт = 0;
		КоличествоБСО = ТабличнаяЧастьБСО.Количество() - 1;
		Для К = 0 По КоличествоБСО Цикл
			ИтогоКоличествоФакт = ИтогоКоличествоФакт + ТабличнаяЧастьБСО[К].КоличествоФакт;
			
			ОблСтрокаФакт.Параметры.Заполнить(ТабличнаяЧастьБСО[К]);
			ОблСтрокаФакт.Параметры.ЕдИзм = СокрЛП(ТабличнаяЧастьБСО[К].ЕдИзм);
			
			ВремСумма = ВремСумма + ТабличнаяЧастьБСО[К].КоличествоФакт * ТабличнаяЧастьБСО[К].Цена;
			ОблСтрокаФакт.Параметры.СуммаФакт = ТабличнаяЧастьБСО[К].КоличествоФакт * ТабличнаяЧастьБСО[К].Цена;
			
			ТаблДокумент.Вывести(ОблСтрокаФакт);
			
			Если К = КоличествоБСО Или ТабличнаяЧастьБСО[К].Номенклатура <> ТабличнаяЧастьБСО[К + 1].Номенклатура
				Или ТабличнаяЧастьБСО[К].СерияБланка <> ТабличнаяЧастьБСО[К + 1].СерияБланка Тогда
				
				ИтогоКоличествоФакт = 0;
			КонецЕсли;
		КонецЦикла;
		
		ОблИтогоФактОбщий.Параметры.КоличествоФакт = ТабличнаяЧастьБСО.Итог("КоличествоФакт");
		ОблИтогоФактОбщий.Параметры.СуммаФакт = ВремСумма;
		ТаблДокумент.Вывести(ОблИтогоФактОбщий);
		
		ТаблДокумент.Вывести(ОблШапкаБУ);
		ТаблицаБух = ТабличнаяЧастьБСО.СкопироватьКолонки("НомерСтроки, Номер");
		ТаблицаФакт = ТаблицаБух.СкопироватьКолонки();
		ТаблицаНедостача = ТаблицаБух.СкопироватьКолонки();
		ТаблицаИзлишек = ТаблицаБух.СкопироватьКолонки();
		
		Для Каждого Стр ИЗ ТабличнаяЧастьБСО Цикл
			ЧислоС = ?(ЗначениеЗаполнено(Стр.НомерС), Число(Стр.НомерС), 0);
			ЧислоПо = ?(ЗначениеЗаполнено(Стр.НомерПо), Число(Стр.НомерПо), 0);
			ЧислоСФакт = ?(ЗначениеЗаполнено(Стр.НомерСФакт), Число(Стр.НомерСФакт), 0);
			ЧислоПоФакт = ?(ЗначениеЗаполнено(Стр.НомерПоФакт), Число(Стр.НомерПоФакт), 0);
			
			Стр.ЛеваяГраница = ЧислоС;
			Стр.ПраваяГраница = ЧислоПо;
			Стр.ЛеваяГраницаФакт = ЧислоСФакт;
			Стр.ПраваяГраницаФакт = ЧислоПоФакт;
			
			Пока ЧислоС <= ЧислоПо И ЧислоС <> 0 Цикл
				Запись = ТаблицаБух.Добавить();
				ЗаполнитьЗначенияСвойств(Запись, Стр);
				Запись.Номер = ЧислоС;
				ЧислоС = ЧислоС + 1;
			КонецЦикла;
			
			Пока ЧислоСФакт <= ЧислоПоФакт И ЧислоСФакт <> 0 Цикл
				Запись = ТаблицаФакт.Добавить();
				ЗаполнитьЗначенияСвойств(Запись, Стр);
				Запись.Номер = ЧислоСФакт;
				ЧислоСФакт = ЧислоСФакт + 1;
			КонецЦикла;
		КонецЦикла;
		
		Для каждого Стр Из ТабличнаяЧастьБСО Цикл
			Стр.ИнтервалНедостача = "";
			Стр.ИнтервалИзлишек = "";
			
			Если Стр.ЛеваяГраница = Стр.ЛеваяГраницаФакт И Стр.ПраваяГраница = Стр.ПраваяГраницаФакт Тогда
				Продолжить;
			КонецЕсли;
			
			ПараметрыОтбора = Новый Структура;
			ПараметрыОтбора.Вставить("НомерСтроки", Стр.НомерСтроки);
			ТаблицаБухПоиск = ТаблицаБух.Скопировать(ПараметрыОтбора);
			ТаблицаФактПоиск = ТаблицаФакт.Скопировать(ПараметрыОтбора);
			
			Для каждого ЭлементПоиска Из ТаблицаБухПоиск Цикл
				Если ТаблицаФактПоиск.Найти(ЭлементПоиска.Номер, "Номер") = Неопределено Тогда
					НоваяСтрока = ТаблицаНедостача.Добавить();
					НоваяСтрока.НомерСтроки = ЭлементПоиска.НомерСтроки;
					НоваяСтрока.Номер = ЭлементПоиска.Номер;
				КонецЕсли;
			КонецЦикла;
			
			Для каждого ЭлементПоиска Из ТаблицаФактПоиск Цикл
				Если ТаблицаБухПоиск.Найти(ЭлементПоиска.Номер, "Номер") = Неопределено Тогда
					НоваяСтрока = ТаблицаИзлишек.Добавить();
					НоваяСтрока.НомерСтроки = ЭлементПоиска.НомерСтроки;
					НоваяСтрока.Номер = ЭлементПоиска.Номер;
				КонецЕсли;
			КонецЦикла;
		КонецЦикла;
		
		Для каждого Стр Из ТабличнаяЧастьБСО Цикл
			Если Стр.ЛеваяГраница = Стр.ЛеваяГраницаФакт И Стр.ПраваяГраница = Стр.ПраваяГраницаФакт Тогда
				Продолжить;
			КонецЕсли;
			
			ПараметрыОтбора = Новый Структура;
			ПараметрыОтбора.Вставить("НомерСтроки", Стр.НомерСтроки);
			
			ТаблицаНедостачаПоиск = ТаблицаНедостача.Скопировать(ПараметрыОтбора);
			ТаблицаИзлишекПоиск = ТаблицаИзлишек.Скопировать(ПараметрыОтбора);
			
			ПолучитьДиапазон(Стр, ТаблицаНедостачаПоиск, "ИнтервалНедостача", "КолНедостача");
			ПолучитьДиапазон(Стр, ТаблицаИзлишекПоиск, "ИнтервалИзлишек", "КолИзлишек");
		КонецЦикла;
		
		СуммаНедостача = 0;
		СуммаИзлишек = 0;
		Для каждого Стр Из ТабличнаяЧастьБСО Цикл
			ОблСтрокаБУ.Параметры.СерияБСОИзлишек = "";
			ОблСтрокаБУ.Параметры.СерияБСОНедостача = "";
			ОблСтрокаБУ.Параметры.СуммаИзлишек = 0;
			ОблСтрокаБУ.Параметры.СуммаНедостача = 0;
			
			Если Стр.КолИзлишек > 0 Тогда
				СуммаИзлишек = СуммаИзлишек + Стр.КолИзлишек * Стр.Цена;
				ОблСтрокаБУ.Параметры.СерияБСОИзлишек = Стр.СерияБланка;
				ОблСтрокаБУ.Параметры.СуммаИзлишек = Стр.КолИзлишек * Стр.Цена;
			КонецЕсли;
			Если Стр.КолНедостача > 0 Тогда
				СуммаНедостача = СуммаНедостача + Стр.КолНедостача * Стр.Цена;
				ОблСтрокаБУ.Параметры.СерияБСОНедостача = Стр.СерияБланка;
				ОблСтрокаБУ.Параметры.СуммаНедостача = Стр.КолНедостача * Стр.Цена;
			КонецЕсли;
			
			ОблСтрокаБУ.Параметры.Заполнить(Стр);
			ТаблДокумент.Вывести(ОблСтрокаБУ);
		КонецЦикла;
		
		ОблИтогоБУ.Параметры.СуммаНедостача = СуммаНедостача;
		ОблИтогоБУ.Параметры.СуммаИзлишек = СуммаИзлишек;
		
		ОблИтогоБУ.Параметры.Количество = ТабличнаяЧастьБСО.Итог("Количество");
		ОблИтогоБУ.Параметры.КолИзлишек = ТабличнаяЧастьБСО.Итог("КолИзлишек");
		ОблИтогоБУ.Параметры.КолНедостача = ТабличнаяЧастьБСО.Итог("КолНедостача");
		ОблИтогоБУ.Параметры.Сумма = ТабличнаяЧастьБСО.Итог("Сумма");
		ТаблДокумент.Вывести(ОблИтогоБУ);
		ТаблДокумент.Вывести(ОблПустаяСтрока);
		
		ОблПодвал1.Параметры.НомП = ЧислоПрописью(ТабличнаяЧастьБСО.Количество(),, ",,,,,,,,0");
		ОблПодвал1.Параметры.КоличествоФактП = ЧислоПрописью(ТабличнаяЧастьБСО.Итог("КоличествоФакт"),, ",,,,,,,,0");
		ОблПодвал1.Параметры.СуммаФактП = ЧислоПрописью(ВремСумма,, ",,,,,,,,0");
		ТаблДокумент.Вывести(ОблПодвал1);
		ТаблДокумент.Вывести(ОблПустаяСтрока);
		
		ОблПодписиМОЛБух.Параметры.Должность = "Бухгалтер";
		ОблПодписиМОЛБух.Параметры.Представление = "";
		ТаблДокумент.Вывести(ОблПодписиМОЛБух);
		ТаблДокумент.Вывести(ОблПустаяСтрока);
		
		// 4D:ERP для Беларуси, Антон, 12.03.2022 14:17:01 
		// Вывести комиссию на печатную форму, №32112
		// {
		Если ЗначениеЗаполнено(Шапка.Комиссия) Тогда
			НомерЧлена = 0;
			ФизЛицоПредседателя = Шапка.Комиссия.Председатель;
			ОбластьПодпись.Параметры.Должность = Шапка.Комиссия.ДолжностьДляПечати;
			ОбластьПодпись.Параметры.Председатель = Строка(ФизЛицоПредседателя.Фамилия) + " " 
			+ ФизическиеЛицаЗарплатаКадрыКлиентСервер.ИнициалыИмени(ФизЛицоПредседателя.Имя) 
			+ ФизическиеЛицаЗарплатаКадрыКлиентСервер.ИнициалыИмени(ФизЛицоПредседателя.Отчество);
			ОбластьПодпись.Параметры.Председатель = СокрЛП(ОбластьПодпись.Параметры.Председатель); 
			ТаблДокумент.Вывести(ОбластьПодпись);
			Для Каждого  СтрокаКомиссии Из Шапка.Комиссия.Комиссия  Цикл
				НомерЧлена = НомерЧлена + 1;
				Если НомерЧлена = 1 Тогда
					ОбластьПодписиСтрока.Параметры.ТекстКомиссииПодпись = "Члены комиссии:";	
				Иначе
					ОбластьПодписиСтрока.Параметры.ТекстКомиссииПодпись = "";	
				КонецЕсли; 
				ОбластьПодписиСтрока.Параметры.ФИОЧленКомиссии = Строка(СтрокаКомиссии.Сотрудник.Фамилия) + " " 
				+ ФизическиеЛицаЗарплатаКадрыКлиентСервер.ИнициалыИмени(СтрокаКомиссии.Сотрудник.Имя) 
				+ ФизическиеЛицаЗарплатаКадрыКлиентСервер.ИнициалыИмени(СтрокаКомиссии.Сотрудник.Отчество);
				ОбластьПодписиСтрока.Параметры.ФИОЧленКомиссии = СокрЛП(ОбластьПодписиСтрока.Параметры.ФИОЧленКомиссии);	
				ОбластьПодписиСтрока.Параметры.ДолжностьЧленКомиссии = СтрокаКомиссии.ДолжностьДляПечати;
				ТаблДокумент.Вывести(ОбластьПодписиСтрока);
			КонецЦикла;
		Иначе
			ТаблДокумент.Вывести(ОбластьПодпись);
			НомерЧлена=0;
			Для Счетчик = 1 По 3 Цикл
				НомерЧлена = НомерЧлена + 1;
				Если НомерЧлена = 1 Тогда
					ОбластьПодписиСтрока.Параметры.ТекстКомиссииПодпись = "Члены комиссии:";	
				Иначе
					ОбластьПодписиСтрока.Параметры.ТекстКомиссииПодпись = "";	
				КонецЕсли; 
				ТаблДокумент.Вывести(ОбластьПодписиСтрока);
			КонецЦикла;
		КонецЕсли;
		// }
		// 4D

		
		ТаблДокумент.Вывести(ОблПодвал2);
		ТаблДокумент.Вывести(ОблПустаяСтрока);
		
		ОблПодписиМОЛ2.Параметры.МатЛицо = "Метериально-ответственные лица (лицо)";
		ТаблДокумент.Вывести(ОблПодписиМОЛ2);
		ТаблДокумент.Вывести(ОблПустаяСтрока);
		
		ТаблДокумент.Вывести(ОблПодвал3);
		ТаблДокумент.Вывести(ОблПустаяСтрока);
		
		ОблПодписиМОЛ2.Параметры.МатЛицо = "Метериально-ответственные лица (лицо)";
		ТаблДокумент.Вывести(ОблПодписиМОЛ2);
		ТаблДокумент.Вывести(ОблПустаяСтрока);
		
		ТаблДокумент.Вывести(ОблПодвал4);
		
		УправлениеПечатью.ЗадатьОбластьПечатиДокумента(ТаблДокумент, НомерСтрокиНачало, ОбъектыПечати, Шапка.Ссылка);
	КонецЦикла;
	
	Возврат ТаблДокумент;
КонецФункции

Процедура ПолучитьДиапазон(Стр, ТаблицаПоиск, Поле, ПолеКоличество)
	Индекс = 0;
	СтрокаЗаписи = "";
	КоличествоСтрок = ТаблицаПоиск.Количество() - 1;
	Стр[ПолеКоличество] = КоличествоСтрок + 1;
	
	Для К = 0 По КоличествоСтрок Цикл
		Если К = КоличествоСтрок Или (ТаблицаПоиск[К].Номер + 1) <> ТаблицаПоиск[К + 1].Номер Тогда
			ДлинаСтр = (Макс(СтрДлина(Стр.Страницы), СтрДлина(Стр.СтраницыФакт)) - 3) / 2;
			
			Если ТаблицаПоиск[Индекс].Номер <> ТаблицаПоиск[К].Номер Тогда
				СтрокаЗаписи = СтрокаЗаписи + УчетБланковСтрогойОтчетностиКлиентСервер.ОтформатироватьНомерБланка(ТаблицаПоиск[Индекс].Номер, ДлинаСтр)
				+ " - " + УчетБланковСтрогойОтчетностиКлиентСервер.ОтформатироватьНомерБланка(ТаблицаПоиск[К].Номер, ДлинаСтр) + ?(К <> КоличествоСтрок, ", ", "");
			Иначе
				СтрокаЗаписи = СтрокаЗаписи + УчетБланковСтрогойОтчетностиКлиентСервер.ОтформатироватьНомерБланка(ТаблицаПоиск[Индекс].Номер, ДлинаСтр) + ?(К <> КоличествоСтрок, ", ", "");
			КонецЕсли;
			
			Индекс = К + 1;
			Стр[Поле] = Стр[Поле] + СтрокаЗаписи;
			СтрокаЗаписи = "";
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "ИНВПриложение18К") Тогда
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, "ИНВПриложение18К", НСтр("ru='Инвентаризационная ведомость'"), СформироватьПечатнуюФормуИНВПриложение18К(МассивОбъектов, ОбъектыПечати));
	КонецЕсли;
	
	ФормированиеПечатныхФорм.ЗаполнитьПараметрыОтправки(ПараметрыВывода.ПараметрыОтправки, МассивОбъектов, КоллекцияПечатныхФорм);
КонецПроцедуры

Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.Идентификатор = "ИНВПриложение18К";
	КомандаПечати.Представление = НСтр("ru = 'Инвентаризационная ведомость'");
	КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
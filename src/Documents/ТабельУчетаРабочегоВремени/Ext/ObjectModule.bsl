﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Подсистема "Управление доступом".

// Процедура ЗаполнитьНаборыЗначенийДоступа по свойствам объекта заполняет наборы значений доступа
// в таблице с полями:
//    НомерНабора     - Число                                     (необязательно, если набор один),
//    ВидДоступа      - ПланВидовХарактеристикСсылка.ВидыДоступа, (обязательно),
//    ЗначениеДоступа - Неопределено, СправочникСсылка или др.    (обязательно),
//    Чтение          - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Добавление      - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Изменение       - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Удаление        - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//
//  Вызывается из процедуры УправлениеДоступомСлужебный.ЗаписатьНаборыЗначенийДоступа(),
// если объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьНаборыЗначенийДоступа" и
// из таких же процедур объектов, у которых наборы значений доступа зависят от наборов этого
// объекта (в этом случае объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьЗависимыеНаборыЗначенийДоступа").
//
// Параметры:
//  Таблица      - ТабличнаяЧасть,
//                 РегистрСведенийНаборЗаписей.НаборыЗначенийДоступа,
//                 ТаблицаЗначений, возвращаемая УправлениеДоступом.ТаблицаНаборыЗначенийДоступа().
//
Процедура ЗаполнитьНаборыЗначенийДоступа(Таблица) Экспорт
	
	ЗарплатаКадры.ЗаполнитьНаборыПоОрганизацииИСотрудникам(ЭтотОбъект, Таблица, "Организация", "ДанныеОВремени.Сотрудник");
	
КонецПроцедуры

// Подсистема "Управление доступом".

#КонецОбласти

#Область ОбработчикиСобытий

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	// Подготовка к регистрации перерасчетов
	ДанныеДляРегистрацииПерерасчетов = Новый МенеджерВременныхТаблиц;
	
	СоздатьВТДанныеДокументов(ДанныеДляРегистрацииПерерасчетов);
	ЕстьПерерасчеты = ПерерасчетЗарплаты.СборДанныхДляРегистрацииПерерасчетов(Ссылка, ДанныеДляРегистрацииПерерасчетов, Организация);
	
	// Проведение документа
	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);
	ИсправлениеПериодическихСведений.ИсправлениеПериодическихСведений(ЭтотОбъект, Отказ, РежимПроведения);
	
	ПараметрыПолученияФО = Новый Структура("Организация", Организация);
	ИспользуютсяТеорритории = ПолучитьФункциональнуюОпцию("ИспользоватьОбособленныеТерритории", ПараметрыПолученияФО);
	ИспользуютсяУсловияТруда = ПолучитьФункциональнуюОпцию("ИспользоватьОсобыеУсловияТруда", ПараметрыПолученияФО);
	
	ДанныеДляПроведения = ТаблицаДанныхОВремени(ИспользуютсяТеорритории, ИспользуютсяУсловияТруда);
	УчетРабочегоВремени.ЗарегистрироватьДанныеТабеля(Движения, ДанныеДляПроведения, ПериодРегистрации);
	Если ЗначениеЗаполнено(ИсправленныйДокумент) Тогда
		УчетРабочегоВремени.ЗарегистрироватьСторноЗаписиПоДокументу(Движения, ПериодРегистрации, ИсправленныйДокумент, СотрудникиДокумента());
	КонецЕсли;
	
	Если ИспользуютсяТеорритории Или ИспользуютсяУсловияТруда Тогда
		УчетСтажаПФРРасширенный.ЗарегистрироватьПодатныеДанныеОТерриторияхИУсловиях(Движения, ДатаНачалаПериода, ДатаОкончанияПериода, ДанныеДляПроведения);
	КонецЕсли;	
	
	// Регистрация перерасчетов
	Если ЕстьПерерасчеты Тогда
		ПерерасчетЗарплаты.РегистрацияПерерасчетов(Движения, ДанныеДляРегистрацииПерерасчетов, Организация);
	КонецЕсли; 
	
КонецПроцедуры

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	Если ТипЗнч(ДанныеЗаполнения) = Тип("Структура") И ДанныеЗаполнения.Свойство("Действие") Тогда
		ЗаполнитьДокументИсправление(ДанныеЗаполнения);
	КонецЕсли;
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	ДопустимоеКоличествоНесоответствийПланФакт = 5;
	
	СтрокиПоСотрудникам = СтрокиПоСотрудникам();
	
	МенеджерВТ = Новый МенеджерВременныхТаблиц;
		
	ТаблицаДанныхОВремени = ТаблицаДанныхОВремени();
	
	УчетРабочегоВремени.СоздатьВТДанныеОВремени(МенеджерВТ, ТаблицаДанныхОВремени);
	
	ОписанияОшибокВводаВремени = Новый Массив;

	УчетРабочегоВремени.ПроверитьСоответствиеФактическогоВремениПлановому(МенеджерВТ, ОписанияОшибокВводаВремени, ПериодРегистрации, Отказ);
	
	Ошибки = Новый Соответствие;
	
	Для Каждого ОписаниеОшибки Из ОписанияОшибокВводаВремени Цикл
		СтрокиСотрудника = СтрокиПоСотрудникам.Получить(ОписаниеОшибки.Сотрудник);
		
		Если ЗначениеЗаполнено(ОписаниеОшибки.ВидВремени) Тогда
			НомерСтроки = 0;
			Для Каждого СтрокаДанныхОВремени Из СтрокиСотрудника Цикл
				Если СтрокаДанныхОВремени["ВидВремени" + День(ОписаниеОшибки.Дата)] = ОписаниеОшибки.ВидВремени Тогда
					НомерСтроки = СтрокаДанныхОВремени.НомерСтроки;
				КонецЕсли;	
			КонецЦикла;	
			
			Если НомерСтроки = 0 Тогда
				Поле = "";
			Иначе
				Поле = "Объект.ДанныеОВремени[" + Формат(НомерСтроки - 1, "ЧГ=") + "].Время" + День(ОписаниеОшибки.Дата) + "Представление";
			КонецЕсли;
			
			УчетРабочегоВремени.ДобавитьОшибкуПоСотруднику(Ошибки, ОписаниеОшибки.Сотрудник, ОписаниеОшибки.ТекстОшибки, Поле, ОписаниеОшибки.Документ);	
		КонецЕсли;	
		
	КонецЦикла;	
	
	Для Каждого ОшибкиПоСотруднику Из Ошибки Цикл
		Если ОшибкиПоСотруднику.Значение.Количество() > ДопустимоеКоличествоНесоответствийПланФакт Тогда
			ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru = 'По сотруднику %1 больше 5-ти несоответствий плановых видов времени фактическим.
                                                                                        |Рекомендуется ввести индивидуальный график.'"),
																				  ОшибкиПоСотруднику.Ключ);																			  													
																						
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстОшибки);
		КонецЕсли;	
		
		ВывестиОшибкиНесоответствияПланФакт = Истина;
	КонецЦикла;	
	
	ОписанияОшибокВводаВремени = Новый Массив;

	УчетРабочегоВремени.ПроверитьРегистрациюЦелосменногоВремени(МенеджерВТ, ОписанияОшибокВводаВремени, Отказ);     
	
	// 4D:ERP для Беларуси, Екатерина, 04.08.2020 15:13:51 
	// №26242 
	// {
	УчетРабочегоВремениРасширенный.ПроверитьЗаполненияЧасов(МенеджерВТ, ОписанияОшибокВводаВремени, Отказ);
	// }
	// 4D
			
	Для Каждого ОписаниеОшибки Из ОписанияОшибокВводаВремени Цикл
		СтрокиСотрудника = СтрокиПоСотрудникам.Получить(ОписаниеОшибки.Сотрудник);
		
		Если ЗначениеЗаполнено(ОписаниеОшибки.ВидВремени) Тогда
			НомерСтроки = 0;
			Для Каждого СтрокаДанныхОВремени Из СтрокиСотрудника Цикл
				Если СтрокаДанныхОВремени["ВидВремени" + День(ОписаниеОшибки.Дата)] = ОписаниеОшибки.ВидВремени Тогда
					НомерСтроки = СтрокаДанныхОВремени.НомерСтроки;
				КонецЕсли;	
			КонецЦикла;	
			
			Если НомерСтроки = 0 Тогда
				Поле = "";
			Иначе
				Поле = "Объект.ДанныеОВремени[" + Формат(НомерСтроки - 1, "ЧГ=") + "].Время" + День(ОписаниеОшибки.Дата) + "Представление";
			КонецЕсли;
			
			УчетРабочегоВремени.ДобавитьОшибкуПоСотруднику(Ошибки, ОписаниеОшибки.Сотрудник, ОписаниеОшибки.ТекстОшибки, Поле, ОписаниеОшибки.Документ);	
		КонецЕсли;	
		
	КонецЦикла;	
	
	ОписанияОшибокВводаВремени = Новый Массив;
	
	УчетРабочегоВремени.ПроверитьСоответствиеРегистрируемыхЧасовДлинеСуток(МенеджерВТ, ОписанияОшибокВводаВремени, Отказ);
	
	Для Каждого ОписаниеОшибки Из ОписанияОшибокВводаВремени Цикл				
		СтрокиСотрудника = СтрокиПоСотрудникам.Получить(ОписаниеОшибки.Сотрудник);
		
		НомерСтроки = СтрокиСотрудника[0].НомерСтроки;
		
		Поле = "Объект.ДанныеОВремени[" + Формат(НомерСтроки - 1, "ЧГ=") + "].Время" + День(ОписаниеОшибки.Дата) + "Представление";

		УчетРабочегоВремени.ДобавитьОшибкуПоСотруднику(Ошибки, ОписаниеОшибки.Сотрудник, ОписаниеОшибки.ТекстОшибки, Поле, ОписаниеОшибки.Документ);		
		
	КонецЦикла;	
	
	ОписанияОшибокВводаВремени = Новый Массив;
	
	УчетРабочегоВремени.ТабельПроверитьЦелостностьЗаполненияПериода(МенеджерВТ, Организация, Подразделение, ДатаНачалаПериода, ДатаОкончанияПериода, ОписанияОшибокВводаВремени, Отказ);
	
	Для Каждого ОписаниеОшибки Из ОписанияОшибокВводаВремени Цикл				
		СтрокиСотрудника = СтрокиПоСотрудникам.Получить(ОписаниеОшибки.Сотрудник);
		
		НомерСтроки = СтрокиСотрудника[0].НомерСтроки;
		
		Поле = "Объект.ДанныеОВремени[" + Формат(НомерСтроки - 1, "ЧГ=") + "].Время" + День(ОписаниеОшибки.Дата) + "Представление";

		УчетРабочегоВремени.ДобавитьОшибкуПоСотруднику(Ошибки, ОписаниеОшибки.Сотрудник, ОписаниеОшибки.ТекстОшибки, Поле, ОписаниеОшибки.Документ);		
		
	КонецЦикла;	
	
	ОписанияОшибокВводаВремени = Новый Массив;
		
	УчетРабочегоВремени.ПроверитьУникальностьВводаИтоговыхДанных(МенеджерВТ, Ссылка, ПериодРегистрации, ОписанияОшибокВводаВремени, Ложь);
	
	Для Каждого ОписаниеОшибки Из ОписанияОшибокВводаВремени Цикл				
		
		УчетРабочегоВремени.ДобавитьОшибкуПоСотруднику(Ошибки, ОписаниеОшибки.Сотрудник, ОписаниеОшибки.ТекстОшибки, , ОписаниеОшибки.Документ);		
		
	КонецЦикла;	
	
	УчетРабочегоВремени.ВывестиОшибкиПоСотрудникам(Ошибки, Отказ);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ТаблицаДанныхОВремени(ИспользуютсяТеорритории = Ложь, ИспользуютсяУсловияТруда = Ложь)	
	ТаблицаДанныхОВремени = Новый ТаблицаЗначений;
	ТаблицаДанныхОВремени.Колонки.Добавить("Дата", Новый ОписаниеТипов("Дата"));
	ТаблицаДанныхОВремени.Колонки.Добавить("Сотрудник", Новый ОписаниеТипов("СправочникСсылка.Сотрудники"));
	ТаблицаДанныхОВремени.Колонки.Добавить("ВидВремени", Новый ОписаниеТипов("СправочникСсылка.ВидыИспользованияРабочегоВремени"));
	ТаблицаДанныхОВремени.Колонки.Добавить("Дней", Новый ОписаниеТипов("Число"));
	ТаблицаДанныхОВремени.Колонки.Добавить("Часов", Новый ОписаниеТипов("Число"));
	ТаблицаДанныхОВремени.Колонки.Добавить("ВЦеломЗаПериод", Новый ОписаниеТипов("Булево"));
	ТаблицаДанныхОВремени.Колонки.Добавить("Территория", Новый ОписаниеТипов("СправочникСсылка.ТерриторииВыполненияРабот"));
	ТаблицаДанныхОВремени.Колонки.Добавить("УсловияТруда", Новый ОписаниеТипов("СправочникСсылка.УсловияТруда"));
	
	НомерДняНачалаПериода = День(ДатаНачалаПериода);
	НомерДняОкончанияПериода = День(ДатаОкончанияПериода);
	Для Каждого СтрокаДанныхОВремени Из ДанныеОВремени Цикл 
		ОбрабатываемаяДата = ДатаНачалаПериода;
		Для НомерДня = НомерДняНачалаПериода По НомерДняОкончанияПериода Цикл
			Если ЗначениеЗаполнено(СтрокаДанныхОВремени["ВидВремени" + НомерДня]) Тогда
				ДанныеПоВидуВремениНаДату = ТаблицаДанныхОВремени.Добавить();
				ДанныеПоВидуВремениНаДату.Дата = ОбрабатываемаяДата;
				ДанныеПоВидуВремениНаДату.Сотрудник = СтрокаДанныхОВремени.Сотрудник;
				ДанныеПоВидуВремениНаДату.ВидВремени = СтрокаДанныхОВремени["ВидВремени" + НомерДня];
				ДанныеПоВидуВремениНаДату.Дней = 1;
				ДанныеПоВидуВремениНаДату.Часов = СтрокаДанныхОВремени["Часов" + НомерДня];
				ДанныеПоВидуВремениНаДату.ВЦеломЗаПериод = Ложь;
				Если ИспользуютсяТеорритории Тогда
					ДанныеПоВидуВремениНаДату.Территория = СтрокаДанныхОВремени["Территория" + НомерДня];
				КонецЕсли;
				Если ИспользуютсяУсловияТруда Тогда
					ДанныеПоВидуВремениНаДату.УсловияТруда = СтрокаДанныхОВремени["УсловияТруда" + НомерДня];
				КонецЕсли;	
			КонецЕсли;	
			ОбрабатываемаяДата = ОбрабатываемаяДата + 86400;
		КонецЦикла;	
	КонецЦикла;	
	
	Возврат ТаблицаДанныхОВремени;
КонецФункции	

Процедура ЗаполнитьДокументИсправление(ДанныеЗаполнения)
	
	Запрос = Новый Запрос;
	
	Запрос.УстановитьПараметр("Ссылка", ДанныеЗаполнения.Ссылка);
	
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ТабельУчетаРабочегоВремени.Организация,
	|	ТабельУчетаРабочегоВремени.Подразделение,
	|	ТабельУчетаРабочегоВремени.ДатаНачалаПериода,
	|	ТабельУчетаРабочегоВремени.ДатаОкончанияПериода
	|ИЗ
	|	Документ.ТабельУчетаРабочегоВремени КАК ТабельУчетаРабочегоВремени
	|ГДЕ
	|	ТабельУчетаРабочегоВремени.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Сотрудник,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов1,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов2,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов3,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов4,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов5,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов6,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов7,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов8,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов9,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов10,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов11,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов12,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов13,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов14,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов15,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов16,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов17,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов18,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов19,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов20,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов21,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов22,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов23,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов24,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов25,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов26,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов27,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов28,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов29,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов30,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Часов31,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени1,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени2,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени3,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени4,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени5,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени6,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени7,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени8,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени9,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени10,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени11,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени12,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени13,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени14,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени15,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени16,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени17,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени18,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени19,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени20,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени21,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени22,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени23,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени24,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени25,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени26,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени27,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени28,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени29,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени30,
	|	ТабельУчетаРабочегоВремениДанныеОВремени.ВидВремени31
	|ИЗ
	|	Документ.ТабельУчетаРабочегоВремени.ДанныеОВремени КАК ТабельУчетаРабочегоВремениДанныеОВремени
	|ГДЕ
	|	ТабельУчетаРабочегоВремениДанныеОВремени.Ссылка = &Ссылка
	|	И &Условие";
	
	ЗаполнятьПоСпискуСотрудников = Ложь;
	Если ДанныеЗаполнения.Свойство("Сотрудники") Тогда
		ТекстУсловия = "ТабельУчетаРабочегоВремениДанныеОВремени.Сотрудник В (&Сотрудники)";
		Запрос.УстановитьПараметр("Сотрудники", ДанныеЗаполнения.Сотрудники);
		ЗаполнятьПоСпискуСотрудников = Истина;
	Иначе
		ТекстУсловия = "ИСТИНА";
	КонецЕсли;
	
	Запрос.Текст = СтрЗаменить(Запрос.Текст, "&Условие", ТекстУсловия); 
	
	Результаты = Запрос.ВыполнитьПакет();
	
	ДанныеШапки = Результаты[0].Выбрать();
	
	Если ДанныеШапки.Следующий() Тогда
		
		ЗаполнитьЗначенияСвойств(ЭтотОбъект, ДанныеШапки);
		
		Дата = ТекущаяДатаСеанса();
		
		ПериодРегистрации = Макс(НачалоМесяца(ТекущаяДатаСеанса()), КонецМесяца(ДанныеШапки.ДатаНачалаПериода) + 1);
		
		ИсправленныйДокумент = ДанныеЗаполнения.Ссылка;
		
		ПериодВводаДанныхОВремени = Перечисления.ПериодыВводаДанныхОВремени.ПрошлыйПериод;
		
		ДанныеОВремени.Загрузить(Результаты[1].Выгрузить());
		
	КонецЕсли;
	
КонецПроцедуры

Функция СтрокиПоСотрудникам()
	СтрокиПоСотрудникам = Новый Соответствие;
	Для Каждого СтрокаДанных Из ДанныеОВремени Цикл
		МассивСтрок = СтрокиПоСотрудникам.Получить(СтрокаДанных.Сотрудник);
		Если МассивСтрок = Неопределено Тогда
			МассивСтрок = Новый Массив;
			СтрокиПоСотрудникам.Вставить(СтрокаДанных.Сотрудник, МассивСтрок);
		КонецЕсли;	
		МассивСтрок.Добавить(СтрокаДанных);
	КонецЦикла;	
	
	Возврат СтрокиПоСотрудникам;
КонецФункции	

Функция СотрудникиДокумента()
	Сотрудники = Новый Массив;
	
	Для Каждого СтрокаДанныхОВремени Из ДанныеОВремени Цикл
		Если Сотрудники.Найти(СтрокаДанныхОВремени.Сотрудник) = Неопределено Тогда
			Сотрудники.Добавить(СтрокаДанныхОВремени.Сотрудник);
		КонецЕсли;
	КонецЦикла;	
	
	Возврат Сотрудники;
КонецФункции

Процедура СоздатьВТДанныеДокументов(МенеджерВременныхТаблиц)
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("Регистратор", Ссылка);
	
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ТаблицаДокумента.Ссылка.Организация КАК Организация,
		|	ТаблицаДокумента.Сотрудник,
		|	НАЧАЛОПЕРИОДА(ТаблицаДокумента.Ссылка.ДатаНачалаПериода, МЕСЯЦ) КАК ПериодДействия,
		|	ТаблицаДокумента.Ссылка КАК ДокументОснование
		|ПОМЕСТИТЬ ВТДанныеДокументов
		|ИЗ
		|	Документ.ТабельУчетаРабочегоВремени.ДанныеОВремени КАК ТаблицаДокумента
		|ГДЕ
		|	ТаблицаДокумента.Ссылка = &Регистратор
		|
		|ОБЪЕДИНИТЬ ВСЕ
		|
		|ВЫБРАТЬ
		|	ТаблицаДокумента.Ссылка.Организация,
		|	ТаблицаДокумента.Сотрудник,
		|	НАЧАЛОПЕРИОДА(ТаблицаДокумента.Ссылка.ДатаОкончанияПериода, МЕСЯЦ),
		|	ТаблицаДокумента.Ссылка
		|ИЗ
		|	Документ.ТабельУчетаРабочегоВремени.ДанныеОВремени КАК ТаблицаДокумента
		|ГДЕ
		|	ТаблицаДокумента.Ссылка = &Регистратор
		|	И ТаблицаДокумента.Ссылка.ДатаОкончанияПериода <> ДАТАВРЕМЯ(1, 1, 1)";
		
	Запрос.Выполнить();
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда


#Область ПрограммныйИнтерфейс

// СтандартныеПодсистемы.ВерсионированиеОбъектов

// Определяет настройки объекта для подсистемы ВерсионированиеОбъектов.
//
// Параметры:
//  Настройки - Структура - настройки подсистемы.
Процедура ПриОпределенииНастроекВерсионированияОбъектов(Настройки) Экспорт

КонецПроцедуры

// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов

// Сторнирует документ по учетам. Используется подсистемой исправления документов.
//
// Параметры:
//  Движения				 - КоллекцияДвижений, Структура	 - Коллекция движений исправляющего документа в которую будут добавлены сторно стоки.
//  Регистратор				 - ДокументСсылка				 - Документ регистратор исправления (документ исправление).
//  ИсправленныйДокумент	 - ДокументСсылка				 - Исправленный документ движения которого будут сторнированы.
//  СтруктураВидовУчета		 - Структура					 - Виды учета, по которым будет выполнено сторнирование исправленного документа.
//  					Состав полей см. в ПроведениеРасширенныйСервер.СтруктураВидовУчета().
//  ДополнительныеПараметры	 - Структура					 - Структура со свойствами:
//  					* ИсправлениеВТекущемПериоде - Булево - Истина когда исправление выполняется в периоде регистрации исправленного документа.
//						* ОтменаДокумента - Булево - Истина когда исправление вызвано документом СторнированиеНачислений.
//  					* ПериодРегистрации	- Дата - Период регистрации документа регистратора исправления.
// 
// Возвращаемое значение:
//  Булево - "Истина" если сторнирование выполнено этой функцией, "Ложь" если специальной процедуры не предусмотрено.
//
Функция СторнироватьПоУчетам(Движения, Регистратор, ИсправленныйДокумент, СтруктураВидовУчета, ДополнительныеПараметры) Экспорт
	
	Если ДополнительныеПараметры.ОтменаДокумента Тогда
		// При отмене документа реквизиты для проведения сформированы документом СторнированиеНачислений, их структура отличается
		// от структуры реквизитов для проведения исправленного документа. Получаем реквизиты для проведения исправленного документа.
		РеквизитыДляПроведения = РеквизитыДляПроведения(ИсправленныйДокумент);
		РеквизитыДляПроведения.ПериодРегистрации = ДополнительныеПараметры.ПериодРегистрации;
		
	Иначе
		РеквизитыДляПроведения = ДополнительныеПараметры.РеквизитыДляПроведения;
	КонецЕсли;
	
	Если СтруктураВидовУчета.ОстальныеВидыУчета Тогда
		
		Сотрудники = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(РеквизитыДляПроведения.Сотрудник);
		
		Если Не РеквизитыДляПроведения.ОсвобождатьСтавку Тогда
			УправлениеШтатнымРасписанием.СторнироватьДвиженияДокумента(Движения, ИсправленныйДокумент);
		КонецЕсли;
		
		УчетРабочегоВремениРасширенный.ЗарегистрироватьСторноЗаписиПоДокументу(Движения, РеквизитыДляПроведения.ПериодРегистрации, ИсправленныйДокумент, Сотрудники);
	КонецЕсли;
	
	Если ДополнительныеПараметры.ОтменаДокумента Или ДополнительныеПараметры.ИсправлениеВТекущемПериоде Тогда
		
		Если СтруктураВидовУчета.ДанныеДляРасчетаСреднего Тогда
			УчетСреднегоЗаработка.СторнироватьДвиженияДокумента(Движения, ИсправленныйДокумент);
		КонецЕсли;
		
		Если СтруктураВидовУчета.ОстальныеВидыУчета Тогда
			РасчетЗарплатыРасширенный.СторнироватьДвиженияДокумента(Движения, ИсправленныйДокумент, ДополнительныеПараметры);
			УчетНачисленнойЗарплатыРасширенный.СторнироватьДвиженияДокумента(Движения, ИсправленныйДокумент, ДополнительныеПараметры);
			ОтражениеЗарплатыВБухучетеРасширенный.СторнироватьДвиженияДокумента(Движения, ИсправленныйДокумент);
			УчетНДФЛРасширенный.СторнироватьДвиженияДокумента(Движения, ИсправленныйДокумент, ДополнительныеПараметры);
			УчетСтраховыхВзносовРасширенный.СторнироватьДвиженияДокумента(Движения, ИсправленныйДокумент, ДополнительныеПараметры);
			
			ИсправлениеДокументовЗарплатаКадры.СторнироватьДвиженияБезСпецификиУчетов(
				Движения, ИсправленныйДокумент, ДополнительныеПараметры);
			
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат Истина;
	
КонецФункции

#Область ДляВызоваИзДругихПодсистем

// СтандартныеПодсистемы.УправлениеДоступом

// См. УправлениеДоступомПереопределяемый.ПриЗаполненииСписковСОграничениемДоступа.
Процедура ПриЗаполненииОграниченияДоступа(Ограничение) Экспорт
	Ограничение.Текст =
	"РазрешитьЧтениеИзменение
	|ГДЕ
	|	ЗначениеРазрешено(Организация)
	|	И ЗначениеРазрешено(ФизическоеЛицо)";
КонецПроцедуры

// Конец СтандартныеПодсистемы.УправлениеДоступом

#КонецОбласти

#КонецОбласти
	
#Область СлужебныйПрограммныйИнтерфейс


// Проводит документ по учетам. Если в параметре ВидыУчетов передано Неопределено, то документ проводится по всем учетам.
// Процедура вызывается из обработки проведения и может вызываться из вне.
// 
// Параметры:
//  ДокументСсылка	- ДокументСсылка.ОтпускБезСохраненияОплаты - Ссылка на документ
//  РежимПроведения - РежимПроведенияДокумента - Режим проведения документа (оперативный, неоперативный)
//  Отказ 			- Булево - Признак отказа от выполнения проведения
//  ВидыУчетов 		- Строка - Список видов учета, по которым необходимо провести документ. Если параметр пустой или Неопределено, то документ проведется по всем учетам
//  Движения 		- Коллекция движений документа - Передается только при вызове из обработки проведения документа
//  Объект			- ДокументОбъект.ОтпускБезСохраненияОплаты - Передается только при вызове из обработки проведения документа
//  ДополнительныеПараметры - Структура - Дополнительные параметры, необходимые для проведения документа
//
Процедура ПровестиПоУчетам(ДокументСсылка, РежимПроведения, Отказ, ВидыУчетов = Неопределено, Движения = Неопределено, Объект = Неопределено, ДополнительныеПараметры = Неопределено) Экспорт
	
	СтруктураВидовУчета = ПроведениеРасширенныйСервер.СтруктураВидовУчета();
	ПроведениеРасширенныйСервер.ПодготовитьНаборыЗаписейКРегистрацииДвиженийПоВидамУчета(РежимПроведения, ДокументСсылка, СтруктураВидовУчета, ВидыУчетов, Движения, Объект, Отказ);
	
	РеквизитыДляПроведения = РеквизитыДляПроведения(ДокументСсылка);
	ДанныеДляПроведения = ДанныеДляПроведения(РеквизитыДляПроведения);
			
	
	Если РеквизитыДляПроведения.ПерерасчетВыполнен Тогда
		
				
		Если СтруктураВидовУчета.ДанныеДляРасчетаСреднего Тогда
			
			// Учет среднего заработка
			УчетСреднегоЗаработка.ЗарегистрироватьДанныеСреднегоЗаработка(Движения, Отказ, ДанныеДляПроведения.НачисленияДляСреднегоЗаработка);
			
		КонецЕсли;
		
	КонецЕсли;
	
		
	ПроведениеРасширенныйСервер.ВыполнитьЗапланированныеКорректировкиДвижений(Движения);
	
	ПроведениеРасширенныйСервер.ЗаписьДвиженийПоУчетам(Движения, СтруктураВидовУчета);
	
КонецПроцедуры

Функция ДанныеДляПроведения(РеквизитыДляПроведения)
	
	ДанныеДляПроведения = РасчетЗарплаты.СоздатьДанныеДляПроведенияНачисленияЗарплаты();
	
	РасчетЗарплатыРасширенный.ЗаполнитьНачисления(ДанныеДляПроведения, РеквизитыДляПроведения.Ссылка, "Начисления,НачисленияПерерасчет", "Ссылка.ПериодРегистрации");
	РасчетЗарплатыРасширенный.ЗаполнитьСписокФизическихЛиц(ДанныеДляПроведения, РеквизитыДляПроведения.Ссылка);
	
	ОтражениеЗарплатыВБухучете.ДополнитьНачисленияДаннымиОЕНВД(ДанныеДляПроведения, РеквизитыДляПроведения.Ссылка, РеквизитыДляПроведения.ПериодРегистрации, РеквизитыДляПроведения.Организация);
	
	УчетСреднегоЗаработка.ЗаполнитьТаблицыДляРегистрацииДанныхСреднегоЗаработка(ДанныеДляПроведения, РеквизитыДляПроведения.Ссылка, РеквизитыДляПроведения.ПериодРегистрации, "Начисления,НачисленияПерерасчет", "ДатаНачала");

	Если РеквизитыДляПроведения.ОсвобождатьСтавку Тогда
		КадровыйУчетРасширенный.ЗаполнитьПериодыОсвобожденияСтавки(ДанныеДляПроведения, ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(РеквизитыДляПроведения.Сотрудник), РеквизитыДляПроведения.ДатаНачала, КонецДня(РеквизитыДляПроведения.ДатаОкончания) + 1);
		
		Если ЗначениеЗаполнено(РеквизитыДляПроведения.ИсправленныйДокумент) Тогда
			ДанныеИсправленногоДокумента = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(РеквизитыДляПроведения.ИсправленныйДокумент, "ФизическоеЛицо,Организация,Сотрудник,ДатаНачала,ДатаОкончания");
			КадровыйУчетРасширенный.ЗаполнитьПериодыОсвобожденияСтавки(ДанныеДляПроведения, ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(ДанныеИсправленногоДокумента.Сотрудник), 
																				ДанныеИсправленногоДокумента.ДатаНачала, КонецДня(ДанныеИсправленногоДокумента.ДатаОкончания) + 1, Истина);
		КонецЕсли;

	КонецЕсли;
	
	Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ГосударственнаяСлужба.РасчетДенежногоСодержания") Тогда
		Модуль = ОбщегоНазначения.ОбщийМодуль("РасчетДенежногоСодержания");
		НачисленияДляРегистрацииДенежногоСодержания = Модуль.СведенияОНачисленияхДляРегистрацииДенежногоСодержанияДокумента(РеквизитыДляПроведения.Ссылка, "Начисления,НачисленияПерерасчет");
		ДанныеДляПроведения.Вставить("НачисленияДляРегистрацииДенежногоСодержания", НачисленияДляРегистрацииДенежногоСодержания);
	КонецЕсли;
	
	Возврат ДанныеДляПроведения;
	
КонецФункции

Функция РеквизитыДляПроведения(ДокументСсылка)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ОтпускБезСохраненияОплаты.Ссылка КАК Ссылка,
	|	ОтпускБезСохраненияОплаты.Организация КАК Организация,
	|	ОтпускБезСохраненияОплаты.ПериодРегистрации КАК ПериодРегистрации,
	|	ОтпускБезСохраненияОплаты.Дата КАК Дата,
	|	ОтпускБезСохраненияОплаты.ПерерасчетВыполнен КАК ПерерасчетВыполнен,
	|	ОтпускБезСохраненияОплаты.ОтсутствиеВТечениеЧастиСмены КАК ОтсутствиеВТечениеЧастиСмены,
	|	ОтпускБезСохраненияОплаты.Сотрудник КАК Сотрудник,
	|	ОтпускБезСохраненияОплаты.ВидОтпуска КАК ВидОтпуска,
	|	ОтпускБезСохраненияОплаты.ДатаНачала КАК ДатаНачала,
	|	ОтпускБезСохраненияОплаты.ДатаОкончания КАК ДатаОкончания,
	|	ОтпускБезСохраненияОплаты.ОсвобождатьСтавку КАК ОсвобождатьСтавку,
	|	ОтпускБезСохраненияОплаты.ИсправленныйДокумент КАК ИсправленныйДокумент,
	|	ОтпускБезСохраненияОплаты.Номер КАК Номер,
	|	ОтпускБезСохраненияОплаты.ДатаОтсутствия КАК ДатаОтсутствия,
	|	ОтпускБезСохраненияОплаты.ВидРасчета КАК ВидРасчета,
	|	ОтпускБезСохраненияОплаты.ВидВремениЗамещаемый КАК ВидВремениЗамещаемый,
	|	ОтпускБезСохраненияОплаты.ЧасовОтпуска КАК ЧасовОтпуска,
	|	ОтпускБезСохраненияОплаты.ДокументЗаполнения КАК ДокументЗаполнения,
	|	ОтпускБезСохраненияОплаты.ДокументЗаполнения.Номер КАК НомерДокументаЗаполнения,
	|	ОтпускБезСохраненияОплаты.ДокументЗаполнения.Дата КАК ДатаДокументаЗаполнения,
	|	ОтпускБезСохраненияОплаты.ФизическоеЛицо КАК ФизическоеЛицо
	|ИЗ
	|	Документ.ОтпускБезСохраненияОплаты КАК ОтпускБезСохраненияОплаты
	|ГДЕ
	|	ОтпускБезСохраненияОплаты.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ОтпускБезСохраненияОплатыРаспределениеПоТерриториямУсловиямТруда.НомерСтроки КАК НомерСтроки,
	|	ОтпускБезСохраненияОплатыРаспределениеПоТерриториямУсловиямТруда.ИдентификаторСтроки КАК ИдентификаторСтроки,
	|	ОтпускБезСохраненияОплатыРаспределениеПоТерриториямУсловиямТруда.Территория КАК Территория,
	|	ОтпускБезСохраненияОплатыРаспределениеПоТерриториямУсловиямТруда.УсловияТруда КАК УсловияТруда,
	|	ОтпускБезСохраненияОплатыРаспределениеПоТерриториямУсловиямТруда.ДоляРаспределения КАК ДоляРаспределения,
	|	ОтпускБезСохраненияОплатыРаспределениеПоТерриториямУсловиямТруда.Результат КАК Результат,
	|	ОтпускБезСохраненияОплатыРаспределениеПоТерриториямУсловиямТруда.ИдентификаторСтрокиПоказателей КАК ИдентификаторСтрокиПоказателей
	|ИЗ
	|	Документ.ОтпускБезСохраненияОплаты.РаспределениеПоТерриториямУсловиямТруда КАК ОтпускБезСохраненияОплатыРаспределениеПоТерриториямУсловиямТруда
	|ГДЕ
	|	ОтпускБезСохраненияОплатыРаспределениеПоТерриториямУсловиямТруда.Ссылка = &Ссылка";
	
	Запрос.УстановитьПараметр("Ссылка", ДокументСсылка);
	Результаты = Запрос.ВыполнитьПакет();
	
	РеквизитыДляПроведения = РеквизитыДляПроведенияПустаяСтруктура();
	
	ВыборкаРеквизиты = Результаты[0].Выбрать();
	
	Пока ВыборкаРеквизиты.Следующий() Цикл
		
		ЗаполнитьЗначенияСвойств(РеквизитыДляПроведения, ВыборкаРеквизиты);
		
	КонецЦикла;
	
	РаспределениеПоТерриториямУсловиямТруда = Результаты[1].Выгрузить();
	
	РеквизитыДляПроведения.РаспределениеПоТерриториямУсловиямТруда = РаспределениеПоТерриториямУсловиямТруда;
	
	Возврат РеквизитыДляПроведения;
	
КонецФункции

Функция РеквизитыДляПроведенияПустаяСтруктура()
	
	РеквизитыДляПроведенияПустаяСтруктура = Новый Структура(
		"Ссылка,
		|Организация,
		|ПериодРегистрации,
		|Дата,
		|ПланируемаяДатаВыплаты,
		|ПерерасчетВыполнен,
		|ОтсутствиеВТечениеЧастиСмены,
		|Сотрудник,
		|ВидОтпуска,
		|ДатаНачала,
		|ДатаОкончания,
		|ОсвобождатьСтавку,
		|ИсправленныйДокумент,
		|Номер,
		|ДатаОтсутствия,
		|ВидРасчета,
		|ВидВремениЗамещаемый,
		|ЧасовОтпуска,
		|РаспределениеПоТерриториямУсловиямТруда,
		|ДокументЗаполнения,
		|НомерДокументаЗаполнения,
		|ДатаДокументаЗаполнения,
		|ФизическоеЛицо");
	
	Возврат РеквизитыДляПроведенияПустаяСтруктура;
	
КонецФункции

// Возвращает описание состава документа
//
// Возвращаемое значение:
//  Структура - см. ЗарплатаКадрыСоставДокументов.НовоеОписаниеСоставаДокумента
Функция ОписаниеСоставаОбъекта() Экспорт
	
	МетаданныеДокумента = Метаданные.Документы.ОтпускБезСохраненияОплаты;
	Возврат ЗарплатаКадрыСоставДокументов.ОписаниеСоставаОбъектаФизическоеЛицоВШапке();
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ДобавитьКомандыСозданияДокументов(КомандыСозданияДокументов, ДополнительныеПараметры) Экспорт
	
	ЗарплатаКадрыРасширенный.ДобавитьВКоллекциюКомандуСозданияДокументаПоМетаданнымДокумента(
		КомандыСозданияДокументов, Метаданные.Документы.ОтпускБезСохраненияОплаты);
	
КонецФункции

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати.
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
	// Приказ о предоставлении отпуска (Т-6).
	КадровыйУчет.ДобавитьКомандуПечатиПриказаОПредоставленииОтпуска(КомандыПечати);
	
КонецПроцедуры

Функция ТекстСообщенияНеЗаполненВидРасчета(ВидОтпуска, ВнутрисменныйОтпуск) Экспорт
	ТекстСообщения = НСтр("ru = 'Не найдено ни одного начисления для регистрации %1""%2""'");
		
	Возврат СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, ?(ВнутрисменныйОтпуск, НСтр("ru = 'внутрисменного '"), ""), ВидОтпуска);	
КонецФункции

Функция ПолныеПраваНаДокумент() Экспорт 
	
	Возврат Пользователи.РолиДоступны("ДобавлениеИзменениеНачисленнойЗарплатыРасширенная, ЧтениеНачисленнойЗарплатыРасширенная", , Ложь);
	
КонецФункции	

Функция ДанныеДляПроверкиОграниченийНаУровнеЗаписей(Объект) Экспорт 

	ФизическоеЛицо = ?(ЗначениеЗаполнено(Объект.Сотрудник), ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Объект.Сотрудник, "ФизическоеЛицо"), Справочники.ФизическиеЛица.ПустаяСсылка());
	
	ДанныеДляПроверкиОграничений = ЗарплатаКадрыРасширенный.ОписаниеСтруктурыДанныхДляПроверкиОграниченийНаУровнеЗаписей();
	
	ДанныеДляПроверкиОграничений.Организация = Объект.Организация;
	ДанныеДляПроверкиОграничений.МассивФизическихЛиц = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(ФизическоеЛицо);
	
	Возврат ДанныеДляПроверкиОграничений;
	
КонецФункции

Функция ДанныеДляРегистрацииВУчетаСтажаФСЗН(МассивСсылок) Экспорт
	ДанныеДляРегистрацииВУчете = Новый Соответствие;
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("МассивСсылок", МассивСсылок);
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ОтпускБезСохраненияОплаты.Сотрудник КАК Сотрудник,
	|	ОтпускБезСохраненияОплаты.ВидОтпуска КАК ВидОтпуска,
	|	ЗНАЧЕНИЕ(Справочник.ВидыДеятельности.НеВключаетсяВСтраховойСтаж) КАК ВидДеятельности,
	|	ОтпускБезСохраненияОплаты.ДатаНачала КАК ДатаНачала,
	|	ОтпускБезСохраненияОплаты.ДатаОкончания КАК ДатаОкончания,
	|	ОтпускБезСохраненияОплаты.ОтсутствиеВТечениеЧастиСмены КАК ОтсутствиеВТечениеЧастиСмены,
	|	ОтпускБезСохраненияОплаты.Ссылка КАК Ссылка,
	|	ОтпускБезСохраненияОплаты.Организация КАК Организация
	|ИЗ
	|	Документ.ОтпускБезСохраненияОплаты КАК ОтпускБезСохраненияОплаты
	|ГДЕ
	|	ОтпускБезСохраненияОплаты.Ссылка В(&МассивСсылок)";
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		ДанныеДляРегистрацииВУчетеПоДокументу = УчетСтажаФСЗН.ДанныеДляРегистрацииВУчетеСтажаФСЗН();
		ДанныеДляРегистрацииВУчете.Вставить(Выборка.Ссылка, ДанныеДляРегистрацииВУчетеПоДокументу);
		
		Если Не Выборка.ОтсутствиеВТечениеЧастиСмены Тогда			
			Если ЗначениеЗаполнено(Выборка.ВидДеятельности) Тогда
				ОписаниеПериода = УчетСтажаФСЗН.ОписаниеРегистрируемогоПериода();
				ОписаниеПериода.Сотрудник = Выборка.Сотрудник;	
				ОписаниеПериода.ДатаНачалаПериода = Выборка.ДатаНачала;
				ОписаниеПериода.ДатаОкончанияПериода = Выборка.ДатаОкончания;
				ОписаниеПериода.Состояние = СостоянияСотрудников.СостояниеПоВидуОтпуска(Выборка.ВидОтпуска);

				РегистрируемыйПериод = УчетСтажаФСЗН.ДобавитьЗаписьВДанныеДляРегистрацииВУчета(ДанныеДляРегистрацииВУчетеПоДокументу, ОписаниеПериода);
								
				УчетСтажаФСЗН.УстановитьЗначениеРегистрируемогоРесурса(РегистрируемыйПериод, "ВидДеятельности", Выборка.ВидДеятельности);
				УчетСтажаФСЗН.УстановитьЗначениеРегистрируемогоРесурса(РегистрируемыйПериод, "Организация", Выборка.Организация);
			КонецЕсли;
		КонецЕсли;	
	КонецЦикла;
	
	Возврат ДанныеДляРегистрацииВУчете;
														
КонецФункции	

#КонецОбласти


#Область ПараметрыВыбораНачислений

Функция ДополнительныеПараметрыВыбораНачислений(Документ, ПутьКРеквизиту) Экспорт
	Результат = Новый Соответствие;
	
	Если ПутьКРеквизиту = "ВидРасчета" Тогда
		ВидВремени = Перечисления.ВидыРабочегоВремениСотрудников.ВидВремениДокументовОтклонений(Документ.ОтсутствиеВТечениеЧастиСмены);
	КонецЕсли;
	
	Результат.Вставить("Отбор.ВидВремени", ВидВремени);
	
	Возврат Результат;
КонецФункции

#КонецОбласти


#КонецЕсли
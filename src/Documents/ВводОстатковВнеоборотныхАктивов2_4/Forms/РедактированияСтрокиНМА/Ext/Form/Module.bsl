﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	СохраняемыеРеквизиты  = Параметры.СохраняемыеРеквизиты;
	ХозяйственнаяОперация = Параметры.ХозяйственнаяОперация;
	Организация = Параметры.Организация;
	Местонахождение = Параметры.Местонахождение;
	ОтражатьВУпрУчете = Параметры.ОтражатьВУпрУчете;
	ОтражатьВРеглУчете = Параметры.ОтражатьВРеглУчете;
	ОтражатьВБУиНУ = Параметры.ОтражатьВБУиНУ;
	ОтражатьВОперативномУчете = Параметры.ОтражатьВОперативномУчете;
	ОтражатьВУУ = Параметры.ОтражатьВУУ;
	Дата = Параметры.Дата;
	Ссылка = Параметры.Ссылка;
	
	Если Параметры.Свойство("ЗначенияРеквизитов") Тогда
		
		ЗаполнитьЗначенияСвойств(ЭтаФорма, Параметры.ЗначенияРеквизитов);
		
		РезервПереоценкиСтоимостиСумма = ?(РезервПереоценкиСтоимости < 0, -РезервПереоценкиСтоимости, РезервПереоценкиСтоимости);
		РезервПереоценкиАмортизацииСумма = ?(РезервПереоценкиАмортизации < 0, -РезервПереоценкиАмортизации, РезервПереоценкиАмортизации);
		РезервПереоценкиЗнак = (РезервПереоценкиСтоимости > 0);
		
	КонецЕсли;
	
	ПриЧтенииСозданииНаСервере();
		
	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	Если Не ЗавершениеРаботы И Модифицированность Тогда
		
		Отказ = Истина;
		
		ПоказатьВопрос(
			Новый ОписаниеОповещения("ПередЗакрытиемЗавершение", ЭтаФорма),
			НСтр("ru = 'Данные были изменены. Сохранить изменения?';
				|en = 'Data has been changed. Save the changes?'"),
			РежимДиалогаВопрос.ДаНетОтмена);
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	ВспомогательныеРеквизиты = Документы.ВводОстатковВнеоборотныхАктивов2_4.ВспомогательныеРеквизиты(ЭтаФорма, Истина);
										
	ПараметрыРеквизитовОбъекта = ВнеоборотныеАктивыКлиентСервер.ЗначенияСвойствЗависимыхРеквизитов_ВводОстатков(ЭтаФорма, ВспомогательныеРеквизиты);
	
	ВнеоборотныеАктивыСлужебный.ОтключитьПроверкуЗаполненияРеквизитовОбъекта(ПараметрыРеквизитовОбъекта, МассивНепроверяемыхРеквизитов);
	
	ПроверитьЗаполнениеАналитик(ПараметрыРеквизитовОбъекта, МассивНепроверяемыхРеквизитов, Отказ);
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "Запись_НематериальныеАктивы" И Источник = НематериальныйАктив Тогда
		ЗаполнитьСведенияНМА();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

#Область СтраницаУчет

&НаКлиенте
Процедура НематериальныйАктивПриИзменении(Элемент)
	
	НематериальныйАктивПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПорядокУчетаУУПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы(Элемент);
	
	ПересчитатьЗависимыеСуммы(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ГруппаФинансовогоУчетаПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы(Элемент);
	
КонецПроцедуры

#КонецОбласти

#Область СтраницаСтоимость

&НаКлиенте
Процедура ТекущаяСтоимостьУУПриИзменении(Элемент)
	
	ПересчитатьЗависимыеСуммы(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура НакопленнаяАмортизацияУУПриИзменении(Элемент)
	
	ПересчитатьЗависимыеСуммы(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ПервоначальнаяСтоимостьУУПриИзменении(Элемент)
	
	ПересчитатьЗависимыеСуммы(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ПервоначальнаяСтоимостьОтличаетсяПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы(Элемент);
	
КонецПроцедуры

#КонецОбласти

#Область СтраницаСобытия

&НаКлиенте
Процедура ЕстьРезервПереоценкиПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы(Элемент);
	
КонецПроцедуры

&НаКлиенте
Процедура РезервПереоценкиСтоимостиСуммаПриИзменении(Элемент)
	
	Если ТекущаяСтоимостьУУ <> 0 И РезервПереоценкиСтоимостиСумма <> 0 Тогда
		РезервПереоценкиАмортизацииСумма = 
			НакопленнаяАмортизацияУУ
			* (РезервПереоценкиСтоимостиСумма / ТекущаяСтоимостьУУ);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СтраницаАмортизация

&НаКлиенте
Процедура СрокИспользованияУУПриИзменении(Элемент)
	
	ПриИзмененииСрокаИспользования("СрокИспользованияУУ", Ложь);
	
КонецПроцедуры

&НаКлиенте
Процедура МетодНачисленияАмортизацииУУПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы(Элемент);
	
КонецПроцедуры

#КонецОбласти

#Область СтраницаОтражениеРасходов

&НаКлиенте
Процедура СтатьяРасходовУУПриИзменении(Элемент)
	
	СтатьяРасходовУУПриИзмененииНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ЛокализацияБел

&НаКлиенте
Процедура Подключаемый_ПриИзмененииРеквизита(Элемент)

	ВводОстатковВнеоборотныхАктивовКлиентЛокализация.ФормаРедактированияСтрокиНМА_ПриИзмененииРеквизита(
		Элемент, ЭтаФорма);

КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗаписатьИЗакрыть(Команда)
	
	ЗавершитьРедактирование();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПриИзмененииРеквизитов

&НаКлиенте
Процедура Подключаемый_ПриИзмененииРеквизитаЗавершение(ИмяЭлемента, ДополнительныеПараметры) Экспорт

	Перем ПараметрыДействия;
	
	Если ДополнительныеПараметры.ТребуетсяВызовСервера Тогда
		ПриИзмененииРеквизитаЗавершениеНаСервере(ИмяЭлемента, ДополнительныеПараметры);
	КонецЕсли;
	
	Если ДополнительныеПараметры.Свойство("Выполнить_НастроитьЗависимыеЭлементыФормы", ПараметрыДействия) Тогда
		НастроитьЗависимыеЭлементыФормы(ПараметрыДействия);
	КонецЕсли;
	Если ДополнительныеПараметры.Свойство("Выполнить_ПересчитатьЗависимыеСуммы") Тогда
		ПересчитатьЗависимыеСуммы(ЭтаФорма);
	КонецЕсли;
	Если ДополнительныеПараметры.Свойство("Выполнить_ПриИзмененииСрокаИспользования", ПараметрыДействия) Тогда
		ПриИзмененииСрокаИспользования(ПараметрыДействия.ИмяРеквизита, ПараметрыДействия.ОбновитьЕслиСовпадают);
	КонецЕсли;

КонецПроцедуры

&НаСервере
Процедура ПриИзмененииРеквизитаЗавершениеНаСервере(Знач ИмяЭлемента, Знач ДополнительныеПараметры)

	Перем ПараметрыДействия;
	
	ВводОстатковВнеоборотныхАктивовЛокализация.ФормаРедактированияСтрокиНМА_ПриИзмененииРеквизита(
		ИмяЭлемента, ЭтаФорма, ДополнительныеПараметры);

	Если ДополнительныеПараметры.Свойство("Выполнить_НастроитьЗависимыеЭлементыФормы", ПараметрыДействия) Тогда
		НастроитьЗависимыеЭлементыФормыНаСервере(ПараметрыДействия);
	КонецЕсли;
	Если ДополнительныеПараметры.Свойство("Выполнить_ПересчитатьЗависимыеСуммы") Тогда
		ПересчитатьЗависимыеСуммы(ЭтаФорма);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура НастроитьЗависимыеЭлементыФормы(Знач ИзмененныеРеквизитыИлиЭлемент = "")

	Если ТипЗнч(ИзмененныеРеквизитыИлиЭлемент) = Тип("Строка") Тогда
		ИзмененныеРеквизиты = ИзмененныеРеквизитыИлиЭлемент;
	Иначе
		ИзмененныеРеквизиты = ИзмененныеРеквизитыИлиЭлемент.Имя;
	КонецЕсли;
	
	СтруктураИзмененныхРеквизитов = Новый Структура(ИзмененныеРеквизиты);
	
	Если ТребуетсяВызовСервераДляНастройкиЭлементовФормы(СтруктураИзмененныхРеквизитов) Тогда
		НастроитьЗависимыеЭлементыФормыНаСервере(ИзмененныеРеквизиты)
	Иначе
		НастроитьЗависимыеЭлементыФормыНаКлиентеНаСервере(ЭтаФорма, ИзмененныеРеквизиты);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура НастроитьЗависимыеЭлементыФормыНаКлиентеНаСервере(Форма, ИзмененныеРеквизиты)

	СтруктураИзмененныхРеквизитов = Новый Структура(ИзмененныеРеквизиты);
	
	ОбновитьВсе = СтруктураИзмененныхРеквизитов.Количество() = 0;
	
	ПараметрыРеквизитовОбъекта = ВнеоборотныеАктивыКлиентСервер.ЗначенияСвойствЗависимыхРеквизитов_ВводОстатков(
									Форма, Форма.ВспомогательныеРеквизиты, ИзмененныеРеквизиты);
	
	ВнеоборотныеАктивыКлиентСервер.НастроитьЗависимыеЭлементыФормы(Форма, ПараметрыРеквизитовОбъекта);
	
	Если НЕ ОбновитьВсе Тогда
		ВнеоборотныеАктивыКлиентСервер.ОчиститьНеиспользуемыеРеквизиты(Форма, ПараметрыРеквизитовОбъекта);
		ИзмененныеРеквизиты = ВнеоборотныеАктивыКлиентСервер.ЗаполнитьРеквизитыВзависимостиОтСвойств_ВводОстатков(
				Форма, ПараметрыРеквизитовОбъекта);
	КонецЕсли;
	
	Элементы = Форма.Элементы;
	
	АмортизацияУУДоступна = 
		Форма.ОтражатьВУпрУчете
		И (Форма.ПорядокУчетаУУ = ПредопределенноеЗначение("Перечисление.ПорядокУчетаСтоимостиВнеоборотныхАктивов.НачислятьАмортизацию")
			ИЛИ Форма.ПорядокУчетаУУ = ПредопределенноеЗначение("Перечисление.ПорядокУчетаСтоимостиВнеоборотныхАктивов.ПустаяСсылка"));
			
	ДоступныПараметрыАмортизацииУУ = 
		Форма.ПорядокУчетаУУ = ПредопределенноеЗначение("Перечисление.ПорядокУчетаСтоимостиВнеоборотныхАктивов.НачислятьАмортизацию")
		И Форма.ВспомогательныеРеквизиты.ОтражатьВУпрУчете;
									
	#Область СтраницаПараметрыУчета
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ВидОбъектаУчета")
		ИЛИ ОбновитьВсе Тогда
		
		ЭлементПорядокУчета = Элементы.ПорядокУчетаНУ.СписокВыбора.НайтиПоЗначению(
									ПредопределенноеЗначение("Перечисление.ПорядокУчетаСтоимостиВнеоборотныхАктивов.СписыватьПриПринятииКУчету"));
		
		Если Форма.ВидОбъектаУчета = ПредопределенноеЗначение("Перечисление.ВидыОбъектовУчетаНМА.РасходыНаНИОКР") Тогда
			Элементы.ПорядокУчетаУУ.СписокВыбора[0].Представление = НСтр("ru = 'Списание расходов';
																		|en = 'Expense write-off'");
		Иначе
			Элементы.ПорядокУчетаУУ.СписокВыбора[0].Представление = НСтр("ru = 'Начисление амортизации';
																		|en = 'Depreciation accrual'");
		КонецЕсли; 
		
	КонецЕсли;	
	
	Если ОбновитьВсе Тогда
		Элементы.ДатаПринятияКУчетуУУ.Заголовок = НСтр("ru = 'Принят к учету';
														|en = 'Recognized'");
		Элементы.ЛиквидационнаяСтоимостьРеглВалюта.Видимость = Элементы.ЛиквидационнаяСтоимостьРегл.Видимость;
		Элементы.ЛиквидационнаяСтоимостьВалюта.Видимость = Элементы.ЛиквидационнаяСтоимость.Видимость;
	КонецЕсли;	
	
	#КонецОбласти
	
	#Область СтраницаСтоимость
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ВидОбъектаУчета")
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("ПорядокУчетаУУ")
		ИЛИ ОбновитьВсе Тогда
		ЗначениеСвойства = Форма.ВидОбъектаУчета <> ПредопределенноеЗначение("Перечисление.ВидыОбъектовУчетаНМА.РасходыНаНИОКР")
							И АмортизацияУУДоступна;
		Элементы.ОстаточнаяСтоимостьУУ.Видимость = ЗначениеСвойства;
	КонецЕсли;
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ТекущаяСтоимостьУУ")
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("НакопленнаяАмортизацияУУ")
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("ПервоначальнаяСтоимостьОтличается")
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("ПрименениеЦелевогоФинансирования")
		ИЛИ ВводОстатковВнеоборотныхАктивовКлиентСерверЛокализация.ФормаРедактированияСтрокиНМА_ТребуетсяПересчитатьЗависимыеСуммы(СтруктураИзмененныхРеквизитов)
		ИЛИ ОбновитьВсе Тогда
		
		ПересчитатьЗависимыеСуммы(Форма);
	КонецЕсли; 
	
	Элементы.ГруппаНакопленнаяАмортизацияЗаголовок.Видимость = 
		Элементы.НакопленнаяАмортизацияУУ.Видимость;
	
	Элементы.ГруппаОстаточнаяСтоимостьЗаголовок.Видимость = 
		Элементы.ОстаточнаяСтоимостьУУ.Видимость;
		
	Элементы.ДекорацияЗаголовокУУ.Видимость = Элементы.ТекущаяСтоимостьУУ.Видимость;
	
	Если ОбновитьВсе Тогда
		Элементы.ГруппаТекущаяСтоимостьЗаголовокЦФ.Видимость = Ложь;
		Элементы.ГруппаНакопленнаяАмортизацияЗаголовокЦФ.Видимость = Ложь;
		Элементы.ДекорацияПервоначальнаяСтоимостьПР.Видимость = Ложь;
		Элементы.ДекорацияПервоначальнаяСтоимостьВР.Видимость = Ложь;
	КонецЕсли; 
	
	#КонецОбласти
	
	#Область СтраницаСобытия
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ОтражатьВРеглУчете")
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("ВидОбъектаУчета")
		ИЛИ ОбновитьВсе Тогда
		
		ЗначениеСвойства = 
			Форма.ОтражатьВРеглУчете 
			И Форма.ВидОбъектаУчета <> ПредопределенноеЗначение("Перечисление.ВидыОбъектовУчетаНМА.РасходыНаНИОКР")
			И Форма.ВспомогательныеРеквизиты.ВедетсяРегламентированныйУчетВНА;
		
		Элементы.РезервПереоценкиРеглЗнак.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиСтоимостиРеглСумма.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиАмортизацииРеглСумма.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиСтоимостиРеглСуммаВалютаРегл.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиАмортизацииРеглСуммаВалютаРегл.Видимость = ЗначениеСвойства;
		
		//
		ЗначениеСвойства = 
			Форма.ОтражатьВРеглУчете
			И Форма.ВидОбъектаУчета <> ПредопределенноеЗначение("Перечисление.ВидыОбъектовУчетаНМА.РасходыНаНИОКР")
			И НЕ Форма.ВспомогательныеРеквизиты.ВедетсяРегламентированныйУчетВНА;
			
		Элементы.РезервПереоценкиАмортизацииРеглСумма1.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиАмортизацииРеглСуммаВалютаРегл1.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиСтоимостиРеглСумма1.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиСтоимостиРеглСуммаВалютаРегл1.Видимость = ЗначениеСвойства;
		
	КонецЕсли;
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ЕстьРезервПереоценки")
		ИЛИ ОбновитьВсе Тогда
		
		ЗначениеСвойства = Не Форма.ЕстьРезервПереоценки;
		Элементы.РезервПереоценкиЗнак.ТолькоПросмотр = ЗначениеСвойства; 
		Элементы.РезервПереоценкиСтоимостиСумма.ТолькоПросмотр = ЗначениеСвойства; 
		Элементы.РезервПереоценкиАмортизацииСумма.ТолькоПросмотр = ЗначениеСвойства; 
		Элементы.РезервПереоценкиСтоимостиРеглСумма1.ТолькоПросмотр = ЗначениеСвойства; 
		Элементы.РезервПереоценкиАмортизацииРеглСумма1.ТолькоПросмотр = ЗначениеСвойства; 
			
	КонецЕсли;
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ОтражатьВУпрУчете")
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("ВидОбъектаУчета")
		ИЛИ ОбновитьВсе Тогда
		
		ЗначениеСвойства = Форма.ВидОбъектаУчета <> ПредопределенноеЗначение("Перечисление.ВидыОбъектовУчетаНМА.РасходыНаНИОКР")
							И Форма.ВспомогательныеРеквизиты.ОтражатьВУпрУчете;
		
		Элементы.РезервПереоценкиЗнак.Видимость = ЗначениеСвойства;
			
		//
		ЗначениеСвойства = Форма.ВидОбъектаУчета <> ПредопределенноеЗначение("Перечисление.ВидыОбъектовУчетаНМА.РасходыНаНИОКР")
							И Форма.ВспомогательныеРеквизиты.ОтражатьВУпрУчете
							И (Форма.ВспомогательныеРеквизиты.ВедетсяРегламентированныйУчетВНА
								ИЛИ НЕ Форма.ВспомогательныеРеквизиты.ВалютыСовпадают);
		
		Элементы.РезервПереоценкиСтоимостиСумма.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиСтоимостиСуммаВалютаУпр1.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиАмортизацииСумма.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиАмортизацииСуммаВалютаУпр.Видимость = ЗначениеСвойства;
			
		//
		ЗначениеСвойства = Форма.ВидОбъектаУчета <> ПредопределенноеЗначение("Перечисление.ВидыОбъектовУчетаНМА.РасходыНаНИОКР")
							И Форма.ВспомогательныеРеквизиты.ОтражатьВУпрУчете
							И НЕ Форма.ВспомогательныеРеквизиты.ВедетсяРегламентированныйУчетВНА;
		
		Элементы.РезервПереоценкиСтоимостиРеглСумма1.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиАмортизацииРеглСумма1.Видимость = ЗначениеСвойства;
			
	КонецЕсли;
	
	#КонецОбласти
	
	#Область СтраницаАмортизация
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ВидОбъектаУчета")
		ИЛИ ОбновитьВсе Тогда
		
		Если Форма.ВидОбъектаУчета = ПредопределенноеЗначение("Перечисление.ВидыОбъектовУчетаНМА.РасходыНаНИОКР") Тогда
			
			Элементы.СтраницаАмортизация.Заголовок = НСтр("ru = 'Списание расходов';
															|en = 'Expense write-off'");
			
			Элементы.МетодНачисленияАмортизацииУУ.Заголовок = НСтр("ru = 'Способ списания';
																	|en = 'Write-off method'");
			Элементы.СрокИспользованияУУ.Заголовок = НСтр("ru = 'Срок списания';
															|en = 'Write-off period'");

		Иначе
			
			Элементы.СтраницаАмортизация.Заголовок = НСтр("ru = 'Амортизация';
															|en = 'Depreciation'");
			
			Элементы.МетодНачисленияАмортизацииУУ.Заголовок = НСтр("ru = 'Метод начисления';
																	|en = 'Accrual method of accounting'");
			Элементы.СрокИспользованияУУ.Заголовок = НСтр("ru = 'Срок использования';
															|en = 'Useful life'");

		КонецЕсли;
		
	КонецЕсли;
	
	Если СтруктураИзмененныхРеквизитов.Свойство("СрокИспользованияУУ")
		ИЛИ ОбновитьВсе Тогда
		
		Форма.СрокИспользованияУУРасшифровка = ВнеоборотныеАктивыКлиентСервер.РасшифровкаСрокаПолезногоИспользования(
			Форма.СрокИспользованияУУ);
			
	КонецЕсли;
	
	Элементы.СрокИспользованияУУРасшифровка.Видимость = Элементы.СрокИспользованияУУ.Видимость;
	
	#КонецОбласти
	
	#Область СтраницаОтражениеРасходов
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ВидОбъектаУчета") 
		ИЛИ ОбновитьВсе Тогда
		
		Если Форма.ВидОбъектаУчета = ПредопределенноеЗначение("Перечисление.ВидыОбъектовУчетаНМА.НематериальныйАктив") Тогда
			Элементы.ГруппаРасходыУУ.Заголовок = НСтр("ru = 'Амортизация';
														|en = 'Depreciation'");
		Иначе
			Элементы.ГруппаРасходыУУ.Заголовок = НСтр("ru = 'Списание расходов';
														|en = 'Expense write-off'");
		КонецЕсли;
		
	КонецЕсли;
	
	#КонецОбласти
	
	ВводОстатковВнеоборотныхАктивовКлиентСерверЛокализация.ФормаРедактированияСтрокиНМА_НастроитьЗависимыеЭлементыФормы(
		Форма, СтруктураИзмененныхРеквизитов, ПараметрыРеквизитовОбъекта);
	
	ЗаполнитьЗначенияРеквизитовДоИзменения(Форма);

КонецПроцедуры

&НаСервере
Процедура НастроитьЗависимыеЭлементыФормыНаСервере(Знач ИзмененныеРеквизитыИлиЭлемент = "")

	Если ТипЗнч(ИзмененныеРеквизитыИлиЭлемент) = Тип("Строка") Тогда
		ИзмененныеРеквизиты = ИзмененныеРеквизитыИлиЭлемент;
	Иначе
		ИзмененныеРеквизиты = ИзмененныеРеквизитыИлиЭлемент.Имя;
	КонецЕсли;
	
	СтруктураИзмененныхРеквизитов = Новый Структура(ИзмененныеРеквизиты);
	
	ОбновитьВсе = СтруктураИзмененныхРеквизитов.Количество() = 0;
	
	Если ОбновитьВсе Тогда
		
		ВспомогательныеРеквизиты = Документы.ВводОстатковВнеоборотныхАктивов2_4.ВспомогательныеРеквизиты(ЭтаФорма, Истина);
		
		Если ВспомогательныеРеквизиты.ВедетсяРегламентированныйУчетВНА Тогда
			Элементы.ДекорацияЗаголовокУУ.Заголовок = СтрШаблон(НСтр("ru = 'УУ (%1):';
																	|en = 'MA (%1) :'"), Строка(ВалютаУпр));
			Элементы.ДекорацияЗаголовокБУ.Заголовок = СтрШаблон(НСтр("ru = 'БУ (%1):';
																	|en = 'AC (%1) :'"), Строка(ВалютаРегл));
		Иначе
			Элементы.ДекорацияЗаголовокУУ.Заголовок = СтрШаблон(НСтр("ru = 'Упр. (%1):';
																	|en = 'Man. (%1):'"), Строка(ВалютаУпр));
			Элементы.ДекорацияЗаголовокБУ.Заголовок = СтрШаблон(НСтр("ru = 'Регл. (%1):';
																	|en = 'Reg. (%1):'"), Строка(ВалютаРегл));
		КонецЕсли; 
		
		Элементы.ГруппаПереоценкаУпр.Заголовок = НСтр("ru = 'Переоценка';
														|en = 'Revaluation'");
		Элементы.ГруппаАмортизацияУУ.ОтображатьЗаголовок = Ложь;
		
		ВнеоборотныеАктивы.УстановитьПараметрыФункциональныхОпцийФормыОбъекта(
			ЭтаФорма, Организация, КонецМесяца(Дата) + 1);
			
		ПараметрыВыбораНМА = Новый Массив;
		
		ОтборСостояние = Новый Массив;
		Если ОтражатьВОперативномУчете Тогда
			ОтборСостояние.Добавить(Перечисления.ВидыСостоянийНМА.НеПринятКУчету);
		Иначе
			ОтборСостояние.Добавить(Перечисления.ВидыСостоянийНМА.ПринятКУчету);
		КонецЕсли; 
		ПараметрыВыбораНМА.Добавить(Новый ПараметрВыбора("Отбор.Состояние", ОтборСостояние));
		ПараметрыВыбораНМА.Добавить(Новый ПараметрВыбора("Контекст", "БУ,УУ"));
		
		Элементы.НематериальныйАктив.ПараметрыВыбора = Новый ФиксированныйМассив(ПараметрыВыбораНМА);
			
	КонецЕсли;
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ВидОбъектаУчета")
		ИЛИ ОбновитьВсе Тогда
		
		НастроитьПараметрыВыбораГФУ();
	КонецЕсли; 
	
	ВводОстатковВнеоборотныхАктивовЛокализация.ФормаРедактированияСтрокиНМА_НастроитьЗависимыеЭлементыФормы(
		ЭтаФорма, СтруктураИзмененныхРеквизитов);
	
	НастроитьЗависимыеЭлементыФормыНаКлиентеНаСервере(ЭтаФорма, ИзмененныеРеквизиты);
	
КонецПроцедуры

&НаКлиенте
Функция ТребуетсяВызовСервераДляНастройкиЭлементовФормы(СтруктураИзмененныхРеквизитов)

	Если СтруктураИзмененныхРеквизитов.Количество() = 0
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("ГруппаФинансовогоУчета") Тогда
		Возврат Истина;
	КонецЕсли; 
	
	Возврат Ложь;

КонецФункции

&НаКлиенте
Процедура ПриИзмененииСрокаИспользования(ИмяРеквизита, ОбновитьЕслиСовпадают)

	СписокРеквизитов = ИмяРеквизита;
	
	ВводОстатковВнеоборотныхАктивовКлиентЛокализация.ФормаРедактированияСтрокиНМА_ПриИзмененииСрокаИспользования(
		ЭтаФорма, ИмяРеквизита, ОбновитьЕслиСовпадают, СписокРеквизитов);
	
	НастроитьЗависимыеЭлементыФормы(СписокРеквизитов);		

КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ЗаполнитьЗначенияРеквизитовДоИзменения(Форма)

	СписокРеквизитов = "ДатаПринятияКУчетуУУ,СрокИспользованияУУ,
						|ТекущаяСтоимостьУУ,НакопленнаяАмортизацияУУ,
						|ПервоначальнаяСтоимостьУУ,ВидОбъектаУчета";
	
	ЗначенияРеквизитовДоИзменения = Новый Структура(СписокРеквизитов);
	ЗаполнитьЗначенияСвойств(ЗначенияРеквизитовДоИзменения, Форма);
	
	ВводОстатковВнеоборотныхАктивовКлиентСерверЛокализация.ФормаРедактированияСтрокиНМА_ДополнитьЗначенияРеквизитовДоИзменения(
		Форма, ЗначенияРеквизитовДоИзменения);
	
	Форма.ЗначенияРеквизитовДоИзменения = Новый ФиксированнаяСтруктура(ЗначенияРеквизитовДоИзменения);

КонецПроцедуры

&НаСервере
Процедура НематериальныйАктивПриИзмененииНаСервере()

	ЗаполнитьСведенияНМА();
	
	ГруппаФинансовогоУчета = Справочники.ГруппыФинансовогоУчетаВнеоборотныхАктивов.ЗначениеПоУмолчанию(ВидАктива);
	ИзмененныеРеквизиты = "ГруппаФинансовогоУчета";
	
	Если ЗначенияРеквизитовДоИзменения.ВидОбъектаУчета <> ВидОбъектаУчета Тогда
		ИзмененныеРеквизиты = ИзмененныеРеквизиты + ",ВидОбъектаУчета";
	КонецЕсли; 
	
	ПриИзмененииРеквизитаЗавершениеНаСервере("НематериальныйАктив", Новый Структура);
	
	НастроитьЗависимыеЭлементыФормыНаСервере(ИзмененныеРеквизиты);
	
	ПересчитатьЗависимыеСуммы(ЭтаФорма);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ПересчитатьЗависимыеСуммы(Форма)
	
	Если Не Форма.ПервоначальнаяСтоимостьОтличается Тогда
		
		Форма.ПервоначальнаяСтоимостьУУ = Форма.ТекущаяСтоимостьУУ;
		
	КонецЕсли;
	
	// Остаточная стоимость не редактируется
	Форма.ОстаточнаяСтоимостьУУ   = Форма.ТекущаяСтоимостьУУ   - Форма.НакопленнаяАмортизацияУУ;
	
	ВводОстатковВнеоборотныхАктивовКлиентСерверЛокализация.ФормаРедактированияСтрокиНМА_ПересчитатьЗависимыеСуммы(Форма);
	
КонецПроцедуры

&НаСервере
Процедура НастроитьПараметрыВыбораГФУ()
	
	МассивПараметров = Новый Массив;
	
	Если ВидОбъектаУчета = Перечисления.ВидыОбъектовУчетаНМА.НематериальныйАктив Тогда
		МассивПараметров.Добавить(Новый ПараметрВыбора("Отбор.ВидАктива", Перечисления.ВидыВнеоборотныхАктивов.НМА));
	Иначе
		МассивПараметров.Добавить(Новый ПараметрВыбора("Отбор.ВидАктива", Перечисления.ВидыВнеоборотныхАктивов.НИОКР));
	КонецЕсли;
	
	Элементы.ГруппаФинансовогоУчета.ПараметрыВыбора = Новый ФиксированныйМассив(МассивПараметров);
	
КонецПроцедуры

&НаСервере
Процедура СтатьяРасходовУУПриИзмененииНаСервере()

	СтатьяРасходовПриИзменении(
		СтатьяРасходовУУ, 
		АналитикаРасходовУУ, 
		АналитикаРасходовУУОбязательна);
	
	НастроитьЗависимыеЭлементыФормыНаСервере("СтатьяРасходовУУ");
	
КонецПроцедуры

&НаСервере
Процедура СтатьяРасходовПриИзменении(СтатьяРасходов, АналитикаРасходов, АналитикаРасходовОбязательна)

	Если НЕ ЗначениеЗаполнено(СтатьяРасходов) Тогда
		АналитикаРасходов = Неопределено;
	КонецЕсли;
	
	АналитикаРасходовОбязательна = 
		ЗначениеЗаполнено(СтатьяРасходов)
		И ОбщегоНазначения.ЗначениеРеквизитаОбъекта(СтатьяРасходов, "КонтролироватьЗаполнениеАналитики");
		
	ДанныеДляЗаполнения = Новый Структура("Организация,Подразделение", Организация, Местонахождение);
	ДоходыИРасходыСервер.СтатьяРасходовПриИзменении(ДанныеДляЗаполнения, СтатьяРасходов, АналитикаРасходов);

КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()

	Если Параметры.НоваяСтрока И НЕ Параметры.Копирование Тогда
		ЗаполнитьЗначенияПоУмолчанию();
	КонецЕсли;
	
	ВалютаУпр = Константы.ВалютаУправленческогоУчета.Получить();
	ВалютаРегл = Константы.ВалютаРегламентированногоУчета.Получить();
	
	Элементы.ЛиквидационнаяСтоимостьВалюта.Заголовок = ВалютаУпр;
	Элементы.ЛиквидационнаяСтоимостьРеглВалюта.Заголовок = ВалютаРегл;
	
	ДатаНачалаУчета = '000101010000';
	
	АналитикаРасходовУУОбязательна =
		ЗначениеЗаполнено(СтатьяРасходовУУ)
		И ОбщегоНазначения.ЗначениеРеквизитаОбъекта(СтатьяРасходовУУ, "КонтролироватьЗаполнениеАналитики");
	
	ВводОстатковВнеоборотныхАктивовЛокализация.ФормаРедактированияСтрокиНМА_ПриЧтенииСозданииНаСервере(ЭтаФорма);
	
	ЗаполнитьСведенияНМА();
	
	НастроитьЗависимыеЭлементыФормыНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	УсловноеОформление.Элементы.Очистить();
	
	ПланыВидовХарактеристик.СтатьиДоходов.УстановитьУсловноеОформлениеАналитик(УсловноеОформление);
	
	ПланыВидовХарактеристик.СтатьиРасходов.УстановитьУсловноеОформлениеАналитик(
		УсловноеОформление,
		"СтатьяРасходовБУ, АналитикаРасходовБУ,
		|СтатьяРасходовУУ, АналитикаРасходовУУ");
	
КонецПроцедуры
	
&НаСервере
Процедура ЗаполнитьСведенияНМА()

	Если НЕ ЗначениеЗаполнено(НематериальныйАктив) Тогда
		ВидАктива = Перечисления.ВидыВнеоборотныхАктивов.НМА;
		ВидОбъектаУчета = Перечисления.ВидыОбъектовУчетаНМА.НематериальныйАктив;
		Элементы.НематериальныйАктив.Заголовок = НСтр("ru = 'Нематериальный актив';
														|en = 'Intangible asset'");
		Элементы.ВидНМА.Видимость = Ложь;
		ВводОстатковВнеоборотныхАктивовЛокализация.ФормаРедактированияСтрокиНМА_ЗаполнитьСведенияНМА(ЭтаФорма);
		Возврат;
	КонецЕсли; 
	
	РеквизитыНМА = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(НематериальныйАктив, "АмортизационнаяГруппа,ВидОбъектаУчета,ВидНМА");
	
	ВидОбъектаУчета = РеквизитыНМА.ВидОбъектаУчета; 
	
	ВидАктива = ?(РеквизитыНМА.ВидОбъектаУчета = Перечисления.ВидыОбъектовУчетаНМА.НематериальныйАктив, 
						Перечисления.ВидыВнеоборотныхАктивов.НМА, 
						Перечисления.ВидыВнеоборотныхАктивов.НИОКР);
	
	Если ВидОбъектаУчета = Перечисления.ВидыОбъектовУчетаНМА.РасходыНаНИОКР Тогда
		
		Заголовок = НСтр("ru = 'Сведения о расходах на НИОКР';
						|en = 'R&D expense information'");
		Элементы.НематериальныйАктив.Заголовок = НСтр("ru = 'Расходы на НИОКР';
														|en = 'R&D expenses'");
		Элементы.ВидНМА.Видимость = Ложь;
		
	Иначе
		
		Заголовок = НСтр("ru = 'Сведения о нематериальном активе';
						|en = 'Intangible asset information'");
		Элементы.НематериальныйАктив.Заголовок = НСтр("ru = 'Нематериальный актив';
														|en = 'Intangible asset'");
		Элементы.ВидНМА.Видимость = Истина;

		Если ЗначениеЗаполнено(РеквизитыНМА.ВидНМА) Тогда
			ВидНМА = Новый ФорматированнаяСтрока(Строка(РеквизитыНМА.ВидНМА));
		Иначе
			ВидНМА = Новый ФорматированнаяСтрока(НСтр("ru = 'не заполнен';
														|en = 'not filled in'"),, ЦветаСтиля.ЦветНедоступногоТекста);
		КонецЕсли; 
		
	КонецЕсли; 
	
	ВводОстатковВнеоборотныхАктивовЛокализация.ФормаРедактированияСтрокиНМА_ЗаполнитьСведенияНМА(ЭтаФорма);
	
КонецПроцедуры

&НаСервере
Процедура ПроверитьЗаполнениеАналитик(ПараметрыРеквизитовОбъекта, МассивНепроверяемыхРеквизитов, Отказ)

	ПроверяемыеРеквизитыСтатейРасходов = Новый Массив;
	Если ВнеоборотныеАктивыКлиентСервер.ЗначениеСвойстваРеквизитаОбъекта("СтатьяРасходовУУ", "Видимость", ПараметрыРеквизитовОбъекта) = Истина Тогда
		ПроверяемыеРеквизитыСтатейРасходов.Добавить("СтатьяРасходовУУ, АналитикаРасходовУУ");
	КонецЕсли; 
	
	Если ПроверяемыеРеквизитыСтатейРасходов.Количество() <> 0 Тогда
		ПланыВидовХарактеристик.СтатьиРасходов.ПроверитьЗаполнениеАналитик(
			ЭтотОбъект, СтрСоединить(ПроверяемыеРеквизитыСтатейРасходов, ","), МассивНепроверяемыхРеквизитов, Отказ);
	КонецЕсли; 
	
	ВводОстатковВнеоборотныхАктивовЛокализация.ФормаРедактированияСтрокиНМА_ПроверитьЗаполнениеАналитик(
		ЭтаФорма, ПараметрыРеквизитовОбъекта, МассивНепроверяемыхРеквизитов, Отказ);

КонецПроцедуры

&НаСервере
Процедура ЗаполнитьЗначенияПоУмолчанию()

	ГруппаФинансовогоУчета = Справочники.ГруппыФинансовогоУчетаВнеоборотныхАктивов.ЗначениеПоУмолчанию(ВидАктива);
	
	ВводОстатковВнеоборотныхАктивовЛокализация.ФормаРедактированияСтрокиНМА_ЗаполнитьЗначенияПоУмолчанию(ЭтаФорма);
	
	ВспомогательныеРеквизиты = Документы.ВводОстатковВнеоборотныхАктивов2_4.ВспомогательныеРеквизиты(ЭтаФорма, Истина);
									
	ПараметрыРеквизитовОбъекта = ВнеоборотныеАктивыКлиентСервер.ЗначенияСвойствЗависимыхРеквизитов_ВводОстатков(
									ЭтаФорма, ВспомогательныеРеквизиты, "");
									
	Документы.ВводОстатковВнеоборотныхАктивов2_4.ЗаполнитьРеквизитыВзависимостиОтСвойств(
			ЭтаФорма, ВспомогательныеРеквизиты, ПараметрыРеквизитовОбъекта);

	Документы.ВводОстатковВнеоборотныхАктивов2_4.ЗаполнитьЗначенияПоУмолчанию(ЭтаФорма, ЭтаФорма);
			
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытиемЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
	
	Если РезультатВопроса = КодВозвратаДиалога.Да Тогда
		ЗавершитьРедактирование();
	ИначеЕсли РезультатВопроса = КодВозвратаДиалога.Нет Тогда
		Модифицированность = Ложь;
		Закрыть();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗавершитьРедактирование()

	ОчиститьСообщения();
	
	Если НЕ ПроверитьЗаполнение() Тогда
		ТекстВопроса = НСтр("ru = 'Не заполнены обязательные поля.
                             |Можно завершить редактирование или продолжить редактирование.';
                             |en = 'Required fields are not filled in.
                             |You can close editing or continue editing.'");
		СписокКнопок = Новый СписокЗначений;
		СписокКнопок.Добавить(КодВозвратаДиалога.Да, НСтр("ru = 'Завершить редактирование';
															|en = 'Finish editing'"));
		СписокКнопок.Добавить(КодВозвратаДиалога.Нет, НСтр("ru = 'Продолжить редактирование';
															|en = 'Continue editing'"));
		ОписаниеОповещения = Новый ОписаниеОповещения("ЗавершитьРедактированиеЗавершение", ЭтотОбъект);
		ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса, СписокКнопок,, КодВозвратаДиалога.Да);
	Иначе
		ЗавершитьРедактированиеЗавершение(КодВозвратаДиалога.Да, Неопределено);
	КонецЕсли; 

КонецПроцедуры

&НаКлиенте
Процедура ЗавершитьРедактированиеЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
	
	Если РезультатВопроса <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;
	
	РезультатРедактирования = Новый Структура(СохраняемыеРеквизиты);
	ЗаполнитьЗначенияСвойств(РезультатРедактирования, ЭтаФорма);
	
	МножительРезерваПереоценки = ?(ЕстьРезервПереоценки, ?(РезервПереоценкиЗнак, 1, -1), 0);
	РезультатРедактирования.Вставить("РезервПереоценкиСтоимости", МножительРезерваПереоценки * РезервПереоценкиСтоимостиСумма);
	РезультатРедактирования.Вставить("РезервПереоценкиАмортизации", МножительРезерваПереоценки * РезервПереоценкиАмортизацииСумма);
	
	Если ВспомогательныеРеквизиты.ОтражатьВУпрУчете
		И НЕ ВспомогательныеРеквизиты.ВедетсяРегламентированныйУчетВНА Тогда
		
		РезультатРедактирования.Вставить("РезервПереоценкиСтоимостиРегл", МножительРезерваПереоценки * РезервПереоценкиСтоимостиРеглСумма);
		РезультатРедактирования.Вставить("РезервПереоценкиАмортизацииРегл", МножительРезерваПереоценки * РезервПереоценкиАмортизацииРеглСумма);
		
	КонецЕсли;
	
	ВводОстатковВнеоборотныхАктивовКлиентЛокализация.ФормаРедактированияСтрокиНМА_ЗавершитьРедактированиеЗавершение(
		ЭтаФорма, РезультатРедактирования);
	
	Модифицированность = Ложь;
	
	Закрыть(РезультатРедактирования);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
	
#Область ЛокализацияБел
 
&НаСервере
Процедура Подключаемый_ПриСозданииНаСервереЛокализацияБел(Отказ, СтандартнаяОбработка)
	
	ПриСозданииНаСервере(Отказ, СтандартнаяОбработка);

	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ГруппаАмортизацияНУДо2009",     "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СпециальныйКоэффициентНУ",      "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СрокИспользованияНУ",           "Доступность", Ложь);
		
КонецПроцедуры

#Область Инициализация

#Если НаСервере Тогда

ПодключаемыеОбработчикиСобытийФормы = Новый Массив;
ПодключаемыеОбработчикиСобытийФормы.Добавить("ПриСозданииНаСервере");

Для Каждого Обработчик Из ПодключаемыеОбработчикиСобытийФормы Цикл
	УстановитьДействие(Обработчик, "Подключаемый_" + Обработчик + "ЛокализацияБел");
КонецЦикла;

#КонецЕсли

#КонецОбласти

#КонецОбласти
﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	СохраняемыеРеквизиты  = Параметры.СохраняемыеРеквизиты;
	ХозяйственнаяОперация = Параметры.ХозяйственнаяОперация;
	Организация = Параметры.Организация;
	Местонахождение = Параметры.Местонахождение;
	ОтражатьВУпрУчете = Параметры.ОтражатьВУпрУчете;
	ОтражатьВРеглУчете = Параметры.ОтражатьВРеглУчете;
	ОтражатьВБУиНУ = Параметры.ОтражатьВБУиНУ;
	ОтражатьВОперативномУчете = Параметры.ОтражатьВОперативномУчете;
	ОтражатьВУУ = Параметры.ОтражатьВУУ;
	Дата = Параметры.Дата;
	Ссылка = Параметры.Ссылка;
	
	Если Параметры.Свойство("ЗначенияРеквизитов") Тогда
		
		ЗаполнитьЗначенияСвойств(ЭтаФорма, Параметры.ЗначенияРеквизитов);
		
		РезервПереоценкиСтоимостиСумма = ?(РезервПереоценкиСтоимости < 0, -РезервПереоценкиСтоимости, РезервПереоценкиСтоимости);
		РезервПереоценкиАмортизацииСумма = ?(РезервПереоценкиАмортизации < 0, -РезервПереоценкиАмортизации, РезервПереоценкиАмортизации);
		РезервПереоценкиЗнак = (РезервПереоценкиСтоимости > 0);
		
		КорректировкаСтоимостиАрендованногоИмуществаВР = -КорректировкаСтоимостиАрендованногоИмуществаНУ;
		
	КонецЕсли;
	
	ПриЧтенииСозданииНаСервере();
		
	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	Если Не ЗавершениеРаботы И Модифицированность Тогда
		
		Отказ = Истина;
		
		ПоказатьВопрос(
			Новый ОписаниеОповещения("ПередЗакрытиемЗавершение", ЭтаФорма),
			НСтр("ru = 'Данные были изменены. Сохранить изменения?';
				|en = 'Data has been changed. Save the changes?'"),
			РежимДиалогаВопрос.ДаНетОтмена);
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	ВспомогательныеРеквизиты = Документы.ВводОстатковВнеоборотныхАктивов2_4.ВспомогательныеРеквизиты(ЭтаФорма, Истина);
	
	ПараметрыРеквизитовОбъекта = ВнеоборотныеАктивыКлиентСервер.ЗначенияСвойствЗависимыхРеквизитов_ВводОстатков(
									ЭтаФорма, ВспомогательныеРеквизиты);
	
	ВнеоборотныеАктивыСлужебный.ОтключитьПроверкуЗаполненияРеквизитовОбъекта(ПараметрыРеквизитовОбъекта, МассивНепроверяемыхРеквизитов);
	
	ПроверитьЗаполнениеАналитик(ПараметрыРеквизитовОбъекта, МассивНепроверяемыхРеквизитов, Отказ);
	
	// {{Локализация_РУ
	Если МассивНепроверяемыхРеквизитов.Найти("СтатьяРасходовНалог") = Неопределено Тогда
		МассивНепроверяемыхРеквизитов.Добавить("СтатьяРасходовНалог");
	КонецЕсли;	
	// Локализация_РУ}}

	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "Запись_ОбъектыЭксплуатации" И Источник = ОсновноеСредство Тогда
		ЗаполнитьСведенияОС();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

#Область СтраницаУчет

&НаКлиенте
Процедура ОсновноеСредствоПриИзменении(Элемент)

	ОсновноеСредствоПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПорядокУчетаУУПриИзменении(Элемент)
	
	ИзмененныеРеквизиты = ВнеоборотныеАктивыКлиентЛокализация.ПриИзмененииПорядкаУчетаУУ_ОС(
							ЭтаФорма, 
							ВспомогательныеРеквизиты.ПрименяетсяУСНДоходыМинусРасходы);
	
	Если ЗначениеЗаполнено(ИзмененныеРеквизиты) Тогда
		НастроитьЗависимыеЭлементыФормы(ИзмененныеРеквизиты);
	КонецЕсли;
	
	ПересчитатьЗависимыеСуммы(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ДатаПринятияКУчетуУУПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы(Элемент.Имя);
	
КонецПроцедуры

&НаКлиенте
Процедура ГруппаФинансовогоУчетаПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы(Элемент.Имя);
	
КонецПроцедуры

&НаКлиенте
Процедура АдресМестонахожденияПриИзменении(Элемент)
	
	ПриИзмененииПредставленияАдреса(Элемент);
	
КонецПроцедуры

&НаКлиенте
Процедура АдресМестонахожденияНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ВнеоборотныеАктивыКлиент.ОткрытьФормуВыбораАдресаИОбработатьРезультат(
		ЭтотОбъект, 
		Элемент,
		ЭтотОбъект,, 
		СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура АдресМестонахожденияОчистка(Элемент, СтандартнаяОбработка)
	
	ПриИзмененииПредставленияАдреса(Элемент);
	
КонецПроцедуры

#КонецОбласти

#Область СтраницаСтоимость

&НаКлиенте
Процедура ТекущаяСтоимостьУУПриИзменении(Элемент)
	
	ПересчитатьЗависимыеСуммы(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура НакопленнаяАмортизацияУУПриИзменении(Элемент)
	
	ПересчитатьЗависимыеСуммы(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ПервоначальнаяСтоимостьУУПриИзменении(Элемент)
	
	ПересчитатьЗависимыеСуммы(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ПервоначальнаяСтоимостьОтличаетсяПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы(Элемент.Имя);
	
КонецПроцедуры

&НаКлиенте
Процедура КорректировкаСтоимостиАрендованногоИмуществаНУПриИзменении(Элемент)
	
	КорректировкаСтоимостиАрендованногоИмуществаВР = -КорректировкаСтоимостиАрендованногоИмуществаНУ;
	
КонецПроцедуры

#КонецОбласти

#Область СтраницаСобытия

&НаКлиенте
Процедура ЕстьИзменениеПараметровАмортизацииУУПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы(Элемент.Имя);
	
КонецПроцедуры

&НаКлиенте
Процедура ЕстьРезервПереоценкиПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы(Элемент.Имя);
	
КонецПроцедуры

&НаКлиенте
Процедура РезервПереоценкиСтоимостиСуммаПриИзменении(Элемент)
	
	Если ТекущаяСтоимостьУУ <> 0 И РезервПереоценкиСтоимостиСумма <> 0 Тогда
		РезервПереоценкиАмортизацииСумма = 
			НакопленнаяАмортизацияУУ
			* (РезервПереоценкиСтоимостиСумма / ТекущаяСтоимостьУУ);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура РезервПереоценкиСтоимостиРеглСумма1ПриИзменении(Элемент)
	
	Если ТекущаяСтоимостьБУ <> 0 И РезервПереоценкиСтоимостиРеглСумма <> 0 Тогда
		РезервПереоценкиАмортизацииРеглСумма = 
			НакопленнаяАмортизацияБУ
			* (РезервПереоценкиСтоимостиРеглСумма / ТекущаяСтоимостьБУ);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СтраницаАмортизация

&НаКлиенте
Процедура СрокИспользованияНУПриИзменении(Элемент)
	
	ПриИзмененииСрокаИспользования("СрокИспользованияНУ", Ложь);
	
КонецПроцедуры

&НаКлиенте
Процедура СрокИспользованияУУПриИзменении(Элемент)
	
	ПриИзмененииСрокаИспользования("СрокИспользованияУУ", Ложь);
	
КонецПроцедуры

&НаКлиенте
Процедура СрокИспользованияУУОстаточныйПриИзменении(Элемент)
	
	ПриИзмененииСрокаИспользования("СрокИспользованияУУОстаточный", Ложь);
	
КонецПроцедуры

&НаКлиенте
Процедура МетодНачисленияАмортизацииУУПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы("МетодНачисленияАмортизацииУУ,КоэффициентАмортизацииБУ");
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказательНаработкиПриИзменении(Элемент)
	
	НастроитьЗависимыеЭлементыФормы(Элемент);
	
КонецПроцедуры

#КонецОбласти

#Область СтраницаОтражениеРасходов

&НаКлиенте
Процедура СтатьяРасходовУУПриИзменении(Элемент)
	
	СтатьяРасходовУУПриИзмененииНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ЛокализацияБел

&НаКлиенте
Процедура Подключаемый_ПриИзмененииРеквизита(Элемент)

	ВводОстатковВнеоборотныхАктивовКлиентЛокализация.ФормаРедактированияСтрокиОС_ПриИзмененииРеквизита(
		Элемент, ЭтаФорма);

КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗаписатьИЗакрыть(Команда)
	
	ЗавершитьРедактирование();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПриИзмененииРеквизитов

&НаКлиенте
Процедура Подключаемый_ПриИзмененииРеквизитаЗавершение(ИмяЭлемента, ДополнительныеПараметры) Экспорт

	Перем ПараметрыДействия;
	
	Если ДополнительныеПараметры.ТребуетсяВызовСервера Тогда
		ПриИзмененииРеквизитаЗавершениеНаСервере(ИмяЭлемента, ДополнительныеПараметры);
	КонецЕсли;
	
	Если ДополнительныеПараметры.Свойство("Выполнить_НастроитьЗависимыеЭлементыФормы", ПараметрыДействия) Тогда
		НастроитьЗависимыеЭлементыФормы(ПараметрыДействия);
	КонецЕсли;
	Если ДополнительныеПараметры.Свойство("Выполнить_ПересчитатьЗависимыеСуммы") Тогда
		ПересчитатьЗависимыеСуммы(ЭтаФорма);
	КонецЕсли;
	Если ДополнительныеПараметры.Свойство("Выполнить_ПриИзмененииСрокаИспользования", ПараметрыДействия) Тогда
		ПриИзмененииСрокаИспользования(ПараметрыДействия.ИмяРеквизита, ПараметрыДействия.ОбновитьЕслиСовпадают);
	КонецЕсли;

КонецПроцедуры

&НаСервере
Процедура ПриИзмененииРеквизитаЗавершениеНаСервере(Знач ИмяЭлемента, Знач ДополнительныеПараметры)

	Перем ПараметрыДействия;
	
	ВводОстатковВнеоборотныхАктивовЛокализация.ФормаРедактированияСтрокиОС_ПриИзмененииРеквизита(
		ИмяЭлемента, ЭтаФорма, ДополнительныеПараметры);

	Если ДополнительныеПараметры.Свойство("Выполнить_НастроитьЗависимыеЭлементыФормы", ПараметрыДействия) Тогда
		НастроитьЗависимыеЭлементыФормыНаСервере(ПараметрыДействия);
	КонецЕсли;
	Если ДополнительныеПараметры.Свойство("Выполнить_ПересчитатьЗависимыеСуммы") Тогда
		ПересчитатьЗависимыеСуммы(ЭтаФорма);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура НастроитьЗависимыеЭлементыФормы(Знач ИзмененныеРеквизитыИлиЭлемент = "")

	Если ТипЗнч(ИзмененныеРеквизитыИлиЭлемент) = Тип("Строка") Тогда
		ИзмененныеРеквизиты = ИзмененныеРеквизитыИлиЭлемент;
	Иначе
		ИзмененныеРеквизиты = ИзмененныеРеквизитыИлиЭлемент.Имя;
	КонецЕсли;
	
	СтруктураИзмененныхРеквизитов = Новый Структура(ИзмененныеРеквизиты);
	
	Если ТребуетсяВызовСервераДляНастройкиЭлементовФормы(СтруктураИзмененныхРеквизитов) Тогда
		НастроитьЗависимыеЭлементыФормыНаСервере(ИзмененныеРеквизиты)
	Иначе
		НастроитьЗависимыеЭлементыФормыНаКлиентеНаСервере(ЭтаФорма, ИзмененныеРеквизиты);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура НастроитьЗависимыеЭлементыФормыНаКлиентеНаСервере(Форма, ИзмененныеРеквизиты = "")

	Элементы = Форма.Элементы;
	
	ПриИзмененииРеквизитов(Форма, ИзмененныеРеквизиты);
	
	СтруктураИзмененныхРеквизитов = Новый Структура(ИзмененныеРеквизиты);
	
	ОбновитьВсе = СтруктураИзмененныхРеквизитов.Количество() = 0;
	
	ПараметрыПринятияКУчетуОС = ВнеоборотныеАктивыКлиентСервер.ПараметрыВводаОстатковОС(Форма, Форма.ВспомогательныеРеквизиты);

	ПараметрыРеквизитовОбъекта = ВнеоборотныеАктивыКлиентСервер.ЗначенияСвойствЗависимыхРеквизитов_ВводОстатков(
									Форма, Форма.ВспомогательныеРеквизиты, ИзмененныеРеквизиты);
									
	ВнеоборотныеАктивыКлиентСервер.НастроитьЗависимыеЭлементыФормы(Форма, ПараметрыРеквизитовОбъекта);
	
	Если НЕ ОбновитьВсе Тогда
		ВнеоборотныеАктивыКлиентСервер.ОчиститьНеиспользуемыеРеквизиты(Форма, ПараметрыРеквизитовОбъекта);
		ИзмененныеРеквизиты = ВнеоборотныеАктивыКлиентСервер.ЗаполнитьРеквизитыВзависимостиОтСвойств_ВводОстатков(
				Форма, ПараметрыРеквизитовОбъекта);
	КонецЕсли;
							
	#Область СтраницаУчет
	
	Если ОбновитьВсе Тогда
		
		Элементы.ДатаПринятияКУчетуУУ.Заголовок = НСтр("ru = 'Принят к учету';
														|en = 'Recognized'");
		Элементы.ГруппаПоследняяМодернизацияУпр.Заголовок = НСтр("ru = 'Модернизация';
																|en = 'Renovation'");
		Элементы.ГруппаОтражениеАмортизационныхРасходовУУ.Заголовок = НСтр("ru = 'Амортизация';
																			|en = 'Depreciation'");
		Элементы.ГруппаПорядокУчета.ОтображатьЗаголовок = Ложь;
		Элементы.ГруппаПостоянныеСведения.ОтображатьЗаголовок = Ложь;
		Элементы.ПорядокУчетаУУ.Заголовок = НСтр("ru = 'Порядок учета';
												|en = 'Accounting procedure'");
		Элементы.ЛиквидационнаяСтоимостьРеглВалюта.Видимость = Элементы.ЛиквидационнаяСтоимостьРегл.Видимость;
		Элементы.ЛиквидационнаяСтоимостьВалюта.Видимость = Элементы.ЛиквидационнаяСтоимость.Видимость;
		
	КонецЕсли;
	
	Если ОбновитьВсе Тогда
	
		ЗначениеВидно = Форма.ВспомогательныеРеквизиты.Дата < Форма.ВспомогательныеРеквизиты.ДатаНачалаУчета;
			
		ПроверяемоеЗначение = ПредопределенноеЗначение("Перечисление.ПорядокУчетаСтоимостиВнеоборотныхАктивов.АмортизацияНачислена");
		
		ВнеоборотныеАктивыКлиентСервер.УстановитьВидимостьЗначенияСпискаВыбора(
			Элементы.ПорядокУчетаУУ.СписокВыбора, 
			ЗначениеВидно, 
			ПроверяемоеЗначение);
			
	КонецЕсли;
	
	#КонецОбласти
	
	#Область СтраницаСтоимость
	
	Если ОбновитьВсе Тогда
		
		Элементы.ГруппаТекущаяСтоимостьЗаголовок.Заголовок       = НСтр("ru = 'Сумма:';
																		|en = 'Amount:'");
		Элементы.ГруппаНакопленнаяАмортизацияЗаголовок.Заголовок = НСтр("ru = 'Сумма:';
																		|en = 'Amount:'");
		Элементы.ГруппаОстаточнаяСтоимостьЗаголовок.Заголовок    = НСтр("ru = 'Сумма:';
																		|en = 'Amount:'");
		
	КонецЕсли;
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ПорядокУчетаУУ")
		ИЛИ ОбновитьВсе Тогда
		Элементы.ОстаточнаяСтоимостьУУ.Видимость = ПараметрыПринятияКУчетуОС.АмортизацияУУДоступна;
	КонецЕсли;
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ПорядокУчетаБУ") 
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("ПорядокУчетаНУ") 
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("ПорядокУчетаУУ") 
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("ОтражатьВРеглУчете") 
		ИЛИ ОбновитьВсе Тогда
		
		ЗначениеСвойства = ПараметрыПринятияКУчетуОС.АмортизацияБУДоступна 
							ИЛИ ПараметрыПринятияКУчетуОС.АмортизацияУУДоступна
								И Форма.ВспомогательныеРеквизиты.ОтражатьВРеглУчете
								И НЕ Форма.ВспомогательныеРеквизиты.ВедетсяРегламентированныйУчетВНА;
								
		Элементы.ОстаточнаяСтоимостьБУ.Видимость = ЗначениеСвойства;
	КонецЕсли;
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ТекущаяСтоимостьУУ")
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("НакопленнаяАмортизацияУУ")
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("ПервоначальнаяСтоимостьОтличается")
		ИЛИ ВводОстатковВнеоборотныхАктивовКлиентСерверЛокализация.ФормаРедактированияСтрокиОС_ТребуетсяПересчитатьЗависимыеСуммы(СтруктураИзмененныхРеквизитов) Тогда
		
		ПересчитатьЗависимыеСуммы(Форма);
	КонецЕсли; 
	
	Если ОбновитьВсе Тогда
		ПересчитатьИтоговыеСуммы(Форма);
	КонецЕсли;
	
	Элементы.ДекорацияЗаголовокУУ.Видимость = Элементы.ТекущаяСтоимостьУУ.Видимость;
	
	#КонецОбласти
	
	#Область СтраницаСобытия
	
	Если ОбновитьВсе Тогда
		
		ЗаголовокГруппы = НСтр("ru = 'Переоценка';
								|en = 'Revaluation'");
		Элементы.ГруппаПереоценкаУпр.Заголовок = ЗаголовокГруппы;
		
	КонецЕсли;
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ОтражатьВРеглУчете")
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("ОтражатьВОперативномУчете")
		ИЛИ ОбновитьВсе Тогда
		
		ЗначениеСвойства = 
			Форма.ОтражатьВРеглУчете 
			И Форма.ВспомогательныеРеквизиты.ВедетсяРегламентированныйУчетВНА
			И Форма.ОтражатьВОперативномУчете;
		
		Элементы.РезервПереоценкиРеглЗнак.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиСтоимостиРеглСумма.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиАмортизацииРеглСумма.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиСтоимостиРеглСуммаВалютаРегл.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиАмортизацииРеглСуммаВалютаРегл.Видимость = ЗначениеСвойства;
		
		//
		ЗначениеСвойства = 
			Форма.ОтражатьВРеглУчете
			И Форма.ОтражатьВОперативномУчете
			И НЕ Форма.ВспомогательныеРеквизиты.ВедетсяРегламентированныйУчетВНА;
			
		Элементы.РезервПереоценкиАмортизацииРеглСумма1.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиАмортизацииРеглСуммаВалютаРегл1.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиСтоимостиРеглСумма1.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиСтоимостиРеглСуммаВалютаРегл1.Видимость = ЗначениеСвойства;
		
	КонецЕсли;
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ОтражатьВУпрУчете")
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("ОтражатьВОперативномУчете")
		ИЛИ ОбновитьВсе Тогда
		
		Элементы.РезервПереоценкиЗнак.Видимость = 
			Форма.ОтражатьВУпрУчете
			И Форма.ОтражатьВОперативномУчете;
		
		//
		ЗначениеСвойства =
			Форма.ОтражатьВУпрУчете
			И Форма.ОтражатьВОперативномУчете
			И (Форма.ВспомогательныеРеквизиты.ВедетсяРегламентированныйУчетВНА
				ИЛИ НЕ Форма.ВспомогательныеРеквизиты.ВалютыСовпадают);
			
		Элементы.РезервПереоценкиСтоимостиСумма.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиСтоимостиСуммаВалютаУпр1.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиАмортизацииСумма.Видимость = ЗначениеСвойства;
		Элементы.РезервПереоценкиАмортизацииСуммаВалютаУпр.Видимость = ЗначениеСвойства;
		
	КонецЕсли;
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ЕстьРезервПереоценки")
		ИЛИ ОбновитьВсе Тогда
		
		ЗначениеСвойства = Не Форма.ЕстьРезервПереоценки;
		Элементы.РезервПереоценкиЗнак.ТолькоПросмотр = ЗначениеСвойства; 
		Элементы.РезервПереоценкиСтоимостиСумма.ТолькоПросмотр = ЗначениеСвойства; 
		Элементы.РезервПереоценкиАмортизацииСумма.ТолькоПросмотр = ЗначениеСвойства; 
		Элементы.РезервПереоценкиСтоимостиРеглСумма1.ТолькоПросмотр = ЗначениеСвойства; 
		Элементы.РезервПереоценкиАмортизацииРеглСумма1.ТолькоПросмотр = ЗначениеСвойства; 
			
	КонецЕсли;
	
	#КонецОбласти
	
	#Область СтраницаАмортизация
	
	Элементы.СрокИспользованияУУРасшифровка.Видимость = Элементы.СрокИспользованияУУ.Видимость;
	Элементы.СрокИспользованияУУОстаточныйРасшифровка.Видимость = Элементы.СрокИспользованияУУОстаточный.Видимость;
	
	Если СтруктураИзмененныхРеквизитов.Свойство("СрокИспользованияУУ")
		ИЛИ ОбновитьВсе Тогда
		
		Форма.СрокИспользованияУУРасшифровка = ВнеоборотныеАктивыКлиентСервер.РасшифровкаСрокаПолезногоИспользования(
			Форма.СрокИспользованияУУ);
			
	КонецЕсли;
	
	Если СтруктураИзмененныхРеквизитов.Свойство("СрокИспользованияУУОстаточный")
		ИЛИ ОбновитьВсе Тогда
		
		Форма.СрокИспользованияУУОстаточныйРасшифровка = ВнеоборотныеАктивыКлиентСервер.РасшифровкаСрокаПолезногоИспользования(
			Форма.СрокИспользованияУУОстаточный);
			
	КонецЕсли;
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ЕстьИзменениеПараметровАмортизацииУУ") 
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("ЕстьРезервПереоценки") 
		ИЛИ ОбновитьВсе Тогда
		
		Если Форма.ЕстьИзменениеПараметровАмортизацииУУ И Форма.ЕстьРезервПереоценки Тогда
			Элементы.СтоимостьДляВычисленияАмортизацииУУ.Подсказка = НСтр("ru = 'Остаточная стоимость после модернизации и переоценки.';
																			|en = 'Residual value after renovation and revaluation.'");
		ИначеЕсли Форма.ЕстьИзменениеПараметровАмортизацииУУ Тогда
			Элементы.СтоимостьДляВычисленияАмортизацииУУ.Подсказка = НСтр("ru = 'Остаточная стоимость на дату последней модернизации.';
																			|en = 'Residual value on the last renovation date.'");
		КонецЕсли;
		
	КонецЕсли;
	
	Если СтруктураИзмененныхРеквизитов.Свойство("ПоказательНаработки")
		ИЛИ ОбновитьВсе Тогда
		Если ЗначениеЗаполнено(Форма.ПоказательНаработки) Тогда
			Элементы.ОбъемНаработки.Подсказка = ЕдиницаИзмеренияПоказателяНаработки(Форма.ПоказательНаработки);
		Иначе
			Элементы.ОбъемНаработки.Подсказка = "";
		КонецЕсли; 
	КонецЕсли;
	
	#КонецОбласти
	
	ВводОстатковВнеоборотныхАктивовКлиентСерверЛокализация.ФормаРедактированияСтрокиОС_НастроитьЗависимыеЭлементыФормы(
		Форма, СтруктураИзмененныхРеквизитов, ПараметрыРеквизитовОбъекта);
	
	ЗаполнитьЗначенияРеквизитовДоИзменения(Форма);
	
КонецПроцедуры

&НаСервере
Процедура НастроитьЗависимыеЭлементыФормыНаСервере(Знач ИзмененныеРеквизитыИлиЭлемент = "")

	Если ТипЗнч(ИзмененныеРеквизитыИлиЭлемент) = Тип("Строка") Тогда
		ИзмененныеРеквизиты = ИзмененныеРеквизитыИлиЭлемент;
	Иначе
		ИзмененныеРеквизиты = ИзмененныеРеквизитыИлиЭлемент.Имя;
	КонецЕсли;
	
	СтруктураИзмененныхРеквизитов = Новый Структура(ИзмененныеРеквизиты);
	ОбновитьВсе = СтруктураИзмененныхРеквизитов.Количество() = 0;
	
	Если ОбновитьВсе
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("ОсновноеСредство") Тогда
		ВспомогательныеРеквизиты = Документы.ВводОстатковВнеоборотныхАктивов2_4.ВспомогательныеРеквизиты(ЭтаФорма, Истина);
	КонецЕсли;
	
	Если ОбновитьВсе Тогда
		
		Элементы.СтраницаОтражениеРасходов.Заголовок = НСтр("ru = 'Отражение расходов';
															|en = 'Record expenses'");
		Элементы.ГруппаАмортизацияУУ.ОтображатьЗаголовок = Ложь;
		
		Если ВспомогательныеРеквизиты.ВедетсяРегламентированныйУчетВНА Тогда
			Элементы.ДекорацияЗаголовокУУ.Заголовок = СтрШаблон(НСтр("ru = 'УУ (%1):';
																	|en = 'MA (%1) :'"), Строка(ВалютаУпр));
			Элементы.ДекорацияЗаголовокБУ.Заголовок = СтрШаблон(НСтр("ru = 'БУ (%1):';
																	|en = 'AC (%1) :'"), Строка(ВалютаРегл));
		Иначе
			Элементы.ДекорацияЗаголовокУУ.Заголовок = СтрШаблон(НСтр("ru = 'Упр. (%1):';
																	|en = 'Man. (%1):'"), Строка(ВалютаУпр));
			Элементы.ДекорацияЗаголовокБУ.Заголовок = СтрШаблон(НСтр("ru = 'Регл. (%1):';
																	|en = 'Reg. (%1):'"), Строка(ВалютаРегл));
		КонецЕсли; 
		
		ПараметрыВыбораОС = Новый Массив;
		
		ОтборСостояние = Новый Массив;
		Если ОтражатьВОперативномУчете Тогда
			ОтборСостояние.Добавить(Перечисления.СостоянияОС.НеПринятоКУчету);
			ОтборСостояние.Добавить(Перечисления.СостоянияОС.СнятоСУчета);
		Иначе
			ОтборСостояние.Добавить(Перечисления.СостоянияОС.ПринятоКУчету);
		КонецЕсли; 
		ПараметрыВыбораОС.Добавить(Новый ПараметрВыбора("Отбор.Состояние", ОтборСостояние));
		ПараметрыВыбораОС.Добавить(Новый ПараметрВыбора("Контекст", "БУ,УУ"));
		
		Элементы.ОсновноеСредство.ПараметрыВыбора = Новый ФиксированныйМассив(ПараметрыВыбораОС);
		
	КонецЕсли;
	
	ВводОстатковВнеоборотныхАктивовЛокализация.ФормаРедактированияСтрокиОС_НастроитьЗависимыеЭлементыФормы(
		ЭтаФорма, СтруктураИзмененныхРеквизитов);
	
	НастроитьЗависимыеЭлементыФормыНаКлиентеНаСервере(ЭтаФорма, ИзмененныеРеквизиты);

КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ПриИзмененииРеквизитов(Объект, ИзмененныеРеквизиты)

	Если НЕ ЗначениеЗаполнено(ИзмененныеРеквизиты) Тогда
		Возврат; 
	КонецЕсли; 
	
	СписокРеквизитов = СтрРазделить(ИзмененныеРеквизиты, ",");
	
	ВводОстатковВнеоборотныхАктивовКлиентСерверЛокализация.ФормаРедактированияСтрокиОС_ПриИзмененииРеквизитов(
		Объект, СписокРеквизитов);
	
	ИзмененныеРеквизиты = СтрСоединить(СписокРеквизитов, ",");
	
КонецПроцедуры

&НаКлиенте
Функция ТребуетсяВызовСервераДляНастройкиЭлементовФормы(СтруктураИзмененныхРеквизитов)

	Если СтруктураИзмененныхРеквизитов.Количество() = 0
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("ГруппаФинансовогоУчета")
		ИЛИ СтруктураИзмененныхРеквизитов.Свойство("ОсновноеСредство")
		ИЛИ ВводОстатковВнеоборотныхАктивовКлиентЛокализация.ФормаРедактированияСтрокиОС_ТребуетсяВызовСервераДляНастройкиЭлементовФормы(ЭтаФорма, СтруктураИзмененныхРеквизитов) Тогда
		Возврат Истина;
	КонецЕсли; 
	
	Возврат Ложь;

КонецФункции

&НаКлиенте
Процедура ПриИзмененииСрокаИспользования(ИмяРеквизита, ОбновитьЕслиСовпадают)

	СписокРеквизитов = ИмяРеквизита;
	
	СрокиИспользованияСовпадают = 
		(НЕ ОтражатьВУпрУчете ИЛИ (ЗначенияРеквизитовДоИзменения.СрокИспользованияБУ = ЗначенияРеквизитовДоИзменения.СрокИспользованияУУ))
		И ВводОстатковВнеоборотныхАктивовКлиентЛокализация.ФормаРедактированияСтрокиОС_СрокиИспользованияСовпадают(ЭтаФорма);
	
	Если ИмяРеквизита <> "СрокИспользованияУУ"
		И ОтражатьВУпрУчете
		И (СрокИспользованияУУ = 0
			ИЛИ СрокиИспользованияСовпадают И ОбновитьЕслиСовпадают) Тогда
		
		СрокИспользованияУУ = ЭтаФорма[ИмяРеквизита];
		СписокРеквизитов = СписокРеквизитов + ",СрокИспользованияУУ";
		
	КонецЕсли;
	
	ВводОстатковВнеоборотныхАктивовКлиентЛокализация.ФормаРедактированияСтрокиОС_ПриИзмененииСрокаИспользования(
		ЭтаФорма, ИмяРеквизита, ОбновитьЕслиСовпадают, СписокРеквизитов);
	
	ЗаполнитьЗначенияРеквизитовДоИзменения(ЭтаФорма);
	
	НастроитьЗависимыеЭлементыФормы(СписокРеквизитов);		

КонецПроцедуры

&НаСервере
Процедура ОсновноеСредствоПриИзмененииНаСервере()

	ЗаполнитьСведенияОС();
	
	ПриИзмененииРеквизитаЗавершениеНаСервере("ОсновноеСредство", Новый Структура);
	
	НастроитьЗависимыеЭлементыФормыНаСервере("ОсновноеСредство");
	
КонецПроцедуры
 
&НаСервере
Процедура СтатьяРасходовУУПриИзмененииНаСервере()

	СтатьяРасходовПриИзменении(
		СтатьяРасходовУУ, 
		АналитикаРасходовУУ, 
		АналитикаРасходовУУОбязательна);
	
	НастроитьЗависимыеЭлементыФормыНаСервере("СтатьяРасходовУУ");
	
КонецПроцедуры

&НаСервере
Процедура СтатьяРасходовПриИзменении(СтатьяРасходов, АналитикаРасходов, АналитикаРасходовОбязательна)

	Если НЕ ЗначениеЗаполнено(СтатьяРасходов) Тогда
		АналитикаРасходов = Неопределено;
	КонецЕсли;
	
	АналитикаРасходовОбязательна = 
		ЗначениеЗаполнено(СтатьяРасходов)
		И ОбщегоНазначения.ЗначениеРеквизитаОбъекта(СтатьяРасходов, "КонтролироватьЗаполнениеАналитики");
		
	ДанныеДляЗаполнения = Новый Структура("Организация,Подразделение", Организация,Местонахождение);
	ДоходыИРасходыСервер.СтатьяРасходовПриИзменении(ДанныеДляЗаполнения, СтатьяРасходов, АналитикаРасходов);

КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ПересчитатьЗависимыеСуммы(Форма)
	
	Если Не Форма.ПервоначальнаяСтоимостьОтличается Тогда
		
		Форма.ПервоначальнаяСтоимостьУУ = Форма.ТекущаяСтоимостьУУ;
		Форма.ПервоначальнаяСтоимостьБУ = Форма.ТекущаяСтоимостьБУ + Форма.ТекущаяСтоимостьБУЦФ;
		
	КонецЕсли;
	
	Если НЕ Форма.ВспомогательныеРеквизиты.ВедетсяРегламентированныйУчетВНА 
		И Форма.ВалютаРегл = Форма.ВалютаУпр Тогда
		Форма.ТекущаяСтоимостьУУ = Форма.ТекущаяСтоимостьБУ;
		Форма.ПервоначальнаяСтоимостьУУ = Форма.ПервоначальнаяСтоимостьБУ;
		Форма.НакопленнаяАмортизацияУУ = Форма.НакопленнаяАмортизацияБУ;
	КонецЕсли; 
	
	ВводОстатковВнеоборотныхАктивовКлиентСерверЛокализация.ФормаРедактированияСтрокиОС_ПересчитатьЗависимыеСуммы(Форма);
	
	ПересчитатьИтоговыеСуммы(Форма);
	
	ЗаполнитьЗначенияРеквизитовДоИзменения(Форма);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ПересчитатьИтоговыеСуммы(Форма)

	// Остаточная стоимость не редактируется
	Форма.ОстаточнаяСтоимостьУУ = Форма.ТекущаяСтоимостьУУ - Форма.НакопленнаяАмортизацияУУ;
	
	ВводОстатковВнеоборотныхАктивовКлиентСерверЛокализация.ФормаРедактированияСтрокиОС_ПересчитатьИтоговыеСуммы(Форма);
	
КонецПроцедуры
 
&НаСервере
Процедура ЗаполнитьСведенияОС()

	Если НЕ ЗначениеЗаполнено(ОсновноеСредство) Тогда
		СведенияОС = Новый ФорматированнаяСтрока("");
		ВводОстатковВнеоборотныхАктивовЛокализация.ФормаРедактированияСтрокиОС_ЗаполнитьСведенияОС(ЭтаФорма);
		Возврат;
	КонецЕсли; 
	
	ПолучаемыеРеквизиты = Новый Структура;
	ПолучаемыеРеквизиты.Вставить("ИнвентарныйНомер");
	РеквизитыОС = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ОсновноеСредство, ПолучаемыеРеквизиты);
	
	СписокСтрок = Новый Массив;
	
	СписокСтрок.Добавить(НСтр("ru = 'Инв. №:';
								|en = 'Inv. No.'"));
	Если ЗначениеЗаполнено(РеквизитыОС.ИнвентарныйНомер) Тогда
		СписокСтрок.Добавить(" ");
		СписокСтрок.Добавить(СокрЛП(РеквизитыОС.ИнвентарныйНомер));
	Иначе	
		СписокСтрок.Добавить(" ");
		ДанныеСтроки = Новый ФорматированнаяСтрока(НСтр("ru = 'не заполнен';
														|en = 'not filled in'"),, ЦветаСтиля.СерыйЦветТекста1);
		СписокСтрок.Добавить(ДанныеСтроки);
	КонецЕсли;	
	
	СведенияОС = Новый ФорматированнаяСтрока(СписокСтрок);
	
	ВводОстатковВнеоборотныхАктивовЛокализация.ФормаРедактированияСтрокиОС_ЗаполнитьСведенияОС(ЭтаФорма);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ЗаполнитьЗначенияРеквизитовДоИзменения(Форма)

	СписокРеквизитов = "ДатаПринятияКУчетуУУ,СрокИспользованияУУ,
						|ТекущаяСтоимостьУУ,НакопленнаяАмортизацияУУ,ПервоначальнаяСтоимостьУУ,
						|ТекущаяСтоимостьБУ,НакопленнаяАмортизацияБУ,ПервоначальнаяСтоимостьБУ";
	
	ЗначенияРеквизитовДоИзменения = Новый Структура(СписокРеквизитов);
	ЗаполнитьЗначенияСвойств(ЗначенияРеквизитовДоИзменения, Форма);
	
	ВводОстатковВнеоборотныхАктивовКлиентСерверЛокализация.ФормаРедактированияСтрокиОС_ДополнитьЗначенияРеквизитовДоИзменения(
		Форма, ЗначенияРеквизитовДоИзменения);
	
	Форма.ЗначенияРеквизитовДоИзменения = Новый ФиксированнаяСтруктура(ЗначенияРеквизитовДоИзменения);

КонецПроцедуры

&НаСервереБезКонтекста
Функция ЕдиницаИзмеренияПоказателяНаработки(Знач ПоказательНаработки)

	Возврат ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ПоказательНаработки, "ЕдиницаИзмерения");

КонецФункции

&НаКлиенте
Процедура ПриИзмененииПредставленияАдреса(Элемент)

	ВнеоборотныеАктивыКлиент.ПриИзмененииПредставленияАдреса(
		Элемент,
		АдресМестонахождения,
		АдресМестонахожденияЗначение);

КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()

	Если Параметры.НоваяСтрока И НЕ Параметры.Копирование Тогда
		ЗаполнитьЗначенияПоУмолчанию();
	КонецЕсли;
	
	ВалютаУпр = Константы.ВалютаУправленческогоУчета.Получить();
	ВалютаРегл = Константы.ВалютаРегламентированногоУчета.Получить();
	Элементы.ЛиквидационнаяСтоимостьВалюта.Заголовок = ВалютаУпр;
	Элементы.ЛиквидационнаяСтоимостьРеглВалюта.Заголовок = ВалютаРегл;
	
	ДатаНачалаУчета = ВнеоборотныеАктивыЛокализация.ДатаНачалаУчетаВнеоборотныхАктивов2_4();
	
	АналитикаРасходовУУОбязательна =
		ЗначениеЗаполнено(СтатьяРасходовУУ)
		И ОбщегоНазначения.ЗначениеРеквизитаОбъекта(СтатьяРасходовУУ, "КонтролироватьЗаполнениеАналитики");
	
	Элементы.ГруппаМестоНахождение.Заголовок = НСтр("ru = 'Местонахождение';
													|en = 'Location'");
	Элементы.МОЛ.Подсказка = НСтр("ru = 'Материально ответственное лицо за основное средство.';
									|en = 'Financially liable person responsible for fixed asset.'");
	
	ВводОстатковВнеоборотныхАктивовЛокализация.ФормаРедактированияСтрокиОС_ПриЧтенииСозданииНаСервере(ЭтаФорма);
	
	ЗаполнитьСведенияОС();
	
	НастроитьЗависимыеЭлементыФормыНаСервере();
	
	ЗаполнитьЗначенияРеквизитовДоИзменения(ЭтаФорма);
	
КонецПроцедуры

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	УсловноеОформление.Элементы.Очистить();
	
	ПланыВидовХарактеристик.СтатьиРасходов.УстановитьУсловноеОформлениеАналитик(
		УсловноеОформление,
		"СтатьяРасходовУУ, АналитикаРасходовУУ");
		
	ВводОстатковВнеоборотныхАктивовЛокализация.ФормаРедактированияСтрокиОС_УстановитьУсловноеОформление(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытиемЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
	
	Если РезультатВопроса = КодВозвратаДиалога.Да Тогда
		ЗавершитьРедактирование();
	ИначеЕсли РезультатВопроса = КодВозвратаДиалога.Нет Тогда
		Модифицированность = Ложь;
		Закрыть();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗавершитьРедактирование()

	ОчиститьСообщения();
	
	Если НЕ ПроверитьЗаполнение() Тогда
		ТекстВопроса = НСтр("ru = 'Не заполнены обязательные поля.
                             |Можно завершить редактирование или продолжить редактирование.';
                             |en = 'Required fields are not filled in.
                             |You can close editing or continue editing.'");
		СписокКнопок = Новый СписокЗначений;
		СписокКнопок.Добавить(КодВозвратаДиалога.Да, НСтр("ru = 'Завершить редактирование';
															|en = 'Finish editing'"));
		СписокКнопок.Добавить(КодВозвратаДиалога.Нет, НСтр("ru = 'Продолжить редактирование';
															|en = 'Continue editing'"));
		ОписаниеОповещения = Новый ОписаниеОповещения("ЗавершитьРедактированиеЗавершение", ЭтотОбъект);
		ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса, СписокКнопок,, КодВозвратаДиалога.Да);
	Иначе
		ЗавершитьРедактированиеЗавершение(КодВозвратаДиалога.Да, Неопределено);
	КонецЕсли; 

КонецПроцедуры

&НаКлиенте
Процедура ЗавершитьРедактированиеЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
	
	Если РезультатВопроса <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;
	
	РезультатРедактирования = Новый Структура(СохраняемыеРеквизиты);
	ЗаполнитьЗначенияСвойств(РезультатРедактирования, ЭтаФорма);
	
	МножительРезерваПереоценки = ?(ЕстьРезервПереоценки, ?(РезервПереоценкиЗнак, 1, -1), 0);
	РезультатРедактирования.Вставить("РезервПереоценкиСтоимости", МножительРезерваПереоценки * РезервПереоценкиСтоимостиСумма);
	РезультатРедактирования.Вставить("РезервПереоценкиАмортизации", МножительРезерваПереоценки * РезервПереоценкиАмортизацииСумма);
	
	Если ВспомогательныеРеквизиты.ОтражатьВУпрУчете
		И НЕ ВспомогательныеРеквизиты.ВедетсяРегламентированныйУчетВНА Тогда
		
		РезультатРедактирования.Вставить("РезервПереоценкиСтоимостиРегл", МножительРезерваПереоценки * РезервПереоценкиСтоимостиРеглСумма);
		РезультатРедактирования.Вставить("РезервПереоценкиАмортизацииРегл", МножительРезерваПереоценки * РезервПереоценкиАмортизацииРеглСумма);
		
	КонецЕсли;
	
	ВводОстатковВнеоборотныхАктивовКлиентЛокализация.ФормаРедактированияСтрокиОС_ЗавершитьРедактированиеЗавершение(
		ЭтаФорма, РезультатРедактирования);
	
	Модифицированность = Ложь;
	
	Закрыть(РезультатРедактирования);
	
КонецПроцедуры

&НаСервере
Процедура ПроверитьЗаполнениеАналитик(ПараметрыРеквизитовОбъекта, МассивНепроверяемыхРеквизитов, Отказ)

	ПроверяемыеРеквизитыСтатейРасходов = Новый Массив;
	Если ВнеоборотныеАктивыКлиентСервер.ЗначениеСвойстваРеквизитаОбъекта("СтатьяРасходовУУ", "Видимость", ПараметрыРеквизитовОбъекта) = Истина Тогда
		ПроверяемыеРеквизитыСтатейРасходов.Добавить("СтатьяРасходовУУ, АналитикаРасходовУУ");
	КонецЕсли; 
	
	Если ПроверяемыеРеквизитыСтатейРасходов.Количество() <> 0 Тогда
		ПланыВидовХарактеристик.СтатьиРасходов.ПроверитьЗаполнениеАналитик(
			ЭтаФорма, СтрСоединить(ПроверяемыеРеквизитыСтатейРасходов, ","), МассивНепроверяемыхРеквизитов, Отказ);
	КонецЕсли; 
	
	ВводОстатковВнеоборотныхАктивовЛокализация.ФормаРедактированияСтрокиОС_ПроверитьЗаполнениеАналитик(
		ЭтаФорма, ПараметрыРеквизитовОбъекта, МассивНепроверяемыхРеквизитов, Отказ);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьЗначенияПоУмолчанию()

	ВспомогательныеРеквизиты = Документы.ВводОстатковВнеоборотныхАктивов2_4.ВспомогательныеРеквизиты(ЭтаФорма, Истина);

	ПараметрыРеквизитовОбъекта = ВнеоборотныеАктивыКлиентСервер.ЗначенияСвойствЗависимыхРеквизитов_ВводОстатков(
									ЭтаФорма, ВспомогательныеРеквизиты, "");
									
	Документы.ВводОстатковВнеоборотныхАктивов2_4.ЗаполнитьРеквизитыВзависимостиОтСвойств(
			ЭтаФорма, ВспомогательныеРеквизиты, ПараметрыРеквизитовОбъекта);
										
	Документы.ВводОстатковВнеоборотныхАктивов2_4.ЗаполнитьЗначенияПоУмолчанию(ЭтаФорма, ЭтаФорма);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ЛокализацияБел
 
&НаСервере
Процедура Подключаемый_ПриСозданииНаСервереЛокализацияБел(Отказ, СтандартнаяОбработка)

	ПриСозданииНаСервере(Отказ, СтандартнаяОбработка);

	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "АмортизационнаяГруппа",            "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СпециальныйКоэффициентНУ",         "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ГруппаДО2009",                     "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ГруппаАмортизацоннаяПремия",       "Заголовок", "Инвестиционный вычет");
    ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ГруппаОтражениеПремия",            "Заголовок", "Инвестиционный вычет");
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ГруппаОтражениеРасходовПоНалогам", "Видимость",   Ложь); 
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СрокИспользованияНУ",              "Доступность", Ложь);
    ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СтраницаУчетГруппаНДС",            "Видимость",   Ложь); 	

	СписокВыбора = Элементы.ПорядокУчетаБУ.СписокВыбора;
	ЭлементСписка = СписокВыбора.НайтиПоЗначению(Перечисления.ПорядокПогашенияСтоимостиОС.НачислениеИзносаПоЕНАОФ);
	Если ЭлементСписка <> Неопределено Тогда
		СписокВыбора.Удалить(ЭлементСписка);
	КонецЕсли;
	
	Элементы.ШифрПоЕНАОФ.Заголовок = "ВРКОС";
	Элементы.ШифрПоЕНАОФ.Подсказка = "Шифр по классификатору ВРКОС (до 01.01.2012)";
	
	Элементы.КодПоОКОФ.Заголовок = "НССОС";
	Элементы.КодПоОКОФ.Подсказка = "Код по классификатору НССОС";
	
	Элементы.ГруппаТекущаяСтоимость.Заголовок = "Переоцененная стоимость";
	Элементы.ПервоначальнаяСтоимостьОтличается.Заголовок = "Отличается от переоцененной:";
	
КонецПроцедуры

#Область Инициализация

#Если НаСервере Тогда

ПодключаемыеОбработчикиСобытийФормы = Новый Массив;
ПодключаемыеОбработчикиСобытийФормы.Добавить("ПриСозданииНаСервере");

Для Каждого Обработчик Из ПодключаемыеОбработчикиСобытийФормы Цикл
	УстановитьДействие(Обработчик, "Подключаемый_" + Обработчик + "ЛокализацияБел");
КонецЦикла;

#КонецЕсли

#КонецОбласти

#КонецОбласти

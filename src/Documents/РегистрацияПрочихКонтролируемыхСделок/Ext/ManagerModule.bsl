﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Определяет список команд создания на основании.
//
// Параметры:
//   КомандыСозданияНаОсновании - ТаблицаЗначений - Таблица с командами создания на основании. Для изменения.
//       См. описание 1 параметра процедуры СозданиеНаОснованииПереопределяемый.ПередДобавлениемКомандСозданияНаОсновании().
//   Параметры - Структура - Вспомогательные параметры. Для чтения.
//       См. описание 2 параметра процедуры СозданиеНаОснованииПереопределяемый.ПередДобавлениемКомандСозданияНаОсновании().
//
Процедура ДобавитьКомандыСозданияНаОсновании(КомандыСозданияНаОсновании, Параметры) Экспорт
	
	
	
КонецПроцедуры

// Определяет список команд отчетов.
//
// Параметры:
//   КомандыОтчетов - ТаблицаЗначений - Таблица с командами отчетов. Для изменения.
//       См. описание 1 параметра процедуры ВариантыОтчетовПереопределяемый.ПередДобавлениемКомандОтчетов().
//   Параметры - Структура - Вспомогательные параметры. Для чтения.
//       См. описание 2 параметра процедуры ВариантыОтчетовПереопределяемый.ПередДобавлениемКомандОтчетов().
//
Процедура ДобавитьКомандыОтчетов(КомандыОтчетов, Параметры) Экспорт
	
	ВариантыОтчетовУТПереопределяемый.ДобавитьКомандуСтруктураПодчиненности(КомандыОтчетов);
	
КонецПроцедуры

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции "УправлениеПечатью.СоздатьКоллекциюКомандПечати".
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт

	
КонецПроцедуры

#Область ДляВызоваИзДругихПодсистем

// СтандартныеПодсистемы.УправлениеДоступом

// См. УправлениеДоступомПереопределяемый.ПриЗаполненииСписковСОграничениемДоступа.
Процедура ПриЗаполненииОграниченияДоступа(Ограничение) Экспорт

	Ограничение.Текст =
	"РазрешитьЧтениеИзменение
	|ГДЕ
	|	ЗначениеРазрешено(Организация)";

КонецПроцедуры

// Конец СтандартныеПодсистемы.УправлениеДоступом

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Проведение

Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства, Регистры = Неопределено) Экспорт
	
	////////////////////////////////////////////////////////////////////////////
	// Создадим запрос инициализации движений
	
	Запрос = Новый Запрос;
	ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка);
	
	////////////////////////////////////////////////////////////////////////////
	// Сформируем текст запроса
	
	ТекстыЗапроса = Новый СписокЗначений;
	ТекстЗапросаКонтролируемыхСделокОрганизаций(Запрос, ТекстыЗапроса, Регистры);
	
	ПроведениеСерверУТ.ИнициализироватьТаблицыДляДвижений(Запрос, ТекстыЗапроса, ДополнительныеСвойства.ТаблицыДляДвижений, Истина);
	
КонецПроцедуры

Процедура ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка)
	
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.УстановитьПараметр("Ссылка", ДокументСсылка);
	Запрос.Текст = "ВЫБРАТЬ
	               |	ПрочиеКонтролируемыеСделки.Ссылка,
	               |	ПрочиеКонтролируемыеСделки.Дата КАК Период,
	               |	ПрочиеКонтролируемыеСделки.УведомлениеОКонтролируемойСделке КАК Уведомление,
	               |	ПрочиеКонтролируемыеСделки.Организация,
	               |	ПрочиеКонтролируемыеСделки.Контрагент,
	               |	ПрочиеКонтролируемыеСделки.ДоговорКонтрагента КАК Аналитика,
	               |	ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперацииКонтролируемыхСделок.ПрочаяОперация) КАК ХозяйственнаяОперация,
	               |	ПрочиеКонтролируемыеСделки.ВалютаДокумента КАК Валюта,
	               |	ПрочиеКонтролируемыеСделки.Комиссионер КАК Комиссионер
	               |ИЗ
	               |	Документ.РегистрацияПрочихКонтролируемыхСделок КАК ПрочиеКонтролируемыеСделки
	               |ГДЕ
	               |	ПрочиеКонтролируемыеСделки.Ссылка = &Ссылка";
	Реквизиты = Запрос.Выполнить().Выбрать();
	Реквизиты.Следующий();
	
	Запрос.УстановитьПараметр("Период", Реквизиты.Период);
	Запрос.УстановитьПараметр("Ссылка", Реквизиты.Ссылка);
	Запрос.УстановитьПараметр("Уведомление", Реквизиты.Уведомление);
	Запрос.УстановитьПараметр("Организация", Реквизиты.Организация);
	Запрос.УстановитьПараметр("Контрагент", Реквизиты.Контрагент);
	Запрос.УстановитьПараметр("Аналитика", Реквизиты.Аналитика);
	Запрос.УстановитьПараметр("ХозяйственнаяОперация", Реквизиты.ХозяйственнаяОперация);
	Запрос.УстановитьПараметр("Валюта", Реквизиты.Валюта);
	Запрос.УстановитьПараметр("Комиссионер", Реквизиты.Комиссионер);
	Запрос.УстановитьПараметр("ВалютаРегламентированногоУчета", ЗначениеНастроекПовтИсп.ПолучитьВалютуРегламентированногоУчета());
	
КонецПроцедуры

Функция ТекстЗапросаКонтролируемыхСделокОрганизаций(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "КонтролируемыеСделкиОрганизаций";
	
	Если НЕ ПроведениеСерверУТ.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат "";
	КонецЕсли;
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	&Ссылка КАК Регистратор,
	|	ВЫБОР
	|		КОГДА НАЧАЛОПЕРИОДА(ПрочиеКонтролируемыеСделкиСделки.ДатаСовершенияСделки, ДЕНЬ) = НАЧАЛОПЕРИОДА(&Период, ДЕНЬ)
	|				ИЛИ ПрочиеКонтролируемыеСделкиСделки.ДатаСовершенияСделки = ДАТАВРЕМЯ(1, 1, 1)
	|			ТОГДА &Период
	|		ИНАЧЕ ПрочиеКонтролируемыеСделкиСделки.ДатаСовершенияСделки
	|	КОНЕЦ КАК Период,
	|	&Уведомление КАК Уведомление,
	|	&Организация КАК Организация,
	|	&Контрагент КАК Контрагент,
	|	&Аналитика КАК Договор,
	|	ПрочиеКонтролируемыеСделкиСделки.ПредметСделки КАК ПредметСделки,
	|	&ХозяйственнаяОперация КАК ХозяйственнаяОперация,
	|	ПрочиеКонтролируемыеСделкиСделки.Ссылка КАК РасчетныйДокумент,
	|	&Валюта КАК Валюта,
	|	ПрочиеКонтролируемыеСделкиСделки.ЕдиницаИзмерения КАК ЕдиницаИзмерения,
	|	ПрочиеКонтролируемыеСделкиСделки.Количество КАК Количество,
	|	ПрочиеКонтролируемыеСделкиСделки.СуммаБезНДСВРублях КАК СуммаБезНДСВРублях,
	|	ВЫБОР
	|		КОГДА &Валюта = &ВалютаРегламентированногоУчета
	|			ТОГДА ПрочиеКонтролируемыеСделкиСделки.СуммаБезНДСВРублях
	|		ИНАЧЕ ПрочиеКонтролируемыеСделкиСделки.СуммаБезНДСВВалютеРасчетов
	|	КОНЕЦ КАК СуммаБезНДСВВалютеРасчетов,
	|	ПрочиеКонтролируемыеСделкиСделки.СуммаНДСВРублях КАК СуммаНДСВРублях,
	|	ВЫБОР
	|		КОГДА &Валюта = &ВалютаРегламентированногоУчета
	|			ТОГДА ПрочиеКонтролируемыеСделкиСделки.СуммаНДСВРублях
	|		ИНАЧЕ ПрочиеКонтролируемыеСделкиСделки.СуммаНДСВВалютеРасчетов
	|	КОНЕЦ КАК СуммаНДСВВалютеРасчетов,
	|	ПрочиеКонтролируемыеСделкиСделки.НаименованиеПредметаСделки КАК НаименованиеПредметаСделки,
	|	ПрочиеКонтролируемыеСделкиСделки.ТипПредметаСделки КАК ТипПредметаСделки,
	|	ПрочиеКонтролируемыеСделкиСделки.СтранаПроисхожденияПредметаСделки КАК СтранаПроисхожденияПредметаСделки,
	|	ПрочиеКонтролируемыеСделкиСделки.СтавкаНДС КАК СтавкаНДС,
	|	ПрочиеКонтролируемыеСделкиСделки.ТипКонтролируемойСделки КАК ТипКонтролируемойСделки,
	|	ПрочиеКонтролируемыеСделкиСделки.Грузоотправитель КАК Грузоотправитель,
	|	ПрочиеКонтролируемыеСделкиСделки.Грузополучатель КАК Грузополучатель,
	|	ПрочиеКонтролируемыеСделкиСделки.ОперацияОблагаетсяЕНВД КАК ОперацияОблагаетсяЕНВД,
	|	ВЫБОР
	|		КОГДА ПрочиеКонтролируемыеСделкиСделки.ТипПредметаСделки = ЗНАЧЕНИЕ(Перечисление.ТипыПредметовКонтролируемыхСделок.ДолговоеОбязательство)
	|			ТОГДА ПрочиеКонтролируемыеСделкиСделки.ПроцентнаяСтавка
	|		ИНАЧЕ 0
	|	КОНЕЦ КАК ПроцентнаяСтавка,
	|	ВЫБОР
	|		КОГДА ПрочиеКонтролируемыеСделкиСделки.ТипПредметаСделки = ЗНАЧЕНИЕ(Перечисление.ТипыПредметовКонтролируемыхСделок.ДолговоеОбязательство)
	|			ТОГДА ПрочиеКонтролируемыеСделкиСделки.ДатаПроцентнойСтавки
	|		ИНАЧЕ НЕОПРЕДЕЛЕНО
	|	КОНЕЦ КАК ДатаПроцентнойСтавки,
	|	&Комиссионер КАК Комиссионер
	|ИЗ
	|	Документ.РегистрацияПрочихКонтролируемыхСделок.Сделки КАК ПрочиеКонтролируемыеСделкиСделки
	|ГДЕ 
	|	ПрочиеКонтролируемыеСделкиСделки.Ссылка = &Ссылка";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
	Возврат ТекстЗапроса;
	
КонецФункции

#КонецОбласти

// Добавляет команду создания документа "Регистрация прочих контролируемых сделок".
//
// Параметры:
//   КомандыСозданияНаОсновании - ТаблицаЗначений - Таблица с командами создания на основании. Для изменения.
//       См. описание 1 параметра процедуры СозданиеНаОснованииПереопределяемый.ПередДобавлениемКомандСозданияНаОсновании().
//
Функция ДобавитьКомандуСоздатьНаОсновании(КомандыСозданияНаОсновании) Экспорт
	Если ПравоДоступа("Добавление", Метаданные.Документы.РегистрацияПрочихКонтролируемыхСделок) Тогда
		КомандаСоздатьНаОсновании = КомандыСозданияНаОсновании.Добавить();
		КомандаСоздатьНаОсновании.Менеджер = Метаданные.Документы.РегистрацияПрочихКонтролируемыхСделок.ПолноеИмя();
		КомандаСоздатьНаОсновании.Представление = ОбщегоНазначенияУТ.ПредставлениеОбъекта(Метаданные.Документы.РегистрацияПрочихКонтролируемыхСделок);
		КомандаСоздатьНаОсновании.РежимЗаписи = "Проводить";
		КомандаСоздатьНаОсновании.ФункциональныеОпции = "ИспользоватьУведомленияОКонтролируемыхСделках";
	

		Возврат КомандаСоздатьНаОсновании;
	КонецЕсли;

	Возврат Неопределено;
КонецФункции

#КонецОбласти

#КонецЕсли

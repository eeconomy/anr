﻿
//++ ЛокализацияБел  
#Область ОбработчикиСобытий

Процедура ОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;

	МассивИсключаемыхЗначений = Новый Массив;
	МассивИсключаемыхЗначений.Добавить(Перечисления.ВидыПлатежейВГосБюджет.РасходыПоСтрахованию);
	МассивИсключаемыхЗначений.Добавить(Перечисления.ВидыПлатежейВГосБюджет.ПолученоИзФонда);
	МассивИсключаемыхЗначений.Добавить(Перечисления.ВидыПлатежейВГосБюджет.НеПринято);
	МассивИсключаемыхЗначений.Добавить(Перечисления.ВидыПлатежейВГосБюджет.ВзносыСвышеПредела);
	МассивИсключаемыхЗначений.Добавить(Перечисления.ВидыПлатежейВГосБюджет.ВзносыБезСпецоценки);
	МассивИсключаемыхЗначений.Добавить(Перечисления.ВидыПлатежейВГосБюджет.Проценты);
	МассивИсключаемыхЗначений.Добавить(Перечисления.ВидыПлатежейВГосБюджет.Больничный);
	
	ОбщегоНазначенияУТВызовСервера.ДоступныеДляВыбораЗначенияПеречисления(
		"ВидыПлатежейВГосБюджет",
		ДанныеВыбора,
		Параметры,
		МассивИсключаемыхЗначений);
		
КонецПроцедуры

#КонецОбласти
//-- ЛокализацияБел

﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

// 4D:ERP для Беларуси
// Локализация интерфейса 
// {
Процедура ОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	МассивИсключаемыхЗначений = Новый Массив;
	МассивИсключаемыхЗначений.Добавить(Перечисления.ВидыНМА.ДеловаяРепутацияОрганизации);
	
	ОбщегоНазначенияУТВызовСервера.ДоступныеДляВыбораЗначенияПеречисления(
			"ВидыНМА",
			ДанныеВыбора,
			Параметры,
			МассивИсключаемыхЗначений);
			
КонецПроцедуры
// }
// 4D	

#КонецЕсли


﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	ДеревоДата = ТекущаяДатаСеанса();
	
	ПараметрыОтбора = ХранилищеНастроекДанныхФорм.Загрузить("ДеревоРесурсныхСпецификаций", "ПараметрыОтбора");
	Если ЗначениеЗаполнено(ПараметрыОтбора) Тогда
		ЗаполнитьЗначенияСвойств(ЭтаФорма, ПараметрыОтбора);
	Иначе
		ДеревоВыводитьЭтапы                = Истина;
		ДеревоВыводитьВыходныеИзделия      = Истина;
		ДеревоВыводитьМатериалы            = Истина;
		ДеревоВыводитьТрудозатраты         = Истина;
		ДеревоВыводитьЗаголовкиГруппировок = Истина;
	КонецЕсли;
	
	Если Параметры.Свойство("Номенклатура") Тогда
		Если Параметры.Номенклатура.Этогруппа Тогда
			Возврат;
		КонецЕсли;
		ДеревоНоменклатура = Параметры.Номенклатура;
		
		// Проверяем использование характеристик для номенклатуры.
		РезультатПроверки = Справочники.Номенклатура.ХарактеристикаИУпаковкаПринадлежатВладельцу(ДеревоНоменклатура,
			Справочники.ХарактеристикиНоменклатуры.ПустаяСсылка(), Справочники.УпаковкиЕдиницыИзмерения.ПустаяСсылка());
		
		НоменклатураХарактеристикаИспользуется = РезультатПроверки.ХарактеристикиИспользуются;
	КонецЕсли;
	
	Если Параметры.Свойство("ХарактеристикаНоменклатуры") Тогда
		ДеревоХарактеристикаНоменклатуры = Параметры.ХарактеристикаНоменклатуры;
	КонецЕсли;
	
	Если Параметры.Свойство("Спецификация") Тогда
		Спецификация = ПроверитьПолучитьСпецификацию(Параметры.Спецификация);
	КонецЕсли;
	
	Если Параметры.Свойство("Режим") Тогда
		Режим = Параметры.Режим;
	Иначе
		Режим = УправлениеДаннымиОбИзделияхКлиентСервер.РежимДеревоСпецификаций();
	КонецЕсли;
	
	Если Режим = УправлениеДаннымиОбИзделияхКлиентСервер.РежимДеревоСпецификацийЗаказа() Тогда
		
		ЗаполнитьРеквизитыСпецификацииЗаказа(Параметры);
		
		Элементы.ДеревоСпецификаций.ТолькоПросмотр = Истина;
		Элементы.СписокДоступныхСпецификаций.Видимость = Ложь;
	
		ЭтаФорма.РежимОткрытияОкна = РежимОткрытияОкнаФормы.БлокироватьОкноВладельца;
		ЭтаФорма.ЗакрыватьПриЗакрытииВладельца = Истина;
		
	КонецЕсли;
	
	ДинамическоеСчитывание = (Режим = УправлениеДаннымиОбИзделияхКлиентСервер.РежимДеревоСпецификаций());
	ПараметрыВыбораСпецификаций = УправлениеДаннымиОбИзделиях.ПараметрыВыбораСпецификаций(Новый Структура(), Обработки.ДеревоРесурсныхСпецификаций);
	
	Если НЕ Параметры.Свойство("Заголовок", Заголовок) И НЕ Спецификация.Пустая() Тогда
		Заголовок = СтрШаблон(НСтр("ru = 'Дерево спецификации ""%1""';
									|en = '""%1"" BOM tree'"), Спецификация);
	КонецЕсли;
	
	Если Параметры.Свойство("СформироватьПриОткрытии") Тогда
		СформироватьДеревоСпецификацийНаСервере();
	КонецЕсли;
	
	УправлениеДаннымиОбИзделиях.НастроитьУсловноеОформлениеДереваСпецификаций(УсловноеОформление);
	
	УстановитьСвойстваДинамическогоСпискаДоступныхСпецификаций();
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ПроверитьПолучитьСпецификацию(Спецификация)
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	Таблица.Ссылка КАК Спецификация
	|ИЗ
	|	Справочник.РесурсныеСпецификации КАК Таблица
	|ГДЕ
	|	Таблица.Ссылка = &Спецификация И НЕ Таблица.ЭтоГруппа");
	Запрос.УстановитьПараметр("Спецификация", Спецификация);
	
	УстановитьПривилегированныйРежим(Истина);
	Выборка = Запрос.Выполнить().Выбрать();
	УстановитьПривилегированныйРежим(Ложь);
	
	Если Выборка.Следующий() Тогда
		Результат = Выборка.Спецификация;
	Иначе
		Результат = Справочники.РесурсныеСпецификации.ПустаяСсылка();
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если Режим <> УправлениеДаннымиОбИзделияхКлиентСервер.РежимДеревоСпецификацийЗаказа()
		И Не ЗначениеЗаполнено(Спецификация)
		И Не ЗначениеЗаполнено(ДеревоНоменклатура) Тогда
		
		ТекстПредупреждения = НСтр("ru = 'Дерево спецификаций строится в контексте номенклатуры, ресурсной спецификации или спецификации заказа.';
									|en = 'BOM tree is built within the context of products, BOR or order BOM.'");
		ПоказатьПредупреждение(,ТекстПредупреждения);
		
		Отказ = Истина;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)
	
	ТекущаяСтрока = Элементы.ДеревоСпецификаций.ТекущаяСтрока;
	
	Если ИсточникВыбора.ИмяФормы = "Справочник.РесурсныеСпецификации.Форма.ФормаВыбораПоНоменклатуре" Тогда
		
		ИзменитьСпецификациюВТекущейСтроке(ТекущаяСтрока, ВыбранноеЗначение);
	
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "Запись_ОсновныеСпецификации" Тогда
		Элементы.СписокДоступныхСпецификаций.Обновить();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ДатаПриИзменении(Элемент)
	
	ОбновитьДанные();
	
КонецПроцедуры

&НаКлиенте
Процедура ВыводитьЭтапыПриИзменении(Элемент)
	
	ИзмененыПараметрыОтбора();
	
КонецПроцедуры

&НаКлиенте
Процедура ВыводитьВыходныеИзделияПриИзменении(Элемент)
	
	ИзмененыПараметрыОтбора();
	
КонецПроцедуры

&НаКлиенте
Процедура ВыводитьМатериалыПриИзменении(Элемент)
	
	ИзмененыПараметрыОтбора();
	
КонецПроцедуры

&НаКлиенте
Процедура ВыводитьТрудозатратыПриИзменении(Элемент)
	
	ИзмененыПараметрыОтбора();
	
КонецПроцедуры

&НаКлиенте
Процедура ГиперссылкаСписокДоступныхСпецификацийНажатие(Элемент)
	
	СписокДоступныхСпецификацийРазвернут = Не СписокДоступныхСпецификацийРазвернут;
	
	УстановитьСвойстваСворачиваемойГруппы(
				Элементы.СписокДоступныхСпецификаций,
				Элементы.ГиперссылкаСписокДоступныхСпецификаций,
				СписокДоступныхСпецификацийРазвернут);
	ПроверитьУстановитьПараметрыСпискаДоступныхСпецификаций();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыДеревоСпецификаций

&НаКлиенте
Процедура ДеревоСпецификацийВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	Если Поле.Имя = "ДеревоСпецификацийНоменклатура" 
		И ЗначениеЗаполнено(Элемент.ТекущиеДанные.Номенклатура) 
		И ТипЗнч(Элемент.ТекущиеДанные.Номенклатура) <> Тип("Строка") Тогда
		
		СтандартнаяОбработка = Ложь;
		ПоказатьЗначение(Неопределено, Элемент.ТекущиеДанные.Номенклатура);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ДеревоСпецификацийПриАктивизацииСтроки(Элемент)
	
	Если Режим = УправлениеДаннымиОбИзделияхКлиентСервер.РежимДеревоСпецификаций() Тогда
		ПодключитьОбработчикОжидания("ОбработатьАктивизациюСтрокиДерева", 0.1, Истина);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ДеревоСпецификацийПередРазворачиванием(Элемент, Строка, Отказ)
	
	УправлениеДаннымиОбИзделияхКлиентСервер.ДеревоСпецификацийПередРазворачиванием(ЭтаФорма, Строка, Отказ);
	
КонецПроцедуры

&НаКлиенте
Процедура ДеревоСпецификацийПередСворачиванием(Элемент, Строка, Отказ)
	
	УправлениеДаннымиОбИзделияхКлиентСервер.ДеревоСпецификацийПередСворачиванием(ЭтаФорма, Строка, Отказ);
	
КонецПроцедуры

&НаКлиенте
Процедура ДеревоСпецификацийСпецификацияНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ТекущиеДанные = Элементы.ДеревоСпецификаций.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если ТекущиеДанные.ВидСтроки = ПредопределенноеЗначение("Перечисление.ВидыСтрокДереваСпецификаций.ВыходноеИзделие") Тогда
		ИмяТЧ = "ВыходныеИзделия";
	Иначе
		ИмяТЧ = "МатериалыИУслуги";
	КонецЕсли;
	
	УправлениеДаннымиОбИзделияхКлиент.ОткрытьФормуВыбораСпецификацийПоНоменклатуре(
		ТекущиеДанные.Номенклатура,
		ТекущиеДанные.Характеристика,
		,
		,
		ПараметрыВыбораСпецификаций[ИмяТЧ],
		ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ДеревоСпецификацийСпецификацияОчистка(Элемент, СтандартнаяОбработка)
	
	ТекущаяСтрока = Элементы.ДеревоСпецификаций.ТекущаяСтрока;
	ИзменитьСпецификациюВТекущейСтроке(ТекущаяСтрока);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыСписокДоступныхСпецификаций

&НаКлиенте
Процедура СписокДоступныхСпецификацийВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ТекущиеДанные = Элементы.СписокДоступныхСпецификаций.ТекущиеДанные;
	Если ТекущиеДанные <> Неопределено Тогда
		ПоказатьЗначение(,ТекущиеДанные.Спецификация);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура СформироватьДерево(Команда)
	
	ОбновитьДанные();
	
КонецПроцедуры

&НаКлиенте
Процедура ПечатьДереваСпецификации(Команда)
	
	ПараметрКоманды = Новый Массив;
	ПараметрКоманды.Добавить(ПредопределенноеЗначение("Справочник.РесурсныеСпецификации.ПустаяСсылка"));
	
	ИменаМакетов = "ДеревоСпецификаций";
	ПараметрыПечати = ПараметрыПечатиДереваСпецификаций();
	
	УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(
		"Обработка.ДеревоРесурсныхСпецификаций",
		ИменаМакетов,
		ПараметрКоманды,
		ЭтаФорма,
		ПараметрыПечати);
	
КонецПроцедуры

&НаКлиенте
Процедура НазначитьОсновнойСпецификацией(Команда)
	
	ТекущиеДанныеДерево = Элементы.ДеревоСпецификаций.ТекущиеДанные;
	ТекущиеДанныеСписокДоступных = Элементы.СписокДоступныхСпецификаций.ТекущиеДанные;
	
	Если ТекущиеДанныеДерево = Неопределено ИЛИ ТекущиеДанныеСписокДоступных = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(ТекущиеДанныеСписокДоступных.Спецификация) Тогда
		Возврат;
	КонецЕсли;
	
	СтруктураОшибки = Новый Структура;
	СвойстваСпецификации = ОбщегоНазначенияУТВызовСервера.ЗначенияРеквизитовОбъекта(ТекущиеДанныеСписокДоступных.Спецификация, "Статус,ТипПроизводственногоПроцесса");
	
	Если НЕ УправлениеДаннымиОбИзделияхКлиентСервер.СпецификациюМожноНазначитьОсновной(СвойстваСпецификации, СтруктураОшибки) Тогда
		ПоказатьПредупреждение(Неопределено, СтруктураОшибки.ТекстОшибки,, НСтр("ru = 'Назначить основной спецификацией';
																				|en = 'Set as the main BOR'"));
		Возврат;
	КонецЕсли;
	
	ПараметрыФормы = Новый Структура("Спецификация, Номенклатура, Характеристика", 
		ТекущиеДанныеСписокДоступных.Спецификация, ТекущиеДанныеДерево.Номенклатура, ТекущиеДанныеДерево.Характеристика);
	
	ОткрытьФорму("РегистрСведений.ОсновныеСпецификации.Форма.НазначитьОсновнойСпецификацией", ПараметрыФормы);
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаРазвернутьВсе(Команда)
	
	РазвернутьВсе();
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаСвернутьВсе(Команда)
	
	СвернутьВсе();
	
КонецПроцедуры

&НаКлиенте
Процедура СкрыватьЗаголовкиГруппировок(Команда)
	
	ДеревоВыводитьЗаголовкиГруппировок = НЕ ДеревоВыводитьЗаголовкиГруппировок;
	Элементы.СкрыватьЗаголовкиГруппировок.Пометка = НЕ ДеревоВыводитьЗаголовкиГруппировок;
	
	ИзмененыПараметрыОтбора();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	СписокДоступныхСпецификаций.УсловноеОформление.Элементы.Очистить();
	Справочники.РесурсныеСпецификации.УстановитьУсловноеОформлениеСпискаДоступныхСпецификаций(СписокДоступныхСпецификаций.УсловноеОформление);
	
КонецПроцедуры

&НаСервере
Процедура УстановитьСвойстваДинамическогоСпискаДоступныхСпецификаций()
	
	СвойстваСписка = ОбщегоНазначения.СтруктураСвойствДинамическогоСписка();
	
	ТекстЗапроса = Справочники.РесурсныеСпецификации.ТекстЗапросаДинамическогоСпискаДоступныхСпецификаций();
	СвойстваСписка.ТекстЗапроса = ТекстЗапроса;
	
	СвойстваСписка.ОсновнаяТаблица = "";
	СвойстваСписка.ДинамическоеСчитываниеДанных = Ложь;
	
	ОбщегоНазначения.УстановитьСвойстваДинамическогоСписка(Элементы.СписокДоступныхСпецификаций, СвойстваСписка);
	
	СписокДоступныхСпецификаций.Параметры.УстановитьЗначениеПараметра("Номенклатура", ДеревоНоменклатура);
	СписокДоступныхСпецификаций.Параметры.УстановитьЗначениеПараметра("Характеристика", ДеревоХарактеристикаНоменклатуры);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьРеквизитыСпецификацииЗаказа(ДанныеЗаполнения)
	
	РеквизитыСпецификацииЗаказа = Новый Структура;
	
	РеквизитыСпецификацииЗаказа.Вставить("КлючСвязи", ДанныеЗаполнения.КлючСвязи);
	
	РеквизитыСпецификацииЗаказа.Вставить("УпаковкаВыходногоИзделия", ДанныеЗаполнения.Упаковка);
	РеквизитыСпецификацииЗаказа.Вставить("КоличествоВыходногоИзделия", ДанныеЗаполнения.Количество);
	РеквизитыСпецификацииЗаказа.Вставить("КоличествоУпаковокВыходногоИзделия", ДанныеЗаполнения.КоличествоУпаковок);
	
	РеквизитыСпецификацииЗаказа.Вставить("АдресВХранилище", Параметры.АдресВХранилище);

КонецПроцедуры

&НаСервере
Функция ПараметрыПечатиДереваСпецификаций()
	
	Дерево = УправлениеДаннымиОбИзделиях.ПрочитатьДеревоСпецификаций(ЭтаФорма);
	
	ПараметрыПечати = Новый Структура;
	ПараметрыПечати.Вставить("АдресВХранилище", ПоместитьВоВременноеХранилище(Дерево, УникальныйИдентификатор));
	
	Возврат ПараметрыПечати;
	
КонецФункции

&НаКлиенте
Процедура ОбновитьДанные()

	СформироватьДеревоСпецификацийНаСервере();
	
	РазвернутьВсе(Ложь);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработатьАктивизациюСтрокиДерева()
	
	ПроверитьУстановитьПараметрыСпискаДоступныхСпецификаций();
	
КонецПроцедуры

&НаСервере
Функция ПараметрыДереваСпецификаций()
	
	ПараметрыДерева = УправлениеДаннымиОбИзделияхКлиентСервер.ПараметрыДереваСпецификаций();
	
	ПараметрыДерева.Номенклатура = ДеревоНоменклатура;
	ПараметрыДерева.ХарактеристикаНоменклатуры = ДеревоХарактеристикаНоменклатуры;
	
	ПараметрыДерева.Спецификация = Спецификация;
	ПараметрыДерева.Дата = ДеревоДата;
	
	ПараметрыДерева.ВыводитьЭтапы = ДеревоВыводитьЭтапы;
	ПараметрыДерева.ВыводитьВыходныеИзделия = ДеревоВыводитьВыходныеИзделия;
	ПараметрыДерева.ВыводитьМатериалы = ДеревоВыводитьМатериалы;
	ПараметрыДерева.ВыводитьТрудозатраты = ДеревоВыводитьТрудозатраты;
	ПараметрыДерева.ВыводитьЗаголовкиГруппировок = ДеревоВыводитьЗаголовкиГруппировок;
	
	ПараметрыДерева.ДинамическоеСчитывание = ДинамическоеСчитывание;
	
	Если Режим = УправлениеДаннымиОбИзделияхКлиентСервер.РежимДеревоСпецификаций() Тогда
		ПараметрыДерева.РазузловыватьПолуфабрикаты = Истина;
	КонецЕсли;
	
	Если Режим = УправлениеДаннымиОбИзделияхКлиентСервер.РежимДеревоСпецификацийЗаказа() Тогда
		ЗаполнитьЗначенияСвойств(ПараметрыДерева, РеквизитыСпецификацииЗаказа);
	КонецЕсли;
	
	Возврат ПараметрыДерева;
	
КонецФункции

&НаСервере
Процедура СформироватьДеревоСпецификацийНаСервере()
	
	Если ИзмененыПараметрыОтбора Тогда
		СохранитьПараметрыОтбора();
	КонецЕсли;
	
	ПараметрыДерева = ПараметрыДереваСпецификаций();
	
	УправлениеДаннымиОбИзделиях.ПостроитьДеревоСпецификаций(ЭтаФорма, ПараметрыДерева);
	
КонецПроцедуры

&НаКлиенте
Функция НайтиСтрокуПоИдентификатору(КоллекцияЭлементовДерева, Идентификатор)
	
	Для Каждого Строка Из КоллекцияЭлементовДерева Цикл
		
		Если Строка.Идентификатор = Идентификатор Тогда
			Возврат Строка;
		Иначе
			Результат = НайтиСтрокуПоИдентификатору(Строка.ПолучитьЭлементы(), Идентификатор);
			Если Результат <> Неопределено Тогда
				Возврат Результат;
			КонецЕсли;
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат Неопределено;
	
КонецФункции

&НаКлиенте
Процедура ПроверитьУстановитьПараметрыСпискаДоступныхСпецификаций()
	
	Если Не СписокДоступныхСпецификацийРазвернут Тогда
		Возврат;
	КонецЕсли;
	
	ТекущиеДанные = Элементы.ДеревоСпецификаций.ТекущиеДанные;
	
	Если ТекущиеДанные <> Неопределено Тогда
		
		Если ТипЗнч(ТекущиеДанные.Номенклатура) = Тип("СправочникСсылка.Номенклатура") Тогда
			НовыйОтборНоменклатура = ТекущиеДанные.Номенклатура;
		Иначе
			НовыйОтборНоменклатура = Неопределено;
		КонецЕсли;
		
		НовыйОтборХарактеристика = ТекущиеДанные.Характеристика;
		
	Иначе
		НовыйОтборНоменклатура = Неопределено;
		НовыйОтборХарактеристика = Неопределено;
	КонецЕсли;
	
	ТекущийОтборНоменклатура = СписокДоступныхСпецификаций.Параметры.Элементы.Найти("Номенклатура").Значение;
	ТекущийОтборХарактеристика = СписокДоступныхСпецификаций.Параметры.Элементы.Найти("Характеристика").Значение;
	
	Если НовыйОтборНоменклатура <> ТекущийОтборНоменклатура ИЛИ НовыйОтборХарактеристика <> ТекущийОтборХарактеристика Тогда
		УстановитьПараметрыСпискаДоступныхСпецификаций(НовыйОтборНоменклатура, НовыйОтборХарактеристика);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьПараметрыСпискаДоступныхСпецификаций(ТекущаяНоменклатура, ТекущаяХарактеристика = Неопределено)
	
	СписокДоступныхСпецификаций.Параметры.УстановитьЗначениеПараметра("Номенклатура", ТекущаяНоменклатура);
	СписокДоступныхСпецификаций.Параметры.УстановитьЗначениеПараметра("Характеристика", ?(ЗначениеЗаполнено(ТекущаяХарактеристика), ТекущаяХарактеристика, Справочники.ХарактеристикиНоменклатуры.ПустаяСсылка()));
	
КонецПроцедуры

&НаСервере
Процедура ИзменитьСпецификациюВСтрокеНаСервере(ТекущаяСтрока)
	
	ТекущиеДанные = ДеревоСпецификаций.НайтиПоИдентификатору(ТекущаяСтрока);
	
	ПараметрыДерева = ПараметрыДереваСпецификаций();
	ПараметрыДерева.Спецификация = Справочники.РесурсныеСпецификации.ПустаяСсылка();
	
	УправлениеДаннымиОбИзделиях.ИзменитьСпецификациюВСтрокеДерева(ЭтаФорма, ПараметрыДерева, ТекущиеДанные);
	
КонецПроцедуры

&НаКлиенте
Процедура ИзменитьСпецификациюВТекущейСтроке(ТекущаяСтрока, НоваяСпецификация = Неопределено)
	
	ТекущиеДанные = ДеревоСпецификаций.НайтиПоИдентификатору(ТекущаяСтрока);
	
	ТекущиеДанные.ПолучитьЭлементы().Очистить();
	
	ТекущиеДанные.Спецификация = НоваяСпецификация;
	
	ТекущиеДанные.ЕстьСпецификация = ЗначениеЗаполнено(НоваяСпецификация);
	ТекущиеДанные.СпецификацияПрочитана = Ложь;
	
	Если ТекущиеДанные.ЕстьСпецификация Тогда
		ИзменитьСпецификациюВСтрокеНаСервере(ТекущаяСтрока);
	КонецЕсли;
	
	РазвернутьДеревоСпецификаций(ТекущаяСтрока, ТекущиеДанные.Идентификатор);

КонецПроцедуры

&НаКлиенте
Процедура РазвернутьДеревоСпецификаций(ТекущаяСтрока, Идентификатор)
	
	Если Элементы.ДеревоСпецификаций.ТекущаяСтрока <> ТекущаяСтрока Тогда
		
		ТекущиеДанные = НайтиСтрокуПоИдентификатору(ДеревоСпецификаций.ПолучитьЭлементы(), Идентификатор);
		Если ТекущиеДанные <> Неопределено Тогда
			Элементы.ДеревоСпецификаций.ТекущаяСтрока = ТекущиеДанные.ПолучитьИдентификатор();
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура РазвернутьВсе(СПодчиненными = Истина)

	СтрокиДерева = ДеревоСпецификаций.ПолучитьЭлементы();
	Для каждого СтрокаДерева Из СтрокиДерева Цикл
		Элементы.ДеревоСпецификаций.Развернуть(СтрокаДерева.ПолучитьИдентификатор(), СПодчиненными);
	КонецЦикла; 

КонецПроцедуры

&НаКлиенте
Процедура СвернутьВсе()

	СтрокиДерева = ДеревоСпецификаций.ПолучитьЭлементы();
	Для каждого СтрокаДерева Из СтрокиДерева Цикл
		Элементы.ДеревоСпецификаций.Свернуть(СтрокаДерева.ПолучитьИдентификатор());
	КонецЦикла; 

КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьСвойстваСворачиваемойГруппы(ЭлементГруппа, КартинкаГруппы, ГруппаРазвернута)

	ЭлементГруппа.Видимость = ГруппаРазвернута;
	
	Если ГруппаРазвернута Тогда
		КартинкаГруппы.Картинка = БиблиотекаКартинок.СтрелкаВниз;
	Иначе
		КартинкаГруппы.Картинка = БиблиотекаКартинок.СтрелкаВправо;
	КонецЕсли; 

КонецПроцедуры

&НаКлиенте
Процедура ИзмененыПараметрыОтбора()
	
	ИзмененыПараметрыОтбора = Истина;
	Элементы.ПечатьДереваСпецификаций.Доступность = Ложь;
	
КонецПроцедуры

&НаСервере
Процедура СохранитьПараметрыОтбора()
	
	ПараметрыОтбора = Новый Структура("
		|ДеревоВыводитьЭтапы,
		|ДеревоВыводитьВыходныеИзделия,
		|ДеревоВыводитьМатериалы,
		|ДеревоВыводитьТрудозатраты,
		|ДеревоВыводитьЗаголовкиГруппировок");
	ЗаполнитьЗначенияСвойств(ПараметрыОтбора, ЭтаФорма);
	
	ХранилищеНастроекДанныхФорм.Сохранить("ДеревоРесурсныхСпецификаций", "ПараметрыОтбора", ПараметрыОтбора);
	
	Элементы.ПечатьДереваСпецификаций.Доступность = Истина;
	ИзмененыПараметрыОтбора = Ложь;
	
КонецПроцедуры

#КонецОбласти

﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда	

#Область СлужебныеПроцедурыИФункции

Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
	ПараметрыВывода.ДоступнаПечатьПоКомплектно = Истина;
	
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "РасчетСреднегоЗаработка") Тогда
		ДанныеДокументов = ДанныеДокументовРасчетаСреднегоЗаработкаФСС(МассивОбъектов);
		ТабличныйДокумент = ТабличныйДокументРасчетаСреднегоЗаработка(ДанныеДокументов, ОбъектыПечати);
		Если НЕ ТабличныйДокумент = Неопределено Тогда
			УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, "РасчетСреднегоЗаработка", НСтр("ru = 'Расчет среднего заработка'"), ТабличныйДокумент);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

Функция ТабличныйДокументРасчетаСреднегоЗаработка(ДанныеДокументов, ОбъектыПечати, ВыводитьЗаголовок = Истина, КомпактныйРежим = Ложь) Экспорт
	
	ТабличныйДокумент = Новый ТабличныйДокумент;
	ТабличныйДокумент.ОриентацияСтраницы = ОриентацияСтраницы.Портрет;
	ТабличныйДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_РасчетСреднегоЗаработкаФСС";
	
	Для каждого ДанныеДокумента Из ДанныеДокументов Цикл
		
			
		Если ТабличныйДокумент.ВысотаТаблицы > 0 Тогда
			ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли;
		
		НомерСтрокиНачало = ТабличныйДокумент.ВысотаТаблицы + 1;
		
		ВывестиТабличныйДокументРасчетаСреднегоЗаработка(ДанныеДокумента, ОбъектыПечати, ТабличныйДокумент, ВыводитьЗаголовок);
			
		Обработки.ПечатьРасчетаСреднегоЗаработка.ЗадатьОбластьПечатиДокумента(ТабличныйДокумент, НомерСтрокиНачало, ОбъектыПечати, ДанныеДокумента.РеквизитыДокумента.Ссылка, ДанныеДокумента.РеквизитыДокумента.Сотрудник);
		
	КонецЦикла;
	
	Возврат ТабличныйДокумент;
	
КонецФункции 

// Подготавливает табличный документ с печатными формами для массива ссылок.
//
// Параметры:
//	МассивСсылок 		- массив ссылок на документы поддерживающие печать среднего заработка.
//	ВыводитьЗаголовок 	- признак того, что надо формировать полную печатную форму.
//
// Возвращаемое значение - структура в которой содержатся
// 		- ТабличныйДокумент, табличный документ с областями для каждой ссылки из массива ссылок.
// 		- ОбъектыПечати, соответствие, ключом которой является ссылка, а значением - имя области табличного документа,
// 			в которой хранится печатная форма для этой ссылки.
//
Функция ОбластиДляВстраивания(ДанныеДокументов, ВыводитьЗаголовок = Ложь, КомпактныйРежим = Ложь) Экспорт
	
	ОбъектыПечати = Обработки.ПечатьРасчетаСреднегоЗаработка.ОбъектыПечатиДляВстраиваемыхОбластей();
	
	ТабличныйДокумент = ТабличныйДокументРасчетаСреднегоЗаработка(ДанныеДокументов, ОбъектыПечати, ВыводитьЗаголовок, КомпактныйРежим);
	
	Возврат Новый Структура("ОбъектыПечати,ТабличныйДокумент", ОбъектыПечати, ТабличныйДокумент);
	
КонецФункции

Процедура ВывестиТабличныйДокументРасчетаСреднегоЗаработка(ДанныеДокумента, ОбъектыПечати, ТабличныйДокумент, ВыводитьЗаголовок)
	
	ОбластиМакета = ОбластиМакета(ДанныеДокумента.ПараметрыРасчета.ИспользоватьДниБолезниУходаЗаДетьми, ДанныеДокумента.ПараметрыРасчета.ПрименятьПредельнуюВеличину И НЕ ДанныеДокумента.ПараметрыРасчета.НеполныйРасчетныйПериод);

	Если ВыводитьЗаголовок Тогда
		ВывестиШапку(ТабличныйДокумент, ОбластиМакета.Шапка, ДанныеДокумента.РеквизитыДокумента, ДанныеДокумента.КадровыеДанныеСотрудника, ДанныеДокумента.ПараметрыРасчета);
	КонецЕсли;
	
	ТабличныйДокумент.Вывести(ОбластиМакета.ЗаголовокНачислений);
	
	ВывестиТаблицуЗаработка(ТабличныйДокумент, ОбластиМакета, ДанныеДокумента);
	
	ТабличныйДокумент.Вывести(ОбластиМакета.РасчетСреднегоЗаработкаЗаголовок);
	
	ВывестиРасчетСреднегоЗаработка(ТабличныйДокумент, ОбластиМакета, ДанныеДокумента);
	
	//ВывестиМРОТ(ТабличныйДокумент, ОбластиМакета, ДанныеДокумента);
	
	Если ДанныеДокумента.ПараметрыРасчета.НеполныйРасчетныйПериод Тогда
		ВывестиМаксимальныйСреднийНеполногоРасчетногоПериода(ТабличныйДокумент, ОбластиМакета, ДанныеДокумента);
	ИначеЕсли ДанныеДокумента.ПараметрыРасчета.ИспользоватьДниБолезниУходаЗаДетьми Тогда	
		ВывестиМаксимальныйСреднийПоМатеринству(ТабличныйДокумент, ОбластиМакета, ДанныеДокумента);
	КонецЕсли;	
	
КонецПроцедуры

Процедура ВывестиТабличныйДокументРасчетаСреднегоЗаработкаКомпактный(ДанныеДокумента, ОбъектыПечати, ТабличныйДокумент)
	
	ОбластиМакета = ОбластиМакета(ДанныеДокумента.ПараметрыРасчета.ИспользоватьДниБолезниУходаЗаДетьми, ДанныеДокумента.ПараметрыРасчета.ПрименятьПредельнуюВеличину);
	
	ТабличныйДокумент.Вывести(ОбластиМакета.ПустойЗаголовокНачислений);
	ВывестиТаблицуЗаработка(ТабличныйДокумент, ОбластиМакета, ДанныеДокумента);
	ВывестиРасчетСреднегоЗаработка(ТабличныйДокумент, ОбластиМакета, ДанныеДокумента);
	
КонецПроцедуры

Процедура ВывестиШапку(ТабличныйДокумент, ОбластьШапка, РеквизитыДокумента, КадровыеДанныеСотрудника, ПараметрыРасчета)
	ЗначенияПараметров = Новый Структура;
	ЗначенияПараметров.Вставить("СинонимДокумента", 			РеквизитыДокумента.Ссылка.Метаданные().Синоним);
	ЗначенияПараметров.Вставить("НомерДокумента", 				РеквизитыДокумента.НомерДокумента);
	ЗначенияПараметров.Вставить("ДатаДокумента", 				Формат(РеквизитыДокумента.ДатаДокумента, "ДЛФ=DD"));
	ЗначенияПараметров.Вставить("ДатаНачалаОтсутствия", 		Формат(РеквизитыДокумента.ДатаНачалаОтсутствия, "ДЛФ=D"));
	ЗначенияПараметров.Вставить("ДатаОкончанияОтсутствия", 		Формат(РеквизитыДокумента.ДатаОкончанияОтсутствия,"ДЛФ=D"));
	ЗначенияПараметров.Вставить("НаименованиеОрганизации", 		?(ЗначениеЗаполнено(РеквизитыДокумента.ПолноеНаименованиеОрганизации), РеквизитыДокумента.ПолноеНаименованиеОрганизации, РеквизитыДокумента.НаименованиеОрганизации)); 	
	ЗначенияПараметров.Вставить("ВидЗанятости", 				КадровыеДанныеСотрудника.ВидЗанятости);
	ЗначенияПараметров.Вставить("Подразделение", 				КадровыеДанныеСотрудника.Подразделение);
	ЗначенияПараметров.Вставить("Должность", 					КадровыеДанныеСотрудника.Должность);
	ЗначенияПараметров.Вставить("ФИОРаботника", 				КадровыеДанныеСотрудника.ФИОПолные);
	ЗначенияПараметров.Вставить("ТабельныйНомер", 				КадровыеДанныеСотрудника.ТабельныйНомер);
	ЗначенияПараметров.Вставить("ПериодРасчетаСреднегоЗаработкаНачало", Формат(ПараметрыРасчета.ПериодРасчетаСреднегоЗаработкаНачало,"ДЛФ=D"));
	ЗначенияПараметров.Вставить("ПериодРасчетаСреднегоЗаработкаОкончание", Формат(ПараметрыРасчета.ПериодРасчетаСреднегоЗаработкаОкончание,"ДЛФ=D"));
	//ЗначенияПараметров.Вставить("РасчетныеГоды", 				ОписаниеРасчетныхЛет(РасчетныеГоды));  //1с-минск
	ЗаполнитьЗначенияСвойств(ОбластьШапка.Параметры, ЗначенияПараметров);
	ТабличныйДокумент.Вывести(ОбластьШапка);
КонецПроцедуры

Процедура ВывестиТаблицуЗаработка(ТабличныйДокумент, ОбластиМакета, ДанныеДокумента)
	
	ТабличныйДокумент.Вывести(ОбластиМакета.ЗаголовокТаблицы);
	
	ЗначенияПараметров = Новый Структура;
	Период = ДанныеДокумента.ПараметрыРасчета.ПериодРасчетаСреднегоЗаработкаНачало;
	
	Заработок 		   = 0;
	КалендарныхДней    = 0;
	ПредельнаяВеличина = 0;
	
	Для каждого Месяц Из ДанныеДокумента.ПараметрыРасчета.ПериодыРасчета Цикл
		
		СтрокаПоМесяцу  = УчетСреднегоЗаработкаКлиентСервер.ЭлементКоллекцииПоОтбору(ДанныеДокумента.ДанныеРасчетаСреднего,   			Новый Структура("Период", Месяц));
		СтрокаВремени   = УчетСреднегоЗаработкаКлиентСервер.ЭлементКоллекцииПоОтбору(ДанныеДокумента.ПараметрыРасчета.ДанныеВремени,    Новый Структура("Период", Месяц));
		СтрокаЗаработка = УчетСреднегоЗаработкаКлиентСервер.ЭлементКоллекцииПоОтбору(ДанныеДокумента.ПараметрыРасчета.ДанныеНачислений, Новый Структура("Период,СоставнаяЧасть", Месяц,Перечисления.УчетНачисленийВСреднемЗаработкеФСС.ОбщийЗаработок));
		СтрокаПремия  	= УчетСреднегоЗаработкаКлиентСервер.ЭлементКоллекцииПоОтбору(ДанныеДокумента.ПараметрыРасчета.ДанныеНачислений, Новый Структура("Период,СоставнаяЧасть", Месяц,Перечисления.УчетНачисленийВСреднемЗаработкеФСС.ПремияМесячная));
		
		Если СтрокаПоМесяцу = Неопределено Тогда
			Продолжить;
		КонецЕсли;
		
		ЗначенияПараметров.Очистить();
		Если СтрокаПоМесяцу.Заработок <> 0 ИЛИ СтрокаПоМесяцу.КалендарныхДней <> 0 Тогда
			ЗначенияПараметров.Очистить();
			Норма 	   = 0;
			Отработано = 0; 			
			
			Если СтрокаЗаработка <> Неопределено Тогда 
				ЗначенияПараметров.Вставить("ОбщийЗаработок", 	СтрокаЗаработка.Сумма);
			КонецЕсли;   
			
			Если СтрокаВремени <> Неопределено Тогда 
				Если ДанныеДокумента.ПараметрыРасчета.РасчетПремии = Перечисления.ВидыУчетаВремени.ПоЧасам Тогда
					Норма 	   = СтрокаВремени.НормаЧасов;
					Отработано = СтрокаВремени.ОтработаноЧасов;
				Иначе
					Норма 	   = СтрокаВремени.НормаДней;
					Отработано = СтрокаВремени.ОтработаноДней;
				КонецЕсли;
				КоэфПремии = ?(Отработано/Норма > 1,1,Отработано/Норма); 
				ЗначенияПараметров.Вставить("ОтработаноНорма", 	"" +Отработано + "/"+Норма); 
			Иначе
				КоэфПремии = 1;
			КонецЕсли;   
			
			Если СтрокаПремия <> Неопределено Тогда 
				ЗначенияПараметров.Вставить("НачисленоПремии", СтрокаПремия.Сумма);
				ЗначенияПараметров.Вставить("УчтеноПремии",    СтрокаПремия.Сумма*КоэфПремии);
			Иначе
				ЗначенияПараметров.Вставить("НачисленоПремии", "");
				ЗначенияПараметров.Вставить("УчтеноПремии",    "");	
			КонецЕсли;   			
			
			ЗначенияПараметров.Вставить("Заработок", 		  СтрокаПоМесяцу.Заработок);
			ЗначенияПараметров.Вставить("КалендарныхДней", 	  СтрокаПоМесяцу.КалендарныхДней);
			ЗначенияПараметров.Вставить("ПредельнаяВеличина", СтрокаПоМесяцу.ПредельнаяВеличина);
			ЗначенияПараметров.Вставить("Год", 				  Год(СтрокаПоМесяцу.Период));
			ЗначенияПараметров.Вставить("Месяц", 			  Месяц(СтрокаПоМесяцу.Период));
			
			Заработок 			= Заработок + СтрокаПоМесяцу.Заработок;
			КалендарныхДней 	= КалендарныхДней + СтрокаПоМесяцу.КалендарныхДней;
			ПредельнаяВеличина 	= ПредельнаяВеличина + СтрокаПоМесяцу.ПредельнаяВеличина;  	

			ЗаполнитьЗначенияСвойств(ОбластиМакета.Строка.Параметры, ЗначенияПараметров);
			ТабличныйДокумент.Вывести(ОбластиМакета.Строка); 
			
		КонецЕсли;
		
	КонецЦикла;
	
	ЗначенияПараметров.Вставить("Заработок", 		  Заработок);
	ЗначенияПараметров.Вставить("КалендарныхДней", 	  КалендарныхДней);
	ЗначенияПараметров.Вставить("ПредельнаяВеличина", ПредельнаяВеличина);
	
	ЗаполнитьЗначенияСвойств(ОбластиМакета.ТекущийИтог.Параметры, ЗначенияПараметров);
	ТабличныйДокумент.Вывести(ОбластиМакета.ТекущийИтог);
	

	
	
	
	//ВсегоЗаработка = УчетПособийСоциальногоСтрахованияРасширенныйКлиентСервер.УчитываемыйЗаработокФСЗН(ДанныеДокумента.ПараметрыРасчета, ДанныеДокумента.ДанныеРасчетаСреднего);
	//ВсегоДней       = УчетПособийСоциальногоСтрахованияРасширенныйКлиентСервер.УчитываемыйЗаработокФСЗН(ДанныеДокумента.ПараметрыРасчета, ДанныеДокумента.ДанныеРасчетаСреднего);
	//
	//ЗначенияПараметров.Вставить("ВсегоЗаработка", Формат(ВсегоЗаработка, "ЧЦ=15; ЧДЦ=2; ЧН=0.00"));
	//ЗначенияПараметров.Вставить("ВсегоДней", ВсегоДней);
	//ЗначенияПараметров.Вставить("Средий", Формат(ВсегоЗаработка/ВсегоДней, "ЧЦ=15; ЧДЦ=2; ЧН=0.00"));
	//ЗаполнитьЗначенияСвойств(ОбластиМакета.ПодвалТаблицыЗаработка.Параметры, ЗначенияПараметров);
	//ТабличныйДокумент.Вывести(ОбластиМакета.ПодвалТаблицыЗаработка);

КонецПроцедуры

Процедура ВывестиСтрокуПоСтрахователю(ТабличныйДокумент, ДанныеДокумента, ОбластиМакета, ЗначенияПараметров)	
	ЗаполнитьЗначенияСвойств(ОбластиМакета.Строка.Параметры, ЗначенияПараметров);
	ТабличныйДокумент.Вывести(ОбластиМакета.Строка);
	Если ДанныеДокумента.ПараметрыРасчета.ИспользоватьДниБолезниУходаЗаДетьми Тогда
		ЗаполнитьЗначенияСвойств(ОбластиМакета.СтрокаДнейБолезниУходаЗаДетьми.Параметры, ЗначенияПараметров);
		ТабличныйДокумент.Присоединить(ОбластиМакета.СтрокаДнейБолезниУходаЗаДетьми);
	КонецЕсли;
	Если ДанныеДокумента.ПараметрыРасчета.УчитыватьЗаработокПредыдущихСтрахователей Тогда
		ЗаполнитьЗначенияСвойств(ОбластиМакета.СтрокаСтрахователь.Параметры, ЗначенияПараметров);
		ТабличныйДокумент.Присоединить(ОбластиМакета.СтрокаСтрахователь);
	КонецЕсли;
КонецПроцедуры

Процедура ВывестиРасчетСреднегоЗаработка(ТабличныйДокумент, ОбластиМакета, ДанныеДокумента)
	
	УчитываемыхДнейВКалендарныхГодах = УчетПособийСоциальногоСтрахованияРасширенныйКлиентСервер.УчитываемыхДнейФСЗН(ДанныеДокумента.ПараметрыРасчета, ДанныеДокумента.ДанныеРасчетаСреднего);
	ВсегоЗаработка = УчетПособийСоциальногоСтрахованияРасширенныйКлиентСервер.УчитываемыйЗаработокФСЗН(ДанныеДокумента.ПараметрыРасчета, ДанныеДокумента.ДанныеРасчетаСреднего);
	СреднедневнойЗаработок = УчетПособийСоциальногоСтрахованияРасширенныйКлиентСервер.СреднедневнойЗаработокФСС(ВсегоЗаработка, УчитываемыхДнейВКалендарныхГодах);

	ЗначенияПараметров = Новый Структура;
	ЗначенияПараметров.Вставить("ВсегоЗаработка", 					Формат(ВсегоЗаработка , "ЧЦ=15; ЧДЦ=2; ЧН=0.00"));
	ЗначенияПараметров.Вставить("УчитываемыхДнейВКалендарныхГодах", Формат(УчитываемыхДнейВКалендарныхГодах, "ЧДЦ="));
	ЗначенияПараметров.Вставить("СреднедневнойЗаработок", 			Формат(СреднедневнойЗаработок, "ЧЦ=15; ЧДЦ=2; ЧН=0.00"));
	ЗаполнитьЗначенияСвойств(ОбластиМакета.РасчетСреднегоЗаработка.Параметры, ЗначенияПараметров);
	ТабличныйДокумент.Вывести(ОбластиМакета.РасчетСреднегоЗаработка);

КонецПроцедуры

Процедура ВывестиМРОТ(ТабличныйДокумент, ОбластиМакета, ДанныеДокумента)
	
	ЗначенияПараметров = Новый Структура;
	ЗначенияПараметров.Вставить("ДатаНачалаСобытия", 	Формат(ДанныеДокумента.ПараметрыРасчета.ДатаНачалаСобытия, "ДЛФ=D"));
	ЗначенияПараметров.Вставить("МРОТ", 				ДанныеДокумента.ПараметрыРасчета.МинимальныйРазмерОплатыТрудаРФ);
	ЗаполнитьЗначенияСвойств(ОбластиМакета.ШапкаМРОТ.Параметры,	ЗначенияПараметров);
	ТабличныйДокумент.Вывести(ОбластиМакета.ШапкаМРОТ);
	
	Если ДанныеДокумента.ПараметрыРасчета.ДоляНеполногоВремени < 1 Тогда
		ЗначенияПараметров.Очистить();
		ЗначенияПараметров.Вставить("ДоляНеполногоВремени", ДанныеДокумента.ПараметрыРасчета.ДоляНеполногоВремени);
		ЗаполнитьЗначенияСвойств(ОбластиМакета.НеполноеВремя.Параметры, ЗначенияПараметров);
		ТабличныйДокумент.Вывести(ОбластиМакета.НеполноеВремя);
	КонецЕсли;
	
	МинимальныйСреднедневнойЗаработок = УчетПособийСоциальногоСтрахованияРасширенныйКлиентСервер.МинимальныйСреднедневнойЗаработокФСС(ДанныеДокумента.ПараметрыРасчета);	

	ЗначенияПараметров.Вставить("СреднийМРОТ", МинимальныйСреднедневнойЗаработок);
	ЗаполнитьЗначенияСвойств(ОбластиМакета.ОкончаниеМРОТ.Параметры,	ЗначенияПараметров);
	ТабличныйДокумент.Вывести(ОбластиМакета.ОкончаниеМРОТ);
	
КонецПроцедуры

Процедура ВывестиМаксимальныйСреднийПоМатеринству(ТабличныйДокумент, ОбластиМакета, ДанныеДокумента)
	
	ПредшествующиеГоды  = "";
	
	Для каждого РасчетныйГод Из ДанныеДокумента.ПараметрыРасчета.РасчетныеГоды Цикл
		ПредшествующиеГоды = ПредшествующиеГоды + Формат(РасчетныйГод, "ЧГ=0") +  НСтр("ru = ' и '");
	КонецЦикла;
	СтроковыеФункцииКлиентСервер.УдалитьПоследнийСимволВСтроке(ПредшествующиеГоды, 3);
	
	ЗначенияПараметров = Новый Структура;
	ЗначенияПараметров.Вставить("ПредшествующиеГоды", 	ПредшествующиеГоды);
	ЗаполнитьЗначенияСвойств(ОбластиМакета.МаксимальныйСреднийПоМатеринствуШапка.Параметры,	ЗначенияПараметров);
	ТабличныйДокумент.Вывести(ОбластиМакета.МаксимальныйСреднийПоМатеринствуШапка);
	
	Для каждого РасчетныйГод Из ДанныеДокумента.ПараметрыРасчета.РасчетныеГоды Цикл
		СтрокаПоГоду = УчетСреднегоЗаработкаКлиентСервер.ЭлементКоллекцииПоОтбору(ДанныеДокумента.ДанныеРасчетаСреднего, Новый Структура("РасчетныйГод", РасчетныйГод));
		ЗначенияПараметров.Очистить();
		ЗначенияПараметров.Вставить("РасчетныйГод", 		Формат(РасчетныйГод, "ЧГ=0"));
		ЗначенияПараметров.Вставить("ПредельнаяВеличина", 	СтрокаПоГоду.ПредельнаяВеличина);
		ЗаполнитьЗначенияСвойств(ОбластиМакета.МаксимальныйСреднийПоМатеринствуСтрока.Параметры, ЗначенияПараметров);
		ТабличныйДокумент.Вывести(ОбластиМакета.МаксимальныйСреднийПоМатеринствуСтрока);
	КонецЦикла;
	
	МаксимальныйСреднедневнойЗаработок = УчетПособийСоциальногоСтрахованияРасширенныйКлиентСервер.МаксимальныйСреднедневнойЗаработокДляОплатыПособияПоМатеринству(ДанныеДокумента.ДанныеРасчетаСреднего);
	
	ЗначенияПараметров.Очистить();
	ЗначенияПараметров.Вставить("МаксимальныйСреднедневнойЗаработок", 	МаксимальныйСреднедневнойЗаработок);
	ЗаполнитьЗначенияСвойств(ОбластиМакета.МаксимальныйСреднийПоМатеринствуПодвал.Параметры,	ЗначенияПараметров);
	ТабличныйДокумент.Вывести(ОбластиМакета.МаксимальныйСреднийПоМатеринствуПодвал);
	
КонецПроцедуры

Процедура ВывестиМаксимальныйСреднийНеполногоРасчетногоПериода(ТабличныйДокумент, ОбластиМакета, ДанныеДокумента)
	
	ДатаНачалаСобытия = ДанныеДокумента.ПараметрыРасчета.ДатаНачалаСобытия;
	
	МаксимальныйСреднедневнойЗаработок = УчетПособийСоциальногоСтрахованияРасширенныйКлиентСервер.МаксимальныйСреднедневнойЗаработокДляНеполногоРасчетногоПериода(ДатаНачалаСобытия);
	
	ЗначенияПараметров = Новый Структура;
	ЗначенияПараметров.Вставить("МаксимальныйСреднедневнойЗаработок", 	МаксимальныйСреднедневнойЗаработок);
	ЗначенияПараметров.Вставить("ГодНаступленияСтраховогоСлучая", 		Формат(ДатаНачалаСобытия, "ДФ=yyyy"));
	ЗаполнитьЗначенияСвойств(ОбластиМакета.МаксимальныйСреднийНеполногоПериода.Параметры, ЗначенияПараметров);
	ТабличныйДокумент.Вывести(ОбластиМакета.МаксимальныйСреднийНеполногоПериода);
	
КонецПроцедуры

Функция ОбластиМакета(ИспользоватьДниБолезниУходаЗаДетьми, ПрименятьПредельнуюВеличину)
	
	ОбластиМакета = Новый Структура;
	
	Макет = УправлениеПечатью.МакетПечатнойФормы("Обработка.ПечатьРасчетаСреднегоЗаработкаФСС.ПФ_MXL_РасчетСреднегоЗаработкаПособийЛокализация");
	
	ОбластиМакета.Вставить("Шапка", 								Макет.ПолучитьОбласть("Заголовок")); 
	//ОбластиМакета.Вставить("НеполноеВремя", 						Макет.ПолучитьОбласть("НеполноеВремя"));
	ОбластиМакета.Вставить("ЗаголовокНачислений", 					Макет.ПолучитьОбласть("ЗаголовокНачислений")); 
	//ОбластиМакета.Вставить("ПустойЗаголовокНачислений", 			Макет.ПолучитьОбласть("ПустойЗаголовокНачислений")); 
	ОбластиМакета.Вставить("РасчетСреднегоЗаработкаЗаголовок", 		Макет.ПолучитьОбласть("РасчетСреднегоЗаработкаЗаголовок"));
	ОбластиМакета.Вставить("РасчетСреднегоЗаработка", 				Макет.ПолучитьОбласть("РасчетСреднегоЗаработка"));
	
	ОбластиМакета.Вставить("ЗаголовокТаблицы", 			Макет.ПолучитьОбласть("ЗаголовокТаблицы")); 
	ОбластиМакета.Вставить("Строка", 					Макет.ПолучитьОбласть("СтрокаЗаработка"));
	ОбластиМакета.Вставить("ПодвалТаблицыЗаработка", 	Макет.ПолучитьОбласть("ПодвалТаблицыЗаработка"));
	ОбластиМакета.Вставить("ТекущийИтог", 				Макет.ПолучитьОбласть("ТекущийИтог"));

	
	
	//Если ПрименятьПредельнуюВеличину Тогда
	//	ОбластиМакета.Вставить("ЗаголовокТаблицы", 			Макет.ПолучитьОбласть("ЗаголовокТаблицыНачало")); 
	//	ОбластиМакета.Вставить("Строка", 					Макет.ПолучитьОбласть("СтрокаЗаработкаНачало"));
	//	ОбластиМакета.Вставить("ТекущийИтог", 				Макет.ПолучитьОбласть("ТекущийИтог"));
	//	ОбластиМакета.Вставить("ПодвалТаблицыЗаработка", 	Макет.ПолучитьОбласть("ПодвалТаблицыЗаработка")); 
	//Иначе
	//	ОбластиМакета.Вставить("ЗаголовокТаблицы", 			Макет.ПолучитьОбласть("ЗаголовокТаблицыНачалоБезОграничений")); 
	//	ОбластиМакета.Вставить("Строка", 					Макет.ПолучитьОбласть("СтрокаЗаработкаНачалоБезОграничений"));
	//	ОбластиМакета.Вставить("ТекущийИтог", 				Макет.ПолучитьОбласть("ТекущийИтогБезОграничений"));
	//	ОбластиМакета.Вставить("ПодвалТаблицыЗаработка", 	Макет.ПолучитьОбласть("ПодвалТаблицыЗаработкаБезОграничений")); 
	//КонецЕсли;
	
	//Если ИспользоватьДниБолезниУходаЗаДетьми Тогда
	//	ОбластиМакета.Вставить("ЗаголовокТаблицыДнейБолезниУходаЗаДетьми", 	Макет.ПолучитьОбласть("ЗаголовокТаблицыДнейБолезниУходаЗаДетьми")); 
	//	ОбластиМакета.Вставить("СтрокаДнейБолезниУходаЗаДетьми", 			Макет.ПолучитьОбласть("СтрокаЗаработкаДнейБолезниУходаЗаДетьми"));
	//	ОбластиМакета.Вставить("ЗаголовокТаблицыСтрахователь", 				Макет.ПолучитьОбласть("ЗаголовокТаблицыСтрахователь")); 
	//	ОбластиМакета.Вставить("СтрокаСтрахователь", 						Макет.ПолучитьОбласть("СтрокаЗаработкаСтрахователь"));
	//	ОбластиМакета.Вставить("МаксимальныйСреднийПоМатеринствуШапка", 	Макет.ПолучитьОбласть("МаксимальныйСреднийПоМатеринствуШапка"));
	//	ОбластиМакета.Вставить("МаксимальныйСреднийПоМатеринствуСтрока", 	Макет.ПолучитьОбласть("МаксимальныйСреднийПоМатеринствуСтрока"));
	//	ОбластиМакета.Вставить("МаксимальныйСреднийПоМатеринствуПодвал", 	Макет.ПолучитьОбласть("МаксимальныйСреднийПоМатеринствуПодвал"));
	//иначе 
	//	ОбластиМакета.Вставить("ЗаголовокТаблицыСтрахователь", 				Макет.ПолучитьОбласть("ДлинныйЗаголовокТаблицыСтрахователь")); 
	//	ОбластиМакета.Вставить("СтрокаСтрахователь", 						Макет.ПолучитьОбласть("ДлиннаяСтрокаЗаработкаСтрахователь"));
	//КонецЕсли;
	
	Возврат ОбластиМакета;
	
КонецФункции

Функция ОписаниеРасчетныхЛет(РасчетныеГоды)
			
	ОписаниеРасчетныхЛет = НСтр("ru = 'Не указаны'");   
	
	ВсегоСтрок = РасчетныеГоды.Количество();
	
	Если ВсегоСтрок = 2 Тогда
		ОписаниеРасчетныхЛет = Формат(РасчетныеГоды[0], "ЧЦ=4; ЧГ=0") + " и " + Формат(РасчетныеГоды[1], "ЧЦ=4; ЧГ=0");
	ИначеЕсли ВсегоСтрок = 1 Тогда	
		ОписаниеРасчетныхЛет = Формат(РасчетныеГоды[0], "ЧЦ=4; ЧГ=0");
	КонецЕсли;
	
	Возврат ОписаниеРасчетныхЛет;
КонецФункции

Функция ДокументыСгруппированныеПоТипам(МассивСсылок)
	
	ДокументыСгруппированныеПоТипам = Новый Соответствие;
	
	Для каждого Ссылка Из МассивСсылок Цикл
		
		Менеджер = ОбщегоНазначения.МенеджерОбъектаПоСсылке(Ссылка);
		
		МассивСсылок = ДокументыСгруппированныеПоТипам.Получить(Менеджер);
		
		Если МассивСсылок = Неопределено Тогда
			ДокументыСгруппированныеПоТипам.Вставить(Менеджер, Новый Массив);
			МассивСсылок = ДокументыСгруппированныеПоТипам.Получить(Менеджер);
		КонецЕсли;
		
		МассивСсылок.Добавить(Ссылка);
		
	КонецЦикла;
	
	Возврат ДокументыСгруппированныеПоТипам;
	
КонецФункции

Функция ДанныеДокументовРасчетаСреднегоЗаработкаФСС(МассивСсылок)
	
	ДанныеДокументов = Новый Массив;
	
	ДокументыСгруппированныеПоТипам = ДокументыСгруппированныеПоТипам(МассивСсылок);
	
	Для каждого ОписаниеТипаДокумента Из ДокументыСгруппированныеПоТипам Цикл
		
		Менеджер = ОписаниеТипаДокумента.Ключ;
		МассивСсылок = ОписаниеТипаДокумента.Значение;
		
		ДанныеДокументовПоТипу = Менеджер.ДанныеДокументовДляПечатиРасчетаСреднегоЗаработкаФСС(МассивСсылок);
		
		ОбщегоНазначенияКлиентСервер.ДополнитьМассив(ДанныеДокументов, ДанныеДокументовПоТипу);
	     		
	КонецЦикла;

	Возврат ДанныеДокументов
	
КонецФункции

Функция ПустаяСтруктураДанныхДляПечатиСреднегоЗаработка() Экспорт
	
	СтруктураДанных = Новый Структура;
	
	СтруктураДанных.Вставить("РеквизитыДокумента", ПустаяСтруктураРеквизитовДокументаДляПечатиСреднегоЗаработка());
	СтруктураДанных.Вставить("КадровыеДанныеСотрудника", ПустаяСтруктураКадровыхДанныхСотрудникаДляПечатиСреднегоЗаработка());
	СтруктураДанных.Вставить("ПараметрыРасчета", УчетПособийСоциальногоСтрахованияРасширенныйКлиентСервер.ПараметрыРасчетаСреднегоДневногоЗаработкаФСС());
	СтруктураДанных.Вставить("ДанныеРасчетаСреднего", Новый Массив);
	
	Возврат СтруктураДанных;
	
КонецФункции

Функция ПустаяСтруктураРеквизитовДокументаДляПечатиСреднегоЗаработка()
	
	СтруктураДанных = Новый Структура;
	
	СтруктураДанных.Вставить("Ссылка");
	СтруктураДанных.Вставить("Сотрудник");
	СтруктураДанных.Вставить("НомерДокумента");
	СтруктураДанных.Вставить("ДатаДокумента");
	СтруктураДанных.Вставить("ДатаНачалаОтсутствия");
	СтруктураДанных.Вставить("ДатаОкончанияОтсутствия");
	СтруктураДанных.Вставить("ПолноеНаименованиеОрганизации");
	СтруктураДанных.Вставить("НаименованиеОрганизации");
	
	Возврат СтруктураДанных;

КонецФункции

Функция ПустаяСтруктураКадровыхДанныхСотрудникаДляПечатиСреднегоЗаработка()
	
	СтруктураДанных = Новый Структура;
	СтруктураДанных.Вставить("ФизическоеЛицо");
	СтруктураДанных.Вставить("ФИОПолные");
	СтруктураДанных.Вставить("ТабельныйНомер");
	СтруктураДанных.Вставить("Подразделение");
	СтруктураДанных.Вставить("Должность");
	СтруктураДанных.Вставить("ВидЗанятости");
	
	Возврат СтруктураДанных;

КонецФункции

#КонецОбласти

#КонецЕсли
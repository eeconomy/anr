﻿
#Область Выгрузка

Процедура ЗаполнитьТаблицуСчетовВыгрузки() Экспорт
	ТаблицаСчетов = Обработки.КлиентБанкЛокализацияБел.ТаблицаСчетов();
	БанковскиеСчета.Загрузить(ТаблицаСчетов);
КонецПроцедуры

Процедура ВыгрузитьПлатежи(ИдФормы = "") Экспорт
	Отбор = Новый Структура("Пометка", Истина);
	ВыгружаемыеБанковскиеСчета = БанковскиеСчета.Выгрузить(БанковскиеСчета.НайтиСтроки(Отбор));
	ИнформацияОбОбъединенииФайлов = ИнформацияОбОбъединенииФайлов(ВыгружаемыеБанковскиеСчета);
	
	Если ИнформацияОбОбъединенииФайлов.СчетаБезПравил.Количество() <> 0 Тогда
		Для каждого Счет Из ИнформацияОбОбъединенииФайлов.СчетаБезПравил Цикл
			Если Не Счет.Пометка Или Не ЗначениеЗаполнено(Счет.НастройкаОбмена) Или Счет.Выгружается Тогда
				Продолжить;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	Если ИнформацияОбОбъединенииФайлов.КоличествоСчетовВПравиле.Количество() <> 0 Тогда
		ВыгрузитьПлатежиПорциямиПоПравилам(ИнформацияОбОбъединенииФайлов, ИдФормы);
	КонецЕсли;
	
	ОбновитьИнформациюОВыгруженныхФайлах(ИнформацияОбОбъединенииФайлов);
КонецПроцедуры

Процедура ВыгрузитьПлатежиПорциямиПоПравилам(ИнформацияОбОбъединенииФайлов, ИдФормы = "") Экспорт
	ПотокиВыгрузки = ИнформацияОбОбъединенииФайлов.ПотокиВыгрузки;
	КоличествоСчетовВПравиле = ИнформацияОбОбъединенииФайлов.КоличествоСчетовВПравиле;
	
	Для Каждого ПотокВыгрузки Из ПотокиВыгрузки Цикл
		СтрокиКВыгрузке = ДокументыКВыгрузкеПоСчетам(ПотокВыгрузки.Счета);
		
		Если СтрокиКВыгрузке.Количество() Тогда
			ПараметрыВыгрузки = Новый Структура;
			ПараметрыВыгрузки.Вставить("БанковскийСчет", ПотокВыгрузки.Счета);
			ПараметрыВыгрузки.Вставить("НастройкаОбмена", ПотокВыгрузки.Правило);
			ПараметрыВыгрузки.Вставить("Кодировка", ПотокВыгрузки.Счета[0].Кодировка);
			ПараметрыВыгрузки.Вставить("КодировкаДругая", ПотокВыгрузки.Счета[0].КодировкаДругая);
			ПараметрыВыгрузки.Вставить("Программа", ПотокВыгрузки.Счета[0].Программа);
			
			ТаблицаДокументов = ДокументыКВыгрузке.Выгрузить(СтрокиКВыгрузке);
			ПараметрыВыгрузки.Вставить("ТаблицаДокументов", ТаблицаДокументов);
			
			ИзменитьРеквизитыСчетовПотока(ПотокВыгрузки.Счета, Новый Структура("Выгружен, АдресХранилищаДокументов", Ложь, ПоместитьВоВременноеХранилище(ТаблицаДокументов, ИдФормы)));
			Результат = ДлительныеОперации.ЗапуститьВыполнениеВФоне(ИдФормы, "Обработки.КлиентБанкЛокализацияБел.Выгрузить", ПараметрыВыгрузки, НСтр("ru='Выгрузка платежей в банк'"));
			ИзменитьРеквизитыСчетовПотока(ПотокВыгрузки.Счета, Новый Структура("АдресХранилищаФайла", Результат.АдресХранилища));
			
			Если Результат.ЗаданиеВыполнено Тогда
				ИзменитьРеквизитыСчетовПотока(ПотокВыгрузки.Счета, Новый Структура("СохранитьФайл", Истина));
			Иначе
				ИзменитьРеквизитыСчетовПотока(ПотокВыгрузки.Счета, Новый Структура("Выгружается, ИдентификаторВыгрузки", Истина, Результат.ИдентификаторЗадания));
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

Функция ПечатьОтчетаОВыгрузке(ВыделенныеСтроки) Экспорт
	ПолеОтчета = Неопределено;
	
	Для каждого ВыделеннаяСтрока Из ВыделенныеСтроки Цикл
		Если ПолеОтчета = Неопределено Тогда
			ПолеОтчета = Новый ТабличныйДокумент;
			ПолеОтчета.ОриентацияСтраницы = ОриентацияСтраницы.Ландшафт;
		КонецЕсли;
		
		Счет = БанковскиеСчета.Получить(ВыделеннаяСтрока);
		
		Если Счет.Выгружен Тогда
			Если ЭтоАдресВременногоХранилища(Счет.АдресХранилищаДокументов) Тогда
				ВыгруженныеДокументы = ПолучитьИзВременногоХранилища(Счет.АдресХранилищаДокументов);
				Если ВыгруженныеДокументы <> Неопределено Тогда
					ОтборПоСчету = Новый Структура("БанковскийСчет", Счет.Ссылка);
					ДокументыПоСчету = ВыгруженныеДокументы.Скопировать(ОтборПоСчету);
					ВывестиСекциюОтчетаОВыгрузке(ПолеОтчета, ДокументыПоСчету, Счет.Ссылка, Счет.Выгружен);
				КонецЕсли;
			КонецЕсли;
		Иначе
			Отбор = Новый Структура;
			Отбор.Вставить("Выгружать", Истина);
			Отбор.Вставить("БанковскийСчет", Счет.Ссылка);
			ДокументыКВыгрузкеПоСчету = ДокументыКВыгрузке.Выгрузить(Отбор);
			Если ДокументыКВыгрузкеПоСчету.Количество() Тогда
				ВывестиСекциюОтчетаОВыгрузке(ПолеОтчета, ДокументыКВыгрузкеПоСчету, Счет.Ссылка, Счет.Выгружен);
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
	Возврат ПолеОтчета;
КонецФункции

Процедура ЗаписатьДатуВыгрузкиПлатежей() Экспорт
	Для каждого Счет Из БанковскиеСчета Цикл
		Если Счет.Выгружен Тогда
			Если ЭтоАдресВременногоХранилища(Счет.АдресХранилищаДокументов) Тогда
				ВыгруженныеДокументы = ПолучитьИзВременногоХранилища(Счет.АдресХранилищаДокументов);
				Если ТипЗнч(ВыгруженныеДокументы) = Тип("ТаблицаЗначений") Тогда
					Для каждого ВыгруженныйДокумент Из ВыгруженныеДокументы Цикл
						ДокументОбъект = ВыгруженныйДокумент.Ссылка.ПолучитьОбъект();
						ДокументОбъект.ДатаВыгрузки = ТекущаяДата();
						ДокументОбъект.Записать();
					КонецЦикла;
				КонецЕсли;
			КонецЕсли;
			
			РегистрыСведений.ПоследнийОбменСБанками.СоздатьЗапись(Счет.Ссылка, Новый Структура("ДатаВыгрузки", ТекущаяДата()));
			Счет.ДатаПоследнейВыгрузки = ТекущаяДата();
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

Процедура ЗаполнитьТаблицуПлатежей(ДокументыОтбор = Неопределено) Экспорт
	ДокументыКВыгрузке.Загрузить(Обработки.КлиентБанкЛокализацияБел.ТаблицаДокументовКВыгрузке(СписокСчетов, ДатаНачалаВыгрузки, ДатаКонцаВыгрузки, ТолькоНеВыгруженные));
	ДокументыКВыгрузке.Сортировать("Дата");
	
	Если ДокументыОтбор <> Неопределено Тогда
		Для Каждого ДокументКВыгрузке Из ДокументыКВыгрузке Цикл
			Отбор = Новый Структура("Ссылка", ДокументКВыгрузке.Ссылка);
			НайденныеСтроки = ДокументыОтбор.НайтиСтроки(Отбор);
			Если НайденныеСтроки.Количество() = 0 Тогда
				ДокументКВыгрузке.Выгружать = Ложь;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	ПроверитьДокументыКВыгрузке();
КонецПроцедуры

Процедура ПроверитьДокументыКВыгрузке()
	Проверки = Новый Соответствие;
	Проверки.Вставить("Номер", НСтр("ru='Номер документа'"));
	Проверки.Вставить("Дата", НСтр("ru='Дата документа'"));
	Проверки.Вставить("Сумма", НСтр("ru='Сумма документа'"));
	
	ПроверкиОрганизацииПР = Новый Соответствие;
	ПроверкиОрганизацииПР.Вставить("ОрганизацияРасчСчет", НСтр("ru='Банковский счет организации'"));
	ПроверкиОрганизацииПР.Вставить("ОрганизацияНаим", НСтр("ru='Наименование организации'"));
	ПроверкиОрганизацииПР.Вставить("ОрганизацияИНН", НСтр("ru='ИНН организации'"));
	
	ПроверкиОрганизацииНПР = Новый Соответствие;
	ПроверкиОрганизацииНПР.Вставить("ОрганизацияРасчСчет", НСтр("ru='Банковский счет организации'"));
	ПроверкиОрганизацииНПР.Вставить("ОрганизацияБанкДляРасчетов", НСтр("ru='Банк для расчетов организации'"));
	ПроверкиОрганизацииНПР.Вставить("ОрганизацияГородБанка", НСтр("ru='Город банка организации'"));
	ПроверкиОрганизацииНПР.Вставить("ОрганизацияБИКРЦБанка", НСтр("ru='БИК РЦ банка организации'"));
	
	ПроверкиКонтрагентаПР = Новый Соответствие;
	ПроверкиКонтрагентаПР.Вставить("КонтрагентРасчСчет", НСтр("ru='Банковский счет контрагента'"));
	ПроверкиКонтрагентаПР.Вставить("КонтрагентНаим", НСтр("ru='Контрагент'"));
	ПроверкиКонтрагентаПР.Вставить("КонтрагентИНН", НСтр("ru='ИНН контрагента'"));
	
	ПроверкиКонтрагентаНПР = Новый Соответствие;
	ПроверкиКонтрагентаНПР.Вставить("КонтрагентРасчСчет", НСтр("ru='Банковский счет контрагента'"));
	ПроверкиКонтрагентаНПР.Вставить("КонтрагентБанкДляРасчетов", НСтр("ru='Банк для расчетов контрагента'"));
	ПроверкиКонтрагентаНПР.Вставить("КонтрагентГородБанка", НСтр("ru='Город банка контрагента'"));
	ПроверкиКонтрагентаНПР.Вставить("КонтрагентБИКРЦБанка", НСтр("ru='БИК РЦ банка контрагента'"));
	
	ПроверкиВыдачаПодотчетнику = Новый Соответствие;
	ПроверкиВыдачаПодотчетнику.Вставить("КонтрагентРасчСчет", НСтр("ru='Банковский счет контрагента'"));
	ПроверкиВыдачаПодотчетнику.Вставить("КонтрагентНаим", НСтр("ru='Контрагент'"));
	ПроверкиВыдачаПодотчетнику.Вставить("НазначениеПлатежа", НСтр("ru='Назначение платежа'"));
	
	ПроверкиОрганизацииПР.Вставить("НазначениеПлатежа", НСтр("ru='Назначение платежа'"));
	ПроверкиОрганизацииНПР.Вставить("НазначениеПлатежа", НСтр("ru='Назначение платежа'"));
	ПроверкиКонтрагентаПР.Вставить("НазначениеПлатежа", НСтр("ru='Назначение платежа'"));
	ПроверкиКонтрагентаНПР.Вставить("НазначениеПлатежа", НСтр("ru='Назначение платежа'"));
	
	Для каждого ДокументКВыгрузке Из ДокументыКВыгрузке Цикл
		СтрокаОшибки = "";
		
		Для каждого Проверка Из Проверки Цикл
			Если Не ЗначениеЗаполнено(ДокументКВыгрузке[Проверка.Ключ]) Тогда
				СтрокаОшибки = СтрокаОшибки + " " + Проверка.Значение + ",";
			КонецЕсли;
		КонецЦикла;
		
		Если Не ЗначениеЗаполнено(ДокументКВыгрузке.ОрганизацияБанкДляРасчетов) Тогда
			ПроверкаСчетаОрганизации = ПроверкиОрганизацииПР;
		Иначе
			ПроверкаСчетаОрганизации = ПроверкиОрганизацииНПР;
		КонецЕсли;
		
		Для каждого Проверка Из ПроверкаСчетаОрганизации Цикл
			Если Проверка.Ключ = "НазначениеПлатежа" И (ЗначениеЗаполнено(ДокументКВыгрузке[Проверка.Ключ]) Или ЗначениеЗаполнено(ДокументКВыгрузке["Комментарий"])) Тогда
				Продолжить;
			КонецЕсли;
			
			Если Не ЗначениеЗаполнено(ДокументКВыгрузке[Проверка.Ключ]) Тогда
				СтрокаОшибки = СтрокаОшибки + " " + Проверка.Значение + ",";
			КонецЕсли;
		КонецЦикла;
		
		Если Не ЗначениеЗаполнено(ДокументКВыгрузке.КонтрагентБанкДляРасчетов) Тогда
			Если ДокументКВыгрузке.ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВыдачаДенежныхСредствПодотчетнику Тогда
				ПроверкаСчетаКонтрагента = ПроверкиВыдачаПодотчетнику;
			Иначе
				ПроверкаСчетаКонтрагента = ПроверкиКонтрагентаПР;
			КонецЕсли;
		Иначе
			ПроверкаСчетаКонтрагента = ПроверкиКонтрагентаНПР;
		КонецЕсли;
		
		Для каждого Проверка Из ПроверкаСчетаКонтрагента Цикл
			Если Проверка.Ключ = "НазначениеПлатежа" И (ЗначениеЗаполнено(ДокументКВыгрузке[Проверка.Ключ]) Или ЗначениеЗаполнено(ДокументКВыгрузке["Комментарий"])) Тогда
				Продолжить;
			КонецЕсли;
			
			// 4D:ERP для Беларуси, ВладимирР, 28.04.2022 18:59:39
			// Проверка УНП, №33366
			// {
			Если (ДокументКВыгрузке.ЮрФизЛицо = Перечисления.ЮрФизЛицо.ЮрЛицоНеРезидент
				Или ДокументКВыгрузке.ЮрФизЛицо = Перечисления.ЮрФизЛицо.ФизЛицо)
				И Проверка.Ключ = "КонтрагентИНН" Тогда
				
				Продолжить;
			КонецЕсли;
			// }
			// 4D
			
			Если Не ЗначениеЗаполнено(ДокументКВыгрузке[Проверка.Ключ]) Тогда
				СтрокаОшибки = СтрокаОшибки + " " + Проверка.Значение + ",";
			КонецЕсли;
		КонецЦикла;
		
		Если ЗначениеЗаполнено(СтрокаОшибки) Тогда
			СтрокаОшибки = Лев(СтрокаОшибки, СтрДлина(СтрокаОшибки) - 1);
			ДокументКВыгрузке.ЕстьОшибка = Истина;
			ДокументКВыгрузке.Выгружать = Ложь;
			ДокументКВыгрузке.ОписаниеОшибок = НСтр("ru='Не заполнены реквизиты платежа:'") + СтрокаОшибки;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

Функция ДокументыКВыгрузкеПоСчетам(Счета)
	ДокументыКВыгрузкеПоСчетам = Новый Массив;
	
	Для Каждого Счет Из Счета Цикл
		Отбор = Новый Структура;
		Отбор.Вставить("Выгружать", Истина);
		Отбор.Вставить("БанковскийСчет", Счет.Ссылка);
		
		НайденыеДокументы = ДокументыКВыгрузке.НайтиСтроки(Отбор);
		
		Для Каждого НайденныйДокумент Из НайденыеДокументы Цикл
			ДокументыКВыгрузкеПоСчетам.Добавить(НайденныйДокумент);
		КонецЦикла;
	КонецЦикла;
	
	Возврат ДокументыКВыгрузкеПоСчетам;
КонецФункции

Процедура ВывестиСекциюОтчетаОВыгрузке(ПолеОтчета, Документы, БанковскийСчет, Выгружен)
	МакетОтчета = ПолучитьМакет("ОтчетОВыгрузке");
	Шапка = МакетОтчета.ПолучитьОбласть("Шапка");
	Строка = МакетОтчета.ПолучитьОбласть("Строка");
	Подвал = МакетОтчета.ПолучитьОбласть("Подвал");
	НазвОрг = МакетОтчета.ПолучитьОбласть("НазваниеОрганизации");
	
	РеквизитыСчета = Справочники.БанковскиеСчетаОрганизаций.ПолучитьРеквизитыБанковскогоСчетаОрганизации(БанковскийСчет);
	
	НазвОрг.Параметры.НазваниеОрганизации = Строка(РеквизитыСчета.Организация);
	ПолеОтчета.Вывести(НазвОрг);
	
	Если Выгружен Тогда
		Шапка.Параметры.ИмяОтчета = НСтр("ru='Отчет о выгруженных документах'");
	Иначе
		Шапка.Параметры.ИмяОтчета = НСтр("ru='Документы к выгрузке'");
	КонецЕсли;
	
	Шапка.Параметры.ИмяСуммыПоступления = "Поступление";
	Шапка.Параметры.ИмяСуммыСписания = "Списание";
	
	ОбрБанковскийСчет = "";
	Индекс = 0;
	ИтогоСуммаП = 0;
	ИтогоСуммаР = 0;
	
	Документы.Сортировать("Дата");
	НачПериода = Документы[0].Дата;
	КонПериода = Документы[Документы.Количество() - 1].Дата;
	
	Валюта = ?(ЗначениеЗаполнено(РеквизитыСчета.Валюта), СокрЛП(Строка(РеквизитыСчета.Валюта)), НСтр("ru='валюта не указана'"));
	НачалоОтчетногоПериода = Формат(НачПериода, "ДЛФ=D");
	КонецОтчетногоПериода = Формат(КонПериода, "ДЛФ=D");
	
	Шапка.Параметры.ОписаниеПериода = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru='по счету %1 (%2) с %3 по %4'"), РеквизитыСчета.НомерСчета, Валюта, НачалоОтчетногоПериода, КонецОтчетногоПериода);
	ПолеОтчета.Вывести(Шапка);
	
	Для Каждого СтрокаИсточника Из Документы Цикл
		Валюта = ?(ЗначениеЗаполнено(СтрокаИсточника.Валюта), СокрЛП(Строка(СтрокаИсточника.Валюта)), НСтр("ru='валюта не указана'"));
		Если СтрокаИсточника.ВидДокумента = Перечисления.ТипыПлатежныхДокументов.ПлатежноеТребование Тогда
			Строка.Параметры.Получатель = СтрокаИсточника.ОрганизацияНаим;
			Строка.Параметры.ПолучательСчет = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru='%1 (%2)'"), СтрокаИсточника.ОрганизацияРасчСчет, Валюта);
			Строка.Параметры.Плательщик = СтрокаИсточника.Контрагент;
			Строка.Параметры.ПлательщикСчет = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru='%1 (%2)'"), СтрокаИсточника.КонтрагентРасчСчет, Валюта);
			Строка.Параметры.СуммаПоступление = СтрокаИсточника.Сумма;
			Строка.Параметры.СуммаСписание = "";
			ИтогоСуммаП = ИтогоСуммаП + СтрокаИсточника.Сумма;
		Иначе
			Строка.Параметры.Плательщик = СтрокаИсточника.ОрганизацияНаим;
			Строка.Параметры.ПлательщикСчет = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru='%1 (%2)'"), СтрокаИсточника.ОрганизацияРасчСчет, Валюта);
			Строка.Параметры.Получатель = СтрокаИсточника.Контрагент;
			Строка.Параметры.ПолучательСчет = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru='%1 (%2)'"), СтрокаИсточника.КонтрагентРасчСчет, Валюта);
			Строка.Параметры.СуммаПоступление = "";
			Строка.Параметры.СуммаСписание = СтрокаИсточника.Сумма;
			ИтогоСуммаР = ИтогоСуммаР + СтрокаИсточника.Сумма;
		КонецЕсли;
		
		Строка.Параметры.Документ = СтрокаИсточника.Ссылка;
		Индекс = Индекс + 1;
		Строка.Параметры.Индекс = Индекс;
		
		ПолеОтчета.Вывести(Строка);
	КонецЦикла;
	
	Подвал.Параметры.ИтогоСуммаП = ИтогоСуммаП;
	Подвал.Параметры.ИтогоСуммаР = ИтогоСуммаР;
	
	ПолеОтчета.Вывести(Подвал);
	ПолеОтчета.ОтображатьГруппировки = Ложь;
	ПолеОтчета.ОтображатьЗаголовки = Ложь;
	ПолеОтчета.ОтображатьСетку = Ложь;
	ПолеОтчета.ТолькоПросмотр = Истина;
КонецПроцедуры

Функция ДокументыКВыгрузкеПоСчету(Счет)
	Отбор = Новый Структура;
	Отбор.Вставить("Выгружать", Истина);
	Отбор.Вставить("БанковскийСчет", Счет.Ссылка);
	Возврат ДокументыКВыгрузке.НайтиСтроки(Отбор);
КонецФункции

Процедура ВыгрузитьПлатежиПоСчету(Счет, ИдФормы = "") Экспорт
	Счет.Выгружен = Ложь;
	
	СтрокиКВыгрузке = ДокументыКВыгрузкеПоСчету(Счет);
	Если СтрокиКВыгрузке.Количество() Тогда
		ПараметрыВыгрузки = Новый Структура;
		ПараметрыВыгрузки.Вставить("БанковскийСчет", Счет.Ссылка);
		ПараметрыВыгрузки.Вставить("НастройкаОбмена", Счет.НастройкаОбмена);
		ПараметрыВыгрузки.Вставить("Кодировка", Счет.Кодировка);
		ПараметрыВыгрузки.Вставить("КодировкаДругая", Счет.КодировкаДругая);
		ПараметрыВыгрузки.Вставить("Программа", Счет.Программа);
		
		ТаблицаДокументов = ДокументыКВыгрузке.Выгрузить(СтрокиКВыгрузке);
		ПараметрыВыгрузки.Вставить("ТаблицаДокументов", ТаблицаДокументов);
		
		Счет.АдресХранилищаДокументов = ПоместитьВоВременноеХранилище(ТаблицаДокументов, ИдФормы);
		Результат = ДлительныеОперации.ЗапуститьВыполнениеВФоне(ИдФормы, "Обработки.КлиентБанкЛокализацияБел.Выгрузить", ПараметрыВыгрузки, НСтр("ru='Выгрузка платежей в банк'"));
		Счет.АдресХранилищаФайла = Результат.АдресХранилища;
		
		Если Результат.ЗаданиеВыполнено Тогда
			Счет.СохранитьФайл = Истина;
		Иначе
			Счет.Выгружается = Истина;
			Счет.ИдентификаторВыгрузки = Результат.ИдентификаторЗадания;
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#Область Загрузка

Процедура ЗаполнитьТаблицуСчетовЗагрузки() Экспорт
	ТаблицаСчетов = Обработки.КлиентБанкЛокализацияБел.ТаблицаСчетов(Истина);
	Для каждого Счет Из ТаблицаСчетов Цикл
		Счет.ПоследняяВыписка = Формат(Счет.ДатаНачалаПоследнейЗагрузки, "ДЛФ=Д") + " - " + Формат(Счет.ДатаКонцаПоследнейЗагрузки, "ДЛФ=Д");
	КонецЦикла;
	БанковскиеСчета.Загрузить(ТаблицаСчетов);
КонецПроцедуры

Процедура ЗагрузитьПлатежи(ИдФормы = "") Экспорт
	УстановитьПривилегированныйРежим(Истина);
	
	Для каждого Счет Из БанковскиеСчета Цикл
		Если Не Счет.Пометка Или Не ЗначениеЗаполнено(Счет.НастройкаОбмена) Или Счет.Загружается Или Не ЭтоАдресВременногоХранилища(Счет.АдресХранилищаФайла) Тогда
			Продолжить;
		КонецЕсли;
		ЗагрузитьПлатежиПоСчету_Локализация(Счет, ИдФормы);
	КонецЦикла;
КонецПроцедуры

Процедура ЗагрузитьПлатежиПоСчету_Локализация(Счет, ИдФормы = "") Экспорт
	ПараметрыОтбора = Новый Структура;
	ПараметрыОтбора.Вставить("Пометка", Истина);
	
	ПараметрыЗагрузки = Новый Структура;
	ПараметрыЗагрузки.Вставить("БанковскийСчет", Счет.Ссылка);
	ПараметрыЗагрузки.Вставить("ДатаНачалаЗагрузки", Счет.ДатаНачалаЗагрузки);
	ПараметрыЗагрузки.Вставить("ДатаКонцаЗагрузки", Счет.ДатаКонцаЗагрузки);
	ПараметрыЗагрузки.Вставить("СтрокиВыписки", ПолучитьИзВременногоХранилища(Счет.АдресХранилищаФайла));
	ПараметрыЗагрузки.Вставить("ДокументыКЗагрузке", ДокументыКЗагрузке.ВыгрузитьКолонки());
	ПараметрыЗагрузки.Вставить("СоздаватьКонтрагентов", СоздаватьКонтрагентов);
	ПараметрыЗагрузки.Вставить("ПроводитьДокументы", ПроводитьДокументы);
	ПараметрыЗагрузки.Вставить("НайденныеОбъекты", ЭтотОбъект.НайденныеОбъекты.Выгрузить(ПараметрыОтбора));
	
	Счет.Загружен = Ложь;
	
	Если ЭтотОбъект.ЗагрузитьПредварительно Тогда
		Результат = Новый Структура;
		Результат.Вставить("АдресХранилища", "");
		Результат.Вставить("ЗаданиеВыполнено", Истина);
		Результат.Вставить("ИдентификаторЗадания", "");
		ПараметрыОбработки = Обработки.КлиентБанкЛокализацияБел.ЗагрузитьПроверкаПоследовательно(ПараметрыЗагрузки, "");
		ЭтотОбъект.ЗагрузитьПредварительно = Ложь;
		
		//Результат = ДлительныеОперации.ЗапуститьВыполнениеВФоне(ИдФормы, "Обработки.КлиентБанк_Локализация.ЗагрузитьПроверка", ПараметрыЗагрузки, НСтр("ru='Загрузка выписки банка'"));
		//ЭтотОбъект.ЗагрузитьПредварительно = Ложь;
		//ПараметрыОбработки = ПолучитьИзВременногоХранилища(Результат.АдресХранилища);
		
		Если ТипЗнч(ПараметрыОбработки) = Тип("Структура") Тогда
			НайденныеДокументы = ПараметрыОбработки.НайденныеДокументы;
			Для каждого Стр Из НайденныеДокументы Цикл
				НоваяСтрока = НайденныеОбъекты.Добавить();
				ЗаполнитьЗначенияСвойств(НоваяСтрока, Стр);
				
				НоваяСтрока.Пометка = Истина;
				
				Если Стр.Исходящий Тогда
					НоваяСтрока.Комментарий = "Контрагент: " + Стр.Получатель + "; Расчетный счет: " + Стр.ПолучательСчет;
				Иначе
					НоваяСтрока.Комментарий = "Контрагент: " + Стр.Плательщик + "; Расчетный счет: " + Стр.ПлательщикСчет;
				КонецЕсли;
				
				Если ЗначениеЗаполнено(Стр.Документ) Тогда
					НоваяСтрока.СуммаДокумента = Стр.Документ.СуммаДокумента;
					НоваяСтрока.Валюта = Стр.Документ.Валюта;
				КонецЕсли;
				
				НоваяСтрока.Номер = Стр.Номер;
			КонецЦикла;
		КонецЕсли;
	Иначе
		Результат = ДлительныеОперации.ЗапуститьВыполнениеВФоне(ИдФормы, "Обработки.КлиентБанкЛокализацияБел.Загрузить", ПараметрыЗагрузки, НСтр("ru='Загрузка выписки банка'"));
		ЭтотОбъект.ЗагрузитьПредварительно = Истина;
		ЭтотОбъект.НайденныеОбъекты.Очистить();
	КонецЕсли;
	
	Счет.АдресХранилищаДокументов = Результат.АдресХранилища;
	
	Если Результат.ЗаданиеВыполнено Тогда
		Счет.СохранитьФайл = Истина;
	Иначе
		Счет.Загружается = Истина;
		Счет.ИдентификаторЗагрузки = Результат.ИдентификаторЗадания;
	КонецЕсли;
КонецПроцедуры

Функция ПечатьОтчетаОЗагрузке(ВыделенныеСтроки) Экспорт
	ПолеОтчета = Неопределено;
	
	Для каждого ВыделеннаяСтрока Из ВыделенныеСтроки Цикл
		Если ПолеОтчета = Неопределено Тогда
			ПолеОтчета = Новый ТабличныйДокумент;
			ПолеОтчета.ОриентацияСтраницы = ОриентацияСтраницы.Ландшафт;
		КонецЕсли;
		
		Счет = БанковскиеСчета.Получить(ВыделеннаяСтрока);
		Если ЭтоАдресВременногоХранилища(Счет.АдресХранилищаДокументов) Тогда
			ЗагруженныеДокументы = ПолучитьИзВременногоХранилища(Счет.АдресХранилищаДокументов);
			Если ЗагруженныеДокументы <> Неопределено И ТипЗнч(ЗагруженныеДокументы) = Тип("ТаблицаЗначений") И ЗагруженныеДокументы.Количество() Тогда
				ВывестиСекциюОтчетаОЗагрузке(ПолеОтчета, ЗагруженныеДокументы, Счет.Ссылка, Счет.Загружен);
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
	Возврат ПолеОтчета;
КонецФункции

Процедура ЗаписатьПериодПоследнейВыписки() Экспорт
	Для каждого Счет Из БанковскиеСчета Цикл
		Если Счет.Загружен Тогда
			РегистрыСведений.ПоследнийОбменСБанками.СоздатьЗапись(Счет.Ссылка, Новый Структура("ДатаНачалаЗагрузки, ДатаКонцаЗагрузки", Счет.ДатаНачалаЗагрузки, Счет.ДатаКонцаЗагрузки));
			Счет.ПоследняяВыписка = Формат(Счет.ДатаНачалаЗагрузки, "ДЛФ=Д") + " - " + Формат(Счет.ДатаКонцаЗагрузки, "ДЛФ=Д");
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

Процедура ЗаписатьОстаткиНаСчетахПоДаннымВыписки() Экспорт
	Для каждого Счет Из БанковскиеСчета Цикл
		Если Счет.Загружен И ЭтоАдресВременногоХранилища(Счет.АдресХранилищаФайла) Тогда
			СтрокиВыписки = ПолучитьИзВременногоХранилища(Счет.АдресХранилищаФайла);
			
			ПараметрыЗагрузки = Новый Структура;
			ПараметрыЗагрузки.Вставить("РасчетныйСчет", Счет.Ссылка);
			ДанныеВыписки = ДенежныеСредстваСерверЛокализацияБел.РазобратьФайлВыписки(СтрокиВыписки, Ложь, ПараметрыЗагрузки);
			
			Если ДанныеВыписки <> Неопределено Тогда
				РасчетныеСчета = ДанныеВыписки.РасчетныеСчета;
				Для каждого РасчетныйСчет Из РасчетныеСчета Цикл
					Если Счет.НомерСчета = РасчетныйСчет.РасчСчет Тогда
						ПараметрыЗаписи = Новый Структура;
						
						НачальныйОстаток = ДенежныеСредстваКлиентСервер.ПреобразоватьВЧисло(РасчетныйСчет.НачальныйОстаток);
						КонечныйОстаток = ДенежныеСредстваКлиентСервер.ПреобразоватьВЧисло(РасчетныйСчет.КонечныйОстаток);
						Поступление = ДенежныеСредстваКлиентСервер.ПреобразоватьВЧисло(РасчетныйСчет.ВсегоПоступило);
						Списание = ДенежныеСредстваКлиентСервер.ПреобразоватьВЧисло(РасчетныйСчет.ВсегоСписано);
						ДатаНачала = ДенежныеСредстваКлиентСервер.ПолучитьДатуИзСтроки(РасчетныйСчет.ДатаНачала);
						ДатаКонца = ДенежныеСредстваКлиентСервер.ПолучитьДатуИзСтроки(РасчетныйСчет.ДатаКонца);
						
						Если ЗначениеЗаполнено(ДатаНачала) И ЗначениеЗаполнено(ДатаКонца) И НачалоДня(ДатаНачала) = НачалоДня(ДатаКонца) Тогда
							ПараметрыЗаписи.Вставить("НачальныйОстаток", НачальныйОстаток);
							ПараметрыЗаписи.Вставить("Поступление", Поступление);
							ПараметрыЗаписи.Вставить("Списание", Списание);
							ПараметрыЗаписи.Вставить("КонечныйОстаток", КонечныйОстаток);
							РегистрыСведений.ОстаткиНаБанковскихСчетахПоДаннымВыписок.СоздатьЗапись(ДатаНачала, Счет.Ссылка, ПараметрыЗаписи);
						Иначе
							Если ЗначениеЗаполнено(ДатаНачала) Тогда
								ПараметрыЗаписи.Вставить("НачальныйОстаток", НачальныйОстаток);
								РегистрыСведений.ОстаткиНаБанковскихСчетахПоДаннымВыписок.СоздатьЗапись(ДатаНачала, Счет.Ссылка, ПараметрыЗаписи);
							КонецЕсли;
							
							Если ЗначениеЗаполнено(ДатаКонца) Тогда
								ПараметрыЗаписи.Вставить("КонечныйОстаток", КонечныйОстаток);
								РегистрыСведений.ОстаткиНаБанковскихСчетахПоДаннымВыписок.СоздатьЗапись(ДатаКонца, Счет.Ссылка, ПараметрыЗаписи);
							КонецЕсли;
						КонецЕсли;
					КонецЕсли;
				КонецЦикла;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

Процедура ВывестиСекциюОтчетаОЗагрузке(ПолеОтчета, Документы, БанковскийСчет, Загружен)
	МакетОтчета = ПолучитьМакет("ОтчетОЗагрузке");
	ИмеетсяСекцияРасчСчет = Ложь;
	
	Шапка = МакетОтчета.ПолучитьОбласть("Шапка");
	Строка = МакетОтчета.ПолучитьОбласть("Строка");
	Подвал = МакетОтчета.ПолучитьОбласть("Подвал");
	Остатки = МакетОтчета.ПолучитьОбласть("Остатки");
	НазвОрг = МакетОтчета.ПолучитьОбласть("НазваниеОрганизации");
	
	РеквизитыСчета = Справочники.БанковскиеСчетаОрганизаций.ПолучитьРеквизитыБанковскогоСчетаОрганизации(БанковскийСчет);
	
	НазвОрг.Параметры.НазваниеОрганизации = Строка(РеквизитыСчета.Организация);
	ПолеОтчета.Вывести(НазвОрг);
	
	Если Загружен Тогда
		Шапка.Параметры.ИмяОтчета = НСтр("ru='Отчет о загруженных документах'");
	Иначе
		Шапка.Параметры.ИмяОтчета = НСтр("ru='Документы выписки'");
	КонецЕсли;
	
	ОбрБанковскийСчет = "";
	Индекс = 0;
	ИтогоСуммаП = 0;
	ИтогоСуммаР = 0;
	
	Документы.Сортировать("Дата");
	НачПериода = Документы[0].ДатаДок;
	КонПериода = Документы[Документы.Количество() - 1].ДатаДок;
	
	Валюта = ?(ЗначениеЗаполнено(РеквизитыСчета.Валюта), СокрЛП(Строка(РеквизитыСчета.Валюта)), НСтр("ru='валюта не указана'"));
	НачалоОтчетногоПериода = Формат(НачПериода, "ДЛФ=D");
	КонецОтчетногоПериода = Формат(КонПериода, "ДЛФ=D");
	
	Шапка.Параметры.ОписаниеПериода = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru='по счету %1 (%2) с %3 по %4'"), РеквизитыСчета.НомерСчета, Валюта, НачалоОтчетногоПериода, КонецОтчетногоПериода);
	ПолеОтчета.Вывести(Шапка);
	
	Для Каждого СтрокаИсточника Из Документы Цикл
		Валюта = ?(ЗначениеЗаполнено(СтрокаИсточника.Валюта), СокрЛП(Строка(СтрокаИсточника.Валюта)), НСтр("ru='валюта не указана'"));
		
		Если СтрокаИсточника.СуммаПоступило > 0 Тогда
			Строка.Параметры.Контрагент = ?(ПустаяСтрока(СтрокаИсточника.Плательщик1), СтрокаИсточника.Плательщик, СтрокаИсточника.Плательщик1);
			Строка.Параметры.Счет = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru='%1 (%2)'"), СтрокаИсточника.ПлательщикСчет, Валюта);
			Строка.Параметры.СуммаПоступление = СтрокаИсточника.СуммаПоступило;
			Строка.Параметры.СуммаСписание = "";
			Строка.Параметры.Дата = Формат(СтрокаИсточника.ДатаДок, "ДЛФ=Д");
			ИтогоСуммаП = ИтогоСуммаП + СтрокаИсточника.СуммаПоступило;
		ИначеЕсли СтрокаИсточника.СуммаСписано > 0 Тогда
			Строка.Параметры.Контрагент = ?(ПустаяСтрока(СтрокаИсточника.Получатель1), СтрокаИсточника.Получатель, СтрокаИсточника.Получатель1);
			Строка.Параметры.Счет = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru='%1 (%2)'"), СтрокаИсточника.ПолучательСчет, Валюта);
			Строка.Параметры.СуммаСписание = СтрокаИсточника.СуммаСписано;
			Строка.Параметры.СуммаПоступление = "";
			Строка.Параметры.Дата = ?(ПустаяСтрока(СтрокаИсточника.Дата), Формат(СтрокаИсточника.ДатаДок, "ДЛФ=Д"), СтрокаИсточника.Дата);
			ИтогоСуммаР = ИтогоСуммаР + СтрокаИсточника.СуммаСписано;
		Иначе
			Продолжить;
		КонецЕсли;
		
		Если Загружен Тогда
			Док = СтрокаИсточника.Документ;
			Если ЗначениеЗаполнено(Док) Тогда
				Строка.Параметры.Документ = Док;
				Если ЗначениеЗаполнено(СтрокаИсточника.ОшибкиЗагрузки) Тогда
					Строка.Параметры.Документ = Строка.Параметры.Документ + ": " + СтрокаИсточника.ОшибкиЗагрузки;
				КонецЕсли;
				Строка.Области.Строка.ЦветТекста = ЦветаСтиля.ЦветТекстаФормы;
			Иначе
				Строка.Параметры.Документ = НСтр("ru='НЕ ЗАГРУЖЕН'");
				Строка.Области.Строка.ЦветТекста = ЦветаСтиля.ЦветОтрицательногоЧисла;
			КонецЕсли;
		КонецЕсли;
		
		Строка.Параметры.Номер = СтрокаИсточника.Номер;
		Строка.Параметры.НазначениеПлатежа = СтрокаИсточника.НазначениеПлатежа;
		
		Индекс = Индекс + 1;
		Строка.Параметры.Индекс = Индекс;
		
		ПолеОтчета.Вывести(Строка);
	КонецЦикла;
	
	Подвал.Параметры.ИтогоСуммаП = ИтогоСуммаП;
	Подвал.Параметры.ИтогоСуммаР = ИтогоСуммаР;
	
	ПолеОтчета.Вывести(Подвал);
	ПолеОтчета.ОтображатьГруппировки = Ложь;
	ПолеОтчета.ОтображатьЗаголовки = Ложь;
	ПолеОтчета.ОтображатьСетку = Ложь;
	ПолеОтчета.ТолькоПросмотр = Истина;
КонецПроцедуры

#КонецОбласти

#Область ВыгрузкаПорциямиПоПравиламОбъединения

Функция ИнформацияОбОбъединенииФайлов(ВыгружаемыеБанковскиеСчета)
	НовыйПоток = Новый Структура("Правило, Счета");
	
	КоличествоСчетовВПравиле = Новый Соответствие;
	ПотокиВыгрузки = Новый Массив;
	СчетаБезПравил = Новый Массив;
	
	ИнформацияОбОбъединенииФайлов = Новый Структура("ТекущееПравило, АдресТекущегоПотокаВыгрузки, ВсегоСчетовВТекущемПотоке, ВыгруженоСчетовВТекущийПоток, КоличествоСчетовВПравиле, СчетаБезПравил, ПотокиВыгрузки");
	ИнформацияОбОбъединенииФайлов.Вставить("ТекущееПравило", "");
	ИнформацияОбОбъединенииФайлов.Вставить("ВсегоСчетовВТекущемПотоке", 0);
	ИнформацияОбОбъединенииФайлов.Вставить("ВсегоСчетовВТекущемПотоке", 0);
	ИнформацияОбОбъединенииФайлов.Вставить("КоличествоСчетовВПравиле", КоличествоСчетовВПравиле);
	ИнформацияОбОбъединенииФайлов.Вставить("СчетаБезПравил", СчетаБезПравил);
	ИнформацияОбОбъединенииФайлов.Вставить("ПотокиВыгрузки", ПотокиВыгрузки);
	
	Для Каждого Счет Из ВыгружаемыеБанковскиеСчета Цикл
		Если ПустаяСтрока(Счет.ПравилоФайловогоОбменаСБанками) Тогда
			СчетаБезПравил.Добавить(Счет);
		Иначе
			Количество = КоличествоСчетовВПравиле.Получить(Счет.ПравилоФайловогоОбменаСБанками);
			Если Количество = Неопределено Тогда
				ИнформацияОбОбъединенииФайлов["КоличествоСчетовВПравиле"].Вставить(Счет.ПравилоФайловогоОбменаСБанками, 1);
			Иначе
				ИнформацияОбОбъединенииФайлов["КоличествоСчетовВПравиле"].Вставить(Счет.ПравилоФайловогоОбменаСБанками, Количество + 1);
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
	Для Каждого Правило Из КоличествоСчетовВПравиле Цикл
		ПравилоПотока = Правило.Ключ;
		
		ТекущийПоток = ОбщегоНазначенияКлиентСервер.СкопироватьСтруктуру(НовыйПоток);
		ТекущийПоток.Вставить("Правило", Правило.Ключ);
		ТекущийПоток.Вставить("Счета", ВыгружаемыеБанковскиеСчета.Скопировать(ВыгружаемыеБанковскиеСчета.НайтиСтроки(Новый Структура("ПравилоФайловогоОбменаСБанками", Правило.Ключ))));
		
		ИнформацияОбОбъединенииФайлов["ПотокиВыгрузки"].Добавить(ТекущийПоток);
	КонецЦикла;
	
	Возврат ИнформацияОбОбъединенииФайлов;
КонецФункции

Процедура ИзменитьРеквизитыСчетовПотока(Счета, ИзменяемыеПараметры)
	Для Каждого Счет Из Счета Цикл
		ЗаполнитьЗначенияСвойств(Счет, ИзменяемыеПараметры);
	КонецЦикла;
КонецПроцедуры

Процедура ОбновитьИнформациюОВыгруженныхФайлах(Знач ИнформацияОбОбъединенииФайлов)
	Для Каждого ПотокВыгрузки Из ИнформацияОбОбъединенииФайлов.ПотокиВыгрузки Цикл
		Для Каждого ВыгруженныйСчет Из ПотокВыгрузки.Счета Цикл
			Отбор = Новый Структура("Ссылка", ВыгруженныйСчет.Ссылка);
			НайденныеСтроки = БанковскиеСчета.НайтиСтроки(Отбор);
			Для Каждого НайденнаяСтрока Из НайденныеСтроки Цикл
				ЗаполнитьЗначенияСвойств(НайденнаяСтрока, ВыгруженныйСчет);
			КонецЦикла;
		КонецЦикла;
	КонецЦикла;
	
	Для Каждого ВыгруженныйСчет Из ИнформацияОбОбъединенииФайлов.СчетаБезПравил Цикл
		Отбор = Новый Структура("Ссылка", ВыгруженныйСчет.Ссылка);
		НайденныеСтроки = БанковскиеСчета.НайтиСтроки(Отбор);
		Для Каждого НайденнаяСтрока Из НайденныеСтроки Цикл
			ЗаполнитьЗначенияСвойств(НайденнаяСтрока, ВыгруженныйСчет);
		КонецЦикла;
	КонецЦикла;
КонецПроцедуры

#КонецОбласти
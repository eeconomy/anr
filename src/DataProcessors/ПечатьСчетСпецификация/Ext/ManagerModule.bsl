﻿Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "СчетСпецификация") Тогда
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, "СчетСпецификация", НСтр("ru = 'Счет-Спецификация'"), ПечатнаяФормаСчетСпецификация(МассивОбъектов, ОбъектыПечати, ПараметрыПечати));
	КонецЕсли;
		    	
	ФормированиеПечатныхФорм.ЗаполнитьПараметрыОтправки(ПараметрыВывода.ПараметрыОтправки, МассивОбъектов, КоллекцияПечатныхФорм);
	
КонецПроцедуры

Функция ПечатнаяФормаСчетСпецификация(МассивОбъектов, ОбъектыПечати, ПараметрыПечати)
	
	УстановитьПривилегированныйРежим(Истина);
	
	Макет = УправлениеПечатью.МакетПечатнойФормы("Обработка.ПечатьСчетСпецификация.ПФ_MXL_СчетСпецификация");
	
	ТабличныйДокумент = Новый ТабличныйДокумент;
	
	ДанныеДляПечати = Документы.ЗаказКлиента.ПолучитьДанныеДляПечатнойФормыЗаказаНаТоварыУслуги(МассивОбъектов, ПараметрыПечати);
	
	ДанныеШапки   = ДанныеДляПечати.РезультатПоШапке.Выбрать();
	ТаблицаТовары = ДанныеДляПечати.РезультатПоТабличнойЧасти.Выгрузить();
	
	ТаблицаТоварыДокумента = Новый ТаблицаЗначений;
	Для Каждого КолонкаТаблицыЗначений Из ТаблицаТовары.Колонки Цикл
		ТаблицаТоварыДокумента.Колонки.Добавить(КолонкаТаблицыЗначений.Имя, КолонкаТаблицыЗначений.ТипЗначения);
	КонецЦикла;
	
	ПервыйДокумент = Истина;
	
	Пока ДанныеШапки.Следующий() Цикл	
				
		Если Не ПервыйДокумент Тогда
			ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли; 
		
		ТабличныйДокумент.АвтоМасштаб = Истина;
		ТабличныйДокумент.ПолеСлева = 20;
		ТабличныйДокумент.ПолеСправа = 10;
		//ТабличныйДокумент.ПолеСверху = 1;
		//ТабличныйДокумент.ПолеСнизу = 1;
		
		ПервыйДокумент = Ложь;
		НомерСтрокиНачало = ТабличныйДокумент.ВысотаТаблицы + 1; 
		
		ИспользоватьНаборы = Ложь;
		Если ТаблицаТовары.Колонки.Найти("ЭтоНабор") <> Неопределено Тогда
			ИспользоватьНаборы = Истина;
		КонецЕсли;
		
		ПараметрыПоиска = Новый Структура;
		ПараметрыПоиска.Вставить("Ссылка", ДанныеШапки.Ссылка);
		
		НайденныеСтроки = ТаблицаТовары.НайтиСтроки(ПараметрыПоиска);
		
		Для Каждого НайденнаяСтрока Из НайденныеСтроки Цикл
			НоваяСтрока = ТаблицаТоварыДокумента.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрока, НайденнаяСтрока);
		КонецЦикла;
		
		ОбластьМакета = Макет.ПолучитьОбласть("ЗаголовокЛоготип");
		ФормированиеПечатныхФорм.ВывестиЛоготипВТабличныйДокумент(Макет, ОбластьМакета, "Заголовок", ДанныеШапки.Организация);
		
		
		ОбластьМакета.Параметры.НомерДок = ДанныеШапки.Номер;
		ОбластьМакета.Параметры.ДатаДок = Формат(ДанныеШапки.Дата, "ДЛФ=Д");
		ОбластьМакета.Параметры.НомерДоговора = ДанныеШапки.Договор.Номер;
		ОбластьМакета.Параметры.ДатаДоговора = Формат(ДанныеШапки.Договор.Дата, "ДЛФ=Д");
		
		ТабличныйДокумент.Вывести(ОбластьМакета);
		
		ОбластьМакета = Макет.ПолучитьОбласть("ПродавецШапка");
		
		ОбластьМакета.Параметры.ОрганизацияНаименование = ДанныеШапки.Организация.НаименованиеПолное;
		Для Каждого СтрокиКонтактИнформацииЮрАдрес Из ДанныеШапки.Организация.КонтактнаяИнформация Цикл
			Если Строка(СтрокиКонтактИнформацииЮрАдрес.Вид) = "Юридический адрес" Тогда
				ОбластьМакета.Параметры.АдресОрганизации = СтрокиКонтактИнформацииЮрАдрес.Представление;
			КонецЕсли;
		КонецЦикла;
		ОбластьМакета.Параметры.УНПОрг = ОписаниеСчетСпецифИНН(ДанныеШапки, "Организация");
		
		БанковскиеРеквизиты = ПолучитьБанковскиеСчетаОрганизации(ДанныеШапки.Организация, ДанныеШапки.Валюта);
		
		ТабличныйДокумент.Вывести(ОбластьМакета);
		
		Если ЗначениеЗаполнено(БанковскиеРеквизиты) Тогда
			Если Не БанковскиеРеквизиты.Ссылка = Неопределено Тогда
				СтрокаБанкИмя = "";
				СтрокаАдресБанка = "";
				СтрокаБанкаБИК = "";
				
				Если ЗначениеЗаполнено(БанковскиеРеквизиты.НаименованиеБанка) Тогда
					СтрокаБанкИмя = БанковскиеРеквизиты.НаименованиеБанка;
				ИначеЕсли ЗначениеЗаполнено(БанковскиеРеквизиты.Банк) Тогда
					СтрокаБанкИмя = БанковскиеРеквизиты.Банк;
				КонецЕсли;
				
				Если Не ЗначениеЗаполнено(БанковскиеРеквизиты.АдресБанка) И Не ЗначениеЗаполнено(БанковскиеРеквизиты.Банк.Адрес) Тогда
					Если ЗначениеЗаполнено(БанковскиеРеквизиты.Банк.Город) Тогда
						СтрокаАдресБанка = БанковскиеРеквизиты.Банк.Город;
					ИначеЕсли ЗначениеЗаполнено(БанковскиеРеквизиты.ГородБанка) Тогда 
						СтрокаАдресБанка = БанковскиеРеквизиты.ГородБанка;
					КонецЕсли;
				Иначе
					Если ЗначениеЗаполнено(БанковскиеРеквизиты.АдресБанка) Тогда
						СтрокаАдресБанка = БанковскиеРеквизиты.АдресБанка;
					ИначеЕсли ЗначениеЗаполнено(БанковскиеРеквизиты.Банк.Адрес) Тогда
						СтрокаАдресБанка = БанковскиеРеквизиты.Банк.Адрес;
					КонецЕсли;
				КонецЕсли;
				
				Если ЗначениеЗаполнено(БанковскиеРеквизиты.БИКБанка) Тогда
					СтрокаБанкаБИК = БанковскиеРеквизиты.БИКБанка;
				ИначеЕсли ЗначениеЗаполнено(БанковскиеРеквизиты.Банк.Код)  Тогда
					СтрокаБанкаБИК = БанковскиеРеквизиты.Банк.Код;
				КонецЕсли;
				
				Если БанковскиеРеквизиты.ИспользуетсяБанкДляРасчетовПередача Тогда	
					
					СтрокаКоррСчет = "";
					СтрокаИмяКоррБанка = "";
					СтрокаАдресКоррБанка = "";
					СтрокаНомерКоррСчета = "";
										
					ОбластьМакета = Макет.ПолучитьОбласть("ПродавецКСБанк");
					ОбластьМакета.Параметры.НомерСчетаОрг = БанковскиеРеквизиты.НомерСчета;
					ОбластьМакета.Параметры.БИКБанкаОрг = СтрокаБанкаБИК;
					ОбластьМакета.Параметры.ВалютаСчетаОрг = БанковскиеРеквизиты.ВалютаДенежныхСредств;
					ОбластьМакета.Параметры.НаименованиеБанкаОрг = СтрокаБанкИмя;        
					ОбластьМакета.Параметры.АдресБанкаОрг = СтрокаАдресБанка;
					
					Если ЗначениеЗаполнено(БанковскиеРеквизиты.СчетВБанкеДляРасчетов) Тогда
						СтрокаНомерКоррСчета = БанковскиеРеквизиты.СчетВБанкеДляРасчетов;
					ИначеЕсли ЗначениеЗаполнено(БанковскиеРеквизиты.КоррСчетБанкаДляРасчетов) Тогда
						СтрокаНомерКоррСчета = БанковскиеРеквизиты.КоррСчетБанкаДляРасчетов;
					КонецЕсли;	
					Если ЗначениеЗаполнено(БанковскиеРеквизиты.БанкДляРасчетов) Тогда
						СтрокаИмяКоррБанка = БанковскиеРеквизиты.БанкДляРасчетов;
					ИначеЕсли ЗначениеЗаполнено(БанковскиеРеквизиты.НаименованиеБанкаДляРасчетовМеждународное) Тогда
						СтрокаИмяКоррБанка = БанковскиеРеквизиты.НаименованиеБанкаДляРасчетовМеждународное;	
					КонецЕсли;
					Если ЗначениеЗаполнено(БанковскиеРеквизиты.АдресБанкаДляРасчетов) Тогда
						СтрокаАдресКоррБанка = БанковскиеРеквизиты.АдресБанкаДляРасчетов;
					ИначеЕсли ЗначениеЗаполнено(БанковскиеРеквизиты.АдресБанкаДляРасчетовМеждународный) Тогда
						СтрокаАдресКоррБанка = БанковскиеРеквизиты.АдресБанкаДляРасчетовМеждународный;
					КонецЕсли;
					
					СтрокаКоррСчет = "К/с: " + Строка(СтрокаНомерКоррСчета) +  " в " + Строка(СтрокаИмяКоррБанка)
					+ ", " + Строка(СтрокаАдресКоррБанка)+ ", " + Строка(БанковскиеРеквизиты.СтранаБанкаДляРасчетов) + ", " + "SWIFT " + Строка(БанковскиеРеквизиты.СВИФТБанкаДляРасчетов) + ", " 
					+ Строка(БанковскиеРеквизиты.БИКБанкаДляРасчетов) + ".";
					ОбластьМакета.Параметры.КоррСчет = СтрокаКоррСчет;
					
					ТабличныйДокумент.Вывести(ОбластьМакета);
					
				Иначе
					
					ОбластьМакета = Макет.ПолучитьОбласть("ПродавецБанк");
					ОбластьМакета.Параметры.НомерСчетаОрг = БанковскиеРеквизиты.НомерСчета;
					ОбластьМакета.Параметры.БИКБанкаОрг = СтрокаБанкаБИК;
					ОбластьМакета.Параметры.ВалютаСчетаОрг = БанковскиеРеквизиты.ВалютаДенежныхСредств;
					ОбластьМакета.Параметры.НаименованиеБанкаОрг = СтрокаБанкИмя;
					ОбластьМакета.Параметры.АдресБанкаОрг = СтрокаАдресБанка;
					ТабличныйДокумент.Вывести(ОбластьМакета);
				КонецЕсли;
			КонецЕсли;
		КонецЕсли;
		
		ОбластьМакета = Макет.ПолучитьОбласть("ПродавецПодвал");
		ОбластьМакета.Параметры.ТелефонОрганизации = ОписаниеСчетСпецифТелефоны(ДанныеШапки, "Организация");
		
		Для Каждого СтрокиКонтактИнформации Из ДанныеШапки.Организация.КонтактнаяИнформация Цикл
			Если Строка(СтрокиКонтактИнформации.Вид) = "Электронная почта" Тогда
				ОбластьМакета.Параметры.ЭлектроннаяПочта = СтрокиКонтактИнформации.Представление; 
			КонецЕсли;
		КонецЦикла;
		ТабличныйДокумент.Вывести(ОбластьМакета);
		
		ОбластьМакета = Макет.ПолучитьОбласть("Покупатель");
		
		ОбластьМакета.Параметры.СокрЮрНаименованиеКлиента = ДанныеШапки.Контрагент.НаименованиеПолное;
	    Для Каждого СтрокиКонтактИнформацииЮрАдрес Из ДанныеШапки.Контрагент.КонтактнаяИнформация Цикл
			Если Строка(СтрокиКонтактИнформацииЮрАдрес.Вид) = "Юридический адрес" Тогда
				ОбластьМакета.Параметры.АдресКлиента = СтрокиКонтактИнформацииЮрАдрес.Представление;
			КонецЕсли;
		КонецЦикла;
		
		СтрокаРегНомер = "";
		Если ДанныеШапки.Контрагент.ЮрФизЛицо = Перечисления.ЮрФизЛицо.ЮрЛицоНеРезидент Тогда
			СтрокаРегНомер = "Рег. номер: " + Строка(ДанныеШапки.Контрагент.РегистрационныйНомер);
			ОбластьМакета.Параметры.УНПКлиента = СтрокаРегНомер;
		Иначе
			ОбластьМакета.Параметры.УНПКлиента = ОписаниеСчетСпецифИНН(ДанныеШапки, "Контрагент");
		КонецЕсли; 
		
		БанковскиеРеквизитыПокупатель = ПолучитьБанковскиеСчетаКонтрагента(ДанныеШапки.Контрагент, ДанныеШапки.Валюта);    
		
		БанковскиеРеквизитыКлиента = "";
		Если ЗначениеЗаполнено(БанковскиеРеквизитыПокупатель) Тогда
			Если Не БанковскиеРеквизитыПокупатель.Ссылка = Неопределено Тогда
				
				СтрокаБанкИмя = "";
				СтрокаАдресБанка = "";
				СтрокаБанкаБИК = "";
				
				Если ЗначениеЗаполнено(БанковскиеРеквизитыПокупатель.НаименованиеБанка) Тогда
					СтрокаБанкИмя = БанковскиеРеквизитыПокупатель.НаименованиеБанка;
				ИначеЕсли ЗначениеЗаполнено(БанковскиеРеквизитыПокупатель.Банк) Тогда
					СтрокаБанкИмя = БанковскиеРеквизитыПокупатель.Банк;
				КонецЕсли;
				
				Если Не ЗначениеЗаполнено(БанковскиеРеквизитыПокупатель.АдресБанка) И Не ЗначениеЗаполнено(БанковскиеРеквизитыПокупатель.Банк.Адрес) Тогда
					Если ЗначениеЗаполнено(БанковскиеРеквизитыПокупатель.Банк.Город) Тогда
						СтрокаАдресБанка = БанковскиеРеквизитыПокупатель.Банк.Город;
					ИначеЕсли ЗначениеЗаполнено(БанковскиеРеквизитыПокупатель.ГородБанка) Тогда 
						СтрокаАдресБанка = БанковскиеРеквизитыПокупатель.ГородБанка;
					КонецЕсли;
				Иначе
					Если ЗначениеЗаполнено(БанковскиеРеквизитыПокупатель.АдресБанка) Тогда
						СтрокаАдресБанка = БанковскиеРеквизитыПокупатель.АдресБанка;
					ИначеЕсли ЗначениеЗаполнено(БанковскиеРеквизитыПокупатель.Банк.Адрес) Тогда
						СтрокаАдресБанка = БанковскиеРеквизитыПокупатель.Банк.Адрес;
					КонецЕсли;
				КонецЕсли;
				
				Если ЗначениеЗаполнено(БанковскиеРеквизитыПокупатель.БИКБанка) Тогда
					СтрокаБанкаБИК = БанковскиеРеквизитыПокупатель.БИКБанка;
				ИначеЕсли ЗначениеЗаполнено(БанковскиеРеквизитыПокупатель.Банк.Код)  Тогда
					СтрокаБанкаБИК = БанковскиеРеквизитыПокупатель.Банк.Код;
				КонецЕсли; 
				
				БанковскиеРеквизитыКлиента = "Банковские реквизиты: " + Строка(БанковскиеРеквизитыПокупатель.НомерСчета)+ 
				" " + "(" + Строка(БанковскиеРеквизитыПокупатель.ВалютаДенежныхСредств) + ")" + " в " + Строка(СтрокаБанкИмя) + 
				", " + Строка(СтрокаАдресБанка) + ", БИК " + Строка(СтрокаБанкаБИК); 
				ОбластьМакета.Параметры.БанковскиеРеквизитыКлиента = БанковскиеРеквизитыКлиента;
			КонецЕсли;	
		КонецЕсли;
		
		ТабличныйДокумент.Вывести(ОбластьМакета);
		
		ОбластьМакета = Макет.ПолучитьОбласть("ШапкаТаблицы");
		ТабличныйДокумент.Вывести(ОбластьМакета);
		
		Сумма = 0;
		СуммаНДС = 0;
		ВсегоСкидок = 0;
		ВсегоБезСкидок = 0;
		НомерСтроки = 0;
        СоответствиеСтавокНДС = Новый Соответствие;
		Для Каждого СтрокаТовары Из ТаблицаТовары Цикл
			
			ОбластьМакета = Макет.ПолучитьОбласть("СтрокаТаблицы");
			ОбластьМакета.Параметры.НомерСтроки = СтрокаТовары.НомерСтроки;
			ОбластьМакета.Параметры.Артикул = СтрокаТовары.Номенклатура.Артикул;
			ОбластьМакета.Параметры.Товар = СтрокаТовары.Номенклатура;
			ОбластьМакета.Параметры.Количество = СтрокаТовары.Количество;
			ОбластьМакета.Параметры.ЕдиницаИзмерения = СтрокаТовары.ЕдиницаИзмерения;
			Если ЗначениеЗаполнено(СтрокаТовары.ЦенаСоСкидкой) Тогда
				ОбластьМакета.Параметры.Цена = СтрокаТовары.ЦенаСоСкидкой;
			Иначе
				ОбластьМакета.Параметры.Цена = СтрокаТовары.Цена;
			КонецЕсли;
			ОбластьМакета.Параметры.Сумма = СтрокаТовары.Сумма;
			ОбластьМакета.Параметры.СтавкаНДС= СтрокаТовары.СтавкаНДС;
			ОбластьМакета.Параметры.СуммаНДС = СтрокаТовары.СуммаНДС;
			ОбластьМакета.Параметры.ВсегоСНДС = СтрокаТовары.Сумма + СтрокаТовары.СуммаНДС;
			Если ТипЗнч(СтрокаТовары.ДатаОтгрузки) = Тип("Дата") Тогда
				ОбластьМакета.Параметры.СрокПоставки = "до " + Строка(Формат(СтрокаТовары.ДатаОтгрузки,"ДЛФ=Д"));
			Иначе
				ОбластьМакета.Параметры.СрокПоставки = Строка(СтрокаТовары.ДатаОтгрузки) + " кд";
			КонецЕсли;
			
			ТабличныйДокумент.Вывести(ОбластьМакета);
			
			Сумма = Сумма + СтрокаТовары.Сумма;
			СуммаНДС = СуммаНДС + СтрокаТовары.СуммаНДС;
			
			СуммаНДСПоСтавке = СоответствиеСтавокНДС[СтрокаТовары.СтавкаНДС];
			Если СуммаНДСПоСтавке = Неопределено Тогда
				СуммаНДСПоСтавке = 0;
			КонецЕсли;
			
			СоответствиеСтавокНДС.Вставить(СтрокаТовары.СтавкаНДС, СуммаНДСПоСтавке + СтрокаТовары.СуммаНДС);
			
		КонецЦикла;
		
		ОбластьМакета = Макет.ПолучитьОбласть("ПодвалТаблицы");
		
		СтруктураДанныхВсего = Новый Структура;
		
		// Подвал таблицы "Товары"
		СтруктураДанныхВсего.Вставить("Всего", ФормированиеПечатныхФорм.ФорматСумм(Сумма));			
		// Область "ПодвалТаблицыНДС"
		Для Каждого ТекСтавкаНДС Из СоответствиеСтавокНДС Цикл
			СтруктураДанныхПодвалНДС = Новый Структура;
			СтруктураДанныхПодвалНДС.Вставить("НДС", ФормированиеПечатныхФорм.ТекстНДСПоСтавке(ТекСтавкаНДС.Ключ, ДанныеШапки.ЦенаВключаетНДС));
			СтруктураДанныхПодвалНДС.Вставить("ВсегоНДС", ФормированиеПечатныхФорм.ФорматСумм(ТекСтавкаНДС.Значение, ,"-"));			
		КонецЦикла;
		СтруктураДанныхПодвалВсегоСНДС = Новый Структура;
		СтруктураДанныхПодвалНДС.Вставить("ВсегоСНДС", ФормированиеПечатныхФорм.ФорматСумм(Сумма + ?(ДанныеШапки.ЦенаВключаетНДС, 0, СуммаНДС)));
		//ОбластьПодвалСНДС.Параметры.Заполнить(СтруктураДанныхПодвалНДС);
		
		ОбластьМакета.Параметры.ИтогСумма = Сумма;
		ОбластьМакета.Параметры.ИтогСуммаНДС = СтруктураДанныхПодвалНДС.ВсегоНДС;
		ОбластьМакета.Параметры.ИтогВсегоНДС = СтруктураДанныхПодвалНДС.ВсегоСНДС;
		ТабличныйДокумент.Вывести(ОбластьМакета);
		
		// Вывести Сумму прописью
		ОбластьМакета = Макет.ПолучитьОбласть("СуммаПрописью");
				
		ИтоговаяСтрока = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			Неопределено,
			0, // Количество
			ФормированиеПечатныхФорм.ФорматСумм(Сумма, ДанныеШапки.Валюта));		
		УстановитьПараметр(ОбластьМакета, "ИтоговаяСтрока", ИтоговаяСтрока);
		УстановитьПараметр(ОбластьМакета, "СуммаПрописью", РаботаСКурсамиВалют.СформироватьСуммуПрописью(
		Число(СтруктураДанныхПодвалНДС.ВсегоСНДС),
		ДанныеШапки.Валюта));
		
		Если Не ТабличныйДокумент.ПроверитьВывод(ОбластьМакета) Тогда 
			
			ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц(); // начнем новую страницу  
			
		КонецЕсли;
		
		
		ТабличныйДокумент.Вывести(ОбластьМакета);
		
		ОбластьМакета = Макет.ПолучитьОбласть("ОбластьДоставка"); 
		
		СтрокаУсловияОплаты = "";
		
		Если ЗначениеЗаполнено(ДанныеШапки.Соглашение.ГрафикОплаты) Тогда
			
			Для Каждого СтрокаЭтапыГрафикаОплаты Из ДанныеШапки.Соглашение.ГрафикОплаты.Этапы Цикл
				Если СтрокаЭтапыГрафикаОплаты.ВариантОплаты = Перечисления.ВариантыОплатыКлиентом.КредитСдвиг Тогда
					ВариантОплатыПредставление = "оплата после отгрузки";
				ИначеЕсли СтрокаЭтапыГрафикаОплаты.ВариантОплаты = Перечисления.ВариантыОплатыКлиентом.ПредоплатаДоОтгрузки Тогда
					ВариантОплатыПредставление = "предоплата";
				ИначеЕсли СтрокаЭтапыГрафикаОплаты.ВариантОплаты = Перечисления.ВариантыОплатыКлиентом.АвансДоОбеспечения Тогда
					ВариантОплатыПредставление = "аванс";
				Иначе
					ВариантОплатыПредставление = СтрокаЭтапыГрафикаОплаты.ВариантОплаты;
				КонецЕсли;
				Если Не СтрокаУсловияОплаты = "" Тогда
					СтрокаУсловияОплаты = СтрокаУсловияОплаты + ", ";
				КонецЕсли;
				Если СтрокаЭтапыГрафикаОплаты.УчетОтсрочки = 0 Тогда
					СтрокаУсловияОплаты = СтрокаУсловияОплаты + ВариантОплатыПредставление + " в размере " + Строка(СтрокаЭтапыГрафикаОплаты.ПроцентПлатежа) +"% в течение " + Строка(СтрокаЭтапыГрафикаОплаты.Сдвиг) + " календарных дней";	
				Иначе
					СтрокаУсловияОплаты = СтрокаУсловияОплаты + ВариантОплатыПредставление + " в размере " + Строка(СтрокаЭтапыГрафикаОплаты.ПроцентПлатежа) +"% в течение " + Строка(СтрокаЭтапыГрафикаОплаты.Сдвиг) + " банковских дней";
				КонецЕсли;
			КонецЦикла;
		Иначе 
			Для Каждого СтрокаЭтапыГрафикаОплаты Из ДанныеШапки.Соглашение.ЭтапыГрафикаОплаты Цикл
				Если СтрокаЭтапыГрафикаОплаты.ВариантОплаты = Перечисления.ВариантыОплатыКлиентом.КредитСдвиг Тогда
					ВариантОплатыПредставление = "оплата после отгрузки";
				ИначеЕсли СтрокаЭтапыГрафикаОплаты.ВариантОплаты = Перечисления.ВариантыОплатыКлиентом.ПредоплатаДоОтгрузки Тогда
					ВариантОплатыПредставление = "предоплата";
				ИначеЕсли СтрокаЭтапыГрафикаОплаты.ВариантОплаты = Перечисления.ВариантыОплатыКлиентом.АвансДоОбеспечения Тогда
					ВариантОплатыПредставление = "аванс";
				Иначе
					ВариантОплатыПредставление = СтрокаЭтапыГрафикаОплаты.ВариантОплаты;
				КонецЕсли;
				Если Не СтрокаУсловияОплаты = "" Тогда
					СтрокаУсловияОплаты = СтрокаУсловияОплаты + ", ";
				КонецЕсли;
				Если СтрокаЭтапыГрафикаОплаты.УчетОтсрочки = 0 Тогда
					СтрокаУсловияОплаты = СтрокаУсловияОплаты + ВариантОплатыПредставление + " в размере " + Строка(СтрокаЭтапыГрафикаОплаты.ПроцентПлатежа) +"% в течение " + Строка(СтрокаЭтапыГрафикаОплаты.Сдвиг) + " календарных дней";	
				Иначе
					СтрокаУсловияОплаты = СтрокаУсловияОплаты + ВариантОплатыПредставление + " в размере " + Строка(СтрокаЭтапыГрафикаОплаты.ПроцентПлатежа) +"% в течение " + Строка(СтрокаЭтапыГрафикаОплаты.Сдвиг) + " банковских дней";
				КонецЕсли;
			КонецЦикла; 
		КонецЕсли;
		
		ОбластьМакета.Параметры.УсловияОплаты = НРег(СтрокаУсловияОплаты);
		
		
		Для Каждого СтрокиКонтактИнформации Из ДанныеШапки.Организация.КонтактнаяИнформация Цикл
			Если Строка(СтрокиКонтактИнформации.Вид) = "Адрес склада" Тогда
				ОбластьМакета.Параметры.Адрес = СтрокиКонтактИнформации.Представление;
			КонецЕсли;
		КонецЦикла;
				
		Если ДанныеШапки.СпособДоставки = Перечисления.СпособыДоставки.ДоКлиента Тогда
			ОбластьМакета.Параметры.УсловияПоставки = "силами поставщика и за его счет";
		Иначе
			ОбластьМакета.Параметры.УсловияПоставки = НРег(ДанныеШапки.СпособДоставки);
		КонецЕсли;
		
		Если Не ТабличныйДокумент.ПроверитьВывод(ОбластьМакета) Тогда 
			
			ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц(); // начнем новую страницу  
			
		КонецЕсли;
		
		ТабличныйДокумент.Вывести(ОбластьМакета);
				
		ОбластьМакета = Макет.ПолучитьОбласть("ОбластьФаксимиле");
		
		ДанныеОтвЛица = ПолучитьОтветсвтенныхЛицОрганизации(ДанныеШапки.Организация, ДанныеШапки.Дата);
		Если ДанныеОтвЛица <> Ложь Тогда
			ОбластьМакета.Параметры.Должность = ДанныеОтвЛица.Должность;
			ОбластьМакета.Параметры.ФИОРуководителя = ДанныеОтвЛица.Наименование;
		КонецЕсли;
		
		ФормированиеПечатныхФорм.ВывестиФаксимилеВТабличныйДокумент(Макет, ОбластьМакета, ДанныеШапки.Организация, Новый Структура("ОтображатьФаксимиле", Истина));
		
		Если Не ТабличныйДокумент.ПроверитьВывод(ОбластьМакета) Тогда 
			
			ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц(); // начнем новую страницу  
			
		КонецЕсли;
		
		ТабличныйДокумент.Вывести(ОбластьМакета);
		
		УправлениеПечатью.ЗадатьОбластьПечатиДокумента(ТабличныйДокумент, НомерСтрокиНачало, ОбъектыПечати, ДанныеШапки.Ссылка);
		
	КонецЦикла;
	
	Возврат ТабличныйДокумент;	
	
КонецФункции 

Процедура УстановитьПараметр(ОбластьМакета, ИмяПараметра, ЗначениеПараметра)
	ОбластьМакета.Параметры.Заполнить(Новый Структура(ИмяПараметра, ЗначениеПараметра));
КонецПроцедуры

Функция ОписаниеОрганизации(ДанныеПечати, ИмяОрганизации)
	
	Сведения = ФормированиеПечатныхФорм.СведенияОЮрФизЛице(ДанныеПечати[ИмяОрганизации], ДанныеПечати.Дата);
	Реквизиты = "ПолноеНаименование,ИНН,КПП,ЮридическийАдрес,Телефоны";
	
	Возврат ФормированиеПечатныхФорм.ОписаниеОрганизации(Сведения, Реквизиты);
	
КонецФункции

Функция ОписаниеОрганизацииАдрес(ДанныеПечати, ИмяОрганизации)
	
	Сведения = ФормированиеПечатныхФорм.СведенияОЮрФизЛице(ДанныеПечати[ИмяОрганизации], ДанныеПечати.Дата);
	Реквизиты = "ЮридическийАдрес";
	
	Возврат ФормированиеПечатныхФорм.ОписаниеОрганизации(Сведения, Реквизиты);
	
КонецФункции

Функция ПолучитьОтветсвтенныхЛицОрганизации(ОрганизацияСсылка, ДатаДокумента)
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	ОтветственныеЛицаОрганизаций.Ссылка КАК Ссылка,
	               |	ОтветственныеЛицаОрганизаций.ВерсияДанных КАК ВерсияДанных,
	               |	ОтветственныеЛицаОрганизаций.ПометкаУдаления КАК ПометкаУдаления,
	               |	ОтветственныеЛицаОрганизаций.Владелец КАК Владелец,
	               |	ОтветственныеЛицаОрганизаций.Наименование КАК Наименование,
	               |	ОтветственныеЛицаОрганизаций.ОтветственноеЛицо КАК ОтветственноеЛицо,
	               |	ОтветственныеЛицаОрганизаций.ФизическоеЛицо КАК ФизическоеЛицо,
	               |	ОтветственныеЛицаОрганизаций.ДолжностьСсылка КАК ДолжностьСсылка,
	               |	ОтветственныеЛицаОрганизаций.Должность КАК Должность,
	               |	ОтветственныеЛицаОрганизаций.ПравоПодписиПоДоверенности КАК ПравоПодписиПоДоверенности,
	               |	ОтветственныеЛицаОрганизаций.ОснованиеПраваПодписи КАК ОснованиеПраваПодписи,
	               |	ОтветственныеЛицаОрганизаций.ДатаНачала КАК ДатаНачала,
	               |	ОтветственныеЛицаОрганизаций.ДатаОкончания КАК ДатаОкончания,
	               |	ОтветственныеЛицаОрганизаций.ДокументПраваПодписи КАК ДокументПраваПодписи,
	               |	ОтветственныеЛицаОрганизаций.НомерДокументаПраваПодписи КАК НомерДокументаПраваПодписи,
	               |	ОтветственныеЛицаОрганизаций.ДатаДокументаПраваПодписи КАК ДатаДокументаПраваПодписи,
	               |	ОтветственныеЛицаОрганизаций.Предопределенный КАК Предопределенный,
	               |	ОтветственныеЛицаОрганизаций.ИмяПредопределенныхДанных КАК ИмяПредопределенныхДанных,
	               |	ОтветственныеЛицаОрганизаций.Представление КАК Представление
	               |ИЗ
	               |	Справочник.ОтветственныеЛицаОрганизаций КАК ОтветственныеЛицаОрганизаций
	               |ГДЕ
	               |	ОтветственныеЛицаОрганизаций.Владелец = &ОрганизацияСсылка
	               |	И ОтветственныеЛицаОрганизаций.ОтветственноеЛицо = &Руководитель
				   |	И ОтветственныеЛицаОрганизаций.ПравоПодписиПоДоверенности = Истина
	               |	И ОтветственныеЛицаОрганизаций.ДатаНачала <= &ДатаДокумента";
	
	Запрос.УстановитьПараметр("ОрганизацияСсылка",ОрганизацияСсылка);
	Запрос.УстановитьПараметр("ДатаДокумента", ДатаДокумента);
	Запрос.УстановитьПараметр("Руководитель",Перечисления.ОтветственныеЛицаОрганизаций.Руководитель);
	РезультатЗапроса = Запрос.Выполнить();
	Выборка = РезультатЗапроса.Выбрать();
	Пока Выборка.Следующий() Цикл
		Если ЗначениеЗаполнено(Выборка.ДатаОкончания) Тогда
			Если Выборка.ДатаОкончания >= ДатаДокумента Тогда
				Возврат Выборка;
			КонецЕсли;
		ИначеЕсли Не ЗначениеЗаполнено(Выборка.ДатаОкончания) Тогда
			Если ДатаДокумента <= ТекущаяДата() Тогда
				Возврат Выборка;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	Возврат Ложь;
КонецФункции

Функция ПолучитьБанковскиеСчетаОрганизации(ОрганизацияСсылка, Валюта)

	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	БанковскиеСчетаОрганизаций.Ссылка КАК Ссылка,
	               |	БанковскиеСчетаОрганизаций.ВерсияДанных КАК ВерсияДанных,
	               |	БанковскиеСчетаОрганизаций.ПометкаУдаления КАК ПометкаУдаления,
	               |	БанковскиеСчетаОрганизаций.Владелец КАК Владелец,
	               |	БанковскиеСчетаОрганизаций.Наименование КАК Наименование,
	               |	БанковскиеСчетаОрганизаций.ТипСчета КАК ТипСчета,
	               |	БанковскиеСчетаОрганизаций.ВалютаДенежныхСредств КАК ВалютаДенежныхСредств,
	               |	БанковскиеСчетаОрганизаций.НомерСчета КАК НомерСчета,
	               |	БанковскиеСчетаОрганизаций.Банк КАК Банк,
	               |	БанковскиеСчетаОрганизаций.БанкДляРасчетов КАК БанкДляРасчетов,
	               |	БанковскиеСчетаОрганизаций.ТекстКорреспондента КАК ТекстКорреспондента,
	               |	БанковскиеСчетаОрганизаций.ТекстНазначения КАК ТекстНазначения,
	               |	БанковскиеСчетаОрганизаций.ВариантВыводаМесяца КАК ВариантВыводаМесяца,
	               |	БанковскиеСчетаОрганизаций.ВыводитьСуммуБезКопеек КАК ВыводитьСуммуБезКопеек,
	               |	БанковскиеСчетаОрганизаций.СрокИсполненияПлатежа КАК СрокИсполненияПлатежа,
	               |	БанковскиеСчетаОрганизаций.ИспользоватьОбменСБанком КАК ИспользоватьОбменСБанком,
	               |	БанковскиеСчетаОрганизаций.Программа КАК Программа,
	               |	БанковскиеСчетаОрганизаций.Кодировка КАК Кодировка,
	               |	БанковскиеСчетаОрганизаций.ФайлЗагрузки КАК ФайлЗагрузки,
	               |	БанковскиеСчетаОрганизаций.ФайлВыгрузки КАК ФайлВыгрузки,
	               |	БанковскиеСчетаОрганизаций.РазрешитьПлатежиБезУказанияЗаявок КАК РазрешитьПлатежиБезУказанияЗаявок,
	               |	БанковскиеСчетаОрганизаций.Подразделение КАК Подразделение,
	               |	БанковскиеСчетаОрганизаций.БИКБанка КАК БИКБанка,
	               |	БанковскиеСчетаОрганизаций.РучноеИзменениеРеквизитовБанка КАК РучноеИзменениеРеквизитовБанка,
	               |	БанковскиеСчетаОрганизаций.НаименованиеБанка КАК НаименованиеБанка,
	               |	БанковскиеСчетаОрганизаций.КоррСчетБанка КАК КоррСчетБанка,
	               |	БанковскиеСчетаОрганизаций.НаименованиеБанкаМеждународное КАК НаименованиеБанкаМеждународное,
	               |	БанковскиеСчетаОрганизаций.ГородБанка КАК ГородБанка,
	               |	БанковскиеСчетаОрганизаций.АдресБанка КАК АдресБанка,
	               |	БанковскиеСчетаОрганизаций.ТелефоныБанка КАК ТелефоныБанка,
	               |	БанковскиеСчетаОрганизаций.БИКБанкаДляРасчетов КАК БИКБанкаДляРасчетов,
	               |	БанковскиеСчетаОрганизаций.РучноеИзменениеРеквизитовБанкаДляРасчетов КАК РучноеИзменениеРеквизитовБанкаДляРасчетов,
	               |	БанковскиеСчетаОрганизаций.НаименованиеБанкаДляРасчетов КАК НаименованиеБанкаДляРасчетов,
	               |	БанковскиеСчетаОрганизаций.КоррСчетБанкаДляРасчетов КАК КоррСчетБанкаДляРасчетов,
	               |	БанковскиеСчетаОрганизаций.ГородБанкаДляРасчетов КАК ГородБанкаДляРасчетов,
	               |	БанковскиеСчетаОрганизаций.АдресБанкаДляРасчетов КАК АдресБанкаДляРасчетов,
	               |	БанковскиеСчетаОрганизаций.ТелефоныБанкаДляРасчетов КАК ТелефоныБанкаДляРасчетов,
	               |	БанковскиеСчетаОрганизаций.ГруппаФинансовогоУчета КАК ГруппаФинансовогоУчета,
	               |	БанковскиеСчетаОрганизаций.ИспользоватьПрямойОбменСБанком КАК ИспользоватьПрямойОбменСБанком,
	               |	БанковскиеСчетаОрганизаций.ОбменСБанкомВключен КАК ОбменСБанкомВключен,
	               |	БанковскиеСчетаОрганизаций.СчетУчета КАК СчетУчета,
	               |	БанковскиеСчетаОрганизаций.СВИФТБанка КАК СВИФТБанка,
	               |	БанковскиеСчетаОрганизаций.СВИФТБанкаДляРасчетов КАК СВИФТБанкаДляРасчетов,
	               |	БанковскиеСчетаОрганизаций.ИностранныйБанк КАК ИностранныйБанк,
	               |	БанковскиеСчетаОрганизаций.СчетВБанкеДляРасчетов КАК СчетВБанкеДляРасчетов,
	               |	БанковскиеСчетаОрганизаций.Закрыт КАК Закрыт,
	               |	БанковскиеСчетаОрганизаций.ОтдельныйСчетГОЗ КАК ОтдельныйСчетГОЗ,
	               |	БанковскиеСчетаОрганизаций.ГосударственныйКонтракт КАК ГосударственныйКонтракт,
	               |	БанковскиеСчетаОрганизаций.НаправлениеДеятельности КАК НаправлениеДеятельности,
	               |	БанковскиеСчетаОрганизаций.ГородБанкаМеждународный КАК ГородБанкаМеждународный,
	               |	БанковскиеСчетаОрганизаций.АдресБанкаМеждународный КАК АдресБанкаМеждународный,
	               |	БанковскиеСчетаОрганизаций.НаименованиеБанкаДляРасчетовМеждународное КАК НаименованиеБанкаДляРасчетовМеждународное,
	               |	БанковскиеСчетаОрганизаций.ГородБанкаДляРасчетовМеждународный КАК ГородБанкаДляРасчетовМеждународный,
	               |	БанковскиеСчетаОрганизаций.АдресБанкаДляРасчетовМеждународный КАК АдресБанкаДляРасчетовМеждународный,
	               |	БанковскиеСчетаОрганизаций.СтранаБанка КАК СтранаБанка,
	               |	БанковскиеСчетаОрганизаций.СтранаБанкаДляРасчетов КАК СтранаБанкаДляРасчетов,
	               |	БанковскиеСчетаОрганизаций.ОсновнойБанковскийСчет КАК ОсновнойБанковскийСчет,
	               |	БанковскиеСчетаОрганизаций.КодировкаДругая КАК КодировкаДругая,
	               |	БанковскиеСчетаОрганизаций.АлгоритмЗагрузки КАК АлгоритмЗагрузки,
	               |	БанковскиеСчетаОрганизаций.АлгоритмВыгрузки КАК АлгоритмВыгрузки,
	               |	БанковскиеСчетаОрганизаций.Кодировка_Выгрузка КАК Кодировка_Выгрузка,
	               |	БанковскиеСчетаОрганизаций.КодировкаДругая_Выгрузка КАК КодировкаДругая_Выгрузка,
	               |	БанковскиеСчетаОрганизаций.ДополнительныеРеквизиты.(
	               |		Ссылка КАК Ссылка,
	               |		НомерСтроки КАК НомерСтроки,
	               |		Свойство КАК Свойство,
	               |		Значение КАК Значение,
	               |		ТекстоваяСтрока КАК ТекстоваяСтрока
	               |	) КАК ДополнительныеРеквизиты,
	               |	БанковскиеСчетаОрганизаций.Предопределенный КАК Предопределенный,
	               |	БанковскиеСчетаОрганизаций.ИмяПредопределенныхДанных КАК ИмяПредопределенныхДанных,
	               |	БанковскиеСчетаОрганизаций.ИспользуетсяБанкДляРасчетовПередача КАК ИспользуетсяБанкДляРасчетовПередача
	               |ИЗ
	               |	Справочник.БанковскиеСчетаОрганизаций КАК БанковскиеСчетаОрганизаций
	               |ГДЕ
	               |	БанковскиеСчетаОрганизаций.Владелец = &ОрганизацияСсылка
	               |	И БанковскиеСчетаОрганизаций.ВалютаДенежныхСредств = &Валюта
	               |	И БанковскиеСчетаОрганизаций.Закрыт = ЛОЖЬ
	               |	И БанковскиеСчетаОрганизаций.ПометкаУдаления = ЛОЖЬ";
	Запрос.УстановитьПараметр("ОрганизацияСсылка", ОрганизацияСсылка);
	Запрос.УстановитьПараметр("Валюта", Валюта);
	РезультатЗапроса = Запрос.Выполнить();
	Выборка = РезультатЗапроса.Выбрать();
	Выборка.Следующий(); 
	Возврат Выборка;

КонецФункции // ПолучитьБанковскиеСчетаОрганизации()

Функция ПолучитьБанковскиеСчетаКонтрагента(КонтрагентСсылка, Валюта)

	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	БанковскиеСчетаКонтрагентов.Ссылка КАК Ссылка,
	               |	БанковскиеСчетаКонтрагентов.ВерсияДанных КАК ВерсияДанных,
	               |	БанковскиеСчетаКонтрагентов.ПометкаУдаления КАК ПометкаУдаления,
	               |	БанковскиеСчетаКонтрагентов.Владелец КАК Владелец,
	               |	БанковскиеСчетаКонтрагентов.Наименование КАК Наименование,
	               |	БанковскиеСчетаКонтрагентов.НомерСчета КАК НомерСчета,
	               |	БанковскиеСчетаКонтрагентов.Банк КАК Банк,
	               |	БанковскиеСчетаКонтрагентов.БанкДляРасчетов КАК БанкДляРасчетов,
	               |	БанковскиеСчетаКонтрагентов.ТекстКорреспондента КАК ТекстКорреспондента,
	               |	БанковскиеСчетаКонтрагентов.ТекстНазначения КАК ТекстНазначения,
	               |	БанковскиеСчетаКонтрагентов.ВалютаДенежныхСредств КАК ВалютаДенежныхСредств,
	               |	БанковскиеСчетаКонтрагентов.БИКБанка КАК БИКБанка,
	               |	БанковскиеСчетаКонтрагентов.РучноеИзменениеРеквизитовБанка КАК РучноеИзменениеРеквизитовБанка,
	               |	БанковскиеСчетаКонтрагентов.НаименованиеБанка КАК НаименованиеБанка,
	               |	БанковскиеСчетаКонтрагентов.КоррСчетБанка КАК КоррСчетБанка,
	               |	БанковскиеСчетаКонтрагентов.ГородБанка КАК ГородБанка,
	               |	БанковскиеСчетаКонтрагентов.АдресБанка КАК АдресБанка,
	               |	БанковскиеСчетаКонтрагентов.ТелефоныБанка КАК ТелефоныБанка,
	               |	БанковскиеСчетаКонтрагентов.БИКБанкаДляРасчетов КАК БИКБанкаДляРасчетов,
	               |	БанковскиеСчетаКонтрагентов.РучноеИзменениеРеквизитовБанкаДляРасчетов КАК РучноеИзменениеРеквизитовБанкаДляРасчетов,
	               |	БанковскиеСчетаКонтрагентов.НаименованиеБанкаДляРасчетов КАК НаименованиеБанкаДляРасчетов,
	               |	БанковскиеСчетаКонтрагентов.КоррСчетБанкаДляРасчетов КАК КоррСчетБанкаДляРасчетов,
	               |	БанковскиеСчетаКонтрагентов.ГородБанкаДляРасчетов КАК ГородБанкаДляРасчетов,
	               |	БанковскиеСчетаКонтрагентов.АдресБанкаДляРасчетов КАК АдресБанкаДляРасчетов,
	               |	БанковскиеСчетаКонтрагентов.ТелефоныБанкаДляРасчетов КАК ТелефоныБанкаДляРасчетов,
	               |	БанковскиеСчетаКонтрагентов.СВИФТБанка КАК СВИФТБанка,
	               |	БанковскиеСчетаКонтрагентов.СВИФТБанкаДляРасчетов КАК СВИФТБанкаДляРасчетов,
	               |	БанковскиеСчетаКонтрагентов.ИностранныйБанк КАК ИностранныйБанк,
	               |	БанковскиеСчетаКонтрагентов.СчетВБанкеДляРасчетов КАК СчетВБанкеДляРасчетов,
	               |	БанковскиеСчетаКонтрагентов.Закрыт КАК Закрыт,
	               |	БанковскиеСчетаКонтрагентов.ОтдельныйСчетГОЗ КАК ОтдельныйСчетГОЗ,
	               |	БанковскиеСчетаКонтрагентов.ГосударственныйКонтракт КАК ГосударственныйКонтракт,
	               |	БанковскиеСчетаКонтрагентов.ИННКорреспондента КАК ИННКорреспондента,
	               |	БанковскиеСчетаКонтрагентов.КППКорреспондента КАК КППКорреспондента,
	               |	БанковскиеСчетаКонтрагентов.НаименованиеБанкаМеждународное КАК НаименованиеБанкаМеждународное,
	               |	БанковскиеСчетаКонтрагентов.ГородБанкаМеждународный КАК ГородБанкаМеждународный,
	               |	БанковскиеСчетаКонтрагентов.АдресБанкаМеждународный КАК АдресБанкаМеждународный,
	               |	БанковскиеСчетаКонтрагентов.НаименованиеБанкаДляРасчетовМеждународное КАК НаименованиеБанкаДляРасчетовМеждународное,
	               |	БанковскиеСчетаКонтрагентов.ГородБанкаДляРасчетовМеждународный КАК ГородБанкаДляРасчетовМеждународный,
	               |	БанковскиеСчетаКонтрагентов.АдресБанкаДляРасчетовМеждународный КАК АдресБанкаДляРасчетовМеждународный,
	               |	БанковскиеСчетаКонтрагентов.СтранаБанка КАК СтранаБанка,
	               |	БанковскиеСчетаКонтрагентов.СтранаБанкаДляРасчетов КАК СтранаБанкаДляРасчетов,
	               |	БанковскиеСчетаКонтрагентов.Предопределенный КАК Предопределенный,
	               |	БанковскиеСчетаКонтрагентов.ИмяПредопределенныхДанных КАК ИмяПредопределенныхДанных
	               |ИЗ
	               |	Справочник.БанковскиеСчетаКонтрагентов КАК БанковскиеСчетаКонтрагентов
	               |ГДЕ
	               |	БанковскиеСчетаКонтрагентов.Владелец = &КонтрагентСсылка
	               |	И БанковскиеСчетаКонтрагентов.ВалютаДенежныхСредств = &Валюта
	               |	И БанковскиеСчетаКонтрагентов.Закрыт = ЛОЖЬ
	               |	И БанковскиеСчетаКонтрагентов.ПометкаУдаления = ЛОЖЬ";
	Запрос.УстановитьПараметр("КонтрагентСсылка", КонтрагентСсылка);
	Запрос.УстановитьПараметр("Валюта", Валюта);
	РезультатЗапроса = Запрос.Выполнить();
	Выборка = РезультатЗапроса.Выбрать();
	Выборка.Следующий(); 
	Возврат Выборка;

КонецФункции // ПолучитьБанковскиеСчетаКонтрагента()

Функция ОписаниеСчетСпецифИНН(ДанныеПечати, ИмяОрганизации)
	
	Сведения = ФормированиеПечатныхФорм.СведенияОЮрФизЛице(ДанныеПечати[ИмяОрганизации], ДанныеПечати.Дата);
	Реквизиты = "ИНН";
	
	Возврат ФормированиеПечатныхФорм.ОписаниеОрганизации(Сведения, Реквизиты);
	
КонецФункции

Функция ОписаниеСчетСпецифЮрАдрес(ДанныеПечати, ИмяОрганизации)
	
	Сведения = ФормированиеПечатныхФорм.СведенияОЮрФизЛице(ДанныеПечати[ИмяОрганизации], ДанныеПечати.Дата);
	Реквизиты = "ЮридическийАдрес";
	
	Возврат ФормированиеПечатныхФорм.ОписаниеОрганизации(Сведения, Реквизиты);
	
КонецФункции

Функция ОписаниеСчетСпецифТелефоны(ДанныеПечати, ИмяОрганизации)
	
	Сведения = ФормированиеПечатныхФорм.СведенияОЮрФизЛице(ДанныеПечати[ИмяОрганизации], ДанныеПечати.Дата);
	Реквизиты = "Телефоны";
	
	Возврат ФормированиеПечатныхФорм.ОписаниеОрганизации(Сведения, Реквизиты);
	
КонецФункции 

Функция ОписаниеСчетСпецифПолноеНаименование(ДанныеПечати, ИмяОрганизации)
	
	Сведения = ФормированиеПечатныхФорм.СведенияОЮрФизЛице(ДанныеПечати[ИмяОрганизации], ДанныеПечати.Дата);
	Реквизиты = "ПолноеНаименование";
	
	Возврат ФормированиеПечатныхФорм.ОписаниеОрганизации(Сведения, Реквизиты);
	
КонецФункции


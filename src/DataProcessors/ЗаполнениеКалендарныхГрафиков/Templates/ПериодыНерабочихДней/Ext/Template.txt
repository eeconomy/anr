﻿<Items Description="Периоды нерабочих дней" Columns="Calendar,Order,StartDate,EndDate,Description">
	<Item Calendar="РФ" Order="1" StartDate="20200330" EndDate="20200403" Description="Указ Президента РФ от 25.03.2020 № 206"/>
	<Item Calendar="РФ" Order="2" StartDate="20200404" EndDate="20200430" Description="Указ Президента РФ от 02.04.2020 № 239"/>
</Items>
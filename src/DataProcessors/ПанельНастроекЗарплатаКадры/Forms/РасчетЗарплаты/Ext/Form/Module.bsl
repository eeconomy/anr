﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	РаботаВБюджетномУчреждении = ПолучитьФункциональнуюОпцию("РаботаВБюджетномУчреждении");
	УчетПоСтатьямФинансированияЗарплата.ДополнитьФормуНастройкаРасчетаЗарплаты(ЭтаФорма);
	
	// Определяем отображается ли глобальная настройка по отключению всей библиотеки.
	// Если отображается, то отдельно настройку использования расчета зарплаты не показываем.
	ДоступностьУстановкиИспользованияКадровогоУчетаИРасчетаЗарплаты = Ложь;
	ЗарплатаКадрыРасширенныйПереопределяемый.ОпределитьДоступностьУстановкиИспользованияЗарплатаКадры(ДоступностьУстановкиИспользованияКадровогоУчетаИРасчетаЗарплаты);
	Если ДоступностьУстановкиИспользованияКадровогоУчетаИРасчетаЗарплаты Тогда
		Элементы.ГруппаИспользоватьРасчетЗарплаты.Видимость = Ложь;
	КонецЕсли;
	
	ПрочитатьНастройки();
		
	ПорядокРасчетаСтоимостиЕдиницыВремениОплатыТрудаПоСреднегодовомуЗначению = Перечисления.ПорядокРасчетаСтоимостиЕдиницыВремениОплатыТруда.ПоСреднегодовомуЗначению;
	ПорядокРасчетаСтоимостиЕдиницыВремениОплатыТрудаПустаяСсылка = Перечисления.ПорядокРасчетаСтоимостиЕдиницыВремениОплатыТруда.ПустаяСсылка();
	
	СпособРасчетаСтоимостиЕдиницыВремениОплатыТрудаПоСовокупнойТарифнойСтавке = Перечисления.СпособРасчетаСтоимостиЕдиницыВремениОплатыТруда.СовокупнаяТарифнаяСтавка;
	СпособРасчетаСтоимостиЕдиницыВремениОплатыТрудаПустаяСсылка = Перечисления.СпособРасчетаСтоимостиЕдиницыВремениОплатыТруда.ПустаяСсылка();
	                                                                        	
	ИспользоватьСтатьиФинансированияОписание = НСтр("ru = 'Возможность учета по статьям финансирования рекомендуется использовать некоммерческим организациям и унитарным предприятиям любого уровня только при наличии целевого финансирования.'");
	ИспользоватьИФООписание = НСтр("ru = 'Ведение учета в разрезе источников финансового обеспечения (ИФО), указание ИФО будет доступно в статье финансирования.'");
	Элементы.ИспользоватьОтчетностьМониторингаРаботниковСоциальнойСферыОписание.Ширина = ?(РаботаВБюджетномУчреждении,70,35);
	Элементы.ИспользоватьОтчетностьМониторингаРаботниковСоциальнойСферыОписание.Высота = ?(РаботаВБюджетномУчреждении,2,5);
	Если РаботаВБюджетномУчреждении Тогда
		Элементы.НастройкиРасчетаЗарплатыИспользоватьТарифныеСеткиПриРасчетеЗарплаты.Заголовок = НСтр("ru = 'Используются ПКГ и ПКУ'");
		Элементы.Декорация10.Заголовок = НСтр("ru = 'Использование профессиональных квалификационных групп и уровней при описании позиции штатного расписания и при расчете заработной платы.'");
	Иначе
		
		Если Не ПолучитьФункциональнуюОпцию("ИспользоватьШтатноеРасписание") Тогда
			Элементы.Декорация10.Заголовок = НСтр("ru = 'Использование разрядов (квалификационных категорий) сотрудников и тарифных групп при расчете заработной платы.'");
		КонецЕсли;
		
	КонецЕсли;
	
	ИспользоватьКвалификационнуюНадбавкуОписание = НСтр("ru = 'Использование квалификационных категорий и квалификационных надбавок можно включить на форме Настройка начислений и удержаний.'");
	
	ОбновитьФормуПоНастройкам(Параметры.ПрочиеНастройки);
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьФормуПоНастройкам(ПрочиеНастройки)

	ОбновитьФормуПоНастройкеИспользоватьНачислениеЗарплаты(ЭтаФорма);
	ОбновитьФормуПоНастройкеИспользоватьСтатьиФинансирования(ЭтаФорма);
	ОбновитьДоступностьНастроекОтчетаМониторингаРаботниковСоциальнойСферы(ЭтаФорма);
	
	Если ЗарплатаКадры.ЭтоБазоваяВерсияКонфигурации() Тогда
		
		ИспользоватьИндексациюЗаработкаИнфоТекст = НСтр("ru = 'Индексация (повышение) заработка сотрудников выполняется в целом по организации (структурному подразделению) и учитывается при расчете среднего заработка.'");
		
		Если ПрочиеНастройки Тогда
			
			Элементы.ГруппаНастройкаНачислений.Видимость = Ложь;
			Элементы.ГруппаРассчитыватьДокументыПриРедактировании.Видимость = Ложь;
			Элементы.ГруппаИндексация.Видимость = Ложь;
			Элементы.ГруппаЗаймы.Видимость = Ложь;
			Элементы.ГруппаДоговоры.Видимость = Ложь;
			Элементы.ГруппаТарифныеСетки.Видимость = Ложь;
			Элементы.ГруппаПрочиеДоходы.Видимость = Ложь;
			Элементы.ГруппаБывшиеСотрудники.Видимость = Ложь;
			Элементы.ГруппаПризыПодарки.Видимость = Ложь;
			Элементы.ГруппаГрафикиРаботы.Видимость = Ложь;
			
			Элементы.РедактированиеПрочихНастроекРасчетаЗарплаты.Видимость = Ложь;
			
		Иначе
			Элементы.ГруппаИспользоватьРасчетЗарплаты.Видимость = Ложь;
			Элементы.ГруппаИспользоватьУчетПоСтатьямФинансирования.Видимость = Ложь;
			Элементы.ГруппаРасчетПоОрганизации.Видимость = Ложь;
			Элементы.ГруппаНесколькоСтавок.Видимость = Ложь;
			Элементы.ГруппаПорядокРасчетаСтоимостиЕдиницыВремени.Видимость = Ложь;
		КонецЕсли;
		
	Иначе
		
		Если РаботаВБюджетномУчреждении Тогда
			Элементы.ГруппаИспользоватьРасчетЗарплаты.Видимость = Ложь;
		Иначе
			Элементы.ГруппаИспользоватьРасчетЗарплаты.Видимость = Истина;
		КонецЕсли;
		
		ИспользоватьИндексациюЗаработкаИнфоТекст = НСтр("ru = 'Индексация (повышение) заработка сотрудников выполняется в целом по организации (филиалу, структурному подразделению) и учитывается при расчете среднего заработка.'");
		
		Элементы.РедактированиеПрочихНастроекРасчетаЗарплаты.Видимость = Ложь;
		
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	
	Если ЭтаФорма.ТребуетсяОбновлениеИнтерфейса Тогда
		ОбновитьИнтерфейс();
	КонецЕсли;
	
	Оповестить("НастройкаРасчераЗарплатыЗаписана",,ЭтаФорма.ВладелецФормы);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "НастройкаРасчераЗарплатыЗаписана" И Источник = ЭтаФорма Тогда
		ОбработкаОповещенияНаСервере();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаОповещенияНаСервере()
	
	ПрочитатьНастройки();

КонецПроцедуры

&НаКлиенте
Процедура НастройкаНачисленийУдержаний(Команда)
	
	ОткрытьФорму("Обработка.ПанельНастроекЗарплатаКадры.Форма.НастройкаНачисленийУдержаний");
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ИспользоватьНачислениеЗарплатыПриИзменении(Элемент)
	
	УстановитьЗначенияЗависимыхНастроекРасчетаЗарплаты();
	
	ЗаписатьНастройкиНаКлиенте("ИспользоватьСтатьиФинансирования,НастройкиСтатистикиПерсонала,НастройкиРасчетаЗарплаты,НастройкиЗаймовСотрудникам,ИспользоватьНачислениеЗарплаты,РассчитыватьДокументыПриРедактировании");
	ПодключитьОбработчикОжиданияОбновленияИнтерфейса();

	ОбновитьФормуПоНастройкеИспользоватьНачислениеЗарплаты(ЭтаФорма);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ОбновитьФормуПоНастройкеИспользоватьНачислениеЗарплаты(Форма)
	
	Форма.Элементы.ГруппаНастройки.Доступность = Форма.ИспользоватьНачислениеЗарплаты;
		
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьИндексациюЗаработкаПриИзменении(Элемент)
	
	НастройкиРасчетаЗарплаты.ИспользоватьИндексациюЗаработка = ИспользоватьИндексациюЗаработка;
	НастройкиРасчетаЗарплатыПрежняя.ИспользоватьИндексациюЗаработка = НастройкиРасчетаЗарплаты.ИспользоватьИндексациюЗаработка;
	
	ЗаписатьНастройкиНаКлиенте("НастройкиРасчетаЗарплаты");
	ПодключитьОбработчикОжиданияОбновленияИнтерфейса();
	
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьПризыПодаркиПриИзменении(Элемент)
	
	НастройкиРасчетаЗарплаты.ИспользоватьПризыПодарки = ИспользоватьПризыПодарки;
	НастройкиРасчетаЗарплатыПрежняя.ИспользоватьПризыПодарки = НастройкиРасчетаЗарплаты.ИспользоватьПризыПодарки;
	
	ЗаписатьНастройкиНаКлиенте("НастройкиРасчетаЗарплаты");
	ПодключитьОбработчикОжиданияОбновленияИнтерфейса();
	
КонецПроцедуры

&НаКлиенте
Процедура ВыполнятьРасчетЗарплатыПоПодразделениямПриИзменении(Элемент)
	
	НастройкиРасчетаЗарплаты.ВыполнятьРасчетЗарплатыПоПодразделениям = Не НеВыполнятьРасчетЗарплатыПоПодразделениям;
	НастройкиРасчетаЗарплатыПрежняя.ВыполнятьРасчетЗарплатыПоПодразделениям = НастройкиРасчетаЗарплаты.ВыполнятьРасчетЗарплатыПоПодразделениям;
	
	ЗаписатьНастройкиНаКлиенте("НастройкиРасчетаЗарплаты");
	ПодключитьОбработчикОжиданияОбновленияИнтерфейса();
	
КонецПроцедуры

// 4D:ERP для Беларуси, ЕленаТ, 21.04.2021 
// Профессиональное пенсионное страхование и ПУ-6, № 25359 
// {
&НаКлиенте
Процедура ИспользоватьПрофессиональноеПенсионноеСтрахованиеПриИзменении(Элемент)
	НастройкиРасчетаЗарплатыПрежняя.ИспользоватьПрофессиональноеПенсионноеСтрахование = НастройкиРасчетаЗарплаты.ИспользоватьПрофессиональноеПенсионноеСтрахование;
	НастройкиРасчетаЗарплаты.ИспользоватьПрофессиональноеПенсионноеСтрахование = ИспользоватьПрофессиональноеПенсионноеСтрахование;
	
	ЗаписатьНастройкиНаКлиенте("НастройкиРасчетаЗарплаты");
	ПодключитьОбработчикОжиданияОбновленияИнтерфейса();
КонецПроцедуры
// }
// 4D

&НаКлиенте
Процедура РассчитыватьДокументыПриРедактированииПриИзменении(Элемент)
	
	ЗаписатьНастройкиНаКлиенте("РассчитыватьДокументыПриРедактировании");
	РассчитыватьДокументыПриРедактированииПрежняя = РассчитыватьДокументыПриРедактировании;
	ПодключитьОбработчикОжиданияОбновленияИнтерфейса();
	
КонецПроцедуры

&НаКлиенте
Процедура ПорядокРасчетаСтоимостиЕдиницыВремениПриИзменении(Элемент)
	
	НастройкиРасчетаЗарплаты.ПорядокРасчетаСтоимостиЕдиницыВремени = ПорядокРасчетаСтоимостиЕдиницыВремени;
	НастройкиРасчетаЗарплатыПрежняя.ПорядокРасчетаСтоимостиЕдиницыВремени = НастройкиРасчетаЗарплаты.ПорядокРасчетаСтоимостиЕдиницыВремени;
	
	ЗаписатьНастройкиНаКлиенте("НастройкиРасчетаЗарплаты");
	
КонецПроцедуры

&НаКлиенте
Процедура СпособРасчетаСтоимостиЕдиницыВремениПриИзменении(Элемент)
		
	НастройкиРасчетаЗарплаты.СпособРасчетаСтоимостиЕдиницыВремени = СпособРасчетаСтоимостиЕдиницыВремени;
	НастройкиРасчетаЗарплатыПрежняя.СпособРасчетаСтоимостиЕдиницыВремени = НастройкиРасчетаЗарплаты.СпособРасчетаСтоимостиЕдиницыВремени;
	
	ЗаписатьНастройкиНаКлиенте("НастройкиРасчетаЗарплаты");

КонецПроцедуры


&НаКлиенте
Процедура ИспользоватьНачисленияПоДоговорамПриИзменении(Элемент)
	
	НастройкиРасчетаЗарплаты.ИспользоватьНачисленияПоДоговорам = ИспользоватьНачисленияПоДоговорам;
	НастройкиРасчетаЗарплатыПрежняя.ИспользоватьНачисленияПоДоговорам = НастройкиРасчетаЗарплаты.ИспользоватьНачисленияПоДоговорам;
	ЗаписатьНастройкиНаКлиенте("НастройкиРасчетаЗарплаты");
	ПодключитьОбработчикОжиданияОбновленияИнтерфейса();
	
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьЗаймыСотрудникамПриИзменении(Элемент)
	
	НастройкиЗаймовСотрудникамПрежняя.ИспользоватьЗаймыСотрудникам = НастройкиЗаймовСотрудникам.ИспользоватьЗаймыСотрудникам;
	ЗаписатьНастройкиНаКлиенте("НастройкиЗаймовСотрудникам");
	ПодключитьОбработчикОжиданияОбновленияИнтерфейса();
	
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьРегистрациюПрочихДоходовПриИзменении(Элемент)
	
	НастройкиРасчетаЗарплаты.ИспользоватьРегистрациюПрочихДоходов = ИспользоватьРегистрациюПрочихДоходов;
	НастройкиРасчетаЗарплатыПрежняя.ИспользоватьРегистрациюПрочихДоходов = НастройкиРасчетаЗарплаты.ИспользоватьРегистрациюПрочихДоходов;
	
	ЗаписатьНастройкиНаКлиенте("НастройкиРасчетаЗарплаты");
	ПодключитьОбработчикОжиданияОбновленияИнтерфейса();
	
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьВыплатыБывшимСотрудникамПриИзменении(Элемент)
	
	НастройкиРасчетаЗарплаты.ИспользоватьВыплатыБывшимСотрудникам = ИспользоватьВыплатыБывшимСотрудникам;
	НастройкиРасчетаЗарплатыПрежняя.ИспользоватьВыплатыБывшимСотрудникам = НастройкиРасчетаЗарплаты.ИспользоватьВыплатыБывшимСотрудникам;
	
	ЗаписатьНастройкиНаКлиенте("НастройкиРасчетаЗарплаты");
	ПодключитьОбработчикОжиданияОбновленияИнтерфейса();
	
КонецПроцедуры

&НаКлиенте
Процедура НастройкиРасчетаЗарплатыИспользоватьТарифныеСеткиПриРасчетеЗарплатыПриИзменении(Элемент)
	
	НастройкиРасчетаЗарплаты.ИспользоватьТарифныеСеткиПриРасчетеЗарплаты = ИспользоватьТарифныеСеткиПриРасчетеЗарплаты;
	НастройкиРасчетаЗарплатыПрежняя.ИспользоватьТарифныеСеткиПриРасчетеЗарплаты = НастройкиРасчетаЗарплаты.ИспользоватьТарифныеСеткиПриРасчетеЗарплаты;

	ЗаписатьНастройкиНаКлиенте("НастройкиРасчетаЗарплаты");
	ПодключитьОбработчикОжиданияОбновленияИнтерфейса();
	
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьНесколькоТарифныхСтавокПриИзменении(Элемент)
	
	НастройкиРасчетаЗарплаты.ИспользоватьНесколькоТарифныхСтавок = ИспользоватьНесколькоТарифныхСтавок;
	НастройкиРасчетаЗарплатыПрежняя.ИспользоватьНесколькоТарифныхСтавок = НастройкиРасчетаЗарплаты.ИспользоватьНесколькоТарифныхСтавок;
	ЗаписатьНастройкиНаКлиенте("НастройкиРасчетаЗарплаты");
	ПодключитьОбработчикОжиданияОбновленияИнтерфейса();
	
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьНесколькоВидовПлановогоВремениПриИзменении(Элемент)
	
	НастройкиУчетаВремениПрежняя.ИспользоватьНесколькоВидовПлановогоВремени = НастройкиУчетаВремени.ИспользоватьНесколькоВидовПлановогоВремени;
	ЗаписатьНастройкиНаКлиенте("НастройкиУчетаВремени");
	ПодключитьОбработчикОжиданияОбновленияИнтерфейса();
	
КонецПроцедуры


&НаКлиенте
Процедура ПроверятьСоответствиеФактическогоВремениПлановомуПриИзменении(Элемент)
	
	НастройкиУчетаВремениПрежняя.ПроверятьСоответствиеФактическогоВремениПлановому = НастройкиУчетаВремени.ПроверятьСоответствиеФактическогоВремениПлановому;
	ЗаписатьНастройкиНаКлиенте("НастройкиУчетаВремени");
	ПодключитьОбработчикОжиданияОбновленияИнтерфейса();
	
КонецПроцедуры


&НаКлиенте
Процедура ИспользоватьИндивидуальныйГрафикПриСменеГрафикаРаботыПереключательПриИзменении(Элемент)
	
	НастройкиРасчетаЗарплаты.ИспользоватьИндивидуальныйГрафикПриСменеГрафикаРаботы =
		?(ИспользоватьИндивидуальныйГрафикПриСменеГрафикаРаботыПереключатель = 0,Ложь,Истина);
		
	НастройкиРасчетаЗарплатыПрежняя.ИспользоватьИндивидуальныйГрафикПриСменеГрафикаРаботы = 
			НастройкиРасчетаЗарплаты.ИспользоватьИндивидуальныйГрафикПриСменеГрафикаРаботы;
	
	ЗаписатьНастройкиНаКлиенте("НастройкиРасчетаЗарплаты");
	ПодключитьОбработчикОжиданияОбновленияИнтерфейса();
	
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьСтатьиФинансированияПриИзменении(Элемент)
	
	УстановитьЗначенияЗависимыхНастроекИспользоватьСтатьиФинансирования();
	ИспользоватьСтатьиФинансированияПрежняя = ИспользоватьСтатьиФинансирования;
	
	ЗаписатьНастройкиНаКлиенте("ИспользоватьСтатьиФинансирования");
	ПодключитьОбработчикОжиданияОбновленияИнтерфейса();

	ОбновитьФормуПоНастройкеИспользоватьСтатьиФинансирования(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьОтчетностьМониторингаРаботниковСоциальнойСферыПриИзменении(Элемент)
	
	УстановитьЗависимыеНастройкиОтчетаМониторингаРаботниковСоциальнойСферы();
	ОбновитьДоступностьНастроекОтчетаМониторингаРаботниковСоциальнойСферы(ЭтаФорма);
	
	Если (НастройкиСтатистикиПерсонала.ИспользоватьОтчетностьМониторингаРаботниковСоциальнойСферы И ЗначениеЗаполнено(НастройкиСтатистикиПерсонала.ФормаОтчетностиМониторингаРаботниковСоциальнойСферы))
		Или Не НастройкиСтатистикиПерсонала.ИспользоватьОтчетностьМониторингаРаботниковСоциальнойСферы Тогда
		ЗаписатьНастройкиНаКлиенте("НастройкиСтатистикиПерсонала");
		ПодключитьОбработчикОжиданияОбновленияИнтерфейса();
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура НастройкиСтатистикиПерсоналаФормаОтчетностиМониторингаРаботниковСоциальнойСферыПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(НастройкиСтатистикиПерсонала.ФормаОтчетностиМониторингаРаботниковСоциальнойСферы) Тогда
		ЗаписатьНастройкиНаКлиенте("НастройкиСтатистикиПерсонала");
		ПодключитьОбработчикОжиданияОбновленияИнтерфейса();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьЗначенияЗависимыхНастроекИспользоватьСтатьиФинансирования()
	
	Если ИспользоватьСтатьиФинансирования Тогда
		НастройкиСтатистикиПерсонала.ИспользоватьОтчетностьМониторингаРаботниковСоциальнойСферы = НастройкиСтатистикиПерсоналаПрежняя.ИспользоватьОтчетностьМониторингаРаботниковСоциальнойСферы;
	Иначе
		НастройкиСтатистикиПерсоналаПрежняя.ИспользоватьОтчетностьМониторингаРаботниковСоциальнойСферы = НастройкиСтатистикиПерсонала.ИспользоватьОтчетностьМониторингаРаботниковСоциальнойСферы;
		НастройкиСтатистикиПерсонала.ИспользоватьОтчетностьМониторингаРаботниковСоциальнойСферы = Ложь;
	КонецЕсли;
	УстановитьЗависимыеНастройкиОтчетаМониторингаРаботниковСоциальнойСферы();
		
КонецПроцедуры

&НаКлиенте
Процедура УстановитьЗависимыеНастройкиОтчетаМониторингаРаботниковСоциальнойСферы()

	Если НастройкиСтатистикиПерсонала.ИспользоватьОтчетностьМониторингаРаботниковСоциальнойСферы Тогда
		НастройкиСтатистикиПерсонала.ФормаОтчетностиМониторингаРаботниковСоциальнойСферы = НастройкиСтатистикиПерсоналаПрежняя.ФормаОтчетностиМониторингаРаботниковСоциальнойСферы;
	Иначе
		НастройкиСтатистикиПерсоналаПрежняя.ФормаОтчетностиМониторингаРаботниковСоциальнойСферы = НастройкиСтатистикиПерсонала.ФормаОтчетностиМониторингаРаботниковСоциальнойСферы;
		НастройкиСтатистикиПерсонала.ФормаОтчетностиМониторингаРаботниковСоциальнойСферы = ПредопределенноеЗначение("Перечисление.ВидыФормМониторингаРаботниковСоциальнойСферы.ПустаяСсылка");
	КонецЕсли;

КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ОбновитьФормуПоНастройкеИспользоватьСтатьиФинансирования(Форма)
	
	Форма.Элементы.Группа2.Доступность = Форма.РаботаВБюджетномУчреждении ИЛИ Форма.ИспользоватьСтатьиФинансирования;
		
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ОбновитьДоступностьНастроекОтчетаМониторингаРаботниковСоциальнойСферы(Форма)

	Форма.Элементы.ФормаОтчетностиМониторингаРаботниковСоциальнойСферы.Доступность = Форма.НастройкиСтатистикиПерсонала.ИспользоватьОтчетностьМониторингаРаботниковСоциальнойСферы;
	
	Если Форма.Элементы.ФормаОтчетностиМониторингаРаботниковСоциальнойСферы.Доступность Тогда
		Форма.Элементы.ФормаОтчетностиМониторингаРаботниковСоциальнойСферы.АвтоОтметкаНезаполненного = Истина;
		Форма.Элементы.ФормаОтчетностиМониторингаРаботниковСоциальнойСферы.ОтметкаНезаполненного = Не ЗначениеЗаполнено(Форма.НастройкиСтатистикиПерсонала.ФормаОтчетностиМониторингаРаботниковСоциальнойСферы);
	Иначе
		Форма.Элементы.ФормаОтчетностиМониторингаРаботниковСоциальнойСферы.АвтоОтметкаНезаполненного = Ложь;
		Форма.Элементы.ФормаОтчетностиМониторингаРаботниковСоциальнойСферы.ОтметкаНезаполненного = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ИспользоватьУчетВРазрезеИФОПриИзменении(Элемент)

	ЗаписатьНастройкиНаКлиенте("НастройкиБюджетныхУчреждений");

КонецПроцедуры


#Область ОбработчикиСобытийТаблицыФормыПоказателисовокупнойтарифнойставки

&НаКлиенте
Процедура ПоказателиСовокупнойТарифнойСтавкиВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ВыбратьПоказателейСовокупнойТарифнойСтавки();
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказателиСовокупнойТарифнойСтавкиВыбранныеВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ОтменитьВыборПоказателейСовокупнойТарифнойСтавки();
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказателиСовокупнойТарифнойСтавкиJnvtybnm(Команда)
	
	ОтменитьВыборПоказателейСовокупнойТарифнойСтавки();
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказателиСовокупнойТарифнойСтавкиВыбрать(Команда)
	
	ВыбратьПоказателейСовокупнойТарифнойСтавки();
	
КонецПроцедуры

&НаКлиенте
Процедура ВыбратьПоказателейСовокупнойТарифнойСтавки()
	
	Если Элементы.ПоказателиСовокупнойТарифнойСтавки.ВыделенныеСтроки.Количество() > 0 Тогда
		УстановитьВыборВыборПоказателейСовокупнойТарифнойСтавки(Элементы.ПоказателиСовокупнойТарифнойСтавки.ВыделенныеСтроки, Истина);
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура ОтменитьВыборПоказателейСовокупнойТарифнойСтавки()
	
	Если Элементы.ПоказателиСовокупнойТарифнойСтавкиВыбранные.ВыделенныеСтроки.Количество() > 0 Тогда
		УстановитьВыборВыборПоказателейСовокупнойТарифнойСтавки(Элементы.ПоказателиСовокупнойТарифнойСтавкиВыбранные.ВыделенныеСтроки, Ложь);
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьВыборВыборПоказателейСовокупнойТарифнойСтавки(СписокЭлементов, ЗначениеВыбора)
	
	Для каждого ИдентификаторЗначения Из СписокЭлементов Цикл
		ЭлементСписка = ПоказателиСовокупнойТарифнойСтавки.НайтиПоИдентификатору(ИдентификаторЗначения);
		Если ЭлементСписка <> Неопределено Тогда
			ЭлементСписка.Выбран = ЗначениеВыбора;
		КонецЕсли; 
	КонецЦикла;
	
	ЗаписатьНастройкиНаКлиенте("ПоказателиСовокупнойТарифнойСтавки");
	
	Элементы.ПоказателиСовокупнойТарифнойСтавки.ОтборСтрок = Новый ФиксированнаяСтруктура("Выбран", Ложь);
	Элементы.ПоказателиСовокупнойТарифнойСтавкиВыбранные.ОтборСтрок = Новый ФиксированнаяСтруктура("Выбран", Истина);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ПрочитатьНастройки()

	ИспользоватьНачислениеЗарплаты = Константы.ИспользоватьНачислениеЗарплаты.Получить();
	ИспользоватьСтатьиФинансирования = РаботаВБюджетномУчреждении Или Константы.ИспользоватьСтатьиФинансированияЗарплата.Получить();
	ИспользоватьСтатьиФинансированияПрежняя = ИспользоватьСтатьиФинансирования;
	РассчитыватьДокументыПриРедактировании = Константы.РассчитыватьДокументыПриРедактировании.Получить();
	РассчитыватьДокументыПриРедактированииПрежняя = РассчитыватьДокументыПриРедактировании;
	
	Настройки = РегистрыСведений.НастройкиРасчетаЗарплатыРасширенный.СоздатьМенеджерЗаписи();
	Настройки.Прочитать();
	
	Если Не Настройки.Выбран() Тогда
		// Если настройки не заполнены, устанавливаем значения по умолчанию.
		Настройки.ПорядокРасчетаСтоимостиЕдиницыВремени = Перечисления.ПорядокРасчетаСтоимостиЕдиницыВремениОплатыТруда.ПоСреднегодовомуЗначению;
		Настройки.СпособРасчетаСтоимостиЕдиницыВремени = Перечисления.СпособРасчетаСтоимостиЕдиницыВремениОплатыТруда.СовокупнаяТарифнаяСтавка;
	КонецЕсли;
		
	ЗначениеВРеквизитФормы(Настройки, "НастройкиРасчетаЗарплатыПрежняя");
	
	Если Настройки.СпособРасчетаСтоимостиЕдиницыВремени = Перечисления.СпособРасчетаСтоимостиЕдиницыВремениОплатыТруда.ПустаяСсылка() Тогда
		Настройки.СпособРасчетаСтоимостиЕдиницыВремени = Перечисления.СпособРасчетаСтоимостиЕдиницыВремениОплатыТруда.СовокупнаяТарифнаяСтавка;
	КонецЕсли;
	
	ЗначениеВРеквизитФормы(Настройки, "НастройкиРасчетаЗарплаты");

	
	НеВыполнятьРасчетЗарплатыПоПодразделениям = Не НастройкиРасчетаЗарплаты.ВыполнятьРасчетЗарплатыПоПодразделениям;
	
	ИспользоватьИндивидуальныйГрафикПриСменеГрафикаРаботыПереключатель = 
		?(НастройкиРасчетаЗарплаты.ИспользоватьИндивидуальныйГрафикПриСменеГрафикаРаботы,1,0);
		
	СтрокаСвойствНастройкиРасчетаЗарплаты = "ИспользоватьВыплатыБывшимСотрудникам,ИспользоватьИндексациюЗаработка,ИспользоватьНачисленияПоДоговорам,
		|ИспользоватьНесколькоТарифныхСтавок,ИспользоватьПризыПодарки,ИспользоватьРегистрациюПрочихДоходов,
		// 4D:ERP для Беларуси, ЕленаТ, 26.04.2021 
		// Профессиональное пенсионное страхование и ПУ-6, № 25359
		// {
		|ИспользоватьПрофессиональноеПенсионноеСтрахование,
		// }
		// 4D
		|ИспользоватьТарифныеСеткиПриРасчетеЗарплаты,ПорядокРасчетаСтоимостиЕдиницыВремени,СпособРасчетаСтоимостиЕдиницыВремени";
	ЗаполнитьЗначенияСвойств(ЭтаФорма,НастройкиРасчетаЗарплаты,СтрокаСвойствНастройкиРасчетаЗарплаты);
		
	Настройки = РегистрыСведений.НастройкиЗаймовСотрудникам.СоздатьМенеджерЗаписи();
	Настройки.Прочитать();
	ЗначениеВРеквизитФормы(Настройки, "НастройкиЗаймовСотрудникам");
	
	Настройки = РегистрыСведений.НастройкиСтатистикиПерсонала.СоздатьМенеджерЗаписи();
	Настройки.Прочитать();
	ЗначениеВРеквизитФормы(Настройки, "НастройкиСтатистикиПерсонала");
	ЗначениеВРеквизитФормы(Настройки, "НастройкиСтатистикиПерсоналаПрежняя");
	
	Настройки = РегистрыСведений.НастройкиУчетаВремени.СоздатьМенеджерЗаписи();
	Настройки.Прочитать();
	ЗначениеВРеквизитФормы(Настройки, "НастройкиУчетаВремени");
	ЗначениеВРеквизитФормы(Настройки, "НастройкиУчетаВремениПрежняя");
	
	УчетПоСтатьямФинансированияЗарплата.НастройкиПрограммыВРеквизитыФормы(ЭтаФорма);
	
	ПоказателиСовокупнойТарифнойСтавки.Загрузить(ЗарплатаКадрыРасширенный_Локализация.СоставПоказателейСовокупнойТарифнойСтавки());
	
	Элементы.ПоказателиСовокупнойТарифнойСтавки.ОтборСтрок = Новый ФиксированнаяСтруктура("Выбран", Ложь);
	Элементы.ПоказателиСовокупнойТарифнойСтавкиВыбранные.ОтборСтрок = Новый ФиксированнаяСтруктура("Выбран", Истина);
	
КонецПроцедуры

&НаСервере
Процедура ЗаписатьНастройкиНаСервере(ИмяНастройки)
	
	ПараметрыНастроек = Обработки.ПанельНастроекЗарплатаКадры.ЗаполнитьСтруктуруПараметровНастроек(ИмяНастройки);
		
	ПараметрыНастроек.ИспользоватьНачислениеЗарплаты = ИспользоватьНачислениеЗарплаты;
	ПараметрыНастроек.НастройкиРасчетаЗарплаты       = ОбщегоНазначения.СтруктураПоМенеджеруЗаписи(НастройкиРасчетаЗарплаты, Метаданные.РегистрыСведений.НастройкиРасчетаЗарплатыРасширенный);
	ПараметрыНастроек.НастройкиЗаймовСотрудникам     = ОбщегоНазначения.СтруктураПоМенеджеруЗаписи(НастройкиЗаймовСотрудникам, Метаданные.РегистрыСведений.НастройкиЗаймовСотрудникам);
	ПараметрыНастроек.НастройкиСтатистикиПерсонала   = ОбщегоНазначения.СтруктураПоМенеджеруЗаписи(НастройкиСтатистикиПерсонала, Метаданные.РегистрыСведений.НастройкиСтатистикиПерсонала);
	ПараметрыНастроек.НастройкиУчетаВремени   		 = ОбщегоНазначения.СтруктураПоМенеджеруЗаписи(НастройкиУчетаВремени, Метаданные.РегистрыСведений.НастройкиУчетаВремени);
	ПараметрыНастроек.ИспользоватьСтатьиФинансирования = ИспользоватьСтатьиФинансирования;
	ПараметрыНастроек.РаботаВБюджетномУчреждении 	   = РаботаВБюджетномУчреждении;  
	ПараметрыНастроек.ПоказателиСовокупнойТарифнойСтавки = ПоказателиСовокупнойТарифнойСтавки.Выгрузить(Новый Структура("Выбран", Истина), "Показатель");
	ПараметрыНастроек.РассчитыватьДокументыПриРедактировании = РассчитыватьДокументыПриРедактировании;
	
	УчетПоСтатьямФинансированияЗарплата.ЗначенияСохраняемыхРеквизитовФормыНастройкаПрограммы(ЭтаФорма, ПараметрыНастроек);
	РазрядыКатегорииДолжностей.ЗначенияСохраняемыхРеквизитовФормыНастройкаПрограммы(ЭтаФорма, ПараметрыНастроек.НастройкиРасчетаЗарплаты);

	АдресХранилища = ПоместитьВоВременноеХранилище(Неопределено, УникальныйИдентификатор);
	Обработки.ПанельНастроекЗарплатаКадры.ЗаписатьНастройки(ПараметрыНастроек, АдресХранилища);
	ОбновитьПовторноИспользуемыеЗначения();

КонецПроцедуры

&НаКлиенте
Процедура ЗаписатьНастройкиНаКлиенте(ИмяНастройки)
	
	ЗаписатьНастройкиНаСервере(ИмяНастройки);
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьЗначенияЗависимыхНастроекРасчетаЗарплаты()
	
	Если Не ИспользоватьНачислениеЗарплаты Тогда
		
		ЗаполнитьЗначенияСвойств(НастройкиРасчетаЗарплатыПрежняя, НастройкиРасчетаЗарплаты);
		ЗаполнитьЗначенияСвойств(НастройкиЗаймовСотрудникамПрежняя, НастройкиЗаймовСотрудникам);
		ЗаполнитьЗначенияСвойств(НастройкиСтатистикиПерсоналаПрежняя, НастройкиСтатистикиПерсонала);
		ЗаполнитьЗначенияСвойств(НастройкиУчетаВремениПрежняя, НастройкиУчетаВремени);
		ИспользоватьСтатьиФинансированияПрежняя = ИспользоватьСтатьиФинансирования;
		РассчитыватьДокументыПриРедактированииПрежняя = РассчитыватьДокументыПриРедактировании;
		
		НастройкиРасчетаЗарплаты.ИспользоватьИндексациюЗаработка         = Ложь;
		НастройкиРасчетаЗарплаты.ИспользоватьПризыПодарки                = Ложь;
		НастройкиРасчетаЗарплаты.ИспользоватьНачисленияПоДоговорам       = Ложь;
		НастройкиРасчетаЗарплаты.ИспользоватьМатериальнуюПомощь          = Ложь;
		НастройкиРасчетаЗарплаты.ИспользоватьРегистрациюПрочихДоходов    = Ложь;
		НастройкиРасчетаЗарплаты.ИспользоватьВыплатыБывшимСотрудникам    = Ложь;
		НастройкиРасчетаЗарплаты.ВыполнятьРасчетЗарплатыПоПодразделениям = Ложь;
		НастройкиРасчетаЗарплаты.ИспользоватьНесколькоТарифныхСтавок     = Ложь;
		НастройкиРасчетаЗарплаты.ИспользоватьИндивидуальныйГрафикПриСменеГрафикаРаботы  = Ложь;
		НастройкиРасчетаЗарплаты.ПорядокРасчетаСтоимостиЕдиницыВремени = ПорядокРасчетаСтоимостиЕдиницыВремениОплатыТрудаПустаяСсылка;
		НастройкиРасчетаЗарплаты.СпособРасчетаСтоимостиЕдиницыВремени = СпособРасчетаСтоимостиЕдиницыВремениОплатыТрудаПустаяСсылка;

		
		НастройкиЗаймовСотрудникам.ИспользоватьЗаймыСотрудникам = Ложь;
		НеВыполнятьРасчетЗарплатыПоПодразделениям 				= Ложь;
		ИспользоватьСтатьиФинансирования = Ложь;
		РассчитыватьДокументыПриРедактировании = Ложь;
		
		НастройкиСтатистикиПерсонала.ИспользоватьОтчетностьМониторингаРаботниковСоциальнойСферы = Ложь;
		НастройкиСтатистикиПерсонала.ФормаОтчетностиМониторингаРаботниковСоциальнойСферы = ФормаОтчетностиМониторингаРаботниковСоциальнойСферыПустоеЗначение;
		
	Иначе
		
		// Восстановим прежние значения зависимых настроек.
		ЗаполнитьЗначенияСвойств(НастройкиРасчетаЗарплаты, НастройкиРасчетаЗарплатыПрежняя);
		ЗаполнитьЗначенияСвойств(НастройкиЗаймовСотрудникам, НастройкиЗаймовСотрудникамПрежняя);
		ЗаполнитьЗначенияСвойств(НастройкиСтатистикиПерсонала, НастройкиСтатистикиПерсоналаПрежняя);
		ЗаполнитьЗначенияСвойств(НастройкиУчетаВремени, НастройкиУчетаВремениПрежняя);
		НеВыполнятьРасчетЗарплатыПоПодразделениям = Не НастройкиРасчетаЗарплаты.ВыполнятьРасчетЗарплатыПоПодразделениям;
		ИспользоватьСтатьиФинансирования = ИспользоватьСтатьиФинансированияПрежняя;
		РассчитыватьДокументыПриРедактировании = РассчитыватьДокументыПриРедактированииПрежняя;
		
		Если Не ЗначениеЗаполнено(НастройкиРасчетаЗарплаты.ПорядокРасчетаСтоимостиЕдиницыВремени) Тогда
			НастройкиРасчетаЗарплаты.ПорядокРасчетаСтоимостиЕдиницыВремени = ПорядокРасчетаСтоимостиЕдиницыВремениОплатыТрудаПоСреднегодовомуЗначению;
		КонецЕсли;
		
		Если Не ЗначениеЗаполнено(НастройкиРасчетаЗарплаты.СпособРасчетаСтоимостиЕдиницыВремени) Тогда
			НастройкиРасчетаЗарплаты.СпособРасчетаСтоимостиЕдиницыВремени = СпособРасчетаСтоимостиЕдиницыВремениОплатыТрудаПоСовокупнойТарифнойСтавке;
		КонецЕсли;

		
	КонецЕсли;
	
	ЗаполнитьЗначенияСвойств(ЭтаФорма, НастройкиРасчетаЗарплаты, СтрокаСвойствНастройкиРасчетаЗарплаты);
	
	УчетПоСтатьямФинансированияЗарплатаКлиент.УстановитьЗначенияЗависимыхНастроекРасчетаЗарплаты(ЭтаФорма, ИспользоватьНачислениеЗарплаты);
	
КонецПроцедуры

&НаКлиенте
Процедура ПодключитьОбработчикОжиданияОбновленияИнтерфейса() Экспорт
	
	ТребуетсяОбновлениеИнтерфейса = Истина;
	
	#Если НЕ ВебКлиент Тогда
		ПодключитьОбработчикОжидания("ОбработчикОжиданияОбновленияИнтерфейса", 1, Истина);
	#КонецЕсли 
	
КонецПроцедуры

&НаКлиенте 
Процедура ОбработчикОжиданияОбновленияИнтерфейса()
	
	ОбновитьИнтерфейс();
	
	ЭтаФорма.ТребуетсяОбновлениеИнтерфейса = Ложь;
	
КонецПроцедуры

&НаКлиенте
Процедура РедактированиеПрочихНастроекРасчетаЗарплаты(Команда)

	ПараметрыФормы = Новый Структура("ПрочиеНастройки", Истина);
	
	ОткрытьФорму(
		"Обработка.ПанельНастроекЗарплатаКадры.Форма.РасчетЗарплаты",
		ПараметрыФормы,
		ЭтаФорма,
		ЭтаФорма,
		,
		,
		,
		РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры


#КонецОбласти

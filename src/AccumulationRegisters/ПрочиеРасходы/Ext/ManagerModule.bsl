﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс
//++ НЕ УТКА

// Определяет источники уточнения счета, доступные в регистре и их свойства.
// Подробнее см. ОбщийМодуль.МеждународныйУчетСерверПовтИсп.ИсточникиУточненияСчета().
//
// Параметры:
//  СвойстваИсточника - Строка - "ИмяПоля" - имя атрибута регистра накопления, из которого планируется получать источник
//                               уточнения счета.
//
// Возвращаемое значение:
//  Соответствие - Ключ - название источника уточнения счета. 
//                 Значение - структура свойств источника уточнения счета.
//
Функция ИсточникиУточненияСчета(СвойстваИсточника) Экспорт
	
	ИсточникиУточненияСчета = Новый Соответствие;
	
	ИсточникиУточненияСчета.Вставить(Перечисления.ТипыИсточниковУточненияСчета.ГруппаФинансовогоУчетаДоходовРасходов,
		Новый Структура(СвойстваИсточника, "ГФУДоходовРасходов"));
		
	Возврат ИсточникиУточненияСчета;
	
КонецФункции

// Определяет источники подразделений регистра и их свойства.
// Подробнее см. ОбщийМодуль.МеждународныйУчетСерверПовтИсп.ИсточникиПодразделений().
//
// Возвращаемое значение:
//  Соответствие - Ключ - имя источника. 
//                 Значение - структура свойств источника. 
//
Функция ИсточникиПодразделений() Экспорт

	ИсточникиПодразделений = Новый Соответствие;
	
	ИсточникиПодразделений.Вставить(Перечисления.ИсточникиПодразделенийАналитическихРегистров.ХозяйственнаяОперация, "Подразделение");

	Возврат ИсточникиПодразделений;

КонецФункции

// Определяет источники направлений регистра и их свойства.
// Подробнее см. ОбщийМодуль.МеждународныйУчетСерверПовтИсп.ИсточникиНаправлений().
//
// Возвращаемое значение:
//  Соответствие - Ключ - имя источника. 
//                 Значение - структура свойств источника. 
//
Функция ИсточникиНаправлений() Экспорт

	Результат = Новый Соответствие;
	
	ИсточникиНаправлений = Перечисления.ИсточникиНаправленийДеятельностиАналитическихРегистров;
	Результат.Вставить(ИсточникиНаправлений.НаправлениеДеятельности, "НаправлениеДеятельности");

	Возврат Результат;

КонецФункции

// Определяет источники заполнения субконто.
// Подробнее см. ОбщийМодуль.МеждународныйУчетСерверПовтИсп.ИсточникиСубконто().
//
// Возвращаемое значение:
//  Массив - массив атрибутов регистра.
//
Функция ИсточникиСубконто() Экспорт

	МассивСубконто = Новый Массив;
	МассивСубконто.Добавить("СтатьяРасходов");
	МассивСубконто.Добавить("АналитикаРасходов");

	Возврат Новый Структура("СубконтоДт, СубконтоКт", МассивСубконто, МассивСубконто);

КонецФункции

// Определяет показатели в валюте регистра.
// Подробнее см. ОбщийМодуль.МеждународныйУчетСерверПовтИсп.ПоказателиВВалюте().
//
// Параметры:
//  СвойстваПоказателей - Строка - "ИсточникВалюты" - источник валюты для показателя регистра.
//
// Возвращаемое значение:
//  Соответствие - Ключ - имя показателя.
//                 Значение - структура свойств показателя.
//
Функция ПоказателиВВалюте(СвойстваПоказателей) Экспорт

	ПоказателиВВалюте = Новый Соответствие;
	Возврат ПоказателиВВалюте;

КонецФункции

// Определяет документы отражаемые в международном учете.
// Подробнее см. ОбщийМодуль.МеждународныйУчетСерверПовтИсп.ДокументыКОтражениюВМФУ().
//
// Возвращаемое значение:
//  Массив - массив регистраторов регистра отражаемых в международном учете.
//
Функция ДокументыКОтражениюВМеждународномУчете() Экспорт

	ДокументыКОтражению = Новый Массив;
	ДокументыКОтражению.Добавить("ВводОстатковВнеоборотныхАктивов2_4");
	
	Возврат ДокументыКОтражению;

КонецФункции
//-- НЕ УТКА

// Определяет показатели регистра.
// Подробнее см. ОбщийМодуль.МеждународныйУчетСерверПовтИсп.Показатели().
//
// Параметры:
//  Свойства - Структура - содержащая ключи СвойстваПоказателей, СвойстваРесурсов.
//
// Возвращаемое значение:
//  Соответствие - Ключ - имя показателя.
//                 Значение - структура свойств показателя.
//
Функция Показатели(Свойства) Экспорт

	Показатели = Новый Соответствие;
	
	СвойстваПоказателей = Свойства.СвойстваПоказателей;
	СвойстваРесурсов = Свойства.СвойстваРесурсов;
	
	МассивРесурсов = Новый Массив;
    МассивРесурсов.Добавить(Новый Структура(СвойстваРесурсов, "Сумма", "ВалютаУпр"));
	МассивРесурсов.Добавить(Новый Структура(СвойстваРесурсов, "СуммаРегл", "ВалютаРегл"));
	Показатели.Вставить(Перечисления.ПоказателиАналитическихРегистров.Сумма, Новый Структура(СвойстваПоказателей, МассивРесурсов));
	
	МассивРесурсов = Новый Массив;
	МассивРесурсов.Добавить(Новый Структура(СвойстваРесурсов, "Сумма", "ВалютаУпр"));
	Показатели.Вставить(Перечисления.ПоказателиАналитическихРегистров.СуммаУпрСНДС, Новый Структура(СвойстваПоказателей, МассивРесурсов));
	
	Возврат Показатели;
	
КонецФункции

// Возвращает текст запроса с типовой структурой временной таблицы "ВтИсходныеПрочиеРасходы".
//
// Параметры:
//  ДополнительныеПоля	 - Строка	 - Список дополнительный полей.
// 
// Возвращаемое значение:
//  Строка - Текст запроса формирования временной таблицы ВтИсходныеПрочиеРасходы.
//
Функция ТекстОписаниеВтИсходныеПрочиеРасходы(ДополнительныеПоля = "") Экспорт
	
	ТекстЗапроса = "
	|ВЫБРАТЬ ПЕРВЫЕ 0
	|	Строки.Период,
	|	Строки.ВидДвижения,
	|	Строки.Организация,
	|	Строки.Подразделение,
	|	Строки.СтатьяРасходов,
	|	Строки.АналитикаРасходов,
	|	Строки.НаправлениеДеятельности,
	|	ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПустаяСсылка) КАК ВидДеятельностиНДС,
	|	0 КАК СуммаСНДС,
	|	0 КАК СуммаБезНДС,
	|	0 КАК СуммаБезНДСУпр,
	|	0 КАК СуммаСНДСРегл,
	|	0 КАК СуммаБезНДСРегл,
	|	0 КАК ПостояннаяРазница,
	|	0 КАК ВременнаяРазница,
	|	Строки.ХозяйственнаяОперация,
	|	Строки.АналитикаУчетаНоменклатуры
	|
	|//ДополнительныеПоля
	|
	|ПОМЕСТИТЬ ВтИсходныеПрочиеРасходы
	|ИЗ
	|	РегистрНакопления.ПрочиеРасходы КАК Строки
	|";
	
	ДобавитьДополнительныеПоляВТекстЗапроса(ДополнительныеПоля, ТекстЗапроса);
	
	Возврат ТекстЗапроса
	
КонецФункции

// Формирует текст запроса для формирования временной таблицы "ВтПрочиеРасходы".
//
// Параметры:
//  ДополнительныеПоля	 - Строка	 - Список дополнительный полей.
// 
// Возвращаемое значение:
//  Строка - Текст запроса формирования временной таблицы ВтПрочиеРасходы.
//
Функция ТекстЗапросаТаблицаВтПрочиеРасходы(ДополнительныеПоля = "") Экспорт
	
	// Условия для заполнения ресурсов регистра аналогичны условиям в функции ТекстСписанияРасходовНаВыбытиеТоваровРегистрПрочиеРасходы() общего модуля ПартионныйУчет22
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	Строки.Период КАК Период,
	|	Строки.ВидДвижения КАК ВидДвижения,
	|	Строки.Организация КАК Организация,
	|	Строки.Подразделение КАК Подразделение,
	|	Строки.СтатьяРасходов КАК СтатьяРасходов,
	|	Строки.АналитикаРасходов КАК АналитикаРасходов,
	|	Строки.НаправлениеДеятельности КАК НаправлениеДеятельности,
	|
	|	(ВЫБОР
	|		КОГДА Статья.ВариантРаспределенияРасходовУпр = ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаСебестоимостьТоваров)
	|			ТОГДА 0
	|		ИНАЧЕ Строки.СуммаСНДС КОНЕЦ) КАК Сумма,
	|	(ВЫБОР
	|		КОГДА Статья.ВариантРаспределенияРасходовУпр = ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаСебестоимостьТоваров)
	|			ТОГДА 0
	|		КОГДА Статья.ВариантРаспределенияРасходовУпр = ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаПроизводственныеЗатраты)
	|			ТОГДА Строки.СуммаБезНДС
	|		ИНАЧЕ 0 КОНЕЦ) КАК СуммаБезНДС,
	|	(ВЫБОР
	|		КОГДА Статья.ВариантРаспределенияРасходовУпр = ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаСебестоимостьТоваров)
	|		 ИЛИ НЕ &УправленческийУчетОрганизаций И НЕ &ЭтоВводОстатковВНА_2_4
	|		 ИЛИ Строки.СуммаБезНДСУпр = 0
	|			ТОГДА 0
	|		КОГДА Строки.ВидДеятельностиНДС В (
	|					ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПродажаНеОблагаетсяНДС),
	|					ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПродажаОблагаетсяЕНВД))
	|				И Статья.ВариантРаздельногоУчетаНДС = ЗНАЧЕНИЕ(Перечисление.ВариантыРаздельногоУчетаНДС.ИзДокумента)
	|			ТОГДА Строки.СуммаБезНДСУпр + (Строки.СуммаСНДС - Строки.СуммаБезНДС)
	|		ИНАЧЕ Строки.СуммаБезНДСУпр КОНЕЦ) КАК СуммаУпр,
	|
	|	(ВЫБОР 
	|		КОГДА НЕ &ИспользоватьУчетПрочихДоходовРасходовРегл ТОГДА 0
	|		КОГДА Статья.ВариантРаспределенияРасходовРегл = ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаСебестоимостьТоваров)
	|			ТОГДА 0
	|		КОГДА Статья.ВариантРаспределенияРасходовРегл = ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаПрочиеАктивы)
	|			ТОГДА 0
	|		КОГДА Статья.ВариантРаспределенияРасходовРегл = ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаВнеоборотныеАктивы)
	|				И НЕ &ИспользуетсяУправлениеВНА_2_4
	|			ТОГДА 0
	|		КОГДА Строки.ВидДеятельностиНДС В (
	|					ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПродажаНеОблагаетсяНДС),
	|					ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПродажаОблагаетсяЕНВД))
	|				И Статья.ВариантРаздельногоУчетаНДС = ЗНАЧЕНИЕ(Перечисление.ВариантыРаздельногоУчетаНДС.ИзДокумента)
	|			ТОГДА Строки.СуммаСНДСРегл
	|		ИНАЧЕ Строки.СуммаБезНДСРегл КОНЕЦ) КАК СуммаРегл,
	|
	|	(ВЫБОР
	|		КОГДА НЕ &ИспользоватьУчетПрочихДоходовРасходовРегл ТОГДА 0
	|		КОГДА Статья.ВариантРаспределенияРасходовРегл = ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаСебестоимостьТоваров)
	|			ТОГДА 0
	|		КОГДА Статья.ВариантРаспределенияРасходовРегл = ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаПрочиеАктивы)
	|			ТОГДА 0
	|		КОГДА Статья.ВариантРаспределенияРасходовРегл = ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаВнеоборотныеАктивы)
	|				И НЕ &ИспользуетсяУправлениеВНА_2_4
	|			ТОГДА 0
	|		КОГДА НЕ Статья.ПринятиеКналоговомуУчету И (Строки.СуммаСНДСРегл <> 0 ИЛИ Строки.ВременнаяРазница <> 0) ТОГДА
	|			(ВЫБОР КОГДА Строки.ВидДеятельностиНДС В (
	|				ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПродажаНеОблагаетсяНДС),
	|				ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПродажаОблагаетсяЕНВД))
	|				И Статья.ВариантРаздельногоУчетаНДС = ЗНАЧЕНИЕ(Перечисление.ВариантыРаздельногоУчетаНДС.ИзДокумента)
	|			ТОГДА Строки.СуммаСНДСРегл
	|			ИНАЧЕ Строки.СуммаБезНДСРегл КОНЕЦ)
	|			- Строки.ВременнаяРазница
	|		ИНАЧЕ Строки.ПостояннаяРазница КОНЕЦ) КАК ПостояннаяРазница,
	|	(ВЫБОР
	|		КОГДА НЕ &ИспользоватьУчетПрочихДоходовРасходовРегл ТОГДА 0
	|		КОГДА Статья.ВариантРаспределенияРасходовРегл = ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаСебестоимостьТоваров)
	|			ТОГДА 0
	|		ИНАЧЕ Строки.ВременнаяРазница КОНЕЦ) КАК ВременнаяРазница,
	|	Строки.ХозяйственнаяОперация,
	|	Строки.АналитикаУчетаНоменклатуры,
	|
	|	ВЫБОР
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Справочник.Организации)
	|		 И Строки.АналитикаРасходов <> ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)
	|			ТОГДА Строки.АналитикаРасходов
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Документ.АктВыполненныхРабот)
	|			ТОГДА ВЫРАЗИТЬ(Строки.АналитикаРасходов КАК Документ.АктВыполненныхРабот).Организация
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Документ.ВводОстатков)
	|			ТОГДА ВЫРАЗИТЬ(Строки.АналитикаРасходов КАК Документ.ВводОстатков).Организация
	//++ НЕ УТКА
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Документ.ЗаказДавальца)
	|			ТОГДА ВЫРАЗИТЬ(Строки.АналитикаРасходов КАК Документ.ЗаказДавальца).Организация
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Документ.ЗаказНаПроизводство)
	|			ТОГДА ВЫРАЗИТЬ(Строки.АналитикаРасходов КАК Документ.ЗаказНаПроизводство).Организация
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Документ.ЗаказНаПроизводство2_2)
	|			ТОГДА ВЫРАЗИТЬ(Строки.АналитикаРасходов КАК Документ.ЗаказНаПроизводство2_2).Организация
	//-- НЕ УТКА
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Документ.ЗаказКлиента)
	|			ТОГДА ВЫРАЗИТЬ(Строки.АналитикаРасходов КАК Документ.ЗаказКлиента).Организация
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Документ.ЗаказНаПеремещение)
	|			ТОГДА ВЫРАЗИТЬ(Строки.АналитикаРасходов КАК Документ.ЗаказНаПеремещение).Организация
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Документ.ЗаказНаСборку)
	|			ТОГДА ВЫРАЗИТЬ(Строки.АналитикаРасходов КАК Документ.ЗаказНаСборку).Организация
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Документ.ЗаказПоставщику)
	|			ТОГДА ВЫРАЗИТЬ(Строки.АналитикаРасходов КАК Документ.ЗаказПоставщику).Организация
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Документ.ЗаявкаНаВозвратТоваровОтКлиента)
	|			ТОГДА ВЫРАЗИТЬ(Строки.АналитикаРасходов КАК Документ.ЗаявкаНаВозвратТоваровОтКлиента).Организация
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Документ.ПередачаТоваровМеждуОрганизациями)
	|			ТОГДА ВЫРАЗИТЬ(Строки.АналитикаРасходов КАК Документ.ПередачаТоваровМеждуОрганизациями).ОрганизацияПолучатель
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Документ.ПеремещениеТоваров)
	|			ТОГДА ВЫРАЗИТЬ(Строки.АналитикаРасходов КАК Документ.ПеремещениеТоваров).Организация
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Документ.ПриобретениеТоваровУслуг)
	|			ТОГДА ВЫРАЗИТЬ(Строки.АналитикаРасходов КАК Документ.ПриобретениеТоваровУслуг).Организация
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Документ.РеализацияТоваровУслуг)
	|			ТОГДА ВЫРАЗИТЬ(Строки.АналитикаРасходов КАК Документ.РеализацияТоваровУслуг).Организация
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Документ.РеализацияУслугПрочихАктивов)
	|			ТОГДА ВЫРАЗИТЬ(Строки.АналитикаРасходов КАК Документ.РеализацияУслугПрочихАктивов).Организация
	|		КОГДА ТИПЗНАЧЕНИЯ(Строки.АналитикаРасходов) = ТИП(Документ.СборкаТоваров)
	|			ТОГДА ВЫРАЗИТЬ(Строки.АналитикаРасходов КАК Документ.СборкаТоваров).Организация
	|		ИНАЧЕ Строки.Организация
	|	КОНЕЦ КАК ОрганизацияПолучатель
	|
	|//ДополнительныеПоля
	|
	|ПОМЕСТИТЬ ВтПрочиеРасходы
	|ИЗ
	|	ВтИсходныеПрочиеРасходы КАК Строки
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		ПланВидовХарактеристик.СтатьиРасходов КАК Статья
	|	ПО
	|		Статья.Ссылка = Строки.СтатьяРасходов
	|ГДЕ
	|	(Статья.ВариантРаспределенияРасходовУпр <> ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаСебестоимостьТоваров)
	|		ИЛИ Статья.ВариантРаспределенияРасходовРегл <> ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаСебестоимостьТоваров))
	|	И &ИспользоватьУчетПрочихДоходовРасходов
	|";
	
	ДобавитьДополнительныеПоляВТекстЗапроса(ДополнительныеПоля, ТекстЗапроса);
	
	Возврат ТекстЗапроса;
	
КонецФункции

// Формирует текст запроса для формирования движений по регистру "Прочие расходы".
//
// Параметры:
//  ДополнительныеПоля	 - Строка	 - Список дополнительный полей.
// 
// Возвращаемое значение:
//  Строка - Текст запроса формирования движений в регистре ПрочиеРасходы.
//
Функция ТекстЗапросаТаблицаПрочиеРасходы(ДополнительныеПоля = "") Экспорт
	
	ТекстЗапроса = "
	// Формирование таблицы для записи в регистр "ПрочиеРасходы".
	|ВЫБРАТЬ
	|	Строки.Период КАК Период,
	|	Строки.ВидДвижения КАК ВидДвижения,
	|	Строки.Организация КАК Организация,
	|	Строки.Подразделение КАК Подразделение,
	|	Строки.СтатьяРасходов КАК СтатьяРасходов,
	|	Строки.АналитикаРасходов КАК АналитикаРасходов,
	|	Строки.НаправлениеДеятельности КАК НаправлениеДеятельности,
	|
	|	Строки.Сумма КАК Сумма,
	|	Строки.СуммаБезНДС КАК СуммаБезНДС,
	|	Строки.СуммаУпр КАК СуммаУпр,
	|
	|	Строки.СуммаРегл КАК СуммаРегл,
	|	Строки.ПостояннаяРазница КАК ПостояннаяРазница,
	|	Строки.ВременнаяРазница КАК ВременнаяРазница,
	|
	|	Строки.ХозяйственнаяОперация,
	|	Строки.АналитикаУчетаНоменклатуры
	|
	|//ДополнительныеПоля
	|
	|ИЗ
	|	ВтПрочиеРасходы КАК Строки
	|ГДЕ
	|	(Строки.Сумма <> 0 ИЛИ Строки.СуммаБезНДС <> 0 ИЛИ Строки.СуммаУпр <> 0
	|	ИЛИ Строки.СуммаРегл <> 0 ИЛИ Строки.ПостояннаяРазница <> 0 ИЛИ Строки.ВременнаяРазница <> 0)
	|
	// Сторнирование расходов в упр. учете у организации - источника.
	|ОБЪЕДИНИТЬ ВСЕ
	|ВЫБРАТЬ
	|	Строки.Период КАК Период,
	|	Строки.ВидДвижения КАК ВидДвижения,
	|	Строки.Организация КАК Организация,
	|	Строки.Подразделение КАК Подразделение,
	|	Строки.СтатьяРасходов КАК СтатьяРасходов,
	|	Строки.АналитикаРасходов КАК АналитикаРасходов,
	|	Строки.НаправлениеДеятельности КАК НаправлениеДеятельности,
	|	-Строки.Сумма КАК Сумма,
	|	-Строки.СуммаБезНДС КАК СуммаБезНДС,
	|	-Строки.СуммаУпр КАК СуммаУпр,
	|	0 КАК СуммаРегл,
	|	0 КАК ПостояннаяРазница,
	|	0 КАК ВременнаяРазница,
	|
	|	ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.СторнированиеРасходовУУ) КАК ХозяйственнаяОперация,
	|	Строки.АналитикаУчетаНоменклатуры
	|
	|//ДополнительныеПоля
	|
	|ИЗ
	|	ВтПрочиеРасходы КАК Строки
	|ГДЕ
	|	Строки.ОрганизацияПолучатель <> Строки.Организация
	|	И Строки.ОрганизацияПолучатель <> ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)
	|	И (Строки.Сумма <> 0 ИЛИ Строки.СуммаБезНДС <> 0 ИЛИ Строки.СуммаУпр <> 0)
	|
	// Регистрация расходов в упр. учете у организации - получателя.
	|ОБЪЕДИНИТЬ ВСЕ
	|ВЫБРАТЬ
	|	Строки.Период КАК Период,
	|	Строки.ВидДвижения КАК ВидДвижения,
	|	Строки.ОрганизацияПолучатель КАК Организация,
	|	Строки.Подразделение КАК Подразделение,
	|	Строки.СтатьяРасходов КАК СтатьяРасходов,
	|	Строки.АналитикаРасходов КАК АналитикаРасходов,
	|	Строки.НаправлениеДеятельности КАК НаправлениеДеятельности,
	|	Строки.Сумма КАК Сумма,
	|	Строки.СуммаБезНДС КАК СуммаБезНДС,
	|	Строки.СуммаУпр КАК СуммаУпр,
	|	0 КАК СуммаРегл,
	|	0 КАК ПостояннаяРазница,
	|	0 КАК ВременнаяРазница,
	|
	|	ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.РегистрацияРасходовУУ) КАК ХозяйственнаяОперация,
	|	Строки.АналитикаУчетаНоменклатуры
	|
	|//ДополнительныеПоля
	|
	|ИЗ
	|	ВтПрочиеРасходы КАК Строки
	|ГДЕ
	|	Строки.ОрганизацияПолучатель <> Строки.Организация
	|	И Строки.ОрганизацияПолучатель <> ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)
	|	И (Строки.Сумма <> 0 ИЛИ Строки.СуммаБезНДС <> 0 ИЛИ Строки.СуммаУпр <> 0)
	|";
	
	ДобавитьДополнительныеПоляВТекстЗапроса(ДополнительныеПоля, ТекстЗапроса);
	
	Возврат ТекстЗапроса;
	
КонецФункции

#Область ДляВызоваИзДругихПодсистем

// СтандартныеПодсистемы.УправлениеДоступом

// См. УправлениеДоступомПереопределяемый.ПриЗаполненииСписковСОграничениемДоступа.
Процедура ПриЗаполненииОграниченияДоступа(Ограничение) Экспорт

	Ограничение.Текст =
	"РазрешитьЧтениеИзменение
	|ГДЕ
	|	ЗначениеРазрешено(Организация)
	|	И ЗначениеРазрешено(Подразделение)";

КонецПроцедуры

// Конец СтандартныеПодсистемы.УправлениеДоступом

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ДобавитьДополнительныеПоляВТекстЗапроса(ДополнительныеПоля, ТекстЗапроса)

	Если НЕ ЗначениеЗаполнено(ДополнительныеПоля) Тогда
		Возврат;
	КонецЕсли; 
	
	ТекстДополнительныеПоля = "";
	
	СписокПолей = СтрРазделить(ДополнительныеПоля, ",");
	Для каждого ИмяПоля Из СписокПолей Цикл
		ТекстДополнительныеПоля = ТекстДополнительныеПоля + "
		|	,Строки." + ИмяПоля;
	КонецЦикла; 
	
	ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "//ДополнительныеПоля", ТекстДополнительныеПоля); 

КонецПроцедуры

#Область ОбновлениеИнформационнойБазы

Процедура ЗарегистрироватьДанныеКОбработкеДляПереходаНаНовуюВерсию(Параметры) Экспорт
	
	ПолноеИмяРегистра = "РегистрНакопления.ПрочиеРасходы";
	
	СписокЗапросов = Новый Массив;
	
	ТекстЗапроса = "
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ПрочиеРасходы.Регистратор КАК Регистратор
	|ИЗ
	|	РегистрНакопления.ПрочиеРасходы КАК ПрочиеРасходы
	|ГДЕ
	|	ПрочиеРасходы.СтатьяРасходов.ВариантРаспределенияРасходовУпр
	|		= ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаНаправленияДеятельности)
	|	И ПрочиеРасходы.СуммаБезНДС <> 0
	|";
	СписокЗапросов.Добавить(ТекстЗапроса);

	
	//++ НЕ УТ
	ТекстЗапроса = 
		"ВЫБРАТЬ
		|	ПрочиеРасходы.Регистратор
		|ИЗ
		|	РегистрНакопления.ПрочиеРасходы КАК ПрочиеРасходы
		|ГДЕ
		|	ТИПЗНАЧЕНИЯ(ПрочиеРасходы.Регистратор) = ТИП(Документ.РаспределениеВозвратныхОтходов)
		|	И ПрочиеРасходы.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход)";
	СписокЗапросов.Добавить(ТекстЗапроса);
	
	ВнеоборотныеАктивыЛокализация.ДополнитьСписокЗапросовЗарегистрацииДанныхКОбработкеРегистраПрочиеРасходы(СписокЗапросов);
	//-- НЕ УТ
	
	Если СписокЗапросов.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	ТекстЗапроса = СтрСоединить(СписокЗапросов, ОбщегоНазначенияУТ.РазделительЗапросовВОбъединении());
	
	Запрос = Новый Запрос(ТекстЗапроса);
	Регистраторы = Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Регистратор");
	ОбновлениеИнформационнойБазы.ОтметитьРегистраторыКОбработке(Параметры, Регистраторы, ПолноеИмяРегистра);
	
КонецПроцедуры

Процедура ОбработатьДанныеДляПереходаНаНовуюВерсию(Параметры) Экспорт
	
	ПолноеИмяРегистра = "РегистрНакопления.ПрочиеРасходы";
	
	МетаданныеРегистра = Метаданные.РегистрыНакопления.ПрочиеРасходы;
	
	Выборка = ОбновлениеИнформационнойБазы.ВыбратьРегистраторыРегистраДляОбработки(Параметры.Очередь, Неопределено, ПолноеИмяРегистра);
	Пока Выборка.Следующий() Цикл
		
		НачатьТранзакцию();
		Попытка
			
			Блокировка = Новый БлокировкаДанных;
			
			ЭлементБлокировки = Блокировка.Добавить("РегистрНакопления.ПрочиеРасходы.НаборЗаписей");
			ЭлементБлокировки.Режим = РежимБлокировкиДанных.Разделяемый;
			ЭлементБлокировки.УстановитьЗначение("Регистратор", Выборка.Регистратор);
			
			Блокировка.Заблокировать();
						
			Запрос = Новый Запрос("
			|ВЫБРАТЬ
			|	ПрочиеРасходы.Период,
			|	ПрочиеРасходы.Регистратор,
			|	ПрочиеРасходы.ВидДвижения,
			// Измерения
			|	ПрочиеРасходы.Организация,
			|	ПрочиеРасходы.Подразделение,
			|	ПрочиеРасходы.НаправлениеДеятельности,
			|	ПрочиеРасходы.СтатьяРасходов,
			|	ПрочиеРасходы.АналитикаРасходов,
			|	ПрочиеРасходы.АналитикаРасходов,
			// Ресурсы
			|	ПрочиеРасходы.Сумма,
			|	(ВЫБОР
			|		КОГДА ПрочиеРасходы.СтатьяРасходов.ВариантРаспределенияРасходовУпр
			|		 = ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаНаправленияДеятельности)
			|			ТОГДА 0
			|		ИНАЧЕ ПрочиеРасходы.СуммаБезНДС КОНЕЦ) КАК СуммаБезНДС,
			|	ПрочиеРасходы.СуммаРегл,
			|	ПрочиеРасходы.ПостояннаяРазница,
			|	ПрочиеРасходы.ВременнаяРазница,
			|	ПрочиеРасходы.СуммаУпр,
			// Реквизиты
			|	ПрочиеРасходы.ХозяйственнаяОперация,
			|	ПрочиеРасходы.АналитикаУчетаНоменклатуры,
			|	ПрочиеРасходы.СчетКт,
			|	ПрочиеРасходы.СуммаПрочихАктивов,
			|	ПрочиеРасходы.РегистрацияВНалоговомОргане,
			|	ПрочиеРасходы.ДокументДвижения,
			|	ПрочиеРасходы.КорСтатьяРасходов,
			|	ПрочиеРасходы.КорОрганизация,
			|	ПрочиеРасходы.ДоляСтоимости,
			|	ПрочиеРасходы.КорПодразделение,
			|	ПрочиеРасходы.КорАналитикаРасходов,
			|	ПрочиеРасходы.ГруппаПродукции,
			|	ПрочиеРасходы.КорСтатьяКалькуляции,
			|	ПрочиеРасходы.РасчетСебестоимости,
			|	ПрочиеРасходы.РасчетПартий,
			|	ПрочиеРасходы.ИдентификаторСтроки,
			|	ПрочиеРасходы.КорНаправлениеДеятельности,
			|	ПрочиеРасходы.УдалитьИсточникДвижений,
			|	ПрочиеРасходы.КорОбъектУчетаРезервов,
			|	ПрочиеРасходы.КорВидРезервов,
			|	ПрочиеРасходы.ВидИсточника
			|ИЗ
			|	РегистрНакопления.ПрочиеРасходы КАК ПрочиеРасходы
			|ГДЕ
			|	ПрочиеРасходы.Регистратор = &Регистратор
			|");
			Запрос.УстановитьПараметр("Регистратор", Выборка.Регистратор);
			РезультатЗапроса = Запрос.Выполнить();
			
			НаборЗаписей = РегистрыНакопления.ПрочиеРасходы.СоздатьНаборЗаписей();
			НаборЗаписей.Отбор.Регистратор.Установить(Выборка.Регистратор);
			Если РезультатЗапроса.Пустой() Тогда
				НаборЗаписей.Прочитать();
			Иначе
				НаборЗаписей.Загрузить(РезультатЗапроса.Выгрузить());
			КонецЕсли;
			
			//++ НЕ УТ
			Если ТипЗнч(Выборка.Регистратор) = Тип("ДокументСсылка.РаспределениеВозвратныхОтходов") Тогда
				Для Каждого Запись Из НаборЗаписей Цикл
					
					Запись.ВидДвижения = ВидДвиженияНакопления.Приход;
					Запись.Сумма = -Запись.Сумма;
					Запись.СуммаБезНДС = -Запись.СуммаБезНДС;
					Запись.СуммаРегл = -Запись.СуммаРегл;
					Запись.ПостояннаяРазница = -Запись.ПостояннаяРазница;
					Запись.ВременнаяРазница = -Запись.ВременнаяРазница;
					Запись.СуммаУпр = -Запись.СуммаУпр;
					
				КонецЦикла;
			КонецЕсли;
			
			ВнеоборотныеАктивыЛокализация.ОбработатьДанныеРегистраПрочиеРасходыДляПереходаНаНовуюВерсию(Выборка.Регистратор, НаборЗаписей);
			//-- НЕ УТ
				
			Если НаборЗаписей.Модифицированность() Тогда
				ОбновлениеИнформационнойБазы.ЗаписатьНаборЗаписей(НаборЗаписей);
			Иначе
				ОбновлениеИнформационнойБазы.ОтметитьВыполнениеОбработки(НаборЗаписей);
			КонецЕсли;
			
			ЗафиксироватьТранзакцию();
		Исключение
			
			ОтменитьТранзакцию();
			
			ТекстСообщения = НСтр("ru = 'Не удалось обработать движения по регистру ""Прочие расходы"" документа ""%1"" по причине: %2';
									|en = 'Cannot process records for the ""Other expenses"" register of the ""%1"" document due to: %2'");
			ТекстСообщения = СтрШаблон(ТекстСообщения, Выборка.Регистратор, ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
			ЗаписьЖурналаРегистрации(
				ОбновлениеИнформационнойБазы.СобытиеЖурналаРегистрации(),
				УровеньЖурналаРегистрации.Ошибка,
				МетаданныеРегистра,
				Выборка.Регистратор,
				ТекстСообщения);
			
		КонецПопытки;
		
	КонецЦикла;
	
	Параметры.ОбработкаЗавершена = НЕ ОбновлениеИнформационнойБазы.ЕстьДанныеДляОбработки(Параметры.Очередь, ПолноеИмяРегистра);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
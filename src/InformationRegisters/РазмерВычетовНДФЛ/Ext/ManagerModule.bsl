﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

Процедура ПередЗаписью(Отказ, Замещение)
		
	//ДополнительныеСвойства;  	
		
КонецПроцедуры

// Процедура выполняет первоначальное заполнение сведений.
Процедура НачальноеЗаполнение() Экспорт
	
	ЗаполнитьРазмерыВычетовНДФЛРБ();

КонецПроцедуры

// Выполняет заполнение регистра сведений РазмерыВычетовНДФЛ.
//
Процедура ЗаполнитьРазмерыВычетовНДФЛРБ() Экспорт
	
	ВычетыНДФЛ = Справочники.ВидыВычетовНДФЛ;
	
	НаборЗаписей = РегистрыСведений.РазмерВычетовНДФЛ.СоздатьНаборЗаписей();
	НаборЗаписей.ДополнительныеСвойства.Вставить("ЗаписьОбщихДанных");
	
	ДатаСведений = '20150101';
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код600,  73, 442);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код610,  21, 0);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код611, 41, 0);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код620, 1030, 0);
		
	
	// Запишем набор записей в режиме добавления.
	НаборЗаписей.Записать();
	НаборЗаписей.Очистить();
	
	ДатаСведений = '20160101';
		
	НаборЗаписей = РегистрыСведений.РазмерВычетовНДФЛ.СоздатьНаборЗаписей();
	НаборЗаписей.ДополнительныеСвойства.Вставить("ЗаписьОбщихДанных");
	НаборЗаписей.Отбор.Период.Установить(ДатаСведений);
	
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код600,  83, 501);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код610,  24, 0);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код611, 46, 0);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код620, 117, 0);
	
	НаборЗаписей.Записать();
	НаборЗаписей.Очистить();
	
	
	ДатаСведений = '20170101';
		
	НаборЗаписей = РегистрыСведений.РазмерВычетовНДФЛ.СоздатьНаборЗаписей();
	НаборЗаписей.ДополнительныеСвойства.Вставить("ЗаписьОбщихДанных");
	НаборЗаписей.Отбор.Период.Установить(ДатаСведений);
	
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код600,  93, 530);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код610,  27, 0);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код611, 52, 0);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код620, 131, 0);
	
	НаборЗаписей.Записать();
	НаборЗаписей.Очистить();
	
	ДатаСведений = '20180101';
		
	НаборЗаписей = РегистрыСведений.РазмерВычетовНДФЛ.СоздатьНаборЗаписей();
	НаборЗаписей.ДополнительныеСвойства.Вставить("ЗаписьОбщихДанных");
	НаборЗаписей.Отбор.Период.Установить(ДатаСведений);
	
		
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код600,  102, 620);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код610,  30, 0);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код611, 57, 0);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код620, 144, 0);
	
	НаборЗаписей.Записать();
	НаборЗаписей.Очистить();
	
	ДатаСведений = '20200101';
		
	НаборЗаписей = РегистрыСведений.РазмерВычетовНДФЛ.СоздатьНаборЗаписей();
	НаборЗаписей.ДополнительныеСвойства.Вставить("ЗаписьОбщихДанных");
	НаборЗаписей.Отбор.Период.Установить(ДатаСведений);
	
		
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код600,  117, 709);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код610,  34, 0);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код611, 65, 0);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код620, 165, 0);
	
	НаборЗаписей.Записать();
	НаборЗаписей.Очистить();


	

КонецПроцедуры  


// Выполняет заполнение регистра сведений РазмерыВычетовНДФЛ.
//
Процедура ЗаполнитьРазмерыВычетовНДФЛ()
	
	ВычетыНДФЛ = Справочники.ВидыВычетовНДФЛ;
	
	НаборЗаписей = РегистрыСведений.РазмерВычетовНДФЛ.СоздатьНаборЗаписей();
	НаборЗаписей.ДополнительныеСвойства.Вставить("ЗаписьОбщихДанных");
	
	ДатаСведений = '20100101';
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код103,  400, 40000);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код104,  500, 0);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код105, 3000, 0);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код108, 1000, 280000);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код109, 2000, 280000);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код110, 2000, 280000);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код111, 2000, 280000);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код112, 4000, 280000);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код113, 4000, 280000);
	ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, ВычетыНДФЛ.Код311, 2000000, 0);
	
	// Запишем набор записей в режиме добавления.
	НаборЗаписей.Записать();

КонецПроцедуры  


// Добавляет запись в набор записей регистра "НДФЛРазмерВычетов".
//
Процедура ДобавитьРазмерВычетаНДФЛ(НаборЗаписей, ДатаСведений, КодВычета, Размер, ОграничениеПоДоходам) 

	НоваяЗапись = НаборЗаписей.Добавить();

	НоваяЗапись.КодВычета            = КодВычета;
	НоваяЗапись.Период               = ДатаСведений;
	НоваяЗапись.Размер               = Размер;
	НоваяЗапись.ОграничениеПоДоходам = ОграничениеПоДоходам;

КонецПроцедуры

#КонецОбласти

#КонецЕсли
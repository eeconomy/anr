﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;

	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Запись, ЭтотОбъект);
	
	Если Параметры.Свойство("ВозвратЗначенияБезЗаписи") Тогда
		ВозвратЗначенияБезЗаписи = Параметры.ВозвратЗначенияБезЗаписи;
	КонецЕсли;
	
	Если ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоОрганизаций") Тогда
		ДоступныеДляВыбораОрганизации = УправлениеДоступом.РазрешенныеЗначенияДляДинамическогоСписка(
			"РегистрСведений.ПорядокОтраженияТМЦВЭксплуатации", Тип("СправочникСсылка.Организации"));
		Если ДоступныеДляВыбораОрганизации <> Неопределено Тогда
			Элементы.Организация.СписокВыбора.ЗагрузитьЗначения(ДоступныеДляВыбораОрганизации);
			Элементы.Организация.РежимВыбораИзСписка = Истина;
			Если ДоступныеДляВыбораОрганизации.Количество() = 1 Тогда
				Запись.Организация = ДоступныеДляВыбораОрганизации.Получить(0);
				Элементы.Организация.ТолькоПросмотр = Истина;
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	
	ЗаполнитьДоступныеСчета();
	Элементы.ФормаЗаписать.Видимость = Не ВозвратЗначенияБезЗаписи;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	
	Если Не ЗначениеЗаполнено(Запись.Организация) Тогда
		Текст = НСтр("ru = 'Поле ""Организация"" не заполнено';
					|en = 'Company is not filled in'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(Текст, Параметры.Ключ, "Запись.Организация", "Запись.Организация", Отказ);
		Возврат;
	КонецЕсли;
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
		
	Если ВозвратЗначенияБезЗаписи Тогда
		
		Отказ = Истина;
		Модифицированность = Ложь;
		СтруктураВозврата = Новый Структура("Организация, КатегорияЭксплуатации, СчетУчета, СчетЗабалансовогоУчета");
		ЗаполнитьЗначенияСвойств(СтруктураВозврата, Запись);
		Закрыть(СтруктураВозврата);
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)

	// СтандартныеПодсистемы.УправлениеДоступом
	Если ОбщегоНазначения.ПодсистемаСуществует("СтандартныеПодсистемы.УправлениеДоступом") Тогда
		МодульУправлениеДоступом = ОбщегоНазначения.ОбщийМодуль("УправлениеДоступом");
		МодульУправлениеДоступом.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	КонецЕсли;
	// Конец СтандартныеПодсистемы.УправлениеДоступом

КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)

	// СтандартныеПодсистемы.УправлениеДоступом
	УправлениеДоступом.ПослеЗаписиНаСервере(ЭтотОбъект, ТекущийОбъект, ПараметрыЗаписи);
	// Конец СтандартныеПодсистемы.УправлениеДоступом

КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Прочее

&НаСервере
Процедура ЗаполнитьДоступныеСчета()
	
	СтруктураСчетовУчета = Обработки.НастройкаОтраженияДокументовВРеглУчете.ДоступныеСчетаУчетаТМЦВЭксплуатации();
	
	// Счета учета в эксплуатации
	МассивПараметров = Новый Массив;
	МассивПараметров.Добавить(Новый ПараметрВыбора("Отбор.Ссылка", Новый ФиксированныйМассив(СтруктураСчетовУчета.СчетаУчета)));
	Элементы.СчетУчета.ПараметрыВыбора = Новый ФиксированныйМассив(МассивПараметров);
	
	// Счета забалансового учета в эксплуатации
	МассивПараметров.Очистить();
	МассивПараметров.Добавить(Новый ПараметрВыбора("Отбор.Ссылка", Новый ФиксированныйМассив(СтруктураСчетовУчета.СчетаЗабалансовогоУчета)));
	Элементы.СчетЗабалансовогоУчета.ПараметрыВыбора = Новый ФиксированныйМассив(МассивПараметров);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

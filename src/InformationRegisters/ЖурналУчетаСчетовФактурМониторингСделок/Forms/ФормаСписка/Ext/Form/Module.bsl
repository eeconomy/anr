﻿
&НаКлиенте
Процедура ЗагрузитьДанныеМониторингаСделок(Команда)
	
	ДиалогВыбораФайла = Новый ДиалогВыбораФайла(РежимДиалогаВыбораФайла.Открытие);
    ДиалогВыбораФайла.Фильтр = НСтр("ru = 'Файлы данных мониторинга сделок (*.csv)'") + "|*.csv";
    ДиалогВыбораФайла.Заголовок = НСтр("ru = 'Выберите файлы данных мониторинга сделок'");
 	ДиалогВыбораФайла.МножественныйВыбор = Ложь;
	
   	ПараметрыЗагрузки = ФайловаяСистемаКлиент.ПараметрыЗагрузкиФайла();
    ПараметрыЗагрузки.ИдентификаторФормы = УникальныйИдентификатор;
    ПараметрыЗагрузки.Интерактивно = Истина;
    ПараметрыЗагрузки.Диалог = ДиалогВыбораФайла;
		 
	Оповещение = Новый ОписаниеОповещения("ЗавершениеВыбораФайла", ЭтотОбъект);
   	ФайловаяСистемаКлиент.ЗагрузитьФайлы(Оповещение, ПараметрыЗагрузки);
   	
КонецПроцедуры

&НаКлиенте
Процедура ЗавершениеВыбораФайла(РезультатПомещенияФайлов, ДополнительныеПараметры) Экспорт 
	Если РезультатПомещенияФайлов <> Неопределено Тогда
		ЗагрузитьФайлНаСервере(РезультатПомещенияФайлов);
	КонецЕсли;	
КонецПроцедуры	

&НаСервере
Процедура ЗагрузитьФайлНаСервере(РезультатПомещенияФайлов)

	Используемый_разделитель = ";";

	Для Каждого ТекущийФайл Из РезультатПомещенияФайлов Цикл

		ДвоичныеДанные = ПолучитьИзВременногоХранилища(ТекущийФайл.Хранение);
		ДвоичныйПоток = ДвоичныеДанные.ОткрытьПотокДляЧтения();

		ФайлДанных = Новый ЧтениеТекста(ДвоичныйПоток);
		ТекущаяСтрока = ФайлДанных.ПрочитатьСтроку();
			
		Организация = Неопределено;
		
		Пока ТекущаяСтрока <> Неопределено Цикл
			ТекущаяСтрока = ФайлДанных.ПрочитатьСтроку();
			ТекущаяСтрокаМассив = СтрРазделить(ТекущаяСтрока, Используемый_разделитель);

			Если ТекущаяСтрокаМассив.Количество() < 45 Тогда
				Продолжить;
			КонецЕсли;
			
			Попытка 
				Сумма = Число(ТекущаяСтрокаМассив[45]);
			Исключение
				Продолжить;
			КонецПопытки;	
			
			Если Организация = Неопределено Тогда
				Организация = Справочники.Организации.НайтиПоРеквизиту("ИНН", ТекущаяСтрокаМассив[8]);
			КонецЕсли;	
			
			НаборЗаписей = РегистрыСведений.ЖурналУчетаСчетовФактурМониторингСделок.СоздатьНаборЗаписей();
			НаборЗаписей.Отбор.Организация.Установить(Организация);
			НаборЗаписей.Отбор.НомерСчетаФактуры.Установить(ТекущаяСтрокаМассив[15]);
			НаборЗаписей.Прочитать();
			
			Если НаборЗаписей.Количество() = 0 Тогда
				НоваяЗапись = НаборЗаписей.Добавить();
				НоваяЗапись.Организация = Организация;
				НоваяЗапись.НомерСчетаФактуры = ТекущаяСтрокаМассив[15];
				НоваяЗапись.ЧастьЖурнала 		= Перечисления.ЧастиЖурналаУчетаСчетовФактур.ПолученныеСчетаФактуры;
			ИначеЕсли НаборЗаписей.Количество() = 1 Тогда
				НоваяЗапись = НаборЗаписей[0];
			КонецЕсли;
			
			НоваяЗапись.СуммаНДС 			= Число(ТекущаяСтрокаМассив[44]);
			НоваяЗапись.СуммаПоСчетуФактуре = Сумма;
			НоваяЗапись.СчетФактураПолученныйОтПродавца = ЭлектронныеСчетаФактурыПолученные.ПолучитьСчетФактураВходящаяПоНомеруЭД(СокрЛП(ТекущаяСтрокаМассив[15]));
			НоваяЗапись.ИННПродавца = ТекущаяСтрокаМассив[1];
			НоваяЗапись.Продавец            = Справочники.Контрагенты.НайтиПоРеквизиту("ИНН", СокрЛП(НоваяЗапись.ИННПродавца));			
			
			НаборЗаписей.Записать();

		КонецЦикла;

		ФайлДанных.Закрыть();

	КонецЦикла;
	
КонецПроцедуры	

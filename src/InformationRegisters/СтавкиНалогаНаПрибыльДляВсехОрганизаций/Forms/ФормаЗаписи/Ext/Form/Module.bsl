﻿// 4D:ERP для Беларуси
// {
// Форма изменена
// }
// 4D

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Ключ.Пустой() Тогда
		ЗаполнитьСтавкиПоУмолчанию();
	КонецЕсли;
	УправлениеФормой(ЭтаФорма);
	
	ВыводитсяПредупреждение = Ложь;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ПериодПриИзменении(Элемент)
	
	ЗаполнитьСтавкиПоУмолчанию();
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСтавкиПоУмолчанию()
	
	Если ЗначениеЗаполнено(Запись.Период) Тогда
		Период = Запись.Период;
	Иначе
		Период = ТекущаяДатаСеанса();
	КонецЕсли;
	
	Запись.СтавкаФБ = РегистрыСведений.СтавкиНалогаНаПрибыльДляВсехОрганизаций.ЗначениеПоУмолчаниюФедеральныйБюджет(Период);
	Запись.СтавкаСубъектРФ = РегистрыСведений.СтавкиНалогаНаПрибыльДляВсехОрганизаций.ЗначениеПоУмолчаниюРегиональныйБюджет(Период);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервереБезКонтекста
Процедура УправлениеФормой(Форма)

	Форма.ПрименятьОднуСтавку = НЕ ПолучитьФункциональнуюОпцию("ПрименяютсяРазныеСтавкиНалогаНаПрибыль");
	
	// 4D:ERP для Беларуси, ВладимирР, 22.10.2022 10:40:01
	// Ставка налога на прибыль, №35111
	// {
	Форма.Элементы.СтавкаСубъектРФ.Видимость = Ложь;
	//Форма.Элементы.СтавкаСубъектРФ.Видимость = Форма.ПрименятьОднуСтавку;
	// }
	// 4D

КонецПроцедуры

#КонецОбласти
﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Добавляет записи в регистр заданий.
//
// Параметры:
//  ОбъектРасчета	 - ДокументСсылка.ЭтапПроизводства2_2	 - документ, для которого необходимо добавить задание.
//  АвтоЗапуск		 - Булево								 - автоматический запуск расчет фоновым заданием.
//
Процедура ДобавитьЗадание(ОбъектРасчета, Автозапуск = Истина) Экспорт
	
	Задание = Новый Структура("ОбъектРасчета", ОбъектРасчета);
	
	Идентификаторы = УправлениеПроизводством.ДобавитьЗаданиеВОчередь("ЗаданияКРасчетуОчередиПроизводственныхОпераций", Задание);
	
	Если Автозапуск Тогда
		
		УправлениеПроизводством.ЗапуститьЗаданиеОбработкиОчереди("ЗаданияКРасчетуОчередиПроизводственныхОпераций", Задание, Идентификаторы);
		
	КонецЕсли;
	
КонецПроцедуры

// Запускает в фоновом режиме задание расчета очереди производственных операций
//
Процедура ЗапуститьЗадание() Экспорт
	
	УправлениеПроизводством.ЗапуститьЗаданиеОбработкиОчереди("ЗаданияКРасчетуОчередиПроизводственныхОпераций");
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ПараметрыОбработки() Экспорт
	
	ПараметрыОбработки = Новый Структура("Обработчик", "РегистрыСведений.ОчередьПроизводственныхОпераций.РассчитатьОчередьОтложенно");
	Возврат ПараметрыОбработки;
	
КонецФункции

Функция ОчередьЗаданий(МенеджерВременныхТаблиц) Экспорт
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	ОбрабатываемыеЗадания.ОбъектРасчета КАК ОбъектРасчета
	|ИЗ
	|	ОбрабатываемыеЗадания КАК ОбрабатываемыеЗадания
	|СГРУППИРОВАТЬ ПО
	|	ОбрабатываемыеЗадания.ОбъектРасчета");
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	УстановитьПривилегированныйРежим(Истина);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	УстановитьПривилегированныйРежим(Ложь);
	
	Результат = Новый Массив;
	
	Пока Выборка.Следующий() Цикл
		
		Результат.Добавить(Новый Структура("ОбъектРасчета", Выборка.ОбъектРасчета));
		
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции

Процедура УдалитьЗадание(Задание, МенеджерВременныхТаблиц) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	ОбрабатываемыеЗадания.Идентификатор КАК Идентификатор
	|ИЗ
	|	ОбрабатываемыеЗадания КАК ОбрабатываемыеЗадания
	|ГДЕ
	|	ОбрабатываемыеЗадания.ОбъектРасчета = &ОбъектРасчета");
	
	Запрос.УстановитьПараметр("ОбъектРасчета", Задание.ОбъектРасчета);
	
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Результат = Новый Массив;
	
	Пока Выборка.Следующий() Цикл
		
		Набор = РегистрыСведений.ЗаданияКРасчетуОчередиПроизводственныхОпераций.СоздатьНаборЗаписей();
		
		Набор.Отбор.Идентификатор.Установить(Выборка.Идентификатор);
		
		Набор.Записать(Истина);
		
	КонецЦикла;
	
	УстановитьПривилегированныйРежим(Ложь);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
﻿
#Область ПрограммныйИнтерфейс

// Определяет следующие свойств регламентных заданий:
//  - зависимость от функциональных опций.
//  - возможность выполнения в различных режимах работы программы.
//  - прочие параметры.
//
// см. РегламентныеЗаданияПереопределяемый.ПриОпредеПриОпределенииНастроекРегламентныхЗаданий()
//
Процедура ПриОпределенииНастроекРегламентныхЗаданий(Настройки) Экспорт

КонецПроцедуры

#КонецОбласти
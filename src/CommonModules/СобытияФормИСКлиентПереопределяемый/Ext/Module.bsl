﻿#Область СлужебныйПрограммныйИнтерфейс

#Область ОбработчикиСобытийФормы

// Возникает на клиенте перед выполнением записи объекта из формы.
//
// Параметры:
//  Форма           - УправляемаяФорма - форма записываемого объекта,
//  Отказ           - Булево           - признак отказа от записи,
//  ПараметрыЗаписи - Структура        - структура, содержащая параметры записи.
Процедура ПередЗаписью(Форма, Отказ, ПараметрыЗаписи) Экспорт
	
	//++ НЕ ГОСИС
	Если ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(Форма, "Объект") Тогда
		
		Если ОбщегоНазначенияКлиентСервер.ЕстьРеквизитИлиСвойствоОбъекта(ПараметрыЗаписи, "РежимЗаписи") Тогда
			
			СобытияФормКлиент.ПередЗаписью(Форма, Отказ, ПараметрыЗаписи);
			
		КонецЕсли;
		
	КонецЕсли;
	//-- НЕ ГОСИС
	
	Возврат;
	
КонецПроцедуры

// Вызывается во всех созданных формах при вызове метода Оповестить.
//
// Параметры:
//  Форма      - УправляемаяФорма - оповещаемая форма,
//  ИмяСобытия - Строка           - имя события,
//  Параметр   - Произвольный     - параметр сообщения. Могут быть переданы любые необходимые данные,
//  Источник   - Произвольный     - источник события.
Процедура ОбработкаОповещения(Форма, ИмяСобытия, Параметр, Источник) Экспорт
	
	//++ НЕ ГОСИС
	СобытияФормКлиент.ОбработкаОповещения(Форма, ИмяСобытия, Параметр, Источник);
	//-- НЕ ГОСИС
	Возврат;
	
КонецПроцедуры

// Обработчики событий обрабатываемых БГосИС в прикладных формах
//
// Параметры:
//  Форма                   - УправляемаяФорма - оповещаемая форма,
//  ИмяСобытия              - Строка           - имя события,
//  Параметр                - Произвольный     - параметр сообщения. Могут быть переданы любые необходимые данные,
//  Источник                - Произвольный     - источник события.
//  ДополнительныеПараметры - Структура        - дополнительные параметры обработки
Процедура ОбработкаОповещенияИС(Форма, ИмяСобытия, Параметр, Источник, ДополнительныеПараметры) Экспорт
	
	//++ НЕ ГОСИС
	Если Форма.ИмяФормы = "ОбщаяФорма.ПроверкаЗаполненияДокументов"
		Или Форма.ИмяФормы = "Документ.ЧекККМ.Форма.ФормаДокументаРМК"
		Или Форма.ИмяФормы = "Документ.ЧекККМВозврат.Форма.ФормаДокументаРМК" Тогда
			
		Если Источник = "ПодключаемоеОборудование" И Форма.ВводДоступен() Тогда
			Если ИмяСобытия = "ScanData" И МенеджерОборудованияУТКлиент.ЕстьНеобработанноеСобытие() Тогда
				Если Форма.ИспользоватьАкцизныеМарки Тогда
					
					ДополнительныеПараметры.СтандартнаяОбработка = Ложь;
					МенеджерОборудованияУТКлиент.ОбработатьСобытие();
					ДанныеШтрихкода = МенеджерОборудованияУТКлиент.ПреобразоватьДанныеСоСканераВСтруктуру(Параметр);
					ОписаниеОповещения = Новый ОписаниеОповещения("ПоискПоШтрихкодуЗавершение", Форма);
					ВыполнитьОбработкуОповещения(ОписаниеОповещения, ДанныеШтрихкода);
					
				КонецЕсли;
			КонецЕсли;
		КонецЕсли;
			
	КонецЕсли;
	//-- НЕ ГОСИС
	Возврат;
	
КонецПроцедуры

//Переопределенный сценарий обработки оповещения прикладных объектов об изменениях в библиотечных.
//   Вызывается для обновления гиперссылок в прикладных документах и при необходимости выполнить дополнительные действия.
//   Для переопределения обработчика установить Событие.Обработано = Истина, для дополнения не менять это значение.
// 
// Параметры:
//   МестоВызова - Структура - сведения о месте в котором требуется обработка:
//    * Форма  - УправляемаяФорма     - источник вызова
//    * Объект - ДанныеФормыСтруктура - основной реквизит формы
//   Событие     - Структура - сведения о событии:
//    * Имя        - Строка       - имя события формы
//    * Параметр   - Произвольный - параметр события формы
//    * Источник   - Произвольный - источник события формы
//    * Обработано - Булево       - признак что событие уже обработано
//
Процедура ОбработкаОповещенияВФормеДокументаОснования(МестоВызова, Событие) Экспорт
	
	//++ НЕ ГОСИС
	//-- НЕ ГОСИС
	Возврат;
	
КонецПроцедуры

// Выполняет переопределяемую команду
//
// Параметры:
//  Форма                   - УправляемаяФорма - форма, в которой расположена команда
//  Команда                 - КомандаФормы     - команда формы
//  ДополнительныеПараметры - Структура        - дополнительные параметры.
//
Процедура ВыполнитьПереопределяемуюКомандуИС(Форма, Команда, ДополнительныеПараметры) Экспорт
	
	//++ НЕ ГОСИС
	Если Форма.ИмяФормы = "Документ.ЧекККМ.Форма.ФормаДокументаРМК"
		Или Форма.ИмяФормы = "Документ.ЧекККМВозврат.Форма.ФормаДокументаРМК" Тогда
	
		Если Форма.Объект.Товары.Количество() Тогда
			ДобавленныеВидыПродукции = ИнтеграцияИСВызовСервераУТ.ВидыПродукцииВТоварах(Форма.Объект.Товары);
		Иначе
			ДобавленныеВидыПродукции = Новый Массив;
		КонецЕсли;
		
		АлкогольнаяПродукция = ПредопределенноеЗначение("Перечисление.ВидыПродукцииИС.Алкогольная");
		ТабачнаяПродукция    = ПредопределенноеЗначение("Перечисление.ВидыПродукцииИС.Табачная");
		ОбувнаяПродукция     = ПредопределенноеЗначение("Перечисление.ВидыПродукцииИС.Обувная");
		
		Если ДобавленныеВидыПродукции.Количество() = 0 Тогда
			Если Форма.ПараметрыИнтеграцииГосИС.Получить("ЕГАИС")<>Неопределено Тогда
				ДобавленныеВидыПродукции.Добавить(АлкогольнаяПродукция);
			КонецЕсли;
			Если Форма.ПараметрыИнтеграцииГосИС.Получить("ИСМП")<>Неопределено Тогда
				Для Каждого ВидПродукции Из Форма.ПараметрыИнтеграцииГосИС.Получить("ИСМП").ВидыПродукции Цикл
					ДобавленныеВидыПродукции.Добавить(ВидПродукции);
				КонецЦикла;
			КонецЕсли;
		КонецЕсли;
		
		Если ДобавленныеВидыПродукции.Количество() = 1 Тогда
			
			Если ДобавленныеВидыПродукции.Найти(АлкогольнаяПродукция) <> Неопределено Тогда
				Команда = Новый Структура("Имя", "ПроверитьАкцизныеМаркиЕГАИС");
			ИначеЕсли ДобавленныеВидыПродукции.Найти(ТабачнаяПродукция) <> Неопределено Тогда
				Команда = Новый Структура("Имя", "ПроверитьАкцизныеМаркиГосИС1");
			ИначеЕсли ДобавленныеВидыПродукции.Найти(ОбувнаяПродукция) <> Неопределено Тогда
				Команда = Новый Структура("Имя", "ПроверитьАкцизныеМаркиГосИС2");
			КонецЕсли;
			
		Иначе
			
			Команда = Новый Структура("Имя", "");
			
			Если ДобавленныеВидыПродукции.Количество() Тогда
				
				СписокВыбора = Новый СписокЗначений;
				Если ДобавленныеВидыПродукции.Найти(АлкогольнаяПродукция) <> Неопределено Тогда
					СписокВыбора.Добавить(АлкогольнаяПродукция, НСтр("ru = 'Алкогольная продукция';
																	|en = 'Алкогольная продукция'"));
				КонецЕсли;
				Если ДобавленныеВидыПродукции.Найти(ТабачнаяПродукция) <> Неопределено Тогда
					СписокВыбора.Добавить(ТабачнаяПродукция, НСтр("ru = 'Табачная продукция';
																	|en = 'Табачная продукция'"));
				КонецЕсли;
				Если ДобавленныеВидыПродукции.Найти(ОбувнаяПродукция) <> Неопределено Тогда
					СписокВыбора.Добавить(ОбувнаяПродукция, НСтр("ru = 'Обувь';
																|en = 'Обувь'"));
				КонецЕсли;
				
				ПараметрыФормы = Новый Структура("СписокВыбора", СписокВыбора);
				
				ДополнительныеПараметры = Новый Структура("Форма", Форма);
				ОповещениеОВыборе = Новый ОписаниеОповещения("ОбработатьВыборФормыСканирования", ИнтеграцияИСУТКлиент, ДополнительныеПараметры);
				ОткрытьФорму("ОбщаяФорма.ФормаВыбораВидовПродукцииГосИС",
					ПараметрыФормы,
					Форма,,,,
					ОповещениеОВыборе,
					РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
				
			КонецЕсли;
			
		КонецЕсли;
		
	КонецЕсли;
	//-- НЕ ГОСИС
	Возврат;
	
КонецПроцедуры

// Обработчики БГосИС элементов прикладных форм
//   Ограничения: не предполагает контекстный серверный вызов
//
// Параметры:
//   Форма                   - УправляемаяФорма - форма, из которой происходит вызов процедуры.
//   Элемент                 - ЭлементФормы     - элемент-источник события "При изменении"
//   ДополнительныеПараметры - Структура        - значения дополнительных параметров влияющих на обработку.
//
Процедура ПриИзмененииЭлемента(Форма, Элемент, ДополнительныеПараметры) Экспорт
	
	//++ НЕ ГОСИС
	Если Форма.ИмяФормы = "ОбщаяФорма.ПроверкаЗаполненияДокументов" Тогда
				
		Если Элемент = "ТоварыПоискПоШтрихкоду" Тогда
			
			Если Форма.ИспользоватьАкцизныеМарки
				И ДополнительныеПараметры.ДанныеШтрихкода <> Неопределено Тогда
				
				ДополнительныеПараметры.ТребуетсяСерверныйВызов = Истина;
				ПараметрыСканирования = ШтрихкодированиеИСКлиент.ПараметрыСканирования(Форма);
				ДополнительныеПараметры.Вставить("ПараметрыСканирования", ПараметрыСканирования);
				ДополнительныеПараметры.ДанныеШтрихкода.Штрихкод = ШтрихкодированиеИСКлиентСервер.ШтрихкодВBase64(
					ДополнительныеПараметры.ДанныеШтрихкода.Штрихкод);
				ДополнительныеПараметры.СтандартнаяОбработка = Ложь;
				
			КонецЕсли;
		
		ИначеЕсли Элемент = "ЗавершитьОбработкуШтрихкода" Тогда
			
			Если Форма.ИспользоватьАкцизныеМарки Тогда
				
				ПараметрыЗавершенияВводаШтрихкода = ШтрихкодированиеИСКлиент.ПараметрыЗавершенияОбработкиШтрихкода();
				ПараметрыЗавершенияВводаШтрихкода.РезультатОбработкиШтрихкода  = ДополнительныеПараметры.РезультатОбработкиШтрихкода;
				ПараметрыЗавершенияВводаШтрихкода.Форма                        = Форма;
				ПараметрыЗавершенияВводаШтрихкода.ПараметрыСканирования        = ДополнительныеПараметры.ПараметрыСканирования;
				ПараметрыЗавершенияВводаШтрихкода.ДанныеШтрихкода              = ДополнительныеПараметры.ДанныеШтрихкода;
				ПараметрыЗавершенияВводаШтрихкода.ОповещениеОбработкиШтрихкода = Новый ОписаниеОповещения("ПоискПоШтрихкодуЗавершение", Форма);
				ПараметрыЗавершенияВводаШтрихкода.ОповещениеВыполнитьДействие  = Новый ОписаниеОповещения("ОбработкаКодаМаркировкиВыполнитьДействие", Форма);
				ШтрихкодированиеИСКлиент.ЗавершитьОбработкуШтрихкода(ПараметрыЗавершенияВводаШтрихкода);
				
			КонецЕсли;
			
		ИначеЕсли Элемент = "Подключаемый_ОткрытьФормуУточненияДанных" Тогда
			
			ШтрихкодированиеИСКлиент.Подключаемый_ОткрытьФормуУточненияДанных(Форма, ДополнительныеПараметры.ОповещениеПовторнойОбработки);
			
		ИначеЕсли Элемент = "РезультатЗакрытия" Тогда
			
			Если Форма.АкцизныеМарки.Количество() Тогда
				ДополнительныеПараметры.ТребуетсяСерверныйВызов = Истина;
			КонецЕсли;
			
		ИначеЕсли Элемент = "Товары" Тогда
			
			Если ДополнительныеПараметры.Свойство("ПередУдалением") Тогда
				Форма.ТребуетсяПересчетМарокПослеУдаленияСтрок = Ложь;
				Для Каждого СтрокаТовары Из ДополнительныеПараметры.УдаляемыеСтроки Цикл
					Если СтрокаТовары.МаркируемаяПродукция Тогда
						Форма.ТребуетсяПересчетМарокПослеУдаленияСтрок = Истина;
					КонецЕсли;
				КонецЦикла;
				Возврат;
			КонецЕсли;
			
			Если ДополнительныеПараметры.Свойство("ПослеУдаления") Тогда
				
				ДополнительныеПараметры.ТребуетсяСерверныйВызов = Форма.ТребуетсяПересчетМарокПослеУдаленияСтрок
					И Форма.АкцизныеМарки.Количество();
					
				Форма.ТребуетсяПересчетМарокПослеУдаленияСтрок = Ложь;
				
				Возврат;
			КонецЕсли;
			
			ТекущаяСтрока = Форма.Элементы.Товары.ТекущиеДанные;
			Если (ТекущаяСтрока = Неопределено) Тогда
				Возврат;
			КонецЕсли;
			
			НужноПересчитатьКеш = ПроверкаИПодборПродукцииИСКлиент.ПрименитьКешПоСтроке(
				Форма, Форма.Товары, ТекущаяСтрока, Форма.ТоварыКешТекущейСтроки,, "КоличествоФакт");
			
			Если НужноПересчитатьКеш Тогда
				ДополнительныеПараметры.Вставить("ТребуетсяСерверныйВызов");
			КонецЕсли;
			
		КонецЕсли;
		
	ИначеЕсли Форма.ИмяФормы = "Документ.ЧекККМ.Форма.ФормаДокументаРМК"
		Или Форма.ИмяФормы = "Документ.ЧекККМВозврат.Форма.ФормаДокументаРМК" Тогда
		
		Если Элемент = "ПраваДоступа" Тогда
			
			Если Форма.ПараметрыИнтеграцииГосИС.Получить("ЕГАИС") <> Неопределено Тогда
				Форма.Элементы.ТоварыНоменклатураЕГАИС.ТолькоПросмотр = Не Форма.ПраваДоступа.КорректировкаСтрок;
			КонецЕсли;
			
		ИначеЕсли Элемент = "ПраваДоступа" Тогда
			
			Если Форма.ПараметрыИнтеграцииГосИС.Получить("ЕГАИС") <> Неопределено Тогда
				Форма.Элементы.ТоварыНоменклатураЕГАИС.ТолькоПросмотр = Не Форма.ПраваДоступа.КорректировкаСтрок;
			КонецЕсли;
			
		ИначеЕсли Элемент = "ТоварыПоискПоШтрихкоду" Тогда
			
			Если Форма.ИспользоватьАкцизныеМарки Тогда
				
				ШтрихкодированиеИСКлиент.ОбработатьВводШтрихкода(
					Форма,
					ДополнительныеПараметры.ДанныеШтрихкода,
					ДополнительныеПараметры.КэшированныеЗначения);
				ДополнительныеПараметры.СтандартнаяОбработка = Ложь;
			
			КонецЕсли;
			
		ИначеЕсли Элемент = "ПроверитьКоличествоВДокументе" Тогда
			
			Если Форма.ИспользоватьАкцизныеМарки Тогда
				Для Каждого СтрокаТЧ Из Форма.Объект.Товары Цикл
					Если ПустаяСтрока(СтрокаТЧ.ИдентификаторСтроки) И СтрокаТЧ.МаркируемаяПродукция = 1 Тогда
						СтрокаТЧ.ИдентификаторСтроки = Строка(Новый УникальныйИдентификатор);
					КонецЕсли;
				КонецЦикла;
			КонецЕсли;
			ДополнительныеПараметры.Вставить("ИспользоватьАкцизныеМарки", Форма.ИспользоватьАкцизныеМарки);
			ДополнительныеПараметры.Вставить("ОрганизацияЕГАИС",          Форма.Объект.ОрганизацияЕГАИС);
			
		ИначеЕсли Элемент = "Товары" Тогда
			
			Если ДополнительныеПараметры.Свойство("ПередУдалением") Тогда
				Форма.ТребуетсяПересчетМарокПослеУдаленияСтрок = Ложь;
				Для Каждого СтрокаТовары Из Форма.Элементы.Товары.ВыделенныеСтроки Цикл
					Если Форма.Элементы.Товары.ДанныеСтроки(СтрокаТовары).МаркируемаяПродукция Тогда
						Форма.ТребуетсяПересчетМарокПослеУдаленияСтрок = Истина;
					КонецЕсли;
				КонецЦикла;
				Возврат;
			КонецЕсли;
			
			Если ДополнительныеПараметры.Свойство("ПослеУдаления") Тогда
				ДополнительныеПараметры.ТребуетсяСерверныйВызов = Форма.ТребуетсяПересчетМарокПослеУдаленияСтрок
					И Форма.Объект.АкцизныеМарки.Количество();
				Возврат;
			КонецЕсли;
			
			ТекущаяСтрока = Форма.Элементы.Товары.ТекущиеДанные;
			Если (ТекущаяСтрока = Неопределено) Тогда
				Возврат;
			КонецЕсли;
			
			НужноПересчитатьКеш = ПроверкаИПодборПродукцииИСКлиент.ПрименитьКешПоСтроке(
				Форма, Форма.Объект.Товары, ТекущаяСтрока, Форма.ТоварыКешТекущейСтроки);
			
			Если НужноПересчитатьКеш Тогда
				ДополнительныеПараметры.Вставить("ТребуетсяСерверныйВызов");
			КонецЕсли;
			
		КонецЕсли;
		
	КонецЕсли;
	//-- НЕ ГОСИС
	Возврат;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКоманд
// Обработчик переопределяемой команды формы.
//
// Параметры:
//  Форма   - УправляемаяФорма - форма объекта справочника или документа,
//  Команда - КомандаФормы     - команда формы.
Процедура ВыполнитьПереопределяемуюКоманду(Форма, Команда) Экспорт
	
	//++ НЕ ГОСИС
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(Форма, Команда);
	//-- НЕ ГОСИС
	Возврат;
	
КонецПроцедуры

#КонецОбласти

#Область ПодключаемоеОборудование

// Вызывается перед обработкой штрихкодов, не привязанных ни к одной номенклатуре.
//
// Параметры:
//  ОписаниеОповещения - ОписаниеОповещения - процедура, которую нужно вызвать после выполнения обработки,
//  Форма - УправляемаяФорма - форма, в которой отсканировали штрихкоды,
//  ИмяСобытия - Строка - имя события, инициировавшее оповещение,
//  Параметр - Структура - данные для обработки,
//  Источник - Произвольный - источник события.
Процедура ОбработкаОповещенияОбработаныНеизвестныеШтрихкоды(ОписаниеОповещения, Форма, ИмяСобытия, Параметр, Источник) Экспорт
	
	//++ НЕ ГОСИС
	Если Источник = "ПодключаемоеОборудование"
		И ИмяСобытия = "НеизвестныеШтрихкоды"
		И Параметр.ФормаВладелец = Форма.УникальныйИдентификатор Тогда
		
		ДанныеШтрихкодов = Новый Массив;
		Для Каждого ПолученныйШтрихкод Из Параметр.ПолученыНовыеШтрихкоды Цикл
			ДанныеШтрихкодов.Добавить(ПолученныйШтрихкод);
		КонецЦикла;
		Для Каждого ПолученныйШтрихкод Из Параметр.ЗарегистрированныеШтрихкоды Цикл
			ДанныеШтрихкодов.Добавить(ПолученныйШтрихкод);
		КонецЦикла;
		
		ВыполнитьОбработкуОповещения(ОписаниеОповещения, ДанныеШтрихкодов);
		
	КонецЕсли;
	//-- НЕ ГОСИС
	
	Возврат;
	
КонецПроцедуры

// В процедуре нужно реализовать алгоритм преобразования данных из подсистемы подключаемого оборудования.
//
// Параметры:
//  Результат - Структура - со свойствами Штрихкод, Количество
//  Параметр  - Массив    - входящие данные.
Процедура ПреобразоватьДанныеСоСканераВСтруктуру(Результат, Параметр) Экспорт
	
	//++ НЕ ГОСИС
	Результат = МенеджерОборудованияУТКлиент.ПреобразоватьДанныеСоСканераВСтруктуру(Параметр);
	//-- НЕ ГОСИС
	Возврат;
	
КонецПроцедуры

// В процедуре нужно реализовать алгоритм преобразования данных из подсистемы подключаемого оборудования.
//
// Параметры:
//  Результат - Массив - Массив структур со свойствами Штрихкод, Количество.
//  Параметр  - Массив - входящие данные.
Процедура ПреобразоватьДанныеСоСканераВМассив(Результат, Параметр) Экспорт
	
	//++ НЕ ГОСИС
	Результат = МенеджерОборудованияУТКлиент.ПреобразоватьДанныеСоСканераВМассив(Параметр);
	//-- НЕ ГОСИС
	Возврат;
	
КонецПроцедуры

Процедура ПриПолученииДанныхИзТСД(ОписаниеОповещения, Форма, РезультатВыполнения) Экспорт
	
	//++ НЕ ГОСИС
	Если РезультатВыполнения.Результат Тогда
		
		ВыполнитьОбработкуОповещения(ОписаниеОповещения, РезультатВыполнения.ТаблицаТоваров);
		
	Иначе
		
		СобытияФормИСКлиент.СообщитьОбОшибке(РезультатВыполнения);
		
	КонецЕсли;
	//-- НЕ ГОСИС
	
	Возврат;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиОповещений

// Вызывает процедуру обработки подбора, если произошло оповещение из формы подбора.
//
// Параметры:
//  ОповещениеПриЗавершении - ОписаниеОповещения - процедура завершения подбора номенклатуры,
//  ИмяСобытия - Строка - имя события, о котором происходит оповещение,
//  Параметр - Произвольный - переданный в сообщение параметр,
//  Источник - УправляемаяФорма - форма, в которой произошло оповещение.
Процедура ОбработкаОповещенияПодборНоменклатуры(ОповещениеПриЗавершении, ИмяСобытия, Параметр, Источник) Экспорт
	
	Возврат;
	
КонецПроцедуры

// Вызывает процедуру обработки подбора, если произошел выбор из формы подбора.
//
// Параметры:
//  ОповещениеПриЗавершении - ОписаниеОповещения - процедура завершения подбора номенклатуры,
//  ВыбранноеЗначение - Произвольный - результат выбора в подчиненной форме,
//  ИсточникВыбора - УправляемаяФорма - форма, где осуществлен выбор.
Процедура ОбработкаВыбораПодборНоменклатуры(ОповещениеПриЗавершении, ВыбранноеЗначение, ИсточникВыбора) Экспорт
	
	//++ НЕ ГОСИС
	Если ИсточникВыбора.ИмяФормы = "Обработка.ПодборТоваровВДокументПродажи.Форма.Форма" Тогда
		ВыполнитьОбработкуОповещения(ОповещениеПриЗавершении, ВыбранноеЗначение);
	КонецЕсли;
	//-- НЕ ГОСИС
	
	Возврат;
	
КонецПроцедуры

// Вызывает процедуру обработки выбора контрагента, если произошел выбор из формы выбора.
//
// Параметры:
//  ОповещениеПриЗавершении - ОписаниеОповещения - процедура завершения подбора номенклатуры,
//  ВыбранноеЗначение - Произвольный - результат выбора в подчиненной форме,
//  ИсточникВыбора - УправляемаяФорма - форма, где осуществлен выбор.
Процедура ОбработкаВыбораКонтрагента(ОповещениеПриЗавершении, ВыбранноеЗначение, ИсточникВыбора) Экспорт
	
	//++ НЕ ГОСИС
	Если СтрНачинаетсяС(ИсточникВыбора.ИмяФормы, "Справочник.Контрагенты") Тогда
		ВыполнитьОбработкуОповещения(ОповещениеПриЗавершении, ВыбранноеЗначение);
	КонецЕсли;
	//-- НЕ ГОСИС
	Возврат;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
﻿

#Область ВводОстатков

Функция ПереданныеНаКомиссиюСоглашениеСКомиссионеромПриИзмененииНаСервере(СоглашениеСКомиссионером) Экспорт
	
	Возврат СоглашениеСКомиссионером.ЦенаВключаетНДС;
	
КонецФункции

#КонецОбласти 


#Область РКО

Процедура УстановитьВидимостьЭлементовРКО(Объект, Форма) Экспорт

	Элементы = Форма.Элементы;
	
	Если Объект.ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратОплатыКлиенту Тогда
		Если УправлениеФормамиУТВызовСервера_Локализация.ЕстьЭлементФормы(Элементы, "ПробитЧек") Тогда
			Элементы.ПробитЧек.Видимость = Истина;	
			Элементы.КассаККМ.Видимость = Истина;
			Элементы.КассаККМ.Доступность = Объект.ПробитЧек;
		КонецЕсли; 
		
	ИначеЕсли Объект.ХозяйственнаяОперация <> Перечисления.ХозяйственныеОперации.ВыдачаДенежныхСредствВКассуККМ Тогда
		Если УправлениеФормамиУТВызовСервера_Локализация.ЕстьЭлементФормы(Элементы, "ПробитЧек") Тогда
			Элементы.ПробитЧек.Видимость = Ложь;	
			Элементы.КассаККМ.Видимость = Ложь;
			Объект.КассаККМ = Справочники.КассыККМ.ПустаяСсылка();				
		КонецЕсли;
	ИначеЕсли Объект.ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВыдачаДенежныхСредствВКассуККМ Тогда
		Элементы.КассаККМ.Доступность = Истина;	
	КонецЕсли; 		
		
КонецПроцедуры
	
#КонецОбласти 


#Область РеализацияТоваровУслуг

Функция ПолучитьСписокВыбораОснованияДляПечатиРеализация(Объект) Экспорт
	
	ЭтоПередачаНаКомиссию = (Объект.ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПередачаНаКомиссию);
	
	Список = Новый СписокЗначений;
	ТекстОснования = ПолучитьТекстОснования(Объект, Перечисления.ПорядокРасчетов.ПоДоговорамКонтрагентов);
	Если ЗначениеЗаполнено(ТекстОснования) Тогда
		Список.Добавить(ТекстОснования + ?(ЭтоПередачаНаКомиссию, ", " + НСтр("ru='передача на комиссию'"), ""));
	КонецЕсли;
	
	ТекстОснования = ПолучитьТекстОснования(Объект, Перечисления.ПорядокРасчетов.ПоЗаказамНакладным);
	Если ЗначениеЗаполнено(ТекстОснования) Тогда
		Список.Добавить(ТекстОснования + ?(ЭтоПередачаНаКомиссию, ", " + НСтр("ru='передача на комиссию'"), ""));
	КонецЕсли;
	
	Если ЭтоПередачаНаКомиссию И Список.Количество()=0 Тогда
		Список.Добавить(НСтр("ru='Передача на комиссию'"));
	КонецЕсли;
	
	Если ТекстОснования = "" И Список.Количество()=0 Тогда
		ТекстОснования = Строка(Объект.Соглашение);	
		Список.Добавить(ТекстОснования);
	КонецЕсли;
	
	Возврат Список;
	
КонецФункции

Функция ПолучитьТекстОснования(Объект, ПорядокРасчетов)
	
	ТекстОснования = "";
	
	Если ПорядокРасчетов = Перечисления.ПорядокРасчетов.ПоДоговорамКонтрагентов
		И ЗначениеЗаполнено(Объект.Договор) Тогда
		
		ТекстОснования = СокрЛП(Объект.Договор.НаименованиеДляПечати);
				
	ИначеЕсли ПорядокРасчетов = Перечисления.ПорядокРасчетов.ПоЗаказамНакладным
		И Объект.РеализацияПоЗаказам Тогда
		
		МассивЗаказов = Неопределено;
		Если ЗначениеЗаполнено(Объект.ЗаказКлиента) Тогда
			МассивЗаказов = Новый Массив;
			МассивЗаказов.Добавить(Объект.ЗаказКлиента);
		ИначеЕсли Объект.Товары.Количество() <> 0 Тогда 
			Если ТипЗнч(Объект) = Тип("Структура") Тогда
				МассивЗаказов = Объект.Товары.ВыгрузитьКолонку("ЗаказКлиента");
			Иначе
				МассивЗаказов = Объект.Товары.Выгрузить(,"ЗаказКлиента").ВыгрузитьКолонку("ЗаказКлиента");
			КонецЕсли;
		КонецЕсли;
		
		Если МассивЗаказов <> Неопределено Тогда
		
			Запрос = Новый Запрос(
				"ВЫБРАТЬ РАЗРЕШЕННЫЕ
				|	ЗаказыКлиентов.НомерПоДаннымКлиента КАК НомерПоДаннымКлиента,
				|	ЗаказыКлиентов.ДатаПоДаннымКлиента  КАК ДатаПоДаннымКлиента,
				|	ЗаказыКлиентов.Номер                КАК Номер,
				|	ЗаказыКлиентов.Дата                 КАК Дата,
				|	&СинонимЗаказа                      КАК Синоним
				|ИЗ
				|	Документ.ЗаказКлиента КАК ЗаказыКлиентов
				|ГДЕ
				|	ЗаказыКлиентов.Ссылка В(&МассивЗаказов)
				|
				|ОБЪЕДИНИТЬ ВСЕ
				|
				|ВЫБРАТЬ
				|	NULL КАК НомерПоДаннымКлиента,
				|	NULL КАК ДатаПоДаннымКлиента,
				|	ЗаявкиНаВозврат.Номер,
				|	ЗаявкиНаВозврат.Дата,
				|	&СинонимЗаявки
				|ИЗ
				|	Документ.ЗаявкаНаВозвратТоваровОтКлиента КАК ЗаявкиНаВозврат
				|ГДЕ
				|	ЗаявкиНаВозврат.Ссылка В(&МассивЗаказов)");
			Запрос.УстановитьПараметр("МассивЗаказов", МассивЗаказов);
			Запрос.УстановитьПараметр("СинонимЗаказа", НСтр("ru='Заказ клиента'"));
			Запрос.УстановитьПараметр("СинонимЗаявки", НСтр("ru='Заявка на замену'"));
			Выборка = Запрос.Выполнить().Выбрать();
			
			ТекстПоЗаказам = "";
			Пока Выборка.Следующий() Цикл
				Если ЗначениеЗаполнено(Выборка.НомерПоДаннымКлиента) И ЗначениеЗаполнено(Выборка.ДатаПоДаннымКлиента) Тогда
					ТекстПоЗаказам = ТекстПоЗаказам + ", " +
						СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
							НСтр("ru = '%1 № %2 от %3'"),
							Выборка.Синоним, Выборка.НомерПоДаннымКлиента, Формат(Выборка.ДатаПоДаннымКлиента, "ДФ='дд ММММ гггг'"));
				Иначе
					ТекстПоЗаказам = ТекстПоЗаказам + ", " + ОбщегоНазначенияУТКлиентСервер.СформироватьЗаголовокДокумента(Выборка, Выборка.Синоним);
				КонецЕсли;
			КонецЦикла;
			ТекстОснования = СокрЛП(Сред(ТекстПоЗаказам, 3));
			
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат ТекстОснования; // Возврат значения по-умолчанию
	
КонецФункции


#КонецОбласти 


#Область ТаможеннаяДекларация

Функция ПолучитьВес(Упаковка) Экспорт

	Если ТипЗнч(Упаковка) = Тип("СправочникСсылка.УпаковкиЕдиницыИзмерения") Тогда
		Возврат Упаковка.Вес;
	Иначе
		Возврат 0;
	КонецЕсли;

КонецФункции 

	
#КонецОбласти 


#Область ПеремещениеТоваров

Функция ПолучитьЦенаВключаетНДС(ВидЦены) Экспорт

	Если ЗначениеЗаполнено(ВидЦены) Тогда
		Возврат ВидЦены.ЦенаВключаетНДС;
	Иначе	
	    Возврат Ложь;
	КонецЕсли; 	

КонецФункции // ()
 
	
#КонецОбласти 


#Область ПередачаТоваровМеждуОрганизациями

Функция ЗаполнитьСписокВыбораОснования(Основание, Договор) Экспорт

	СписокВыбораОснования = Новый СписокЗначений;
	
	Если ЗначениеЗаполнено(Основание) Тогда 
		СписокВыбораОснования.Добавить(Основание);	
	КонецЕсли; 
	
	Если ЗначениеЗаполнено(Договор) И Договор.НаименованиеДляПечати <> Основание Тогда
		СписокВыбораОснования.Добавить(Договор.НаименованиеДляПечати);	
	КонецЕсли;
	
	Возврат СписокВыбораОснования; 		

КонецФункции // ()
 

Функция ИспользуетсяОтветственноеХранение(Склад) Экспорт

	Возврат Склад.СкладОтветственногоХранения;	

КонецФункции // ()
 
#КонецОбласти 


#Область ВозвратПоставщику

Функция ПолучитьСписокСкладов(Склад) Экспорт

	МассивСкладов = Новый Массив;
	
	МассивСкладов.Добавить(Склад);
	
	Возврат МассивСкладов;
	
КонецФункции

	
#КонецОбласти 
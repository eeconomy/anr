﻿
#Область СлужебныеПроцедурыИФункции

Процедура УстановитьИнформационнуюСсылкуПереносаДанных(ИмяИсточника, ИнформационнаяСсылка) Экспорт
	
	Если ИмяИсточника = "ЗУП" Тогда
		ИнформационнаяСсылка = "http://its.1c.ru/db/metod81#content:5501:1";
	ИначеЕсли ИмяИсточника = "ЗУПКОРП" Тогда
		ИнформационнаяСсылка = "http://its.1c.ru/db/metod81#content:5501:1";
	ИначеЕсли ИмяИсточника = "ЗиК" Тогда
		ИнформационнаяСсылка = "http://its.1c.ru/db/metod81#content:6219:hdoc";
	ИначеЕсли ИмяИсточника = "БП3" Тогда
		ИнформационнаяСсылка = "";
	КонецЕсли;
	
КонецПроцедуры

Процедура ОткрытьФормуСпискаДокументаОтражениеЗарплатыВБухучете(ПараметрыФормы = Неопределено, ВладелецФормы = Неопределено) Экспорт
	
	ОткрытьФорму("Документ.ОтражениеЗарплатыВБухучете.ФормаСписка", ПараметрыФормы, ВладелецФормы);
	
КонецПроцедуры

#КонецОбласти

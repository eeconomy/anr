﻿////////////////////////////////////////////////////////////////////////////////
// Модуль содержит переопределяемые процедуры и функции.
//
////////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// Процедуры и функции механизма автоматического заполнения.

// Функция возвращает структуру показателей.
// Ключ структуры – идентификатор показателя.
// Значение структуры – массив из двух элементов:
// 	- признак автозаполнения показателя;
//	- признак расшифровки показателя.
//
// Параметры:
// 	ИДОтчета         - идентификатор отчета;
//	ИДРедакцииОтчета - идентификатор редакции формы отчета;
//  ПараметрыОтчета  - структура параметров отчета.
//
// Пример:
//	ПоказателиОтчета = Новый Структура;
//	Если ИДОтчета = "РегламентированныйОтчетРСВ1" Тогда
//		Если ИДРедакцииОтчета = "ФормаОтчета2011Кв1" Тогда
//			РегламентированнаяОтчетность.ВставитьПоказательВСтруктуру(ПоказателиОтчета, "П000100010003", Истина, Истина);
//		КонецЕсли;
//	КонецЕсли;
//	Возврат ПоказателиОтчета;
//
Функция ПолучитьСведенияОПоказателяхОтчета(ИДОтчета, ИДРедакцииОтчета, ПараметрыОтчета = Неопределено) Экспорт
	
	ПоказателиОтчета = Новый Структура;
	
	Если ИДОтчета = "РегламентированныйОтчетБаланс_Локализация"
		ИЛИ ИДОтчета = "РегламентированныйОтчетПриложение3_Локализация" 
		ИЛИ ИДОтчета = "РегламентированныйОтчетПриложение4_Локализация" Тогда	
		
		Если ИДРедакцииОтчета = "ФормаОтчета2017" Тогда
			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ПолучитьСведенияОПоказателяхОтчетаБухОтчетностьФормаОтчета2017(ПоказателиОтчета,ПараметрыОтчета);
			
		КонецЕсли;
		
	ИначеЕсли ИДОтчета = "РегламентированныйОтчетПриложение2_Локализация" Тогда	
		
         Если ИДРедакцииОтчета = "ФормаОтчета2017" Тогда
			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ПолучитьСведенияОПоказателяхОтчетаБухОтчетностьФормаОтчета2017(ПоказателиОтчета,ПараметрыОтчета);
			
		КонецЕсли;
		
	ИначеЕсли ИДОтчета = "РегламентированныйОтчетНалоговаяДекларацияПоНДС_Локализация" Тогда	
		
         Если ИДРедакцииОтчета = "ФормаОтчета2018" ИЛИ ИДРедакцииОтчета = "ФормаОтчета052018" ИЛИ ИДРедакцииОтчета = "ФормаОтчета2019" ИЛИ ИДРедакцииОтчета = "ФормаОтчета2020" Тогда
			
			УчетНДС_Локализация.ПолучитьСведенияОПоказателяхОтчетаНДСФормаОтчета2018(ПоказателиОтчета);
			
		КонецЕсли;
		
	ИначеЕсли ИДОтчета = "РегламентированныйОтчетНалогНаНедвижимость_Локализация" Тогда	
		
         Если ИДРедакцииОтчета = "ФормаОтчета2018" Тогда
			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ПолучитьСведенияОПоказателяхОтчетаНалогНаНедвижимостьФормаОтчета2018(ПоказателиОтчета);
			
		КонецЕсли;
		
	ИначеЕсли ИДОтчета = "РегламентированныйОтчетНалогНаПрибыль_Локализация" Тогда	
		
         Если ИДРедакцииОтчета = "ФормаОтчета2018" ИЛИ ИДРедакцииОтчета = "ФормаОтчета2019" Тогда
			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ПолучитьСведенияОПоказателяхОтчетаНалогНаПрибыльФормаОтчета2018(ПоказателиОтчета);
			
         ИначеЕсли ИДРедакцииОтчета = "ФормаОтчета2021" Тогда
         	
         	ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ПолучитьСведенияОПоказателяхОтчетаНалогНаПрибыльФормаОтчета2021(ПоказателиОтчета);
         
         КонецЕсли;
		
	ИначеЕсли ИДОтчета = "РегистрНалоговогоУчета1_Локализация" Тогда	
		
         Если ИДРедакцииОтчета = "ФормаОтчета072016" Тогда
			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ПолучитьСведенияОПоказателяхРегистрНалоговогоУчета1ФормаОтчета072016(ПоказателиОтчета);
			
		КонецЕсли;
		
	ИначеЕсли ИДОтчета = "РегламентированныйОтчетОСредствахПоОбязательномуСтрахованию_Локализация" Тогда
		
		Если ИДРедакцииОтчета = "ФормаОтчета2018"  Тогда
			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ПолучитьСведенияОПоказателяхОтчетаОСредствахПоОбязательномуСтрахованиюФормаОтчета2018(ПоказателиОтчета);
		// 4D:ERP для Беларуси Екатерина, 05.07.2021 17:07:10 
		// №29659
		// {
		ИначеЕсли ИДРедакцииОтчета = "ФормаОтчета2020" Тогда
			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ПолучитьСведенияОПоказателяхОтчетаОСредствахПоОбязательномуСтрахованиюФормаОтчета2020(ПоказателиОтчета);
		// }
		// 4D
			
		КонецЕсли;
		
		
	ИначеЕсли ИДОтчета = "РегламентированныйОтчетОСредствахФондаСоциальнойЗащиты_Локализация" Тогда
		
		Если ИДРедакцииОтчета = "ФормаОтчета2018" ИЛИ ИДРедакцииОтчета = "ФормаОтчета2019" ИЛИ ИДРедакцииОтчета = "ФормаОтчета2021" Тогда
			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ПолучитьСведенияОПоказателяхОтчетаОСредствахФондаСоциальнойЗащитыФормаОтчета2018(ПоказателиОтчета);
			
		КонецЕсли;
		
	// 4D:ERP для Беларуси, ВладимирР, 18.04.2022 10:25:07 
	// №27553, №33088
	// {
	ИначеЕсли ИДОтчета = "РегламентированныйОтчетСтатистикаФорма1ТКадры_Локализация" Тогда
		Если ИДРедакцииОтчета = "ФормаОтчета2020" Тогда
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ПолучитьСведенияОПоказателяхОтчетаОЧисленностиСоставеОбученииКадровФормаОтчета2020(ПоказателиОтчета);
		КонецЕсли;
	// }
	// 4D
	
	Иначе
		
		ЗарплатаКадры.ЗаполнитьПоказателиРегламентированногоОтчета(ПоказателиОтчета, ИДОтчета, ИДРедакцииОтчета);
		
	КонецЕсли;
	
	Возврат ПоказателиОтчета;
	
КонецФункции

// Функция возвращает признак реализации настройки автозаполняемых показателей регл. отчета.
//
// Параметры:
// 	ИДОтчета         - идентификатор отчета;
//	ИДРедакцииОтчета - идентификатор редакции формы отчета;
//
// Пример:
//  Если ИДОтчета = "РегламентированныйОтчетСтатистикаФормаП1" Тогда
//		Если ИДРедакцииОтчета = "ФормаОтчета2010Кв1" Тогда	
//			Возврат Истина;
//		КонецЕсли;
//	КонецЕсли;
//	Возврат Ложь;
//
Функция РеализованаНастройкаАвтозаполняемыхПоказателейРеглОтчета(ИДОтчета, ИДРедакцииОтчета) Экспорт
	Возврат Истина;
КонецФункции

// Процедура заполняет переданную в виде контейнера структуру данных отчета.
//
// Пример:
//	Если ИДОтчета = "РегламентированныйОтчетРСВ1" Тогда
//		Если ИДРедакцииОтчета = "ФормаОтчета2011Кв1" Тогда	
//			Контейнер.Раздел30.П000300030103 = 100;
//			Контейнер.Раздел41.Добавить();
//		КонецЕсли;
//	КонецЕсли;
//
Процедура ЗаполнитьОтчет(ИДОтчета, ИДРедакцииОтчета, ПараметрыОтчета, Контейнер) Экспорт
	
	ПоказателиОтчета = Новый Структура;
	
	Если ИДОтчета = "РегламентированныйОтчетБаланс_Локализация" 
		ИЛИ ИДОтчета = "РегламентированныйОтчетПриложение2_Локализация"
		ИЛИ ИДОтчета = "РегламентированныйОтчетПриложение3_Локализация"
		ИЛИ ИДОтчета = "РегламентированныйОтчетПриложение4_Локализация"
		Тогда	
		
		Если ИДРедакцииОтчета = "ФормаОтчета2017" Тогда
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ЗаполнитьОтчетБухОтчетностьФормаОтчета2017(ПараметрыОтчета, Контейнер);   
		КонецЕсли;		
	КонецЕсли;
	
    Если ИДОтчета = "РегламентированныйОтчетНалоговаяДекларацияПоНДС_Локализация" Тогда		
		Если  ИДРедакцииОтчета = "ФормаОтчета2018" ИЛИ ИДРедакцииОтчета = "ФормаОтчета052018" Тогда			
			УчетНДС_Локализация.ЗаполнитьОтчетНДСФормаОтчета2018(ПараметрыОтчета, Контейнер);
		ИначеЕсли  ИДРедакцииОтчета = "ФормаОтчета2019" Тогда			
			УчетНДС_Локализация.ЗаполнитьОтчетНДСФормаОтчета2019(ПараметрыОтчета, Контейнер); 
		ИначеЕсли  ИДРедакцииОтчета = "ФормаОтчета072019" ИЛИ ИДРедакцииОтчета = "ФормаОтчета2020" Тогда			
			УчетНДС_Локализация.ЗаполнитьОтчетНДСФормаОтчета072019(ПараметрыОтчета, Контейнер); 		
		КонецЕсли; 		
	КонецЕсли;
	
	Если ИДОтчета = "РегламентированныйОтчетНалогНаНедвижимость_Локализация" Тогда  		
		Если ИДРедакцииОтчета = "ФормаОтчета2018" Тогда 	 			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ЗаполнитьОтчетНалогНаНедвижимостьФормаОтчета2018(ПараметрыОтчета, Контейнер);  
		ИначеЕсли ИДРедакцииОтчета = "ФормаОтчета2019" Тогда 	 			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ЗаполнитьОтчетНалогНаНедвижимостьФормаОтчета2019(ПараметрыОтчета, Контейнер);  
	    КонецЕсли; 		
	КонецЕсли;

	Если ИДОтчета = "РегистрНалоговогоУчета1_Локализация" Тогда  		
		Если ИДРедакцииОтчета = "ФормаОтчета072016" Тогда 			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ЗаполнитьОтчетРегистрНалоговогоУчета1ФормаОтчета2016(ПараметрыОтчета, Контейнер); 			
		КонецЕсли;    		
	КонецЕсли;	
	
	Если ИДОтчета = "РегистрНалоговогоУчета2_Локализация" Тогда  		
		Если ИДРедакцииОтчета = "ФормаОтчета072016" Тогда				
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ЗаполнитьОтчетРегистрНалоговогоУчета2ФормаОтчета2016(ПараметрыОтчета, Контейнер);  		
		КонецЕсли; 		
	КонецЕсли;	
	
	Если ИДОтчета = "РегистрНалоговогоУчета3_Локализация" Тогда 		
		Если ИДРедакцииОтчета = "ФормаОтчета072016" Тогда 			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ЗаполнитьОтчетРегистрНалоговогоУчета3ФормаОтчета2016(ПараметрыОтчета, Контейнер); 			
		КонецЕсли;  		
	КонецЕсли;	
	
	Если ИДОтчета = "РегистрНалоговогоУчета4_Локализация" Тогда		
		Если ИДРедакцииОтчета = "ФормаОтчета072016" Тогда			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ЗаполнитьОтчетРегистрНалоговогоУчета4ФормаОтчета2016(ПараметрыОтчета, Контейнер);
		ИначеЕсли ИДРедакцииОтчета = "ФормаОтчета2018" Тогда			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ЗаполнитьОтчетРегистрНалоговогоУчета4ФормаОтчета2018(ПараметрыОтчета, Контейнер); 		
		КонецЕсли; 		
	КонецЕсли;	
	
	Если ИДОтчета = "РегистрНалоговогоУчета5_Локализация" Тогда  		
		Если ИДРедакцииОтчета = "ФормаОтчета072016" Тогда    			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ЗаполнитьОтчетРегистрНалоговогоУчета5ФормаОтчета2016(ПараметрыОтчета, Контейнер); 			
		КонецЕсли;
	КонецЕсли;	
	
	Если ИДОтчета = "РегистрНалоговогоУчета6_Локализация" Тогда   		
		Если ИДРедакцииОтчета = "ФормаОтчета072016"  Тогда      			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ЗаполнитьОтчетРегистрНалоговогоУчета6ФормаОтчета2016(ПараметрыОтчета, Контейнер);  			
		КонецЕсли;
	КонецЕсли;
	
	Если ИДОтчета = "РегламентированныйОтчетОСредствахПоОбязательномуСтрахованию_Локализация" Тогда
		
		Если  ИДРедакцииОтчета = "ФормаОтчета2018" Тогда 			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ЗаполнитьОтчетОСредствахПоОбязательномуСтрахованиюФормаОтчета2018(ПараметрыОтчета, Контейнер); 			
		// 4D:ERP для Беларуси Екатерина, 05.07.2021 17:15:17 
		// №29659
		// {
		ИначеЕсли ИДРедакцииОтчета = "ФормаОтчета2020" Тогда
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ЗаполнитьОтчетОСредствахПоОбязательномуСтрахованиюФормаОтчета2020(ПараметрыОтчета, Контейнер);
		// }
		// 4D
			
		КонецЕсли;
	КонецЕсли;
	
	Если ИДОтчета = "РегламентированныйОтчетОСредствахФондаСоциальнойЗащиты_Локализация" Тогда   		
		Если ИДРедакцииОтчета = "ФормаОтчета2018" Тогда 			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ЗаполнитьОСредствахФондаСоциальнойЗащитыФормаОтчета2018(ПараметрыОтчета, Контейнер);   			
		ИначеЕсли ИДРедакцииОтчета = "ФормаОтчета2019" Тогда 			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ЗаполнитьОСредствахФондаСоциальнойЗащитыФормаОтчета2019(ПараметрыОтчета, Контейнер);   			
		КонецЕсли;
	КонецЕсли;
	
	Если ИДОтчета = "РегламентированныйОтчетНалогНаПрибыль_Локализация" Тогда 		            
		Если ИДРедакцииОтчета = "ФормаОтчета2018" ИЛИ ИДРедакцииОтчета = "ФормаОтчета2019" Тогда 			
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ЗаполнитьОтчетНалогНаПрибыльФормаОтчета2018(ПараметрыОтчета, Контейнер);
		КонецЕсли;
	КонецЕсли;
	
	// 4D:ERP для Беларуси, ВладимирР, 18.04.2022 10:25:07 
	// №27553, №33088
	// {
	Если ИДОтчета = "РегламентированныйОтчетСтатистикаФорма1ТКадры_Локализация" Тогда
		Если ИДРедакцииОтчета = "ФормаОтчета2020" Тогда
			ЗаполнениеБухгалтерскойОтчетностиЛокализацияБел.ЗаполнитьОЧисленностиСоставеОбученииКадровФормаОтчета2020(ПараметрыОтчета, Контейнер);
		КонецЕсли;
	КонецЕсли;
	// }
	// 4D
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Процедуры и функции статистики.

// Автоматически заполняет отчет в фоновом режиме.
//
Процедура ЗаполнитьОтчетВФоне(ПараметрыОтчета, АдресХранилища) Экспорт
	
	// Заполняет переданную во временном хранилище структуру данных отчета
	ЗаполнитьОтчет(ПараметрыОтчета.ИДОтчета, ПараметрыОтчета.ИДРедакцииОтчета, ПараметрыОтчета.ПараметрыОтчета, ПараметрыОтчета.Контейнер);
	Попытка
		ТаблицаРасшифровки = ПолучитьИзВременногоХранилища(ПараметрыОтчета.ПараметрыОтчета.АдресВременногоХранилищаРасшифровки);
	Исключение
		ТаблицаРасшифровки = Неопределено;
	КонецПопытки;
	
	СтруктураДанных = Новый Структура();
	СтруктураДанных.Вставить("Контейнер", ПараметрыОтчета.Контейнер);
	СтруктураДанных.Вставить("ТаблицаРасшифровки", ТаблицаРасшифровки);
	
	ПоместитьВоВременноеХранилище(СтруктураДанных, АдресХранилища);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Процедуры интеграции со списком задач бухгалтера.

// Процедура изменят заголовки кнопок.
//
Процедура НастроитьКнопкиКалендаряБухгалтера(КнопкаСправочникОтчетов = Неопределено, КнопкаКалендарь = Неопределено) Экспорт
			
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Процедуры интерфейса взаимодействия с конфигурациями (библиотеками)
// - потребителями.

// Процедура вызывается при создании на сервере формы регламентированного отчета.
//
// Параметры:
//  ИмяОтчета - Строка - имя отчета в дереве объектов метаданных;
//  ИмяФормы  - Строка - имя формы отчета;
//  Форма     - УправляемаФорма - форма, из которой вызывается процедура.
//
Процедура ПриСозданииФормыРеглОтчета(ИмяОтчета, ИмяФормы, Форма) Экспорт
			
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Служебные процедуры и функции.

// Функция возвращает список используемых в конфигурации регламентированных отчетов.
//
// Пример:
// 	СписокРегламентированныхОтчетов = Новый СписокЗначений;
//	СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетНДС");
//	Возврат СписокРегламентированныхОтчетов;
//
Функция ПолучитьСписокРегламентированныхОтчетов() Экспорт
	
	СписокРегламентированныхОтчетов = Новый СписокЗначений;
	// Бухгалтерская отчетность
	СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетБаланс_Локализация");
	СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетПриложение2_Локализация");
	СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетПриложение3_Локализация");
	СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетПриложение4_Локализация");
	СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетПриложение5_Локализация");
	СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетПриложение6_Локализация");
	
	// Недвижимость, остаточная стоимость
	СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетНалогНаНедвижимость_Локализация");
	
	// Экологические налоги
	СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетНалогЗаВыбросыЗагрязняющихВеществВАтмосферныйВоздух_Локализация");
    СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетНалогЗаИспользованиеПриродныхРесурсов_Локализация");
	СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетНалогЗаСбросыСточныхВодИлиЗагрязняющихВеществВВодныеОбъекты_Локализация");
	СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетНалогЗаХранениеЗахорОтхПроизводства_Локализация");
	
	// Акцизы
	СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетРасчетПоАкцизам_Локализация");
	
	// Доходы
	СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетНалогНаДоходыИностранногоЮридическогоЛица_Локализация");
    СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетНалогНаДоходыОтЛотерДеят_Локализация");

	// НДС
	СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетНалоговаяДекларацияПоНДС_Локализация");
	
	// Налог на прибыль
	СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетНалогНаПрибыль_Локализация");

	// Налоги и отчисления из з/п
    СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетОСредствахФондаСоциальнойЗащиты_Локализация");
    СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетОСредствахПоОбязательномуСтрахованию_Локализация");
	
	// Прочие местные налоги и сборы
    СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетПоСборуСЗаготовителей_Локализация");
    
	// Регистры налогового учета
    СписокРегламентированныхОтчетов.Добавить("РегистрНалоговогоУчета1_Локализация");
	СписокРегламентированныхОтчетов.Добавить("РегистрНалоговогоУчета2_Локализация");
	СписокРегламентированныхОтчетов.Добавить("РегистрНалоговогоУчета3_Локализация");
    СписокРегламентированныхОтчетов.Добавить("РегистрНалоговогоУчета4_Локализация");
	СписокРегламентированныхОтчетов.Добавить("РегистрНалоговогоУчета5_Локализация");
	СписокРегламентированныхОтчетов.Добавить("РегистрНалоговогоУчета6_Локализация"); 
	
	// 4D:ERP для Беларуси, ВладимирР, 18.04.2022 10:25:07 
	// №27553, №33088
	// {
	СписокРегламентированныхОтчетов.Добавить("РегламентированныйОтчетСтатистикаФорма1ТКадры_Локализация");
	// }
	// 4D
	
	Возврат СписокРегламентированныхОтчетов;
	
КонецФункции

// Процедура добавляет в форму назначаемую команду.
//
// Параметры:
//   Форма - форма, в которую добавляется команда.
//   ГруппаФормы - группа формы, в которой будет располагаться команда.
//
Процедура ДобавитьНазначаемуюКоманду(Форма, ГруппаФормы) Экспорт
	
КонецПроцедуры

// Функция возвращает заголовок формы "УправлениеОтчетностью" обработки "ОбщиеОбъектыРеглОтчетности".
// Если функция возвращает пустую строку, то используется заголовок формы, указанный по умолчанию.
//
// Пример:
//  Возврат "";
//
Функция ЗаголовокФормыУправлениеОтчетностью() Экспорт
	
КонецФункции

#Область НоваяФормаРегламентированнойОтчетности

// Процедура переопределяет обработчик подписки на событие "ЗаписьОбъектовРегламентированнойОтчетности*"
//
// Параметры:
//	Ссылка - СправочникСсылка, ДокументСсылка - Ссылка на объект - источник события
//	Отказ  - Булево - Управляет значением параметра "Отказ" вызывающего обработчика подписки на событие
//	СтандартнаяОбработка - Булево - Если СтандартнаяОбработка = Истина, то будут выполнены действия вызывающего обработчика подписки на событие
//
Процедура ЗаписьОбъектовРегламентированнойОтчетности(Ссылка, Отказ, СтандартнаяОбработка) Экспорт
	ЗарплатаКадры.ЗаписьОбъектовРегламентированнойОтчетности(Ссылка, Отказ, СтандартнаяОбработка);	
КонецПроцедуры

// Процедура переопределяет свойства объекта, с которыми он будет отображен в форме Отчетность
// Параметры:
//  СвойстваОбъектов  - ТаблицаЗначений - Таблица, содержащая в себе описания ссылок, переданных в закладке Ссылка таблицы
//		Каждая колонка таблицы соотвествует свойству объекта.
//		Таблица содержит следующие колонки:
//			- ДатаСоздания - Дата - Дата создания объекта
//			- Наименование - Строка - Наименование объекта, с которым он будет отображаться в форме Отчетность
//			- КодКонтролирующегоОргана - Строка - Код контролирующего органа, в который отправляется отчетности по ТКС
//			- ДатаНачала - Дата - Дата начала периода. Пустая, если дата начала периода отсуствует
//			- ДатаОкончания - Дата - Дата окончания периода. Пустая, если дата окончания периода отсуствует 
//			- ВариантОтчета - Строка - Если первичный, то указываем "П", если нет признака первичный/корректирующий - "-", иначе - "К/" + <номер корректировки>
//			- ПометкаУдаления - Булево - Истина, если объект помечен на удаление 
//			- Организация - СправочникСсылка.Организации - Организация, которой принадлежит объект
//			- Комментарий - Строка - Комментарий из объекта
//
Процедура ОпределитьСвойстваОбъектовДляОтображенииВФормеОтчетность(СвойстваОбъектов) Экспорт
	
	КонтролируемыеСделки.ОпределитьСвойстваОбъектовДляОтображенииВФормеОтчетность(СвойстваОбъектов);
	
	ЗарплатаКадры.ОпределитьСвойстваОбъектовДляОтображенииВФормеОтчетность(СвойстваОбъектов);
	
	МассивЖурналовСчетовФактур = Новый Массив();
	Для Каждого СвойстваОбъекта ИЗ СвойстваОбъектов Цикл
		Если ТипЗнч(СвойстваОбъекта.Ссылка) = Тип("ДокументСсылка.ЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде") Тогда
			МассивЖурналовСчетовФактур.Добавить(СвойстваОбъекта.Ссылка);
		КонецЕсли;
	КонецЦикла;
	
	Если МассивЖурналовСчетовФактур.Количество() > 0 Тогда
		Запрос = Новый Запрос();
		Запрос.Параметры.Вставить("МассивЖурналовСчетовФактур", МассивЖурналовСчетовФактур);
		Запрос.Текст =
		"ВЫБРАТЬ
		|	ЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде.Ссылка,
		|	ЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде.Дата КАК ДатаСоздания,
		|	ЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде.Организация КАК Организация,
		|	0 КАК НомерКорректировки,
		|	ЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде.НалоговыйПериод КАК ДатаНачала,
		|	КОНЕЦПЕРИОДА(ЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде.НалоговыйПериод, КВАРТАЛ) КАК ДатаОкончания,
		|	ЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде.ПометкаУдаления,
		|	ЕСТЬNULL(ЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде.Организация.РегистрацияВНалоговомОргане.Код, """") КАК КодКонтролирующегоОргана
		|ИЗ
		|	Документ.ЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде КАК ЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде
		|ГДЕ
		|	ЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде.Ссылка В(&МассивЖурналовСчетовФактур)";
		Выборка = Запрос.Выполнить().Выбрать();
		Пока Выборка.Следующий() Цикл
			
			СтрокиОбъектов = СвойстваОбъектов.НайтиСтроки(Новый Структура("Ссылка", Выборка.Ссылка));
			Для Каждого СтрокаОбъекта ИЗ СтрокиОбъектов Цикл
				ЗаполнитьЗначенияСвойств(СтрокаОбъекта, Выборка);
				СтрокаОбъекта.Наименование = НСтр("ru = 'Журнал учета счетов-фактур для передачи в электронном виде'");
				СтрокаОбъекта.ВариантОтчета = Формат(Выборка.НомерКорректировки, "ЧЦ=3; ЧГ=0");
			КонецЦикла;
			
		КонецЦикла;
		
	КонецЕсли;
	
	МассивЗаявленийОВвозеТоваров = Новый Массив();
	Для Каждого СвойстваОбъекта ИЗ СвойстваОбъектов Цикл
		Если ТипЗнч(СвойстваОбъекта.Ссылка) = Тип("ДокументСсылка.ЗаявлениеОВвозеТоваров") Тогда
			МассивЗаявленийОВвозеТоваров.Добавить(СвойстваОбъекта.Ссылка);
		КонецЕсли;
	КонецЦикла;
	
	Если МассивЗаявленийОВвозеТоваров.Количество() > 0 Тогда
		Запрос = Новый Запрос();
		Запрос.Параметры.Вставить("МассивЗаявленийОВвозеТоваров", МассивЗаявленийОВвозеТоваров);
		Запрос.Текст =
		"ВЫБРАТЬ
		|	ЗаявлениеОВвозеТоваров.Ссылка,
		|	ЗаявлениеОВвозеТоваров.Дата КАК ДатаСоздания,
		|	ЗаявлениеОВвозеТоваров.Организация КАК Организация,
		|	НАЧАЛОПЕРИОДА(ЗаявлениеОВвозеТоваров.Дата, МЕСЯЦ) КАК ДатаНачала,
		|	КОНЕЦПЕРИОДА(ЗаявлениеОВвозеТоваров.Дата, МЕСЯЦ) КАК ДатаОкончания,
		|	ЗаявлениеОВвозеТоваров.ПометкаУдаления,
		|	ЗаявлениеОВвозеТоваров.СтруктураРеквизитовВыгрузки,
		|	""-"" КАК ВариантОтчета,
		|	ЗаявлениеОВвозеТоваров.Комментарий КАК Комментарий
		|ИЗ
		|	Документ.ЗаявлениеОВвозеТоваров КАК ЗаявлениеОВвозеТоваров
		|ГДЕ
		|	ЗаявлениеОВвозеТоваров.Ссылка В(&МассивЗаявленийОВвозеТоваров)";
		
		Выборка = Запрос.Выполнить().Выбрать();
		Пока Выборка.Следующий() Цикл
			
			СтрокиОбъектов = СвойстваОбъектов.НайтиСтроки(Новый Структура("Ссылка", Выборка.Ссылка));
			Для Каждого СтрокаОбъекта ИЗ СтрокиОбъектов Цикл
				
				ЗаполнитьЗначенияСвойств(СтрокаОбъекта, Выборка);
				СтрокаОбъекта.Наименование = НСтр("ru = 'Заявление о ввозе товаров (на основании поступления)'");
				
				СтруктураРеквизитовВыгрузки = Выборка.СтруктураРеквизитовВыгрузки.Получить();
		
				Если ТипЗнч(СтруктураРеквизитовВыгрузки) = Тип("Структура") И СтруктураРеквизитовВыгрузки.Свойство("КодИФНС") Тогда
					СтрокаОбъекта.КодКонтролирующегоОргана = СтруктураРеквизитовВыгрузки.КодИФНС;
				Иначе
					СтрокаОбъекта.КодКонтролирующегоОргана = "";
				КонецЕсли;
			
			КонецЦикла;
			
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры

// Определяет свойства, касающиеся общих свойств объектов конфигураций-потребителей для отображения в форме Отчетность
// и возможности содания новый объектов из формы Отчетность
//
// Параметры:
//  ТаблицаОписания  - ТаблицаЗначений - Таблица, содержащая в себе описания объектов конфигураций-потребителей
//		Каждая колонка таблицы соотвествует свойству объекта.
//		Таблица содержит следующие колонки:
//			- Наименование - Строка - Наименование вида отчета, как оно должно отображаться в форме создания отчета
//			- ТипОбъекта - Тип - Тип описываемого объекта
//			- ВидКонтролирующегоОргана - Перечисления.ТипыКонтролирующихОрганов - контролирующий орган, в который сдается отчетности
//			- ГруппаВДереве - Строка - Определяет имя группы, в которой будет отображаться объект в случае иерархическкого отображения списка объектов
//			- ВидДокумента - Перечисления.СтраницыЖурналаОтчетность - Определяет страницу формы Отчетность, на которой будет отображаться объект
//				Может принимать одно из следующих значений:
//						Перечисления.СтраницыЖурналаОтчетность.Уведомления
//						Перечисления.СтраницыЖурналаОтчетность.Отчеты
//			- НеОтправляетсяВКонтролирующийОрган - Булево - Истина, если объект не отправляется в контролирующий орган по ТКС
//			- ЯвляетсяАктуальным - Булево - Определяет, можно ли создавать новые объекты данного вида
//			- ИмяОсновногоМакетаДляПечати - Строка - имя макета, который будет использоваться для печати по умолчанию
//			- ВидЭлектронногоПредставления - СправочникСсылка.ВидыОтправляемыхДокументов - вид электронного представления (справочник 
// ЭлектронныеПредставленияРегламентированныхОтчетов), соответствующий данному типу объекта. Неопределено, если такого вида отправляемого документа не существует
//
Процедура ОпределитьТаблицуОписанияОбъектовРегламентированнойОтчетности(ТаблицаОписания) Экспорт
	
	КонтролируемыеСделки.ОпределитьТаблицуОписанияОбъектовРегламентированнойОтчетности(ТаблицаОписания);
	ЗарплатаКадры.ОпределитьТаблицуОписанияОбъектовРегламентированнойОтчетности(ТаблицаОписания);
	
	ОписаниеОбъекта = ТаблицаОписания.Добавить();
	ОписаниеОбъекта.ТипОбъекта = Тип("ДокументСсылка.ЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде");
	ОписаниеОбъекта.ВидКонтролирующегоОргана = Перечисления.ТипыКонтролирующихОрганов.ФНС;
	ОписаниеОбъекта.ГруппаВДереве = "Налоговая отчетность";
	ОписаниеОбъекта.ВидДокумента = Перечисления.СтраницыЖурналаОтчетность.Отчеты;
	ОписаниеОбъекта.НеОтправляетсяВКонтролирующийОрган = Ложь;
	ОписаниеОбъекта.ЯвляетсяАктуальным = Истина;
	ОписаниеОбъекта.ИмяОсновногоМакетаДляПечати = "";
	
	ОписаниеОбъекта = ТаблицаОписания.Добавить();
	ОписаниеОбъекта.Наименование = НСтр("ru='Заявление о ввозе товаров (на основании поступления)'");
	ОписаниеОбъекта.ТипОбъекта = Тип("ДокументСсылка.ЗаявлениеОВвозеТоваров");
	ОписаниеОбъекта.ВидКонтролирующегоОргана = Перечисления.ТипыКонтролирующихОрганов.ФНС;
	ОписаниеОбъекта.ГруппаВДереве = "Налоговая отчетность";
	ОписаниеОбъекта.ВидДокумента = Перечисления.СтраницыЖурналаОтчетность.Отчеты;
	ОписаниеОбъекта.НеОтправляетсяВКонтролирующийОрган = Ложь;
	ОписаниеОбъекта.ЯвляетсяАктуальным = Истина;
	ОписаниеОбъекта.ИмяОсновногоМакетаДляПечати = "";
	
КонецПроцедуры

// Дополняет дерево регламентированных отчетов для использования в форме выбора видов отчетов,
// которая вызывается из формы "Отчетность" при нажатии на кнопку "Создать" на вкладке "Отчеты"
//
// Параметры:
//  ДеревоОтчетов - ДеревоЗначений - Дерево содержит описания видов отчетов, дополняемые из конфигураций-потребителей
//		Деревоз значений содержит следующие колонки:
//			- Наименование - Строка - Наименование вида отчета, как оно должно отображаться в форме
//			- ПолноеИмя - Строка - Полное имя (как в метаданных) документа или справочник для хранения данных отчета,
//				например: "Документ.РегламентированныйОтчет" или "Документ.УниверсальныйРегламентированныйОтчет" (для БГУ)
//			- Ссылка - СправочникСсылка - Ссылка на элемент справочника, содержащий описание вида отчета (если существует),
//				например, ссылка на элемент справочника "ВидыОтчетов" в БГУ
//			- ТипПолучателя - Перечисления.ТипыКонтролирующихОрганов - контролирующий орган, в который сдается отчетность
//			- Категория - Строка - Наименование категории отчета или название группы, в которую входит отчет (для группировки в форме)
//			- ЭтоГруппа - Булево - Если элемент дерева должен отображаться в форме как группа
//
Процедура ДобавитьВДеревоРегламентированныхОтчетовДругиеОтчеты(ДеревоОтчетов) Экспорт
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ФильтрацияИзбранного

// Заполняется таблица избранного по правилам, определяемым в самом потребителе.
//
// Параметры:
// ПараметрыОтбора - структура, содержащая значения:
//   Организация - СправочникСсылка.Организации - ссылка на элемент справочника Организации.
//   РанееСозданныеОтчеты - пустая таблица избранного с колонками:
//     * Наименование - Строка - наименование отчета.
//     * ПолноеИмя    - Строка - имя объекта отчета.
//     * Ссылка       - ДокументСсылка.РегламентированныйОтчет или иной документ, который приравнен к отчетам.
//     * ЭтоГруппа    - Булево.
//
Процедура ЗаполнитьИзбранноеДоступнымиОтчетами(ПараметрыОтбора) Экспорт
	
КонецПроцедуры

// Процедура возвращает объект налогообложения УСН.
//
// Параметры:
//  НачалоПериода - Дата - начало периода отчета (СтруктураРеквизитовФормы.мДатаНачалаПериодаОтчета).
//  КонецПериода  - Дата - окончание периода отчета (СтруктураРеквизитовФормы.мДатаКонцаПериодаОтчета).
//  Организация   - СправочникСсылка - ссылка на элемент справочника "Организации".
//  ОбъектНалУСН  - Число или Неопределено - объект налогообложения УСН.
//                    1 - объект налогообложения - доходы;
//                    2 - объект налогообложения - доходы, уменьшенные на величину расходов;
//                    Неопределено - не применялась УСН за период или в конфигурации не реализован
//                                   функционал получения объекта налогообложения УСН за период.
//
Процедура ПолучитьОбъектНалогообложенияУСНЗаПериод(Организация, НачалоПериода, КонецПериода, ОбъектНалУСН) Экспорт
	
КонецПроцедуры

// Процедура переопределяет параметры отчета
//
// Параметры:
//  Отчет                      – Структура("Наименование,ПолноеИмя,Ссылка").
//  ОрганизацияОтчета          – Ссылка или Неопределено, организация отчета.
//  ДатаНачалаПериодаОтчета    – Дата или Неопределено, дата начала периода отчета.
//  ДатаОкончанияПериодаОтчета – Дата или Неопределено, дата окончания периода отчета.
//  ПараметрыОткрытия          - Структура, переопределяемые параметры открытия формы отчета.
//
Процедура ПереопределитьПараметрыОтчета(Отчет, ОрганизацияОтчета, ДатаНачалаПериодаОтчета, ДатаОкончанияПериодаОтчета, ПараметрыОткрытия) Экспорт
			
КонецПроцедуры

// Заполняет контейнер данными бухгалтерской отчетности и помещает его во временное хранилище.
Процедура ПолучитьПоказателиАвтозаполненияБухгалтерскойОтчетности(ПараметрыОтчета, Контейнер) Экспорт
	
КонецПроцедуры

// Дополняет функционал обработчика "ПриСозданииНаСервере" общей формы "Регламентированная отчетность",
// здесь можно добавлять новые конопки в командные панели, расширять возможности интерфейса.
//
// Параметры:
//  Форма - УправляемаяФорма, общая форма "Регламентированная отчетность".
//
// Пример:
//  ГруппаКоманды = Форма.Элементы["ОтчетыГруппа3"];
//  
//  ИмяКоманды = "ОтчетыПоказатьДополнительнуюИнформацию";
//  Команда = Форма.Команды.Добавить(ИмяКоманды);
//  Команда.Подсказка = НСтр("ru = 'Показать дополнительную информацию'");
//  Команда.Отображение = ОтображениеКнопки.КартинкаИТекст;
//  Команда.Картинка = БиблиотекаКартинок.Лупа;
//  Команда.Действие = "Подключаемый_ВыполнитьНазначаемуюКоманду";
//  
//  Кнопка = Форма.Элементы.Добавить(ИмяКоманды, Тип("КнопкаФормы"), ГруппаКоманды);
//  Кнопка.Заголовок = НСтр("ru = 'Инфо'");
//  Команда.ИзменяетСохраняемыеДанные = Ложь;
//  Кнопка.ИмяКоманды = ИмяКоманды;
//
Процедура ФормаРегламентированнойОтчетности_ПриСозданииНаСервере(Форма) Экспорт
	
	Элементы = Форма.Элементы;
	Форма.Заголовок = "Регламентированная отчетность";
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ГиперссылкаУведомления", "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ГиперссылкаПисьма", "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ГиперссылкаСверки", "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ГиперссылкаПисьма", "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ГиперссылкаЕГРЮЛ", "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ГиперссылкаВходящие", "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ГиперссылкаСервисы", "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ГиперссылкаНастройки", "Видимость", Ложь);  
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ОтчетыГруппаЗагрузитьИзФайла", "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ОтчетыГруппаПроверка", "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ОтчетыОтправитьОтчет", "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ОтчетыВыгрузить", "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "СтатусГосорган", "Видимость", Ложь);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ОтчетыГруппаПечать", "Видимость", Ложь);
	
КонецПроцедуры
#КонецОбласти

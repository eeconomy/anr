﻿#Область ПрограммныйИнтерфейс

Функция НоменклатураСодержитДМ(Номенклатура = Неопределено) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ВидыНоменклатуры.СодержитДрагоценныеМатериалы КАК СодержитДрагоценныеМатериалы
		|ИЗ
		|	Справочник.ВидыНоменклатуры КАК ВидыНоменклатуры
		|ГДЕ
		|	ВидыНоменклатуры.Ссылка В
		|		(ВЫБРАТЬ
		|			Номенклатура.ВидНоменклатуры КАК Ссылка
		|		ИЗ
		|			Справочник.Номенклатура КАК Номенклатура
		|		ГДЕ
		|			Номенклатура.Ссылка = &Номенклатура)";
	
	Запрос.УстановитьПараметр("Номенклатура", Номенклатура);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		Возврат ВыборкаДетальныеЗаписи.СодержитДрагоценныеМатериалы;
	КонецЦикла;
	
	Возврат Ложь;
	
КонецФункции

Функция ПолучитьЦенуДрагоценногоМатериала(ДрагоценныйМатериал, Период) Экспорт
	
	Цена = 0;
	
	Если ЗначениеЗаполнено(ДрагоценныйМатериал) Тогда
	
		Запрос = Новый Запрос;
		Запрос.Текст = "ВЫБРАТЬ
		               |	ЦеныДрагоценныхМатериаловСрезПоследних.Цена КАК Цена
		               |ИЗ
		               |	РегистрСведений.ЦеныДрагоценныхМатериалов.СрезПоследних(&Период, ДрагоценныйМатериал = &ДрагоценныйМатериал) КАК ЦеныДрагоценныхМатериаловСрезПоследних";
		
		Запрос.УстановитьПараметр("Период", ?(ЗначениеЗаполнено(Период), Период, ТекущаяДатаСеанса()));
		Запрос.УстановитьПараметр("ДрагоценныйМатериал", ДрагоценныйМатериал);
		
		РезультатЗапроса = Запрос.Выполнить();
		
		Если Не РезультатЗапроса.Пустой() Тогда
		
			Выборка = РезультатЗапроса.Выбрать();
			
			Если Выборка.Следующий() Тогда
				Цена = Выборка.Цена;
			КонецЕсли;
			
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат Цена;
	
КонецФункции

#КонецОбласти
﻿
#Область ПрограммныйИнтерфейс

// Переопределение настроек присоединенных файлов.
//
// см. РаботаСФайламиПереопределяемый.ПриОпределенииНастроек()
//
Процедура ПриОпределенииНастроек(Настройки) Экспорт
	
	
КонецПроцедуры

// Позволяет переопределить справочники хранения файлов по типам владельцев.
// 
// см. РаботаСФайламиПереопределяемый.ПриОпределенииСправочниковХраненияФайлов()
//
Процедура ПриОпределенииСправочниковХраненияФайлов(ТипВладелецФайла, ИменаСправочников) Экспорт
	
	// ЭлектронныеСчетаФактуры
	Если ТипВладелецФайла = Тип("ДокументСсылка.СчетФактураВыданный") 
			ИЛИ ТипВладелецФайла = Тип("ДокументСсылка.СчетФактураПолученный")  Тогда
			
		ИменаСправочников.Вставить("ЭДПрисоединенныеФайлы", Ложь);
		
	КонецЕсли;	
	// Конец ЭлектронныеСчетаФактуры
	
КонецПроцедуры

#КонецОбласти
﻿////////////////////////////////////////////////////////////////////////////////
// Процедуры подсистемы управления ремонтами.
// 
////////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

// Производит запись движений в регистр "ПериодыАктуальностиОбъектовЭксплуатации"
//
// Параметры:
// 		ДополнительныеСвойства - Структура - Структура дополнительных свойств объекта документа
// 		Движения - КоллекцияДвижений - Коллекция движения объекта документа
// 		Отказ - Булево - Возвращаемый параметр, признак ошибки при выполнении процедуры.
//
Процедура ОтразитьПериодыАктуальностиОбъектовЭксплуатации(ДополнительныеСвойства, Движения, Отказ) Экспорт
	
	Таблица = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаПериодыАктуальностиОбъектовЭксплуатации;
	
	Если Отказ ИЛИ Таблица.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	ДвиженияПериоды = Движения.ПериодыАктуальностиОбъектовЭксплуатации;
	ДвиженияПериоды.Записывать = Истина;
	ДвиженияПериоды.Загрузить(Таблица);
	
КонецПроцедуры

// Функция возвращает текстовое представление остаточного ресурса
//
// Параметры:
// 		КоличествоДней - Число - Количество дней.
//
// Возвращаемое значение:
// 		Строка - Строковое представление количества дней.
//
Функция ОстаточныйРесурсПоВремениСтрокой(КоличествоДней) Экспорт
	
	Исчисление = "";
	Количество = 0;
	
	Если Не ЗначениеЗаполнено(КоличествоДней) Или КоличествоДней <= 0 Тогда
		Возврат "";
	ИначеЕсли КоличествоДней > 370 Тогда
		Исчисление = НСтр("ru = 'год,года,лет';
							|en = 'year, year, years'");
		Количество = Окр(КоличествоДней/365, 1);
	ИначеЕсли КоличествоДней > 31 Тогда
		Исчисление = НСтр("ru = 'месяц,месяца,месяцев';
							|en = 'month, month, months'");
		Количество = Окр(КоличествоДней/31, 1);
	Иначе
		Исчисление = НСтр("ru = 'день,дня,дней';
							|en = 'day,day,days'");
		Количество = Окр(КоличествоДней, 1);
	КонецЕсли;
	
	СтруктураИсчисления = СтроковыеФункцииКлиентСервер.РазложитьСтрокуВМассивПодстрок(Исчисление);
	
	ЧислоПервыхДвухЗнаков = Цел(Количество) - Цел(Количество/100)*100;
	ЧислоПервогоЗнака = Цел(Количество) - Цел(Количество/10)*10;
	
	Если (ЧислоПервыхДвухЗнаков>10 И ЧислоПервыхДвухЗнаков<15)
		Или ЧислоПервогоЗнака=0 Или ЧислоПервогоЗнака>4 Тогда
		
		Возврат Формат(Количество, "ЧДЦ=1") + " " + СтруктураИсчисления[2];
		
	ИначеЕсли ЧислоПервогоЗнака=1 Тогда
		
		Возврат Формат(Количество, "ЧДЦ=1") + " " + СтруктураИсчисления[0];
		
	КонецЕсли;
	
	Возврат Формат(Количество, "ЧДЦ=1") + " " + СтруктураИсчисления[1];
	
КонецФункции

#КонецОбласти

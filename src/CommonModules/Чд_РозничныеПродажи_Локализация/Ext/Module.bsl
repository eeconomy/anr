﻿

#Область Локализация

Процедура ПересчитатьЦенуСУчетомСкидокВСтрокеТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения) Экспорт 

	ИмяКоличества = Неопределено;
	ТекущаяСтрокаСкидка = 0;
	
	Если СтруктураДействий.Свойство("ПересчитатьСуммуРучнойСкидки", ИмяКоличества) 
		ИЛИ СтруктураДействий.Свойство("ПересчитатьСуммуСУчетомАвтоматическойСкидки")
		ИЛИ СтруктураДействий.Свойство("ПересчитатьСуммуСУчетомРучнойСкидки") Тогда 
		
		Если НЕ ЗначениеЗаполнено(ИмяКоличества) Тогда
			ИмяКоличества = "КоличествоУпаковок";
		КонецЕсли;
		
		Если ТипЗнч(ТекущаяСтрока) = Тип("Структура") Тогда
			Если Не ТекущаяСтрока.Свойство(ИмяКоличества) Тогда
				ИмяКоличества = "Количество";
			КонецЕсли;	
		Иначе
			Попытка
				Если Не ТекущаяСтрока.Свойство(ИмяКоличества) Тогда
					ИмяКоличества = "Количество";
				КонецЕсли;
			
			Исключение
			
			КонецПопытки;
			
		КонецЕсли;	
			
		Сумма           = ТекущаяСтрока.Цена * ТекущаяСтрока[ИмяКоличества];
		СуммаСкидки     = 0;
		СуммаСкидкиЦены = 0;
							
		Если СтруктураДействий.Свойство("ПересчитатьСуммуСУчетомАвтоматическойСкидки") Тогда
			СуммаСкидки = ТекущаяСтрока[ИмяКоличества] * ТекущаяСтрока.Цена * ТекущаяСтрока.ПроцентАвтоматическойСкидки / 100;
			СуммаСкидкиЦены = ТекущаяСтрока.Цена * ТекущаяСтрока.ПроцентАвтоматическойСкидки / 100;
		КонецЕсли;
		
		Если ТипЗнч(ТекущаяСтрока) = Тип("Структура") Тогда
			Если ТекущаяСтрока.Свойство("Скидка") И ТекущаяСтрока.Скидка <> Неопределено Тогда
				ТекущаяСтрокаСкидка = ТекущаяСтрока.Скидка;
			КонецЕсли;	
		Иначе
			Попытка
				Если ТекущаяСтрока.Скидка <> 0 ТОгда
					ТекущаяСтрокаСкидка = ТекущаяСтрока.Скидка;	
				КонецЕсли;	
			Исключение
			     
			КонецПопытки;
				
		КонецЕсли;	
			
		Если ТекущаяСтрокаСкидка = 0  Тогда
			СуммаСкидки = СуммаСкидки + ТекущаяСтрока[ИмяКоличества] * ТекущаяСтрока.Цена * ТекущаяСтрока.ПроцентРучнойСкидки / 100;
			СуммаСкидкиЦены = СуммаСкидкиЦены + ТекущаяСтрока.Цена * ТекущаяСтрока.ПроцентРучнойСкидки / 100;
		Иначе
			СуммаСкидки = СуммаСкидки + ТекущаяСтрокаСкидка * ТекущаяСтрока[ИмяКоличества]; 
		КонецЕсли; 
				
		ТекущаяСтрока.Сумма = Сумма - Окр(СуммаСкидки, 2);
		
		ЕстьЦенаСоСкидкой = Ложь;
		
		Попытка
			ЕстьЦенаСоСкидкой = ТекущаяСтрока.Свойство("ЦенаСоСкидкой");
		Исключение
		    
		КонецПопытки;
		
		Если НЕ ЕстьЦенаСоСкидкой Тогда
			Попытка
				ЕстьЦенаСоСкидкой = ТипЗнч(ТекущаяСтрока.ЦенаСоСкидкой) = Тип("Число");
			
			Исключение
			
			КонецПопытки;
			
		КонецЕсли; 
		
		Если ЕстьЦенаСоСкидкой Тогда
			Если СуммаСкидкиЦены <> 0 Тогда
				ТекущаяСтрока.ЦенаСоСкидкой = Окр(ТекущаяСтрока.Цена - СуммаСкидкиЦены, 2);	
			ИначеЕсли ТекущаяСтрокаСкидка <> 0   Тогда
				ТекущаяСтрока.ЦенаСоСкидкой = ТекущаяСтрока.Цена - ТекущаяСтрокаСкидка;	
			Иначе 
				ТекущаяСтрока.ЦенаСоСкидкой = 0;
			КонецЕсли;
			
			Если ТекущаяСтрокаСкидка <> 0 и ТекущаяСтрока.ЦенаСоСкидкой <> 0 Тогда
				ТекущаяСтрока.Скидка = Окр(ТекущаяСтрокаСкидка, 2);
				ТекущаяСтрока.ЦенаСоСкидкой = Окр(ТекущаяСтрока.ЦенаСоСкидкой, 2);		
			КонецЕсли;
		КонецЕсли; 	  		
	КонецЕсли;
		
КонецПроцедуры

Процедура ЗаполнитьРозничныеСтавкиНДС(ЭтаФорма, Объект) Экспорт

	Если ЭтаФорма["ЭтоРозничныйСклад"] И ЭтаФорма["РассчитыватьРеквизитыРозницы"] И  Не ЗначениеЗаполнено(Объект.Ссылка)  Тогда
		СтруктураДействий = Новый Структура;
		СтруктураДействий.Вставить("ЗаполнитьРозничнуюСтавкуНДС");
		СтруктураДействий.Вставить("РассчитатьРозничнуюЦену", Новый Структура("ИмяДокумента,ЦенаВключаетНДС, ВидЦен","Поступление", Объект.ЦенаВключаетНДС, ЭтаФорма["РозничныйВидЦен"]));
		Чд_РозничныеПродажи_Локализация.ЗаполнитьРозничныеСтавкиВТабличнойЧасти(Объект.Товары, Объект.Склад,Объект.Дата,  СтруктураДействий);
	КонецЕсли;	

КонецПроцедуры
 
Процедура ЗаполнитьСтруктуруТорговыеНадбавкиРозничныйНДС(Объект, ЭтаФорма, СтруктураДействий) Экспорт

	Если ЭтаФорма.РассчитыватьРеквизитыРозницы И ЭтаФорма.ЭтоРозничныйСклад Тогда
		СтруктураДействий.Вставить("ЗаполнитьСтавкуТорговойНадбавки", Новый Структура("Склад,Дата",Объект.Склад, Объект.Дата));
		СтруктураДействий.Вставить("ЗаполнитьРозничнуюСтавкуНДС");
		СтруктураДействий.Вставить("РассчитатьРозничнуюЦену", Новый Структура("ИмяДокумента, ЦенаВключаетНДС, ВидЦен","Поступление", Объект.ЦенаВключаетНДС, ЭтаФорма["РозничныйВидЦен"]));
    КонецЕсли;

КонецПроцедуры

#КонецОбласти 


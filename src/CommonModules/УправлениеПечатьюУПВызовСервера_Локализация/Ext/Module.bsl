﻿
#Область СлужебныеПроцедурыИФункции

Процедура КомплектПечатныхФорм(КомплектПечатныхФорм) Экспорт
	
	РегистрыСведений.НастройкиПечатиОбъектов.ДобавитьПечатнуюФормуВКомплект(КомплектПечатныхФорм, "ТН_Вертикальная",
		НСтр("ru = 'ТН-2 (вертикальная)'"), 0);
	РегистрыСведений.НастройкиПечатиОбъектов.ДобавитьПечатнуюФормуВКомплект(КомплектПечатныхФорм, "ТН_Горизонтальная",
		НСтр("ru = 'ТН-2 (горизонтальная)'"), 0);
	РегистрыСведений.НастройкиПечатиОбъектов.ДобавитьПечатнуюФормуВКомплект(КомплектПечатныхФорм,
		"ТН_ВертикальнаяСПриложением", НСтр("ru = 'ТН-2 (вертикальная, приложение)'"), 0);
	РегистрыСведений.НастройкиПечатиОбъектов.ДобавитьПечатнуюФормуВКомплект(КомплектПечатныхФорм,
		"ТН_ГоризонтальнаяСПриложением", НСтр("ru = 'ТН-2 (горизонтальная, приложение)'"), 0);

	Если ПолучитьФункциональнуюОпцию("ИспользоватьТТН") Тогда
		РегистрыСведений.НастройкиПечатиОбъектов.ДобавитьПечатнуюФормуВКомплект(КомплектПечатныхФорм,
			"ТТН_Вертикальная", НСтр("ru = 'ТТН-1 (вертикальная)'"), 0);
		РегистрыСведений.НастройкиПечатиОбъектов.ДобавитьПечатнуюФормуВКомплект(КомплектПечатныхФорм,
			"ТТН_Горизонтальная", НСтр("ru = 'ТТН-1 (горизонтальная)'"), 0);
		РегистрыСведений.НастройкиПечатиОбъектов.ДобавитьПечатнуюФормуВКомплект(КомплектПечатныхФорм,
			"ТТН_ВертикальнаяСПриложением", НСтр("ru = 'ТТН-1 (вертикальная, приложение)'"), 0);
		РегистрыСведений.НастройкиПечатиОбъектов.ДобавитьПечатнуюФормуВКомплект(КомплектПечатныхФорм,
			"ТТН_ГоризонтальнаяСПриложением", НСтр("ru = 'ТТН-1 (горизонтальная, приложение)'"), 0);
	КонецЕсли;
	
КонецПроцедуры

Процедура СформироватьКомплектПечатныхФорм(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, КомплектПечатныхФорм) Экспорт
	ИмяОбъекта = МассивОбъектов[0].Ссылка.Метаданные().Имя;
	
	КомплектыПечатиПоОбъектам = РегистрыСведений.НастройкиПечатиОбъектов.КомплектыПечатиПоОбъектам(
		КоллекцияПечатныхФорм, КомплектПечатныхФорм, МассивОбъектов, "ТН_Вертикальная");
	Для Каждого КомплектПечати Из КомплектыПечатиПоОбъектам Цикл
		СтруктураТипов = Новый Соответствие;
		СтруктураТипов.Вставить("Документ." + ИмяОбъекта, КомплектПечати.Объекты);
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, КомплектПечати.Имя,
			КомплектПечати.Представление, Обработки.ПечатьТранспортнойНакладной.СформироватьПечатнуюФормуТН2(
			СтруктураТипов, ОбъектыПечати, Новый Структура("ВыводитьУслуги, ВыводитьГТД, Приложение, Горизонтальная",
			Истина, Истина, Ложь, Ложь)));
	КонецЦикла;

	КомплектыПечатиПоОбъектам = РегистрыСведений.НастройкиПечатиОбъектов.КомплектыПечатиПоОбъектам(
		КоллекцияПечатныхФорм, КомплектПечатныхФорм, МассивОбъектов, "ТН_ВертикальнаяСПриложением");
	Для Каждого КомплектПечати Из КомплектыПечатиПоОбъектам Цикл
		СтруктураТипов = Новый Соответствие;
		СтруктураТипов.Вставить("Документ." + ИмяОбъекта, КомплектПечати.Объекты);
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, КомплектПечати.Имя,
			КомплектПечати.Представление, Обработки.ПечатьТранспортнойНакладной.СформироватьПечатнуюФормуТН2(
			СтруктураТипов, ОбъектыПечати, Новый Структура("ВыводитьУслуги, ВыводитьГТД, Приложение, Горизонтальная",
			Истина, Истина, Истина, Ложь)));
	КонецЦикла;

	КомплектыПечатиПоОбъектам = РегистрыСведений.НастройкиПечатиОбъектов.КомплектыПечатиПоОбъектам(
		КоллекцияПечатныхФорм, КомплектПечатныхФорм, МассивОбъектов, "ТН_Горизонтальная");
	Для Каждого КомплектПечати Из КомплектыПечатиПоОбъектам Цикл
		СтруктураТипов = Новый Соответствие;
		СтруктураТипов.Вставить("Документ." + ИмяОбъекта, КомплектПечати.Объекты);
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, КомплектПечати.Имя,
			КомплектПечати.Представление, Обработки.ПечатьТранспортнойНакладной.СформироватьПечатнуюФормуТН2(
			СтруктураТипов, ОбъектыПечати, Новый Структура("ВыводитьУслуги, ВыводитьГТД, Приложение, Горизонтальная",
			Истина, Истина, Ложь, Истина)));
	КонецЦикла;

	КомплектыПечатиПоОбъектам = РегистрыСведений.НастройкиПечатиОбъектов.КомплектыПечатиПоОбъектам(
		КоллекцияПечатныхФорм, КомплектПечатныхФорм, МассивОбъектов, "ТН_ГоризонтальнаяСПриложением");
	Для Каждого КомплектПечати Из КомплектыПечатиПоОбъектам Цикл
		СтруктураТипов = Новый Соответствие;
		СтруктураТипов.Вставить("Документ." + ИмяОбъекта, КомплектПечати.Объекты);
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, КомплектПечати.Имя,
			КомплектПечати.Представление, Обработки.ПечатьТранспортнойНакладной.СформироватьПечатнуюФормуТН2(
			СтруктураТипов, ОбъектыПечати, Новый Структура("ВыводитьУслуги, ВыводитьГТД, Приложение, Горизонтальная",
			Истина, Истина, Истина, Истина)));
	КонецЦикла;

	КомплектыПечатиПоОбъектам = РегистрыСведений.НастройкиПечатиОбъектов.КомплектыПечатиПоОбъектам(
		КоллекцияПечатныхФорм, КомплектПечатныхФорм, МассивОбъектов, "ТТН_Вертикальная");
	Для Каждого КомплектПечати Из КомплектыПечатиПоОбъектам Цикл
		СтруктураТипов = Новый Соответствие;
		СтруктураТипов.Вставить("Документ." + ИмяОбъекта, КомплектПечати.Объекты);
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, КомплектПечати.Имя,
			КомплектПечати.Представление, Обработки.ПечатьТТН.СформироватьПечатнуюФормуТТН(МассивОбъектов,
			ОбъектыПечати, Новый Структура("ВыводитьУслуги, ВыводитьГТД, Приложение, Горизонтальная", Истина, Истина,
			Ложь, Ложь)));
	КонецЦикла;

	КомплектыПечатиПоОбъектам = РегистрыСведений.НастройкиПечатиОбъектов.КомплектыПечатиПоОбъектам(
		КоллекцияПечатныхФорм, КомплектПечатныхФорм, МассивОбъектов, "ТТН_ВертикальнаяСПриложением");
	Для Каждого КомплектПечати Из КомплектыПечатиПоОбъектам Цикл
		СтруктураТипов = Новый Соответствие;
		СтруктураТипов.Вставить("Документ." + ИмяОбъекта, КомплектПечати.Объекты);
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, КомплектПечати.Имя,
			КомплектПечати.Представление, Обработки.ПечатьТТН.СформироватьПечатнуюФормуТТН(МассивОбъектов,
			ОбъектыПечати, Новый Структура("ВыводитьУслуги, ВыводитьГТД, Приложение, Горизонтальная", Истина, Истина,
			Истина, Ложь)));
	КонецЦикла;

	КомплектыПечатиПоОбъектам = РегистрыСведений.НастройкиПечатиОбъектов.КомплектыПечатиПоОбъектам(
		КоллекцияПечатныхФорм, КомплектПечатныхФорм, МассивОбъектов, "ТТН_Горизонтальная");
	Для Каждого КомплектПечати Из КомплектыПечатиПоОбъектам Цикл
		СтруктураТипов = Новый Соответствие;
		СтруктураТипов.Вставить("Документ." + ИмяОбъекта, КомплектПечати.Объекты);
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, КомплектПечати.Имя,
			КомплектПечати.Представление, Обработки.ПечатьТТН.СформироватьПечатнуюФормуТТН(МассивОбъектов,
			ОбъектыПечати, Новый Структура("ВыводитьУслуги, ВыводитьГТД, Приложение, Горизонтальная", Истина, Истина,
			Ложь, Истина)));
	КонецЦикла;

	КомплектыПечатиПоОбъектам = РегистрыСведений.НастройкиПечатиОбъектов.КомплектыПечатиПоОбъектам(
		КоллекцияПечатныхФорм, КомплектПечатныхФорм, МассивОбъектов, "ТТН_ГоризонтальнаяСПриложением");
	Для Каждого КомплектПечати Из КомплектыПечатиПоОбъектам Цикл
		СтруктураТипов = Новый Соответствие;
		СтруктураТипов.Вставить("Документ." + ИмяОбъекта, КомплектПечати.Объекты);
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, КомплектПечати.Имя,
			КомплектПечати.Представление, Обработки.ПечатьТТН.СформироватьПечатнуюФормуТТН(МассивОбъектов,
			ОбъектыПечати, Новый Структура("ВыводитьУслуги, ВыводитьГТД, Приложение, Горизонтальная", Истина, Истина,
			Истина, Истина)));
	КонецЦикла;
	
КонецПроцедуры

Функция МассивИзСтроки(Знач ВходящиеДанные) Экспорт
	ВходящиеДанные = СтрЗаменить(ВходящиеДанные, ",", Символ(13));
	
	МассивТегов = Новый Массив;
	Для Счетчик = 1 По СтрЧислоСтрок(ВходящиеДанные) Цикл
		ТекСтрока = СокрЛП(СтрПолучитьСтроку(ВходящиеДанные, Счетчик));
		МассивТегов.Добавить(ТекСтрока);
	КонецЦикла;
	
	Возврат МассивТегов;
КонецФункции

Процедура ДобавитьКомандыПечатиТН2(КомандыПечати, Порядок = Неопределено) Экспорт
	
	ЭтоПартнер = ПраваПользователяПовтИсп.ЭтоПартнер();
	
	Если Не ЭтоПартнер Тогда // Товарная накладная (ТН-2)
		КомандаПечати = КомандыПечати.Добавить();
		КомандаПечати.Обработчик = "УправлениеПечатьюУПКлиент_Локализация.ПечатьТранспортнойНакладной";
		КомандаПечати.Идентификатор = "ТН_Вертикальная";
		КомандаПечати.Представление = НСтр("ru = 'ТН-2 (вертикальная)'");
		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
		КомандаПечати.ДополнительныеПараметры.Вставить("ВыводитьГТД", Истина);
		КомандаПечати.ДополнительныеПараметры.Вставить("ВыводитьУслуги", Истина);
		Если Порядок <> Неопределено Тогда
			КомандаПечати.Порядок = Порядок + 1;
		КонецЕсли;
		
		КомандаПечати = КомандыПечати.Добавить();
		КомандаПечати.Обработчик = "УправлениеПечатьюУПКлиент_Локализация.ПечатьТранспортнойНакладной";
		КомандаПечати.Идентификатор = "ТН_ВертикальнаяСПриложением";
		КомандаПечати.Представление = НСтр("ru = 'ТН-2 (вертикальная) с приложением'");
		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
		КомандаПечати.ДополнительныеПараметры.Вставить("ВыводитьГТД", Истина);
		КомандаПечати.ДополнительныеПараметры.Вставить("ВыводитьУслуги", Истина);
		Если Порядок <> Неопределено Тогда
			КомандаПечати.Порядок = Порядок + 1;
		КонецЕсли;
		
		КомандаПечати = КомандыПечати.Добавить();
		КомандаПечати.Обработчик = "УправлениеПечатьюУПКлиент_Локализация.ПечатьТранспортнойНакладной";
		КомандаПечати.Идентификатор = "ТН_Горизонтальная";
		КомандаПечати.Представление = НСтр("ru = 'ТН-2 (горизонтальная)'");
		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
		КомандаПечати.ДополнительныеПараметры.Вставить("ВыводитьГТД", Истина);
		КомандаПечати.ДополнительныеПараметры.Вставить("ВыводитьУслуги", Истина);
		Если Порядок <> Неопределено Тогда
			КомандаПечати.Порядок = Порядок + 1;
		КонецЕсли;
		
		КомандаПечати = КомандыПечати.Добавить();
		КомандаПечати.Обработчик = "УправлениеПечатьюУПКлиент_Локализация.ПечатьТранспортнойНакладной";
		КомандаПечати.Идентификатор = "ТН_ГоризонтальнаяСПриложением";
		КомандаПечати.Представление = НСтр("ru = 'ТН-2 (горизонтальная) с приложением'");
		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
		КомандаПечати.ДополнительныеПараметры.Вставить("ВыводитьГТД", Истина);
		КомандаПечати.ДополнительныеПараметры.Вставить("ВыводитьУслуги", Истина);
		Если Порядок <> Неопределено Тогда
			КомандаПечати.Порядок = Порядок + 1;
		КонецЕсли;
	КонецЕсли;
	
	
	//Если ПечататьCMR Тогда
	//	НайденнаяСтрока = КомандыПечати.Найти("ПФ_MXL_CMR", "Идентификатор");
	//	Если НайденнаяСтрока = Неопределено Тогда
	//		КомандаПечати = КомандыПечати.Добавить();
	//		КомандаПечати.Обработчик = "УправлениеПечатьюУПКлиент_Локализация.ПечатьCMR";
	//		КомандаПечати.Идентификатор = "ПФ_MXL_CMR";
	//		КомандаПечати.Представление = НСтр("ru = 'CMR (международная товарно-транспортная накладная)'");
	//		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
	//		Если ТипЗнч(Порядок) = Тип("Массив") Тогда
	//			КомандаПечати.Порядок = Порядок[0];
	//		КонецЕсли;
	//	КонецЕсли;
	//КонецЕсли;
	//
КонецПроцедуры

Функция ОбщаяИнформацияПоСкладу(Склад) Экспорт
	ДанныеСклада = Новый Структура;
	
	ФамилияИмяОтчество = СокрЛП(Склад.ТекущийОтветственный.Наименование);
	СтруктураФИО = ФизическиеЛицаКлиентСервер.ЧастиИмени(ФамилияИмяОтчество);
	НаименованиеСотрудника = Строка(СтруктураФИО.Фамилия) + " " + СтруктураФИО.Имя + " " + СтруктураФИО.Отчество;
	ФамилияИнициалы = ФизическиеЛицаКлиентСервер.ФамилияИнициалы(ФамилияИмяОтчество);
	
	ДанныеСклада.Вставить("НаименованиеСотрудника", НаименованиеСотрудника);
	ДанныеСклада.Вставить("ФамилияИнициалы", ФамилияИнициалы);
	ДанныеСклада.Вставить("ТекущийОтветственный", Склад.ТекущийОтветственный);
	ДанныеСклада.Вставить("ДолжностьОтветственного", Склад.ТекущаяДолжностьОтветственного);
	
	Возврат ДанныеСклада;
КонецФункции

Процедура ЗаполнитьРеквизитыШапки(ДанныеПечати, Макет, ТабличныйДокумент, ТН = Ложь) Экспорт
	ОбластьМакета = Макет.ПолучитьОбласть("Шапка");
	ШтрихкодированиеПечатныхФорм.ВывестиШтрихкодВТабличныйДокумент(ТабличныйДокумент, Макет, ОбластьМакета, ДанныеПечати.Ссылка);
	ОбластьМакета.Параметры.Заполнить(ДанныеПечати);
	ОбластьМакета.Параметры.ДатаДокумента = Формат(ДанныеПечати.Дата, "ДЛФ=DD");
	
	СведенияОГрузополучателе = ФормированиеПечатныхФорм.СведенияОЮрФизЛице(ДанныеПечати.Грузополучатель, ДанныеПечати.Дата);
	СведенияОГрузоотправитель = ФормированиеПечатныхФорм.СведенияОЮрФизЛице(ДанныеПечати.Грузоотправитель, ДанныеПечати.Дата);
	
	Если ТипЗнч(ДанныеПечати.Ссылка) = Тип("ДокументСсылка.ПеремещениеТоваров") Тогда
		АдресСкладаОтправитель = ФормированиеПечатныхФорм.ПолучитьАдресИзКонтактнойИнформации(ДанныеПечати.СкладОтправитель,, ДанныеПечати.Дата);
		ОбластьМакета.Параметры.ПредставлениеОрганизации = ФормированиеПечатныхФорм.ОписаниеОрганизации(СведенияОГрузоотправитель, "ОфициальноеНаименование") + ?(ПустаяСтрока(АдресСкладаОтправитель), "", ", " + АдресСкладаОтправитель);
		
		АдресСкладаПолучатель = ?(ПустаяСтрока(ДанныеПечати.АдресДоставки), ФормированиеПечатныхФорм.ПолучитьАдресИзКонтактнойИнформации(ДанныеПечати.СкладПолучатель,, ДанныеПечати.Дата), ДанныеПечати.АдресДоставки);
		ОбластьМакета.Параметры.ПредставлениеГрузополучателя = ФормированиеПечатныхФорм.ОписаниеОрганизации(СведенияОГрузополучателе, "ОфициальноеНаименование") + ?(ПустаяСтрока(АдресСкладаПолучатель), "", ", " + АдресСкладаПолучатель);
	Иначе
		ОбластьМакета.Параметры.ПредставлениеОрганизации = ФормированиеПечатныхФорм.ОписаниеОрганизации(СведенияОГрузоотправитель, "ОфициальноеНаименование, ЮридическийАдрес");
		ОбластьМакета.Параметры.ПредставлениеГрузополучателя = ФормированиеПечатныхФорм.ОписаниеОрганизации(СведенияОГрузополучателе, "ОфициальноеНаименование, ЮридическийАдрес");
	КонецЕсли;
	
	ОбластьМакета.Параметры.ГрузоотправительУНП = СведенияОГрузоотправитель.ИНН;
	ОбластьМакета.Параметры.ГрузополучательУНП = СведенияОГрузополучателе.ИНН;
	
	Если Не ТН Тогда
		Если ЗначениеЗаполнено(ДанныеПечати.ЗаказчикПеревозки) Тогда
			СведенияОЗаказчике = ФормированиеПечатныхФорм.СведенияОЮрФизЛице(ДанныеПечати.ЗаказчикПеревозки, ДанныеПечати.Дата);
			ОбластьМакета.Параметры.ЗаказчикУНП = СведенияОЗаказчике.ИНН;
			ОбластьМакета.Параметры.ПредставлениеПлательщика = ФормированиеПечатныхФорм.ОписаниеОрганизации(СведенияОЗаказчике, "ПолноеНаименование, ЮридическийАдрес");
		КонецЕсли;
		
		 // 4D:ERP для Беларуси, ЕкатеринаМ, 02.04.2020 16:16:40
		 // Печать информации о транспорте в ТТН,№25110
		 // {
		 
		Если ЗначениеЗаполнено(ДанныеПечати.МаркаАвтомобиля) 
		И ЗначениеЗаполнено(ДанныеПечати.ГосНомерАвтомобиля)Тогда
			ОбластьМакета.Параметры.МаркаАвтомобиля = ДанныеПечати.МаркаАвтомобиля + ",гос. номер " + ДанныеПечати.ГосНомерАвтомобиля;
		ИначеЕсли ЗначениеЗаполнено(ДанныеПечати.ГосНомерАвтомобиля)Тогда
		    ОбластьМакета.Параметры.МаркаАвтомобиля = "гос. номер " + ДанныеПечати.ГосНомерАвтомобиля;
		КонецЕсли;
		// }
		// 4D
		
		Если ЗначениеЗаполнено(ДанныеПечати.Прицеп) Тогда
			ОбластьМакета.Параметры.Прицеп = ДанныеПечати.Прицеп + ", гос. номер " + ДанныеПечати.ГосНомерПрицепа;
		КонецЕсли;
	КонецЕсли;
	
	ТабличныйДокумент.Вывести(ОбластьМакета);
КонецПроцедуры

Функция ЗаполнитьРеквизитыПодвала(ДанныеПечати, ИтоговыеСуммы, Макет, КоэффициентПересчетаВТонны, ТН = Ложь, Приложение = Ложь) Экспорт
	
	ОбластьМакета = Макет.ПолучитьОбласть("Подвал");
	ОбластьМакета.Параметры.Заполнить(ДанныеПечати);
	
	ПолнаяДатаДокумента = Формат(ДанныеПечати.Дата, "ДФ='дд ММММ гггг ""года""'");
	ДлинаСтроки = СтрДлина(ПолнаяДатаДокумента);
	ПервыйРазделитель = Найти(ПолнаяДатаДокумента, " ");
	ВторойРазделитель = Найти(Прав(ПолнаяДатаДокумента, ДлинаСтроки - ПервыйРазделитель), " ") + ПервыйРазделитель;
	
	СтруктураПараметров = Новый Структура;
	СтруктураПараметров.Вставить("ДатаДокументаДень", """" + Лев(ПолнаяДатаДокумента, ПервыйРазделитель - 1 ) + """");
	СтруктураПараметров.Вставить("ДатаДокументаМесяц", Сред(ПолнаяДатаДокумента, ПервыйРазделитель + 1, ВторойРазделитель - ПервыйРазделитель - 1));
	СтруктураПараметров.Вставить("ДатаДокументаГод", Прав(ПолнаяДатаДокумента, ДлинаСтроки - ВторойРазделитель));
	
	Если ТипЗнч(ДанныеПечати.Ссылка) = Тип("ДокументСсылка.ПеремещениеТоваров") Тогда
		ИнфоПоСкладу = ОбщаяИнформацияПоСкладу(ДанныеПечати.СкладОтправитель);
		СтруктураПараметров.Вставить("Разрешил", НРег(ИнфоПоСкладу.ДолжностьОтветственного) + ?(ПустаяСтрока(ИнфоПоСкладу.ФамилияИнициалы), "", ", " + ИнфоПоСкладу.ФамилияИнициалы));
		СтруктураПараметров.Вставить("Отпустил", СтруктураПараметров.Разрешил);
		
		ИнфоПоСкладу = ОбщаяИнформацияПоСкладу(ДанныеПечати.СкладПолучатель);
		СтруктураПараметров.Вставить("Принял", НРег(ИнфоПоСкладу.ДолжностьОтветственного) + ?(ПустаяСтрока(ИнфоПоСкладу.ФамилияИнициалы), "", ", " + ИнфоПоСкладу.ФамилияИнициалы));
	Иначе
		СтруктураПараметров.Вставить("Разрешил", ?(ЗначениеЗаполнено(ДанныеПечати.РазрешилДолжность), ДанныеПечати.РазрешилДолжность + ", ", "") + ДанныеПечати.Разрешил);
		СтруктураПараметров.Вставить("Отпустил", ?(ЗначениеЗаполнено(ДанныеПечати.ОтпустилДолжность), ДанныеПечати.ОтпустилДолжность + ", ", "") + ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(ДанныеПечати.Отпустил));
	КонецЕсли;
	
	СтруктураПараметров.Вставить("ДоверенностьДата", Формат(ДанныеПечати.ДоверенностьДата, "ДЛФ=DD"));
	СтруктураПараметров.Вставить("СуммаНДСПрописью", ИтоговыеСуммы.СуммаНДСПрописью);
	СтруктураПараметров.Вставить("СуммаСНДСПрописью", ИтоговыеСуммы.СуммаСНДСПрописью);
	СтруктураПараметров.Вставить("ПереданыДокументы", ?(ЗначениеЗаполнено(ДанныеПечати.ПереданыДокументы), ДанныеПечати.ПереданыДокументы, СформироватьОписаниеДокументов(ДанныеПечати, ТН)));
	
	Если Не ТН Тогда
		Если ИтоговыеСуммы.ИтогоКоличествоМест > 0 Тогда
			СтруктураПараметров.Вставить("КоличествоМестПрописью", ЧислоПрописью(ИтоговыеСуммы.ИтогоКоличествоМест,, ",,,,,,,,0"));
		КонецЕсли;
		
		Если ИтоговыеСуммы.ИтогоМассаНетто > 0 Тогда
			ПараметрыМассыПрописью = ПараметрыМассыПрописью(ИтоговыеСуммы.ИтогоМассаНетто, КоэффициентПересчетаВТонны);
			СтруктураПараметров.Вставить("МассаНеттоПрописью", ЧислоПрописью(ИтоговыеСуммы.ИтогоМассаНетто * ПараметрыМассыПрописью.Коэффициент, "ДП=Истина", ПараметрыМассыПрописью.Формат));
		КонецЕсли;
		
		Если ИтоговыеСуммы.ИтогоМассаБрутто > 0 Тогда
			ПараметрыМассыПрописью = ПараметрыМассыПрописью(ИтоговыеСуммы.ИтогоМассаБрутто, КоэффициентПересчетаВТонны);
			СтруктураПараметров.Вставить("МассаБруттоПрописью", ЧислоПрописью(ИтоговыеСуммы.ИтогоМассаБрутто * ПараметрыМассыПрописью.Коэффициент, "ДП=Истина", ПараметрыМассыПрописью.Формат));
		КонецЕсли;
	КонецЕсли;
	
	ОбластьМакета.Параметры.Заполнить(СтруктураПараметров);
	
	Возврат ОбластьМакета;
КонецФункции

Процедура ЗаполнитьРеквизитыСтрокиТовара(ДанныеПечати, СтрокаТовары, ОбластьМакета, НомерСтроки, ВыводитьКодНоменклатуры = Истина, ВыводитьГТД = Ложь, ТН = Ложь) Экспорт
	ИспользоватьНаборы = ОбщегоНазначенияУТКлиентСервер.ЕстьРеквизитОбъекта(СтрокаТовары, "ЭтоНабор");
	ПрефиксИПостфикс = НаборыСервер.ПолучитьПрефиксИПостфикс(СтрокаТовары, ИспользоватьНаборы);
	
	Если ИспользоватьНаборы И СтрокаТовары.ЭтоКомплектующие И СтрокаТовары.ВариантПредставленияНабораВПечатныхФормах = Перечисления.ВариантыПредставленияНаборовВПечатныхФормах.НаборИКомплектующие
		И (СтрокаТовары.ВариантРасчетаЦеныНабора = Перечисления.ВариантыРасчетаЦенНаборов.ЦенаЗадаетсяЗаНаборРаспределяетсяПоДолям
		Или СтрокаТовары.ВариантРасчетаЦеныНабора = Перечисления.ВариантыРасчетаЦенНаборов.ЦенаЗадаетсяЗаНаборРаспределяетсяПоЦенам) Тогда
		
		ОбластьМакета.Параметры.Заполнить(НаборыСервер.ПустыеДанные());
	ИначеЕсли ИспользоватьНаборы И СтрокаТовары.ЭтоНабор
		И СтрокаТовары.ВариантПредставленияНабораВПечатныхФормах = Перечисления.ВариантыПредставленияНаборовВПечатныхФормах.НаборИКомплектующие
		И СтрокаТовары.ВариантРасчетаЦеныНабора = Перечисления.ВариантыРасчетаЦенНаборов.РассчитываетсяИзЦенКомплектующих Тогда
		
		ОбластьМакета.Параметры.Заполнить(НаборыСервер.ПустыеДанные());
	Иначе
		ОбластьМакета.Параметры.Заполнить(СтрокаТовары);
	КонецЕсли;
	
	СтруктураПараметров = Новый Структура("КоличествоМест, КоличествоВОдномМесте, НоменклатураКод", 0, 0, "");
	ЗаполнитьЗначенияСвойств(СтруктураПараметров, СтрокаТовары);
	ОкруглитьДоЦелого(СтруктураПараметров.КоличествоМест);
	ОкруглитьДоЦелого(СтруктураПараметров.КоличествоВОдномМесте);
	СтруктураПараметров.Вставить("НомерСтроки", НомерСтроки);
	
	ДополнительныеПараметры = НоменклатураКлиентСервер.ДополнительныеПараметрыПредставлениеНоменклатурыДляПечати();
	ЗаполнитьЗначенияСвойств(ДополнительныеПараметры, СтрокаТовары);
	
	Если ЗначениеЗаполнено(СтрокаТовары.Номенклатура) Тогда
		РеквизитыНоменклатуры = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(СтрокаТовары.Номенклатура, "СтранаПроисхождения, Артикул, Код");
		Если ЗначениеЗаполнено(РеквизитыНоменклатуры.СтранаПроисхождения) Тогда
			СтранаПроисхождения = " (" + ОбщегоНазначения.ЗначениеРеквизитаОбъекта(РеквизитыНоменклатуры.СтранаПроисхождения, "НаименованиеПолное") + ")";
		Иначе
			СтранаПроисхождения = "";
		КонецЕсли;
		
		ИмяДополнительнойКолонки = ФормированиеПечатныхФорм.ИмяДополнительнойКолонки();
		ДополнительнаяКолонка = "";
		
		Если ИмяДополнительнойКолонки = "Артикул" Тогда
			Если ЗначениеЗаполнено(РеквизитыНоменклатуры.Артикул) Тогда
				ДополнительнаяКолонка = Символы.ПС + "арт. " + СокрЛП(РеквизитыНоменклатуры.Артикул);
			КонецЕсли;
		ИначеЕсли ИмяДополнительнойКолонки = "Код" Тогда
			Если ЗначениеЗаполнено(РеквизитыНоменклатуры.Код) Тогда
				ДополнительнаяКолонка = Символы.ПС + "код " + СокрЛП(РеквизитыНоменклатуры.Код);
			КонецЕсли;
		КонецЕсли;
		
		НоменклатураНаименование = "";
		Если СтрокаТовары.Владелец().Колонки.Найти("ТоварНаименование") <> Неопределено Тогда
			НоменклатураНаименование = СтрокаТовары.ТоварНаименование;
		ИначеЕсли СтрокаТовары.Владелец().Колонки.Найти("НоменклатураНаименование") <> Неопределено Тогда
			НоменклатураНаименование = СтрокаТовары.НоменклатураНаименование;
		Иначе	
			НоменклатураНаименование = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(СтрокаТовары.Номенклатура, "НаименованиеПолное");
		КонецЕсли;	
		
		СтруктураПараметров.Вставить("ПредставлениеНоменклатуры",
			ПрефиксИПостфикс.Префикс + НоменклатураКлиентСервер.ПредставлениеНоменклатурыДляПечати(НоменклатураНаименование + 
											СтранаПроисхождения +
											?(ПустаяСтрока(ДополнительнаяКолонка), "", "," + ДополнительнаяКолонка), 
											Строка(СтрокаТовары.Характеристика),,, ДополнительныеПараметры) + ПрефиксИПостфикс.Постфикс);
	Иначе
		СтруктураПараметров.Вставить("ПредставлениеНоменклатуры",
			ПрефиксИПостфикс.Префикс + НоменклатураКлиентСервер.ПредставлениеНоменклатурыДляПечати(СтрокаТовары.НоменклатураНаименование,
			Строка(СтрокаТовары.Характеристика),,, ДополнительныеПараметры) + ПрефиксИПостфикс.Постфикс);
	КонецЕсли;
	
	Если Не ВыводитьКодНоменклатуры Тогда
		СтруктураПараметров.НоменклатураКод = "";
	КонецЕсли;
	
	Если ВыводитьГТД И ЗначениеЗаполнено(СтрокаТовары.НомерГТД) И Не ЗначениеЗаполнено(РеквизитыНоменклатуры.СтранаПроисхождения) Тогда
		РеквизитыГТД = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(СтрокаТовары.НомерГТД, "СтранаПроисхождения");
		СтранаПроисхождения = " /" + СокрЛП(РеквизитыГТД.СтранаПроисхождения) + "/";
		СтруктураПараметров.ПредставлениеНоменклатуры = СтруктураПараметров.ПредставлениеНоменклатуры + СтранаПроисхождения;
	КонецЕсли;
	
	СтруктураПараметров.Вставить("Цена", Окр(СтрокаТовары.Цена, 2));
	СтруктураПараметров.Вставить("СуммаНДС", Окр(СтрокаТовары.СуммаНДС, 2));
	СтруктураПараметров.Вставить("СуммаСНДС", Окр(СтрокаТовары.СуммаСНДС, 2));
	
	Если ТН Тогда
		СтруктураПараметров.Вставить("СуммаБезНДС", Окр(СтрокаТовары.СуммаБезНДС, 2));
	Иначе
		ПоляЗапроса = Новый Структура;
		ПоляЗапроса.Вставить("Сумма", 0);
		
		ЗаполнитьЗначенияСвойств(ПоляЗапроса, СтрокаТовары);
		СтруктураПараметров.Вставить("Сумма", Окр(ПоляЗапроса.Сумма, 2));
	КонецЕсли;
		
	ОбластьМакета.Параметры.Заполнить(СтруктураПараметров);
КонецПроцедуры

Функция ЗаполнитьРеквизитыТранспорта(ДанныеПечати, Макет) Экспорт
	ОбластьМакета = Макет.ПолучитьОбласть("Транспорт");
	ОбластьМакета.Параметры.Заполнить(ДанныеПечати);
	
	СтруктураПараметров = Новый Структура;
	СтруктураПараметров.Вставить("ПогрузкаДатаПрибытия", Формат(ДанныеПечати.ПогрузкаДатаПрибытия, "ДФ='dd.MM.yyyy HH:mm'"));
	СтруктураПараметров.Вставить("ПогрузкаДатаУбытия", Формат(ДанныеПечати.ПогрузкаДатаУбытия, "ДФ='dd.MM.yyyy HH:mm'"));
	СтруктураПараметров.Вставить("ПогрузкаПростой", Формат(ДанныеПечати.ПогрузкаПростой, "ДФ=HH:mm"));
	СтруктураПараметров.Вставить("ПогрузкаДопОперацииВремя", Формат(ДанныеПечати.ПогрузкаДопОперацииВремя, "ДФ=HH:mm"));
	СтруктураПараметров.Вставить("РазгрузкаДатаПрибытия", Формат(ДанныеПечати.РазгрузкаДатаПрибытия, "ДФ='dd.MM.yyyy HH:mm'"));
	СтруктураПараметров.Вставить("РазгрузкаДатаУбытия", Формат(ДанныеПечати.РазгрузкаДатаУбытия, "ДФ='dd.MM.yyyy HH:mm'"));
	СтруктураПараметров.Вставить("РазгрузкаПростой", Формат(ДанныеПечати.РазгрузкаПростой, "ДФ=HH:mm"));
	СтруктураПараметров.Вставить("РазгрузкаДопОперацииВремя", Формат(ДанныеПечати.РазгрузкаДопОперацииВремя, "ДФ=HH:mm"));
	
	ОбластьМакета.Параметры.Заполнить(СтруктураПараметров);
	
	Возврат ОбластьМакета;
КонецФункции

Процедура ДобавитьИтоговыеДанныеПодвала(ИтоговыеСуммы, ДанныеПечати) Экспорт
	
	ИтоговыеСуммы.Вставить("СуммаНДСПрописью", РаботаСКурсамиВалют.СформироватьСуммуПрописью(ИтоговыеСуммы.ИтогоСуммаНДС, ДанныеПечати.Валюта));
	ИтоговыеСуммы.Вставить("СуммаСНДСПрописью", РаботаСКурсамиВалют.СформироватьСуммуПрописью(ИтоговыеСуммы.ИтогоСуммаСНДС, ДанныеПечати.Валюта));
	
	ПорядокОплаты = Новый Структура("ПсевдовалютныйДоговор, СуммаВзаиморасчетов, ВалютаВзаиморасчетов",	Ложь, 0, Неопределено); 
	ЗаполнитьЗначенияСвойств(ПорядокОплаты, ДанныеПечати);
	
	Если ПорядокОплаты.ПсевдовалютныйДоговор Тогда
		ИтогоСуммаСНДС_Вал = ПорядокОплаты.СуммаВзаиморасчетов;
		ИтоговыеСуммы.СуммаСНДСПрописью = ИтоговыеСуммы.СуммаСНДСПрописью + " (" + РаботаСКурсамиВалют.СформироватьСуммуПрописью(ИтогоСуммаСНДС_Вал, ПорядокОплаты.ВалютаВзаиморасчетов) + ")";
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбнулитьИтогиПоСтранице(ИтоговыеСуммы) Экспорт
	СтруктураРесурсовДляИтогов = СтруктураРесурсовДляИтогов();
	
	Для Каждого Элемент Из СтруктураРесурсовДляИтогов Цикл
		ИтоговыеСуммы.Вставить("Итого" + Элемент.Ключ + "НаСтранице", 0);
	КонецЦикла;
КонецПроцедуры

Процедура РассчитатьИтоговыеСуммы(ИтоговыеСуммы, СтрокаТовары) Экспорт
	СтруктураСуммПоСтроке = СтруктураРесурсовДляИтогов();
	ЗаполнитьЗначенияСвойств(СтруктураСуммПоСтроке, СтрокаТовары);
	ОкруглитьДоЦелого(СтруктураСуммПоСтроке.КоличествоМест);
	
	Для Каждого Элемент Из СтруктураСуммПоСтроке Цикл
		Если ЗначениеЗаполнено(Элемент.Значение) Тогда
			Если Найти(Элемент.Ключ, "Сумма") > 0 Тогда
				ИтоговыеСуммы["Итого" + Элемент.Ключ + "НаСтранице"] = ИтоговыеСуммы["Итого" + Элемент.Ключ + "НаСтранице"] + Окр(Элемент.Значение, 2);
				ИтоговыеСуммы["Итого" + Элемент.Ключ] = ИтоговыеСуммы["Итого" + Элемент.Ключ] + Окр(Элемент.Значение, 2);
			ИначеЕсли Найти(Элемент.Ключ, "Масса") > 0 Тогда
				ИтоговыеСуммы["Итого" + Элемент.Ключ + "НаСтранице"] = ИтоговыеСуммы["Итого" + Элемент.Ключ + "НаСтранице"] + Элемент.Значение;
				ИтоговыеСуммы["Итого" + Элемент.Ключ] = ИтоговыеСуммы["Итого" + Элемент.Ключ] + Элемент.Значение;
			Иначе
				ИтоговыеСуммы["Итого" + Элемент.Ключ + "НаСтранице"] = ИтоговыеСуммы["Итого" + Элемент.Ключ + "НаСтранице"] + Элемент.Значение;
				ИтоговыеСуммы["Итого" + Элемент.Ключ] = ИтоговыеСуммы["Итого" + Элемент.Ключ] + Элемент.Значение;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

Функция СтруктураРесурсовДляИтогов()
	Структура = Новый Структура;
	Структура.Вставить("Сумма", 0);
	Структура.Вставить("СуммаБезНДС", 0);
	Структура.Вставить("СуммаНДС", 0);
	Структура.Вставить("СуммаСНДС", 0);
	Структура.Вставить("Количество", 0);
	Структура.Вставить("КоличествоМест", 0);
	Структура.Вставить("КоличествоПринято", 0);
	Структура.Вставить("МассаБрутто", 0);
	Структура.Вставить("МассаНетто", 0);
	Структура.Вставить("Сумма", 0);
	
	Возврат Структура;
КонецФункции

Функция СтруктураИтоговыеСуммы() Экспорт
	Структура = Новый Структура;
	СтруктураРесурсовДляИтогов = СтруктураРесурсовДляИтогов();
	
	Для Каждого Элемент Из СтруктураРесурсовДляИтогов Цикл
		Структура.Вставить("Итого" + Элемент.Ключ + "НаСтранице", 0);
		Структура.Вставить("Итого" + Элемент.Ключ, 0);
	КонецЦикла;
	
	Возврат Структура;
КонецФункции

Процедура ОкруглитьДоЦелого(ОкругляемоеЧисло)
	Если ЗначениеЗаполнено(ОкругляемоеЧисло) Тогда
		Если ОкругляемоеЧисло <> Цел(ОкругляемоеЧисло) Тогда
			ОкругляемоеЧисло = Цел(ОкругляемоеЧисло) + 1;
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

Функция ПараметрыМассыПрописью(Масса, КоэффициентПересчетаВТонны)
	МассаТонны = Масса * КоэффициентПересчетаВТонны;
	
	Если МассаТонны > 1 Тогда
		Коэффициент = 1;
		СтрокаФормат = "т, т, т, ж, кг, кг, кг, м, " + ?(Окр(МассаТонны) = МассаТонны, "0", "3");
	ИначеЕсли МассаТонны * 1000 > 1 Тогда
		Коэффициент = 1000;
		СтрокаФормат = "кг, кг, кг, м, г, г, г, м, " + ?(Окр(МассаТонны * Коэффициент) = МассаТонны * Коэффициент, "0", "3");
	Иначе
		Коэффициент = 1000000;
		СтрокаФормат = "г, г, г, м, г, г, г, м, 0";
	КонецЕсли;
	
	Возврат Новый Структура("Формат, Коэффициент", СтрокаФормат, Коэффициент * КоэффициентПересчетаВТонны);
КонецФункции

Функция СформироватьОписаниеДокументов(ДанныеПечати, ТН = Ложь, Приложение = Ложь) Экспорт
	
	НазваниеДокумента = ?(ТН, "ТН-2", "ТТН-1");
	ПереданыДокументы = НазваниеДокумента + " серия " + ДанныеПечати.СерияБланкаСтрогойОтчетности + " № " + ДанныеПечати.НомерБланкаСтрогойОтчетности;
	ПереданыДокументы = ПереданыДокументы + ?(Приложение, ", приложение к " + НазваниеДокумента + " " + ДанныеПечати.СерияБланкаСтрогойОтчетности + " № " + ДанныеПечати.НомерБланкаСтрогойОтчетности + " от " + Формат(ДанныеПечати.Дата, "ДЛФ=DD"), "");
	ПереданыДокументы = ПереданыДокументы + ?(Значениезаполнено(ДанныеПечати.ПереданыДокументы), ", " + ДанныеПечати.ПереданыДокументы, "");
	
	Возврат ПереданыДокументы;
	
КонецФункции

Процедура ПриСозданииНаСервереРеквизитыПечатиРеализации(Форма, Родитель) Экспорт
	
	НоваяКоманда = Форма.Команды.Добавить("ОткрытьРеквизитыПечатиРеализации");
	НоваяКоманда.Действие = "ОткрытьРеквизитыПечатиРеализации";
	НоваяКоманда.Заголовок = НСтр("ru = 'Реквизиты печати регламентированных форм ТТН-1, ТН-2'");
	НовыйЭлемент = Форма.Элементы.Добавить("ОткрытьРеквизитыПечатиРеализации", Тип("КнопкаФормы"), Родитель);
	НовыйЭлемент.ИмяКоманды = "ОткрытьРеквизитыПечатиРеализации";
	НовыйЭлемент.Вид = ВидКнопкиФормы.Гиперссылка;
	
КонецПроцедуры

Функция ОсновныеРеквизитыПечатиРеализации() Экспорт
	
	СтруктураПараметров = Новый Структура;
	СтруктураПараметров.Вставить("АдресДоставки", "");
	СтруктураПараметров.Вставить("БанковскийСчетГрузоотправителя", "");
	СтруктураПараметров.Вставить("БанковскийСчетГрузополучателя", "");
	СтруктураПараметров.Вставить("БанковскийСчетКонтрагента", "");
	СтруктураПараметров.Вставить("БанковскийСчетОрганизации", "");
	СтруктураПараметров.Вставить("БанковскийСчетОрганизацииПолучателя", "");
	СтруктураПараметров.Вставить("Грузоотправитель", "");
	СтруктураПараметров.Вставить("Грузополучатель", "");
	СтруктураПараметров.Вставить("ДоверенностьВыдана", "");
	СтруктураПараметров.Вставить("ДоверенностьДата", "");
	СтруктураПараметров.Вставить("ДоверенностьЛицо", "");
	СтруктураПараметров.Вставить("ДоверенностьНомер", "");
	СтруктураПараметров.Вставить("Договор", "");
	СтруктураПараметров.Вставить("Основание", "");
	СтруктураПараметров.Вставить("ОснованиеДата", "");
	СтруктураПараметров.Вставить("ОснованиеНомер", "");
	СтруктураПараметров.Вставить("Отпустил", "");
	СтруктураПараметров.Вставить("ОтпустилДолжность", "");
	СтруктураПараметров.Вставить("Партнер", "");
	СтруктураПараметров.Вставить("ХозяйственнаяОперация", "");
	СтруктураПараметров.Вставить("Контрагент", "");
	СтруктураПараметров.Вставить("ТолькоПросмотр", Ложь);
	СтруктураПараметров.Вставить("Организация", "");
	СтруктураПараметров.Вставить("Дата", "");
	СтруктураПараметров.Вставить("Руководитель", "");
	СтруктураПараметров.Вставить("ГлавныйБухгалтер", "");
	СтруктураПараметров.Вставить("ПереданыДокументы", "");
	СтруктураПараметров.Вставить("Принял", "");
	СтруктураПараметров.Вставить("НомерБланкаСтрогойОтчетности", "");
	СтруктураПараметров.Вставить("СерияБланкаСтрогойОтчетности", "");
	СтруктураПараметров.Вставить("ТипБланкаСтрогойОтчетности", "");
	СтруктураПараметров.Вставить("Склад", "");
	СтруктураПараметров.Вставить("Менеджер", "");
	
	Возврат СтруктураПараметров;
	
КонецФункции

#КонецОбласти
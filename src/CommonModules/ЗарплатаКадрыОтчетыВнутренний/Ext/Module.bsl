﻿////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

Процедура НастроитьВариантыОтчетов(Настройки) Экспорт
	
	ЗарплатаКадрыОтчетыРасширенный.НастроитьВариантыОтчетов(Настройки);
	
КонецПроцедуры

Процедура ЗаполнитьПользовательскиеПоляВариантаОтчета(КлючВарианта, НастройкиОтчета) Экспорт
	
	ЗарплатаКадрыОтчетыРасширенный.ЗаполнитьПользовательскиеПоляВариантаОтчета(КлючВарианта, НастройкиОтчета);
	
КонецПроцедуры

Функция УстановитьОтборВариантаОтчета(КлючВарианта, НастройкиОтчета) Экспорт
	
	Возврат ЗарплатаКадрыОтчетыРасширенный.УстановитьОтборВариантаОтчета(КлючВарианта, НастройкиОтчета);

КонецФункции

Процедура НастроитьВариантОтчетаРасчетныйЛисток(НастройкиОтчета) Экспорт
	
	ЗарплатаКадрыОтчетыРасширенный.НастроитьВариантОтчетаРасчетныйЛисток(НастройкиОтчета);
	
КонецПроцедуры     

Функция НаборыВнешнихДанныхАнализНачисленийИУдержаний() Экспорт
	
	Возврат ЗарплатаКадрыОтчетыРасширенный.НаборыВнешнихДанныхАнализНачисленийИУдержаний();
	
КонецФункции


Функция ЭтоКлючВариантаОтчетаРасчетныйЛисток(КлючВарианта) Экспорт
	
	Возврат ЗарплатаКадрыОтчетыРасширенный.ЭтоКлючВариантаОтчетаРасчетныйЛисток(КлючВарианта);
	
КонецФункции

